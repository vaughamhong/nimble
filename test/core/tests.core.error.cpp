//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//////////////////////////////////////////////////////////////////////////

#include "gtest/gtest.h"
#include "gmock/gmock.h"

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/error.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;

//////////////////////////////////////////////////////////////////////////

TEST(CoreError, Initialization){
	core::Error error(0, "category", "message");

    EXPECT_EQ(error.getCode(), 0);
	EXPECT_STREQ(error.getCategory(), "category");
	EXPECT_STREQ(error.getString(), "message");
}
TEST(CoreError, CopyConstructor){
	core::Error error1(0, "category", "message");
	core::Error error2(error1);

    EXPECT_EQ((error1 == error2), true);
}
TEST(CoreError, Compare){
	core::Error error1(0, "category", "message");
	core::Error error2;

	EXPECT_EQ((error1.getCode() == 0), true);
	EXPECT_EQ((error1 != error2), true);
	EXPECT_EQ((error1 == error1), true);
}

//////////////////////////////////////////////////////////////////////////