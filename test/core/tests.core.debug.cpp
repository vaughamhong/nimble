//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//////////////////////////////////////////////////////////////////////////

#include "gtest/gtest.h"
#include "gmock/gmock.h"

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;

//////////////////////////////////////////////////////////////////////////

TEST(CoreDebug, AssertError){
    // core::assert_error(false);
    // core::assert_error(false, "assert with error");
    // core::assert_error(true);
    // core::assert_error(true, "assert with error");
    // core::assert_error(true, "assert with %s", "error");
}
TEST(CoreDebug, AssertWarning){
    // core::assert_warn(true, "assert with warning");
    // core::assert_warn(false, "assert with warning");
}
TEST(CoreDebug, Output){
    // core::output("category", "output using core::output with category %s", "category");
    // core::print("output using core::print");
    // core::warning("output using core::warning");
    // core::error("output using core::error");
}
TEST(CoreDebug, Category){
    // ASSERT_EQ(core::isLogCategoryEnable(NULL), false);
    // core::enableLogCategory("category");
    // ASSERT_EQ(core::isLogCategoryEnable("category"), true);
    // core::disableLogCategory("category");
    // ASSERT_EQ(core::isLogCategoryEnable("category"), false);
    // core::enableLogCategory(NULL);
    // ASSERT_EQ(core::isLogCategoryEnable("category"), true);
    // core::disableLogCategory(NULL);
    // ASSERT_EQ(core::isLogCategoryEnable("category"), false);
}

//////////////////////////////////////////////////////////////////////////