//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//////////////////////////////////////////////////////////////////////////

#include "gtest/gtest.h"
#include "gmock/gmock.h"

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/buffer.ring.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;

//////////////////////////////////////////////////////////////////////////

TEST(CoreRingBuffer, Construct){
    core::RingBuffer buffer;
    EXPECT_EQ(buffer.getSize(), 0);
    EXPECT_EQ(buffer.getPointer(), (void*)NULL);
    EXPECT_EQ(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreRingBuffer, ConstructWithZeroSize){
    core::RingBuffer buffer;
    EXPECT_EQ(buffer.getSize(), 0);
    EXPECT_EQ(buffer.getPointer(), (void*)NULL);
    EXPECT_EQ(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreRingBuffer, ConstructWithPositiveSize){
	core::RingBuffer buffer(100);
    EXPECT_EQ(buffer.getSize(), 100);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreRingBuffer, CopyConstruct){
	core::RingBuffer buffer1(100);
	core::RingBuffer buffer2 = buffer1;
    EXPECT_EQ(buffer1.getSize(), buffer2.getSize());
	EXPECT_NE(buffer1.getPointer(), (void*)NULL);
    EXPECT_NE(buffer1.getConstPointer(), (void*)NULL);
    EXPECT_NE(buffer2.getPointer(), (void*)NULL);
    EXPECT_NE(buffer2.getConstPointer(), (void*)NULL);
}

TEST(CoreRingBuffer, ShrinkBuffer){
	core::RingBuffer buffer(200);
	EXPECT_EQ(buffer.getSize(), 200);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
	buffer.resize(100);
	EXPECT_EQ(buffer.getSize(), 100);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
    buffer.resize(0);
    EXPECT_EQ(buffer.getSize(), 0);
    EXPECT_EQ(buffer.getPointer(), (void*)NULL);
    EXPECT_EQ(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreRingBuffer, ExpandBuffer){
	core::RingBuffer buffer(0);
	EXPECT_EQ(buffer.getSize(), 0);
    EXPECT_EQ(buffer.getPointer(), (void*)NULL);
    EXPECT_EQ(buffer.getConstPointer(), (void*)NULL);
	buffer.resize(100);
    EXPECT_EQ(buffer.getSize(), 100);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
    buffer.resize(200);
    EXPECT_EQ(buffer.getSize(), 200);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreRingBuffer, CopyBuffer){
	core::RingBuffer buffer1(100);
	core::RingBuffer buffer2;
    EXPECT_EQ(buffer1.getSize(), 100);
    EXPECT_NE(buffer1.getPointer(), (void*)NULL);
    EXPECT_NE(buffer1.getConstPointer(), (void*)NULL);
    buffer2 = buffer1;
    EXPECT_EQ(buffer2.getSize(), 100);
    EXPECT_NE(buffer2.getPointer(), (void*)NULL);
    EXPECT_NE(buffer2.getConstPointer(), (void*)NULL);
}

TEST(CoreRingBuffer, Push){
    core::RingBuffer buffer(100);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 100);
    EXPECT_EQ(buffer.isEmpty(), true);
    const char *inBuffer = "hello world";
    buffer.push(inBuffer, strlen(inBuffer));
    EXPECT_EQ(buffer.allocatedSize(), strlen(inBuffer));
    EXPECT_EQ(buffer.freeSize(), 100 - strlen(inBuffer));
    EXPECT_EQ(buffer.isEmpty(), false);
    buffer.clear();
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 100);
    EXPECT_EQ(buffer.isEmpty(), true);
}

TEST(CoreRingBuffer, PushUntilFull){
    core::RingBuffer buffer(100);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 100);
    EXPECT_EQ(buffer.isEmpty(), true);
    for(int i = 0; i < 100; i++){
        buffer.push("a", 1);
    }
    EXPECT_EQ(buffer.allocatedSize(), 100);
    EXPECT_EQ(buffer.freeSize(), 0);
    EXPECT_EQ(buffer.isEmpty(), false);
}

TEST(CoreRingBuffer, PushOverflow){
    core::RingBuffer buffer(100);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 100);
    EXPECT_EQ(buffer.isEmpty(), true);
    for(int i = 0; i < 101; i++){
        buffer.push("a", 1);
    }
    EXPECT_EQ(buffer.allocatedSize(), 101);
    EXPECT_EQ(buffer.freeSize(), 99);
    EXPECT_EQ(buffer.isEmpty(), false);
}

TEST(CoreRingBuffer, PushPop){
    core::RingBuffer buffer(100);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 100);
    EXPECT_EQ(buffer.isEmpty(), true);
    const char *inBuffer = "hello world";
    buffer.push(inBuffer, strlen(inBuffer));
    EXPECT_EQ(buffer.allocatedSize(), strlen(inBuffer));
    EXPECT_EQ(buffer.freeSize(), 100 - strlen(inBuffer));
    EXPECT_EQ(buffer.isEmpty(), false);
    char outBuffer[strlen(inBuffer) + 1];
    memset(outBuffer, 0, strlen(inBuffer) + 1);
    char *pOutputBuffer = outBuffer;
    buffer.pop(&pOutputBuffer, strlen(inBuffer));
    EXPECT_STREQ(pOutputBuffer, inBuffer);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 100);
    EXPECT_EQ(buffer.isEmpty(), true);
}

TEST(CoreRingBuffer, EqualPushPop){
    core::RingBuffer buffer(10);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 10);
    EXPECT_EQ(buffer.isEmpty(), true);
    buffer.push("aaaaaaaaaa", 10);
    EXPECT_EQ(buffer.allocatedSize(), 10);
    EXPECT_EQ(buffer.freeSize(), 0);
    EXPECT_EQ(buffer.isEmpty(), false);
    char outBuffer[10];
    char *pOutBuffer = outBuffer;
    buffer.pop(&pOutBuffer, 10);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 10);
    EXPECT_EQ(buffer.isEmpty(), true);
}

TEST(CoreRingBuffer, PopFromEmpty){
    core::RingBuffer buffer(100);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 100);
    EXPECT_EQ(buffer.isEmpty(), true);
    char a;
    char *pA = &a;
    buffer.pop(&pA, 1);
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 100);
    EXPECT_EQ(buffer.isEmpty(), true);
}

TEST(CoreRingBuffer, PushPopWrap){
    core::RingBuffer buffer(10);
    // [|---------]
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 10);
    EXPECT_EQ(buffer.isEmpty(), true);
    for(int i = 0; i < 5; i++){
        buffer.push("a", 1);
    }
    // [BxxxF-----]
    EXPECT_EQ(buffer.allocatedSize(), 5);
    EXPECT_EQ(buffer.freeSize(), 5);
    EXPECT_EQ(buffer.isEmpty(), false);
    for(int i = 0; i < 2; i++){
        char a;
        char *pA = &a;
        buffer.pop(&pA, 1);
    }
    // [--BxF-----]
    EXPECT_EQ(buffer.allocatedSize(), 3);
    EXPECT_EQ(buffer.freeSize(), 7);
    EXPECT_EQ(buffer.isEmpty(), false);
    for(int i = 0; i < 6; i++){
        buffer.push("a", 1);
    }
    // [F-Bxxxxxxx]
    EXPECT_EQ(buffer.allocatedSize(), 9);
    EXPECT_EQ(buffer.freeSize(), 1);
    EXPECT_EQ(buffer.isEmpty(), false);
    for(int i = 0; i < 5; i++){
        char a;
        char *pA = &a;
        buffer.pop(&pA, 1);
    }
    // [F------Bxx]
    EXPECT_EQ(buffer.allocatedSize(), 4);
    EXPECT_EQ(buffer.freeSize(), 6);
    EXPECT_EQ(buffer.isEmpty(), false);
}

TEST(CoreRingBuffer, PushPopWrapResize1){
    core::RingBuffer buffer(10);
    // [|---------]
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 10);
    EXPECT_EQ(buffer.isEmpty(), true);
    for(int i = 0; i < 5; i++){
        buffer.push("a", 1);
    }
    // [BxxxF-----]
    EXPECT_EQ(buffer.allocatedSize(), 5);
    EXPECT_EQ(buffer.freeSize(), 5);
    EXPECT_EQ(buffer.isEmpty(), false);
    for(int i = 0; i < 2; i++){
        char a;
        char *pA = &a;
        buffer.pop(&pA, 1);
    }
    // [--BxF-----]
    EXPECT_EQ(buffer.allocatedSize(), 3);
    EXPECT_EQ(buffer.freeSize(), 7);
    EXPECT_EQ(buffer.isEmpty(), false);
    buffer.push("aaaaaaaaaa", 10);
    // [BxxxxxxxxxxxF-------]
    EXPECT_EQ(buffer.allocatedSize(), 13);
    EXPECT_EQ(buffer.freeSize(), 7);
    EXPECT_EQ(buffer.isEmpty(), false);
}

TEST(CoreRingBuffer, PushPopWrapResize2){
    core::RingBuffer buffer(10);
    // [|---------]
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 10);
    EXPECT_EQ(buffer.isEmpty(), true);
    for(int i = 0; i < 5; i++){
        buffer.push("a", 1);
    }
    // [BxxxF-----]
    EXPECT_EQ(buffer.allocatedSize(), 5);
    EXPECT_EQ(buffer.freeSize(), 5);
    EXPECT_EQ(buffer.isEmpty(), false);
    for(int i = 0; i < 3; i++){
        char a;
        char *pA = &a;
        buffer.pop(&pA, 1);
    }
    // [---BF-----]
    EXPECT_EQ(buffer.allocatedSize(), 2);
    EXPECT_EQ(buffer.freeSize(), 8);
    EXPECT_EQ(buffer.isEmpty(), false);
    for(int i = 0; i < 7; i++){
        buffer.push("a", 1);
    }
    // [xF-Bxxxxxx]
    EXPECT_EQ(buffer.allocatedSize(), 9);
    EXPECT_EQ(buffer.freeSize(), 1);
    EXPECT_EQ(buffer.isEmpty(), false);
    buffer.push("aaaa", 4);
    // [BxxxxxxxxxxxF-------]
    EXPECT_EQ(buffer.allocatedSize(), 13);
    EXPECT_EQ(buffer.freeSize(), 7);
    EXPECT_EQ(buffer.isEmpty(), false);
}

TEST(CoreRingBuffer, PushPopWrapResize3){
    core::RingBuffer buffer(10);
    char outBuffer[10];
    char *pOutBuffer = outBuffer;

    // [|---------]
    EXPECT_EQ(buffer.allocatedSize(), 0);
    EXPECT_EQ(buffer.freeSize(), 10);
    EXPECT_EQ(buffer.isEmpty(), true);
    buffer.push("aa", 2);
    buffer.push("aa", 2);
    buffer.push("aa", 2);
    buffer.push("aa", 2);
    buffer.push("aa", 2);
    // [BxxxxxxxxF]
    EXPECT_EQ(buffer.allocatedSize(), 10);
    EXPECT_EQ(buffer.freeSize(), 0);
    EXPECT_EQ(buffer.isEmpty(), false);
    buffer.pop(&pOutBuffer, 2);
    buffer.pop(&pOutBuffer, 2);
    buffer.pop(&pOutBuffer, 2);
    buffer.pop(&pOutBuffer, 2);
    // [--------BF]
    EXPECT_EQ(buffer.allocatedSize(), 2);
    EXPECT_EQ(buffer.freeSize(), 8);
    EXPECT_EQ(buffer.isEmpty(), false);
    buffer.push("aaa", 3);
    // [xxF-----Bx]
    EXPECT_EQ(buffer.allocatedSize(), 5);
    EXPECT_EQ(buffer.freeSize(), 5);
    EXPECT_EQ(buffer.isEmpty(), false);
    buffer.pop(&pOutBuffer, 3);
    // [BF-------]
    EXPECT_EQ(buffer.allocatedSize(), 3);
    EXPECT_EQ(buffer.freeSize(), 7);
    EXPECT_EQ(buffer.isEmpty(), false);
}

//////////////////////////////////////////////////////////////////////////