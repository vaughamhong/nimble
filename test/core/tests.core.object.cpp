//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//////////////////////////////////////////////////////////////////////////

#include "gtest/gtest.h"
#include "gmock/gmock.h"

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/object.h>
#include <nimble/core/objectfactory.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;

//////////////////////////////////////////////////////////////////////////

class CoreObject 
: public ::testing::Test{
public:

	// struct Foo
	// : public core::Object{
	//     int x, y;

	//     enum{
	//     	kEnum1 = 0,
	//     	kEnum2 = 1
	//     };
	// };
	
	// core::ObjectFactory factory;

public:
	CoreObject(){
	}
	virtual ~CoreObject(){
	}
	virtual void SetUp(){
	    // core::Field fields[] = {
	    //     core::Field("x", &Foo::x),
	    //     core::Field("y", &Foo::y)
	    // };
	    // core::Enumeration enumerations[] = {
	    // 	core::Enumeration("kEnum1", Foo::kEnum1),
	    // 	core::Enumeration("kEnum2", Foo::kEnum2),
	    // };
	    // factory.registerType<Foo>("Foo")
	    // 	->fields(fields)
	    // 	->enumerations(enumerations);
	}
	virtual void TearDown(){
	}
};

//////////////////////////////////////////////////////////////////////////

TEST_F(CoreObject, ValidName){
	// core::Object *pObject = factory.createObject("Foo");
	// EXPECT_STREQ(pObject->getTypeInfo()->name(), "Foo");
	// factory.destroyObject(pObject);
}
TEST_F(CoreObject, ValidSize){
// 	core::Object *pObject = factory.createObject("Foo");
// 	EXPECT_EQ(pObject->getTypeInfo()->size(), sizeof(Foo));
// 	factory.destroyObject(pObject);
}

//////////////////////////////////////////////////////////////////////////