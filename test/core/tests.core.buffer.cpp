//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//////////////////////////////////////////////////////////////////////////

#include "gtest/gtest.h"
#include "gmock/gmock.h"

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/buffer.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;

//////////////////////////////////////////////////////////////////////////

TEST(CoreBuffer, Construct){
	core::Buffer buffer;
    EXPECT_EQ(buffer.getSize(), 0);
    EXPECT_EQ(buffer.getPointer(), (void*)NULL);
    EXPECT_EQ(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreBuffer, ConstructWithZeroSize){
	core::Buffer buffer;
    EXPECT_EQ(buffer.getSize(), 0);
    EXPECT_EQ(buffer.getPointer(), (void*)NULL);
    EXPECT_EQ(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreBuffer, ConstructWithPositiveSize){
	core::Buffer buffer(100);
    EXPECT_EQ(buffer.getSize(), 100);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreBuffer, CopyConstruct){
	core::Buffer buffer1(100);
	core::Buffer buffer2 = buffer1;
    EXPECT_EQ(buffer1.getSize(), buffer2.getSize());
	EXPECT_NE(buffer1.getPointer(), (void*)NULL);
    EXPECT_NE(buffer1.getConstPointer(), (void*)NULL);
    EXPECT_NE(buffer2.getPointer(), (void*)NULL);
    EXPECT_NE(buffer2.getConstPointer(), (void*)NULL);
}

TEST(CoreBuffer, ShrinkBuffer){
	core::Buffer buffer(200);
	EXPECT_EQ(buffer.getSize(), 200);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
	buffer.resize(100);
	EXPECT_EQ(buffer.getSize(), 100);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
    buffer.resize(0);
    EXPECT_EQ(buffer.getSize(), 0);
    EXPECT_EQ(buffer.getPointer(), (void*)NULL);
    EXPECT_EQ(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreBuffer, ExpandBuffer){
	core::Buffer buffer(0);
	EXPECT_EQ(buffer.getSize(), 0);
    EXPECT_EQ(buffer.getPointer(), (void*)NULL);
    EXPECT_EQ(buffer.getConstPointer(), (void*)NULL);
	buffer.resize(100);
    EXPECT_EQ(buffer.getSize(), 100);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
    buffer.resize(200);
    EXPECT_EQ(buffer.getSize(), 200);
    EXPECT_NE(buffer.getPointer(), (void*)NULL);
    EXPECT_NE(buffer.getConstPointer(), (void*)NULL);
}

TEST(CoreBuffer, CopyBuffer){
	core::Buffer buffer1(100);
	core::Buffer buffer2;
    EXPECT_EQ(buffer1.getSize(), 100);
    EXPECT_NE(buffer1.getPointer(), (void*)NULL);
    EXPECT_NE(buffer1.getConstPointer(), (void*)NULL);
    buffer2 = buffer1;
    EXPECT_EQ(buffer2.getSize(), 100);
    EXPECT_NE(buffer2.getPointer(), (void*)NULL);
    EXPECT_NE(buffer2.getConstPointer(), (void*)NULL);
}

//////////////////////////////////////////////////////////////////////////