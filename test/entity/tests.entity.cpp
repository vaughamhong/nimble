//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//////////////////////////////////////////////////////////////////////////

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#if 0
//////////////////////////////////////////////////////////////////////////

#include <nimble/core/locator.h>
#include <nimble/core/filesystem.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/entity/entitymanager.h>
#include <nimble/resource/resourcemanager.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;

//////////////////////////////////////////////////////////////////////////

// To use a test fixture, derive a class from testing::Test.
class EntityFixture : public testing::Test{
protected:

    // virtual void SetUp() will be called before each test is run.
    virtual void SetUp(){
        m_pFileSystemStreamProvider = new core::FileSystemStreamProvider();
        core::Locator<core::FileSystemStreamProvider>::provide(m_pFileSystemStreamProvider);

        m_pResourceManager = new resource::ResourceManager(m_pFileSystemStreamProvider);
        core::Locator<resource::ResourceManager>::provide(m_pResourceManager);

        m_pComponentManager = new entity::ComponentManager();
        m_pComponentManager->initialize();
        core::Locator<entity::ComponentManager>::provide(m_pComponentManager);

        m_pEntityManager = new entity::EntityManager(m_pResourceManager);
        m_pEntityManager->initialize();
        core::Locator<entity::EntityManager>::provide(m_pEntityManager);
    }

    // virtual void TearDown() will be called after each test is run.
    virtual void TearDown() {
        delete m_pEntityManager; m_pEntityManager = NULL;
        core::Locator<entity::EntityManager>::release();

        delete m_pComponentManager; m_pComponentManager = NULL;
        core::Locator<entity::ComponentManager>::release();

        delete m_pResourceManager; m_pResourceManager = NULL;
        core::Locator<resource::ResourceManager>::release();

        delete m_pFileSystemStreamProvider; m_pFileSystemStreamProvider = NULL;
        core::Locator<core::FileSystemStreamProvider>::release();
    }

    core::FileSystemStreamProvider  *m_pFileSystemStreamProvider;
    resource::ResourceManager       *m_pResourceManager;
    entity::ComponentManager        *m_pComponentManager;
    entity::EntityManager           *m_pEntityManager;
};

//////////////////////////////////////////////////////////////////////////

TEST_F(EntityFixture, Initialization){
	core::FileSystemStreamProvider *pFileSystem = core::Locator<core::FileSystemStreamProvider>::acquire();
    resource::ResourceManager *pResourceManager = core::Locator<resource::ResourceManager>::acquire();
    entity::ComponentManager *pComponentManager = core::Locator<entity::ComponentManager>::acquire();
    entity::EntityManager *pEntityManager = core::Locator<entity::EntityManager>::acquire();
    EXPECT_NE(pFileSystem, (core::FileSystemStreamProvider*)NULL);
    EXPECT_NE(pResourceManager, (resource::ResourceManager*)NULL);
    EXPECT_NE(pComponentManager, (entity::ComponentManager*)NULL);
    EXPECT_NE(pEntityManager, (entity::EntityManager*)NULL);
    
    entity::IComponent *pComponent = pComponentManager->createComponent("test");
    EXPECT_EQ((pComponent == NULL), true);
}

//////////////////////////////////////////////////////////////////////////

#endif