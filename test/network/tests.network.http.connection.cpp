//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

//////////////////////////////////////////////////////////////////////////

#include "gtest/gtest.h"
#include "gmock/gmock.h"

//////////////////////////////////////////////////////////////////////////

#include <nimble/network/http.connection.h>
#include <nimble/network/http.request.file.download.h>
#include <nimble/network/http.request.file.upload.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;

//////////////////////////////////////////////////////////////////////////

TEST(NetworkHttpConnection, Construct){
	network::HttpConnection connection;
}
TEST(NetworkHttpConnection, DispatchGetRequest){
	network::HttpConnection connection;
	network::HttpRequest req(network::HttpRequest::kGet, network::Url("www.google.com"));	
	network::HttpResponse res = connection.dispatch(req);
}
TEST(NetworkHttpConnection, DispatchPostRequest){
	network::HttpConnection connection;
	network::HttpRequest req(network::HttpRequest::kPost, network::Url("www.google.com"));
	network::HttpResponse res = connection.dispatch(req);
}
TEST(NetworkHttpConnection, DispatchPutRequest){
	network::HttpConnection connection;
	network::HttpRequest req(network::HttpRequest::kPut, network::Url("www.google.com"));
	network::HttpResponse res = connection.dispatch(req);
}
TEST(NetworkHttpConnection, DispatchDeleteRequest){
	network::HttpConnection connection;
	network::HttpRequest req(network::HttpRequest::kDelete, network::Url("www.google.com"));
	network::HttpResponse res = connection.dispatch(req);
}
// TEST(NetworkHttpConnection, DispatchFileDownloadRequest){
// 	network::HttpConnection connection;
// 	network::HttpFileDownloadRequest req("./builds/tests/muppet.jpg", network::Url("http://4.bp.blogspot.com/-ocj5bqA4hdE/TtkSdso6yAI/AAAAAAAAB4M/rpKYchTbYqQ/s320/animal_muppet_13.jpg"));
// 	network::HttpResponse res = connection.dispatch(req);
// }
// TEST(NetworkHttpConnection, DispatchFileUploadRequest){
// 	network::HttpConnection connection;
// 	network::HttpFileUploadRequest req("./builds/tests/muppet.jpg", network::Url("http://4.bp.blogspot.com/-ocj5bqA4hdE/TtkSdso6yAI/AAAAAAAAB4M/rpKYchTbYqQ/s320/animal_muppet_13.jpg"));
// 	network::HttpResponse res = connection.dispatch(req);
// }

//////////////////////////////////////////////////////////////////////////