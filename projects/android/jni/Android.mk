LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

# The Applications will need to link against these libraries, we
# can't link them into a static VrLib ourselves.
#
# LOCAL_LDLIBS += -lGLESv3         # OpenGL ES 3.0
# LOCAL_LDLIBS += -lEGL            # GL platform interface
# LOCAL_LDLIBS += -llog            # logging
# LOCAL_LDLIBS += -landroid        # native windows

LOCAL_MODULE        := nimble
LOCAL_ARM_MODE      := arm

include $(LOCAL_PATH)/cflags.mk

SRC_ACTION			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/action/*.cpp)
SRC_ACTION 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/action/actions/*.cpp)
SRC_APP 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/application/*.cpp)
SRC_APP 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/application/platform/android/*.cpp)
SRC_CORE 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/core/*.cpp)
SRC_CORE 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/core/hash/*.cpp)
SRC_CORE 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/core/platform/android/*.cpp)

SRC_AUDIO 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/audio/*.cpp)
SRC_AUDIODEVICE 	:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/audiodevice/*.cpp)
SRC_DATABASE 		:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/database/*.cpp)
SRC_ENTITY 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/*.cpp)
SRC_ENTITY 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/action/*.cpp)
SRC_ENTITY 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/graphics/*.cpp)
SRC_ENTITY 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/input/*.cpp)
SRC_ENTITY 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/mediaplayer/*.cpp)
SRC_ENTITY 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/physics/*.cpp)
SRC_ENTITY 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/scene/*.cpp)
#SRC_ENTITY 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/script/*.cpp)
SRC_ENTITY 			+= $(wildcard $(LOCAL_PATH)/../../../source/nimble/entity/web/*.cpp)
SRC_FONT 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/font/*.cpp)
SRC_GRAPHICS 		:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/graphics/*.cpp)
SRC_IMAGE 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/image/*.cpp)
SRC_INPUT 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/input/*.cpp)
SRC_MATH 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/math/*.cpp)
SRC_MESSAGE 		:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/message/*.cpp)
SRC_MODEL 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/model/*.cpp)
SRC_NETWORK 		:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/network/*.cpp)
SRC_PHYSICS 		:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/physics/*.cpp)
SRC_RENDERDEVICE 	:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/renderdevice/*.cpp)
SRC_RESOURCE 		:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/resource/*.cpp)
SRC_SCENE 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/scene/*.cpp)
#SRC_SCRIPT 			:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/script/*.cpp)

SRC_MEDIAPLAYER 		:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/mediaplayer/*.cpp)
SRC_AUDIODEVICE_OPENAL 	:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/audiodevice-openal/*.cpp)
SRC_RENDERDEVICE_OPENGL := $(wildcard $(LOCAL_PATH)/../../../source/nimble/renderdevice-opengl/*.cpp)
SRC_WEB 				:= $(wildcard $(LOCAL_PATH)/../../../source/nimble/web/*.cpp)

SRC_TINYXML2 			:= $(wildcard $(LOCAL_PATH)/../../../external/tinyxml2/*.cpp)
SRC_JSONCPP 			:= $(wildcard $(LOCAL_PATH)/../../../external/jsoncpp/src/lib_json/*.cpp)

SRC_GLES30	 			:= $(wildcard $(LOCAL_PATH)/../../../dependencies/nimble_renderdevice_opengles/source/nimble/renderdevice/opengles_3_0/*.cpp)

LOCAL_C_INCLUDES := $(ANDROID_NDK_ROOT)\platforms\android-19\arch-arm\usr\include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/curl/osx/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/openssl/osx/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/assimp/osx/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/freetype2/osx/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/freeimage/osx/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/sqlite3/osx/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/tinyxml2/osx/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/jsoncpp/osx/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/v8/android/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../prebuilt/v8/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../external/bullet/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../dependencies/nimble_renderdevice_opengles/include

LOCAL_SRC_FILES  := $(SRC_ACTION:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_APP:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_CORE:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_CORE_PLATFORM:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_CORE_HASH:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_DATABASE:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_ENTITY:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_FONT:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_GRAPHICS:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_IMAGE:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_INPUT:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_MATH:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_MESSAGE:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_MODEL:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_NETWORK:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_PHYSICS:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_RENDERDEVICE:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_RESOURCE:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_SCENE:$(LOCAL_PATH)/%=%)
#LOCAL_SRC_FILES  += $(SRC_SCRIPT:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES  += $(SRC_MEDIAPLAYER:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_WEB:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES  += $(SRC_TINYXML2:$(LOCAL_PATH)/%=%)
LOCAL_SRC_FILES  += $(SRC_JSONCPP:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES  += $(SRC_GLES30:$(LOCAL_PATH)/%=%)

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)

# OpenGL ES 3.0
LOCAL_EXPORT_LDLIBS := -lGLESv3
LOCAL_EXPORT_LDLIBS += -lEGL
LOCAL_EXPORT_LDLIBS += -lOpenMAXAL
LOCAL_EXPORT_LDLIBS += -lOpenSLES
LOCAL_EXPORT_LDLIBS += -llog
LOCAL_EXPORT_LDLIBS += -landroid
LOCAL_EXPORT_LDLIBS += -lz

include $(BUILD_STATIC_LIBRARY)		# start building based on everything since CLEAR_VARS
