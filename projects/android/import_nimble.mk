#
# import_nimble.mk
#
# Common settings used by all VRLib based projects.  Import at the start of any module using VRLib.
#
# Use the LOCAL_EXPORT_ variants for the oculus prebuilt library instead of
# LOCAL_. This works for the following variables:
# 	LOCAL_EXPORT_LDLIBS
# 	LOCAL_EXPORT_CFLAGS
# 	LOCAL_EXPORT_C_INCLUDES
#
# See the NDK documentation for more details.

################################################################################

# save off the local path
LOCAL_PATH_TEMP := $(LOCAL_PATH)
LOCAL_PATH := $(call my-dir)

################################################################################

include $(CLEAR_VARS)

LOCAL_MODULE := nimble
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../include

# OpenGL ES 3.0
LOCAL_EXPORT_LDLIBS := -lGLESv3
LOCAL_EXPORT_LDLIBS += -lEGL
LOCAL_EXPORT_LDLIBS += -lOpenMAXAL 
LOCAL_EXPORT_LDLIBS += -llog
LOCAL_EXPORT_LDLIBS += -landroid
LOCAL_EXPORT_LDLIBS += -lz
LOCAL_EXPORT_LDLIBS += -lOpenSLES

LOCAL_SRC_FILES := obj/local/armeabi-v7a/libnimble.a

include $(PREBUILT_STATIC_LIBRARY)

################################################################################

# Restore the local path since we overwrote it when we started
LOCAL_PATH := $(LOCAL_PATH_TEMP)

################################################################################