#!/bin/sh

ROOT=`pwd`
INSTALL_DIR=$1

# Build for native
mkdir -p $INSTALL_DIR
pushd $ROOT/external/curl
./configure --prefix=$INSTALL_DIR --exec-prefix=$INSTALL_DIR \
--disable-shared \
--enable-static \
--enable-http \
--disable-crypto-auth \
--disable-ipv6 \
--disable-manual \
--disable-ftp \
--disable-file \
--disable-ldap \
--disable-ldaps \
--disable-rtsp \
--disable-proxy \
--disable-dict \
--disable-telnet \
--disable-tftp \
--disable-pop3 \
--disable-imap \
--disable-smtp \
--disable-gopher \
--disable-manual \
--disable-ntlm \
--disable-ntlm-wb \
--disable-ipv6 \
--disable-sspi \
--disable-cookies \
--without-zlib \
--without-ssl \
--host="i386-apple-darwin"
make -j `sysctl -n hw.logicalcpu_max`
make install
popd