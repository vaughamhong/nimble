#!/bin/sh

################################################################################

ROOT=`pwd`
CURL_DIR="${ROOT}/external/curl"
LIB_DIR="${HOME}/Desktop/curl"

################################################################################

export CC="/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang"

################################################################################

build(){
    ARCH="$1"
    CURL_DIR="$2"
    OUT_DIR="$3"

    mkdir -p ${OUT_DIR}/${ARCH}

    pushd ${CURL_DIR}

    make distclean

    ./configure \
    --disable-shared \
    --enable-static \
    --enable-http \
    --disable-crypto-auth \
    --disable-ipv6 \
    --disable-manual \
    --disable-ftp \
    --disable-file \
    --disable-ldap \
    --disable-ldaps \
    --disable-rtsp \
    --disable-proxy \
    --disable-dict \
    --disable-telnet \
    --disable-tftp \
    --disable-pop3 \
    --disable-imap \
    --disable-smtp \
    --disable-gopher \
    --disable-manual \
    --disable-ntlm \
    --disable-ntlm-wb \
    --disable-ipv6 \
    --disable-sspi \
    --disable-cookies \
    --without-zlib \
    --enable-threaded-resolver \
    --host="${ARCH}-apple-darwin" \
    --with-ssl \
    --with-libssl-prefix=/usr/local/ssl
    
    make -j `sysctl -n hw.logicalcpu_max`

    cp -rf include "${OUT_DIR}/${ARCH}/include"
    cp lib/.libs/libcurl.a "${OUT_DIR}/${ARCH}/libcurl.a"

    popd
}

################################################################################

# IOS_SDKROOT="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS.sdk"
# export IPHONEOS_DEPLOYMENT_TARGET="7.0"

# ARCH="armv7"
# export CFLAGS="-arch ${ARCH} -pipe -Os -gdwarf-2 -isysroot ${IOS_SDKROOT}"
# export LDFLAGS="-arch ${ARCH} -isysroot ${IOS_SDKROOT}"
# build ${ARCH} ${CURL_DIR} ${LIB_DIR}/release

# ARCH="armv7"
# export CFLAGS="-arch ${ARCH} -pipe -g -gdwarf-2 -isysroot ${IOS_SDKROOT}"
# export LDFLAGS="-arch ${ARCH} -isysroot ${IOS_SDKROOT}"
# build ${ARCH} ${CURL_DIR} ${LIB_DIR}/debug

# ARCH="armv7s"
# export CFLAGS="-arch ${ARCH} -pipe -Os -gdwarf-2 -isysroot ${IOS_SDKROOT}"
# export LDFLAGS="-arch ${ARCH} -isysroot ${IOS_SDKROOT}"
# build ${ARCH} ${CURL_DIR} ${LIB_DIR}/release

# ARCH="armv7s"
# export CFLAGS="-arch ${ARCH} -pipe -g -gdwarf-2 -isysroot ${IOS_SDKROOT}"
# export LDFLAGS="-arch ${ARCH} -isysroot ${IOS_SDKROOT}"
# build ${ARCH} ${CURL_DIR} ${LIB_DIR}/debug

# unset IPHONEOS_DEPLOYMENT_TARGET

################################################################################

OSX_SDKVER="10.10"
OSX_SDKROOT="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX${OSX_SDKVER}.sdk"
export PKG_CONFIG_PATH=/usr/local/ssl/lib/pkgconfig

ARCH="i386"
export CFLAGS="-arch ${ARCH} -pipe -Os -gdwarf-2 -isysroot ${OSX_SDKROOT}"
export LDFLAGS="-arch ${ARCH} -isysroot ${OSX_SDKROOT}"
build ${ARCH} ${CURL_DIR} ${LIB_DIR}/release

ARCH="i386"
export CFLAGS="-arch ${ARCH} -pipe -g -gdwarf-2 -isysroot ${OSX_SDKROOT}"
export LDFLAGS="-arch ${ARCH} -isysroot ${OSX_SDKROOT}"
build ${ARCH} ${CURL_DIR} ${LIB_DIR}/debug

ARCH="x86_64"
export CFLAGS="-arch ${ARCH} -pipe -Os -gdwarf-2 -isysroot ${OSX_SDKROOT}"
export LDFLAGS="-arch ${ARCH} -isysroot ${OSX_SDKROOT}"
build ${ARCH} ${CURL_DIR} ${LIB_DIR}/release

ARCH="x86_64"
export CFLAGS="-arch ${ARCH} -pipe -g -gdwarf-2 -isysroot ${OSX_SDKROOT}"
export LDFLAGS="-arch ${ARCH} -isysroot ${OSX_SDKROOT}"
build ${ARCH} ${CURL_DIR} ${LIB_DIR}/debug

################################################################################

sudo lipo -create -output "${LIB_DIR}/release/libcurl.a" \
# "${LIB_DIR}/release/armv7/libcurl.a" \
# "${LIB_DIR}/release/armv7s/libcurl.a" \
"${LIB_DIR}/release/i386/libcurl.a" \
"${LIB_DIR}/release/x86_64/libcurl.a"

sudo lipo -create -output "${LIB_DIR}/debug/libcurl.a" \
# "${LIB_DIR}/debug/armv7/libcurl.a" \
# "${LIB_DIR}/debug/armv7s/libcurl.a" \
"${LIB_DIR}/debug/i386/libcurl.a" \
"${LIB_DIR}/debug/x86_64/libcurl.a"

################################################################################