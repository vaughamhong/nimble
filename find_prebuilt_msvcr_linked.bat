:: When we link all our (prebuilt) libraries we must be sure they are
:: all linked to the same runtime libraries or else our application will not
:: link properly. This script finds the list of libs in the prebuilt directory
:: and checks if they are linked to the MSVCR Visual Studio runtime library.
:: This is especially useful when we are building prebuilt libs and our application
:: stops linking with an error similar to -
:: error LNK2005: "void __cdecl operator delete[](void *)" (??_V@YAXPAX@Z) already defined in LIBCMTD.lib(delete.obj)
FOR /F %%j IN ('DIR prebuilt\*.lib /b/s') DO (
	echo | set /P=%%j:
	dumpbin /all %%j | findstr "MSVCR"
)