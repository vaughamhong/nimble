#!/bin/sh

SDKVER="7.0"
ROOT=`pwd`
INSTALL_DIR=$1
mkdir -p $INSTALL_DIR
pushd $ROOT/external/curl

# Build for device (iOS)
DEVROOT="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer"
SDKROOT="$DEVROOT/SDKs/iPhoneOS$SDKVER.sdk"
ARM_INSTALL_DIR=$INSTALL_DIR/arm
mkdir -p $ARM_INSTALL_DIR
export CC=`xcrun -sdk iphoneos -find clang`
export CFLAGS="-arch armv7s -isysroot $SDKROOT"
export CPPFLAGS="-I$ARM_INSTALL_DIR/include/curl -I$ARM_INSTALL_DIR/include -I$ARM_INSTALL_DIR"
export LDFLAGS="-isysroot $SDKROOT -Wl,-syslibroot $SDKROOT"
sudo make clean
sudo ./configure --prefix=$ARM_INSTALL_DIR --exec-prefix=$ARM_INSTALL_DIR \
--disable-shared \
--enable-static \
--enable-http \
--disable-crypto-auth \
--disable-ipv6 \
--disable-manual \
--disable-ftp \
--disable-file \
--disable-ldap \
--disable-ldaps \
--disable-rtsp \
--disable-proxy \
--disable-dict \
--disable-telnet \
--disable-tftp \
--disable-pop3 \
--disable-imap \
--disable-smtp \
--disable-gopher \
--disable-manual \
--disable-ntlm \
--disable-ntlm-wb \
--disable-ipv6 \
--disable-sspi \
--disable-cookies \
--without-zlib \
--without-ssl \
--with-darwinssl \
--host="arm-apple-darwin10"
sudo make -j `sysctl -n hw.logicalcpu_max`
sudo make install

# Build for simulator (iOS)
DEVROOT="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer"
SDKROOT="$DEVROOT/SDKs/iPhoneSimulator$SDKVER.sdk"
I385_INSTALL_DIR=$INSTALL_DIR/i386
mkdir -p $I385_INSTALL_DIR
export CC="/usr/bin/gcc"
export CFLAGS="-pipe -m32 -gdwarf-2 -isysroot $SDKROOT"
export CPPFLAGS="-I$I385_INSTALL_DIR/include/curl -I$I385_INSTALL_DIR/include -I$I385_INSTALL_DIR"
export LDFLAGS="-arch i386 -isysroot $SDKROOT"
sudo make clean
./configure --prefix=$I385_INSTALL_DIR --exec-prefix=$I385_INSTALL_DIR \
--disable-shared \
--enable-static \
--enable-http \
--disable-crypto-auth \
--disable-ipv6 \
--disable-manual \
--disable-ftp \
--disable-file \
--disable-ldap \
--disable-ldaps \
--disable-rtsp \
--disable-proxy \
--disable-dict \
--disable-telnet \
--disable-tftp \
--disable-pop3 \
--disable-imap \
--disable-smtp \
--disable-gopher \
--disable-manual \
--disable-ntlm \
--disable-ntlm-wb \
--disable-ipv6 \
--disable-sspi \
--disable-cookies \
--without-zlib \
--without-ssl \
--with-darwinssl \
--host="i386-apple-darwin"
sudo make -j `sysctl -n hw.logicalcpu_max`
sudo make install
popd

# Create universal lib
lipo -create "$ARM_INSTALL_DIR/lib/libcurl.a" "$I385_INSTALL_DIR/lib/libcurl.a" -output "$INSTALL_DIR/libcurl.a"
