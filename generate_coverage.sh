export CC=/usr/bin/clang
export CXX=/usr/bin/clang++

ROOT=`pwd`
PROJ_DIR=$ROOT/projects/coverage
BUILD_DIR=$ROOT/builds/coverage
LIB_DIR=$ROOT/builds/coverage/libs
BIN_DIR=$ROOT/builds/coverage/bins
mkdir -p $PROJ_DIR
mkdir -p $LIB_DIR
mkdir -p $BIN_DIR

pushd $PROJ_DIR
cmake \
-DCMAKE_BUILD_TYPE=Debug \
-DNIMBLE_LIBS_PATH=$LIB_DIR \
-DNIMBLE_BINS_PATH=$BIN_DIR \
-DNIMBLE_GENPROJ_TESTS_WITHCOVERAGE=1 \
-DNIMBLE_DISABLE_ASSERTS=1 \
-G "Unix Makefiles" $ROOT
make -j `sysctl -n hw.logicalcpu_max`
popd

# mkdir -p valgrind
# valgrind --leak-check=full --suppressions=./valgrind.supp --xml=yes --xml-file=./valgrind/valgrind.xml ./builds/coverage/bins/nimble-runtests
./builds/coverage/bins/nimble-runtests
TEST_BUILD_DIR=./generated/coverage/CMakeFiles
TEST_COVERAGE_DIR=$TEST_BUILD_DIR/coverage
mkdir -p $TEST_COVERAGE_DIR
find $TEST_BUILD_DIR -name "*.gcda" -exec cp {} $TEST_COVERAGE_DIR \;
find $TEST_BUILD_DIR -name "*.gcno" -exec cp {} $TEST_COVERAGE_DIR \;
lcov --capture --directory $TEST_COVERAGE_DIR --output-file $TEST_COVERAGE_DIR/coverage.info1
lcov --remove $TEST_COVERAGE_DIR/coverage.info1 "/external/curl*" --output-file $TEST_COVERAGE_DIR/coverage.info2
lcov --remove $TEST_COVERAGE_DIR/coverage.info2 "/external/freeimage*" --output-file $TEST_COVERAGE_DIR/coverage.info3
lcov --remove $TEST_COVERAGE_DIR/coverage.info3 "/external/freetype*" --output-file $TEST_COVERAGE_DIR/coverage.info4
lcov --remove $TEST_COVERAGE_DIR/coverage.info4 "/external/bullet*" --output-file $TEST_COVERAGE_DIR/coverage.info5
lcov --remove $TEST_COVERAGE_DIR/coverage.info5 "/external/gtest*" --output-file $TEST_COVERAGE_DIR/coverage.info6
lcov --remove $TEST_COVERAGE_DIR/coverage.info6 "/external/gmock*" --output-file $TEST_COVERAGE_DIR/coverage.info7
lcov --remove $TEST_COVERAGE_DIR/coverage.info7 "/external/lua*" --output-file $TEST_COVERAGE_DIR/coverage.info8
lcov --remove $TEST_COVERAGE_DIR/coverage.info8 "/external/rapidxml*" --output-file $TEST_COVERAGE_DIR/coverage.info9
lcov --remove $TEST_COVERAGE_DIR/coverage.info9 "/external/sqlite*" --output-file $TEST_COVERAGE_DIR/coverage.info10
lcov --remove $TEST_COVERAGE_DIR/coverage.info10 "/external/zlib*" --output-file $TEST_COVERAGE_DIR/coverage.info11
lcov --remove $TEST_COVERAGE_DIR/coverage.info11 "/usr*" --output-file $TEST_COVERAGE_DIR/coverage.info
genhtml $TEST_COVERAGE_DIR/coverage.info --title nimble --output-directory coverage
# xsltproc valgrind.xsl valgrind/valgrind.xml > valgrind/valgrind.html