var searchData=
[
  ['analog_5fstick_5f1',['ANALOG_STICK_1',['../classnimble_1_1input_1_1_joystick_device.html#a2a934c38dfe6060ebf879a491d5e57caa5f30587b36f7ac212c1a425a2c3e5282',1,'nimble::input::JoystickDevice']]],
  ['analog_5fstick_5f2',['ANALOG_STICK_2',['../classnimble_1_1input_1_1_joystick_device.html#a2a934c38dfe6060ebf879a491d5e57caa47053f0c03b8ba104e490374b75324d0',1,'nimble::input::JoystickDevice']]],
  ['axis_5fx',['AXIS_X',['../classnimble_1_1input_1_1_joystick_device.html#a743d88c28c02d2589bbf9c6b04deb998a2b23730d08273fa2ece5769272f5de3b',1,'nimble::input::JoystickDevice::AXIS_X()'],['../classnimble_1_1input_1_1_mouse_device.html#ad59cdae26f286c66415be0e060655c87aa90594cbd2dbb8d13d47fe731975140e',1,'nimble::input::MouseDevice::AXIS_X()']]],
  ['axis_5fy',['AXIS_Y',['../classnimble_1_1input_1_1_joystick_device.html#a743d88c28c02d2589bbf9c6b04deb998abb4af35e837db32f0ed5f7a9d15aa72f',1,'nimble::input::JoystickDevice::AXIS_Y()'],['../classnimble_1_1input_1_1_mouse_device.html#ad59cdae26f286c66415be0e060655c87a8f59128f47afcea703da6d24abf8b006',1,'nimble::input::MouseDevice::AXIS_Y()']]],
  ['axis_5fz',['AXIS_Z',['../classnimble_1_1input_1_1_joystick_device.html#a743d88c28c02d2589bbf9c6b04deb998ad0769b49d00a3980fcc1377cdab9372e',1,'nimble::input::JoystickDevice::AXIS_Z()'],['../classnimble_1_1input_1_1_mouse_device.html#ad59cdae26f286c66415be0e060655c87af1fc4e5f76711d660469d554825e3dc7',1,'nimble::input::MouseDevice::AXIS_Z()']]]
];
