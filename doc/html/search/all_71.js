var searchData=
[
  ['quaternion',['Quaternion',['../classnimble_1_1math_1_1_quaternion.html',1,'nimble::math']]],
  ['quaternion',['Quaternion',['../classnimble_1_1math_1_1_quaternion.html#af13a0625ce6a7f672604878b29176fc3',1,'nimble::math::Quaternion::Quaternion()'],['../classnimble_1_1math_1_1_quaternion.html#a603ed44512789e10c84a2a4fff536991',1,'nimble::math::Quaternion::Quaternion(Quaternion&lt; T, N &gt; const &amp;q)'],['../classnimble_1_1math_1_1_quaternion.html#a182d91d26ddc555609a6e1c6d678c514',1,'nimble::math::Quaternion::Quaternion(math::Vector&lt; T, N-1 &gt; v, float theta)']]],
  ['quaternion_2eh',['quaternion.h',['../quaternion_8h.html',1,'']]],
  ['quaternion2',['Quaternion2',['../classnimble_1_1math_1_1_quaternion2.html',1,'nimble::math']]],
  ['quaternion2',['Quaternion2',['../classnimble_1_1math_1_1_quaternion2.html#ad3b0c381a86758c27981ec3481d35161',1,'nimble::math::Quaternion2::Quaternion2()'],['../classnimble_1_1math_1_1_quaternion2.html#a612bdabe6cd9a19c59d861a080a680a3',1,'nimble::math::Quaternion2::Quaternion2(Quaternion&lt; T, 2 &gt; const &amp;v1)'],['../classnimble_1_1math_1_1_quaternion2.html#ad65ac5f7b3d5e31b7b17cb47cd8484a8',1,'nimble::math::Quaternion2::Quaternion2(math::Vector&lt; T, 1 &gt; v, float theta)'],['../classnimble_1_1math_1_1_quaternion2.html#a6ade9468ed65035119ad44dc26740d3f',1,'nimble::math::Quaternion2::Quaternion2(T x, T y)']]],
  ['quaternion2f',['Quaternion2f',['../namespacenimble_1_1math.html#aeb0a27fff396fe43ef9b409b4986839d',1,'nimble::math']]],
  ['quaternion3',['Quaternion3',['../classnimble_1_1math_1_1_quaternion3.html',1,'nimble::math']]],
  ['quaternion3',['Quaternion3',['../classnimble_1_1math_1_1_quaternion3.html#ae1336c850c9749f795fe87409a2e60f6',1,'nimble::math::Quaternion3::Quaternion3()'],['../classnimble_1_1math_1_1_quaternion3.html#a8a25007e949d1c0d3fa5bd776cebe782',1,'nimble::math::Quaternion3::Quaternion3(Quaternion&lt; T, 3 &gt; const &amp;v1)'],['../classnimble_1_1math_1_1_quaternion3.html#abf0b5b2e147cce70aa792844d61ccdeb',1,'nimble::math::Quaternion3::Quaternion3(math::Vector&lt; T, 2 &gt; v, float theta)'],['../classnimble_1_1math_1_1_quaternion3.html#a241ae4214638c93e9aa0fbf246c3e62a',1,'nimble::math::Quaternion3::Quaternion3(T x, T y, T z, T w)']]],
  ['quaternion3_3c_20float_20_3e',['Quaternion3&lt; float &gt;',['../classnimble_1_1math_1_1_quaternion3.html',1,'nimble::math']]],
  ['quaternion3f',['Quaternion3f',['../namespacenimble_1_1math.html#a2ede86795482039552cad7c4cb0227ff',1,'nimble::math']]],
  ['quaternion4',['Quaternion4',['../classnimble_1_1math_1_1_quaternion4.html#a9e2eda4fd18331d5591f7357ecfa0b80',1,'nimble::math::Quaternion4::Quaternion4()'],['../classnimble_1_1math_1_1_quaternion4.html#acb924a04de55fdc9f62ae1af0f4f5578',1,'nimble::math::Quaternion4::Quaternion4(Quaternion&lt; T, 4 &gt; const &amp;v1)'],['../classnimble_1_1math_1_1_quaternion4.html#ae388cb89745437aa675e0df069386d58',1,'nimble::math::Quaternion4::Quaternion4(math::Vector&lt; T, 3 &gt; v, float theta)'],['../classnimble_1_1math_1_1_quaternion4.html#ad3aee5797dc189963fecee6f1278c59f',1,'nimble::math::Quaternion4::Quaternion4(T x, T y, T z, T w)']]],
  ['quaternion4',['Quaternion4',['../classnimble_1_1math_1_1_quaternion4.html',1,'nimble::math']]],
  ['quaternion4_3c_20float_20_3e',['Quaternion4&lt; float &gt;',['../classnimble_1_1math_1_1_quaternion4.html',1,'nimble::math']]],
  ['quaternion4f',['Quaternion4f',['../namespacenimble_1_1math.html#af727d79c03ad24348261be223da4ca41',1,'nimble::math']]],
  ['quaternion_3c_20float_2c_203_20_3e',['Quaternion&lt; float, 3 &gt;',['../classnimble_1_1math_1_1_quaternion.html',1,'nimble::math']]],
  ['quaternion_3c_20float_2c_204_20_3e',['Quaternion&lt; float, 4 &gt;',['../classnimble_1_1math_1_1_quaternion.html',1,'nimble::math']]],
  ['quaternion_3c_20t_2c_202_20_3e',['Quaternion&lt; T, 2 &gt;',['../classnimble_1_1math_1_1_quaternion.html',1,'nimble::math']]],
  ['quaternion_3c_20t_2c_203_20_3e',['Quaternion&lt; T, 3 &gt;',['../classnimble_1_1math_1_1_quaternion.html',1,'nimble::math']]],
  ['quaternion_3c_20t_2c_204_20_3e',['Quaternion&lt; T, 4 &gt;',['../classnimble_1_1math_1_1_quaternion.html',1,'nimble::math']]],
  ['quaternion_3c_20t_2c_20r_20_3e',['Quaternion&lt; T, R &gt;',['../classnimble_1_1math_1_1_matrix.html#a3204cb14cecba1c1a8b93e04099487bb',1,'nimble::math::Matrix']]],
  ['queryinterface',['queryInterface',['../classnimble_1_1core_1_1_unknown.html#a0c3686b7d5798982cb5dbcdc3177ad2d',1,'nimble::core::Unknown::queryInterface(T **ppInterface, core::Bool includeBasePointer=true)'],['../classnimble_1_1core_1_1_unknown.html#a96417e0992c0db2c31f40ce8c82e80a5',1,'nimble::core::Unknown::queryInterface(const T **ppInterface, core::Bool includeBasePointer=true) const ']]]
];
