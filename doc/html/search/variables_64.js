var searchData=
[
  ['data',['data',['../unionnimble_1_1graphics_1_1color_r5_g6_b5__t.html#aebda48d722ae086e9b76765231b43954',1,'nimble::graphics::colorR5G6B5_t::data()'],['../unionnimble_1_1graphics_1_1color_r8_g8_b8__t.html#a9a3388c51c217ecdb63c1e8d43f6322a',1,'nimble::graphics::colorR8G8B8_t::data()'],['../unionnimble_1_1graphics_1_1color_r8_g8_b8_a8__t.html#abb4f8835cb7ca1baea2a6dbfc7184fe5',1,'nimble::graphics::colorR8G8B8A8_t::data()']]],
  ['datatype',['dataType',['../structnimble_1_1graphics_1_1_param_block_1_1param_entry__t.html#ae1057f6511cb927975802148be1bc7e3',1,'nimble::graphics::ParamBlock::paramEntry_t']]],
  ['dimension',['dimension',['../structnimble_1_1graphics_1_1_i_texture_1_1descriptor__t.html#a4676e902dff8e12b32b84e359d725a8a',1,'nimble::graphics::ITexture::descriptor_t::dimension()'],['../structnimble_1_1graphics_1_1_texture_1_1descriptor__t.html#aa4123a9a69d874c6401b2b3a9f30d5e7',1,'nimble::graphics::Texture::descriptor_t::dimension()']]],
  ['dimensionvalues',['dimensionValues',['../structnimble_1_1graphics_1_1_i_texture_1_1descriptor__t.html#a848e690ca76af3fbe5c2ccb036454fd8',1,'nimble::graphics::ITexture::descriptor_t::dimensionValues()'],['../structnimble_1_1graphics_1_1_texture_1_1descriptor__t.html#a9dacb86560121c91745cb2bf8de4751f',1,'nimble::graphics::Texture::descriptor_t::dimensionValues()']]]
];
