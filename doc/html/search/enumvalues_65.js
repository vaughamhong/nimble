var searchData=
[
  ['error_5fduplicateadd',['ERROR_DUPLICATEADD',['../classnimble_1_1core_1_1_message_dispatcher.html#aec4cef566491b556ba5305120b0127dda8efa382da5d53b53467fbff879a59405',1,'nimble::core::MessageDispatcher']]],
  ['error_5femptyqueue',['ERROR_EMPTYQUEUE',['../classnimble_1_1core_1_1_message_queue.html#ac21617167e8d5a6cb7a82f1beb8cec48acf37de5c171a5a56274655ef1fc27226',1,'nimble::core::MessageQueue']]],
  ['error_5ffailure',['ERROR_FAILURE',['../classnimble_1_1core_1_1_unknown.html#a317e086fa9cc78e7b88eb8d61fe4c132a5a301831ac300d8c2b514b1bde7410ff',1,'nimble::core::Unknown']]],
  ['error_5fnosuchinterface',['ERROR_NOSUCHINTERFACE',['../classnimble_1_1core_1_1_unknown.html#a317e086fa9cc78e7b88eb8d61fe4c132abb2d0eef45c1f4054c6df8ea1c706592',1,'nimble::core::Unknown']]],
  ['error_5freceivernotfound',['ERROR_RECEIVERNOTFOUND',['../classnimble_1_1core_1_1_message_dispatcher.html#aec4cef566491b556ba5305120b0127dda805fabf753a6b56770beff4a39e15b54',1,'nimble::core::MessageDispatcher']]],
  ['error_5fsuccess',['ERROR_SUCCESS',['../classnimble_1_1core_1_1_message_dispatcher.html#aec4cef566491b556ba5305120b0127ddae295093a600b6174969796cc4a3b14ea',1,'nimble::core::MessageDispatcher::ERROR_SUCCESS()'],['../classnimble_1_1core_1_1_message_queue.html#ac21617167e8d5a6cb7a82f1beb8cec48ad961e4534002dd1ebec3c9902bd01e49',1,'nimble::core::MessageQueue::ERROR_SUCCESS()']]]
];
