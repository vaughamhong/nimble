var searchData=
[
  ['fambient',['fAmbient',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_material.html#af597a31a7e9ca55ec66c539cefc1bc47',1,'nimble::model::Milkshape3dModel::ms3dMaterial']]],
  ['fanimationfps',['fAnimationFPS',['../classnimble_1_1model_1_1_milkshape3d_model.html#ad03914849075961d47fff0f3ecd5a908',1,'nimble::model::Milkshape3dModel']]],
  ['fau',['faU',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_triangle.html#a308ee01d420b1565129c7d2563a765a9',1,'nimble::model::Milkshape3dModel::ms3dTriangle']]],
  ['fav',['faV',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_triangle.html#a254874369dd9123fe1097714504426a7',1,'nimble::model::Milkshape3dModel::ms3dTriangle']]],
  ['fcurrenttime',['fCurrentTime',['../classnimble_1_1model_1_1_milkshape3d_model.html#a995e4728c20c179bfc2eedcbc8cea217',1,'nimble::model::Milkshape3dModel']]],
  ['fdiffuse',['fDiffuse',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_material.html#a95e6e06c2315f898be9b490bab3e8926',1,'nimble::model::Milkshape3dModel::ms3dMaterial']]],
  ['femissive',['fEmissive',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_material.html#a40a41589d888f6f8840fda4e968750c1',1,'nimble::model::Milkshape3dModel::ms3dMaterial']]],
  ['format',['format',['../structnimble_1_1graphics_1_1_i_texture_1_1descriptor__t.html#a749c638febca1778216752ba3bf38f64',1,'nimble::graphics::ITexture::descriptor_t::format()'],['../structnimble_1_1graphics_1_1_texture_1_1descriptor__t.html#ac3765e6390e336a4efba9cb83e126268',1,'nimble::graphics::Texture::descriptor_t::format()']]],
  ['fposition',['fPosition',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_key_frame_rotation.html#aa73d1bae7234c539149a07ab064326bd',1,'nimble::model::Milkshape3dModel::ms3dKeyFrameRotation::fPosition()'],['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_key_frame_position.html#ae4de653ba9501f38949821f04a774c81',1,'nimble::model::Milkshape3dModel::ms3dKeyFramePosition::fPosition()'],['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_joint.html#a4ff153cdecc227adcf59a53a697c4608',1,'nimble::model::Milkshape3dModel::ms3dJoint::fPosition()']]],
  ['frequency',['frequency',['../structnimble_1_1audio_1_1_audio_buffer_1_1descriptor__t.html#a3c14170284a1de1b8ea5254f319f3954',1,'nimble::audio::AudioBuffer::descriptor_t']]],
  ['frotation',['fRotation',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_joint.html#a2c08e43bf578b62bce101431b19cc5a9',1,'nimble::model::Milkshape3dModel::ms3dJoint']]],
  ['fshininess',['fShininess',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_material.html#a5ce64745d56fabc9edc3ef601cf12e15',1,'nimble::model::Milkshape3dModel::ms3dMaterial']]],
  ['fspecular',['fSpecular',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_material.html#a61c2e5da357c4f62a92908d324794444',1,'nimble::model::Milkshape3dModel::ms3dMaterial']]],
  ['ftime',['fTime',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_key_frame_rotation.html#aaf10a18332b6437c6b80c4b5c5e56775',1,'nimble::model::Milkshape3dModel::ms3dKeyFrameRotation::fTime()'],['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_key_frame_position.html#a290d6a30f82aeb5456127362f6de665e',1,'nimble::model::Milkshape3dModel::ms3dKeyFramePosition::fTime()']]],
  ['ftransparency',['fTransparency',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_material.html#a346b7ba56243324bdeb3d9a92db3cea6',1,'nimble::model::Milkshape3dModel::ms3dMaterial']]]
];
