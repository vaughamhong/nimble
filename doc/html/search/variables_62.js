var searchData=
[
  ['b',['b',['../unionnimble_1_1graphics_1_1color_r5_g6_b5__t.html#ae19c5324e47f9f854cdce36df5a0defc',1,'nimble::graphics::colorR5G6B5_t::b()'],['../unionnimble_1_1graphics_1_1color_r8_g8_b8__t.html#af9662845f612865ad90ff020a37604ae',1,'nimble::graphics::colorR8G8B8_t::b()'],['../unionnimble_1_1graphics_1_1color_r8_g8_b8_a8__t.html#a27c590f686162fc3b550c6ca5002dcf4',1,'nimble::graphics::colorR8G8B8A8_t::b()']]],
  ['bitrate',['bitrate',['../structnimble_1_1audio_1_1_audio_buffer_1_1descriptor__t.html#af4f93ba47f7614f065c2b40ff843e53b',1,'nimble::audio::AudioBuffer::descriptor_t']]],
  ['byflags',['byFlags',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_group.html#a235697fee264e6e0568833031d9431f6',1,'nimble::model::Milkshape3dModel::ms3dGroup::byFlags()'],['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_joint.html#a3e9c9cdc3cb8d991079790a8b6e2f76a',1,'nimble::model::Milkshape3dModel::ms3dJoint::byFlags()']]],
  ['bygroupindex',['byGroupIndex',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_triangle.html#a1c53ad7afcdbc389e411a3db7c90a571',1,'nimble::model::Milkshape3dModel::ms3dTriangle']]],
  ['bysmoothinggroup',['bySmoothingGroup',['../structnimble_1_1model_1_1_milkshape3d_model_1_1ms3d_triangle.html#a5dd914fb79dc3a0bb59dc16363a97ab5',1,'nimble::model::Milkshape3dModel::ms3dTriangle']]]
];
