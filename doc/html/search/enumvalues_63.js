var searchData=
[
  ['cap_5fkmaxlights',['CAP_kMaxLights',['../classnimble_1_1graphics_1_1_i_render_caps.html#ae2567ad5772f707527236d3657979b6caaf1287ff8f835dbb67a60f305109de60',1,'nimble::graphics::IRenderCaps']]],
  ['cap_5fmax_5fmodelstackdepth',['CAP_MAX_MODELSTACKDEPTH',['../classnimble_1_1graphics_1_1_i_render_caps.html#ae2567ad5772f707527236d3657979b6ca52733d5d283f989788d9b27568611ac1',1,'nimble::graphics::IRenderCaps']]],
  ['cap_5fmax_5fprojectionstackdepth',['CAP_MAX_PROJECTIONSTACKDEPTH',['../classnimble_1_1graphics_1_1_i_render_caps.html#ae2567ad5772f707527236d3657979b6ca6413d8dc48749cbd03504faf99f2f54e',1,'nimble::graphics::IRenderCaps']]],
  ['cap_5fmax_5ftexturesunits',['CAP_MAX_TEXTURESUNITS',['../classnimble_1_1graphics_1_1_i_render_caps.html#ae2567ad5772f707527236d3657979b6ca575396e130f0f042ff20e3ebdf39894f',1,'nimble::graphics::IRenderCaps']]],
  ['cap_5fmax_5fviewstackdepth',['CAP_MAX_VIEWSTACKDEPTH',['../classnimble_1_1graphics_1_1_i_render_caps.html#ae2567ad5772f707527236d3657979b6ca4dbd45fc4265befbc54562bbf123c34e',1,'nimble::graphics::IRenderCaps']]],
  ['class_5finvalid',['CLASS_INVALID',['../uidevice_09detect_8h.html#a133e92597739340bac439d1b0916dcb6a1ca620102bfa61e74c0c7a7e845d1e2c',1,'uidevice+detect.h']]]
];
