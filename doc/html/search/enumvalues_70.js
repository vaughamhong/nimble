var searchData=
[
  ['primitivetype_5flinelist',['PRIMITIVETYPE_LINELIST',['../classnimble_1_1graphics_1_1_index_buffer.html#a66a4eeed23d5960d7fb39134003167f7aa7fa3f7b60f8643e56d0bb3748f5e694',1,'nimble::graphics::IndexBuffer']]],
  ['primitivetype_5fpointlist',['PRIMITIVETYPE_POINTLIST',['../classnimble_1_1graphics_1_1_index_buffer.html#a66a4eeed23d5960d7fb39134003167f7ac28c3c423e4194597d697fe6f0deff1e',1,'nimble::graphics::IndexBuffer']]],
  ['primitivetype_5fquadlist',['PRIMITIVETYPE_QUADLIST',['../classnimble_1_1graphics_1_1_index_buffer.html#a66a4eeed23d5960d7fb39134003167f7a227fdc583a6bb94120748d203c16cbbc',1,'nimble::graphics::IndexBuffer']]],
  ['primitivetype_5ftrianglefan',['PRIMITIVETYPE_TRIANGLEFAN',['../classnimble_1_1graphics_1_1_index_buffer.html#a66a4eeed23d5960d7fb39134003167f7a95386936e63102d49bc676ad996c31fe',1,'nimble::graphics::IndexBuffer']]],
  ['primitivetype_5ftrianglelist',['PRIMITIVETYPE_TRIANGLELIST',['../classnimble_1_1graphics_1_1_index_buffer.html#a66a4eeed23d5960d7fb39134003167f7a8a165dbfb1bf556847a5bd087a877651',1,'nimble::graphics::IndexBuffer']]],
  ['primitivetype_5ftrianglestrip',['PRIMITIVETYPE_TRIANGLESTRIP',['../classnimble_1_1graphics_1_1_index_buffer.html#a66a4eeed23d5960d7fb39134003167f7ab3810e411521148728d03a97f31c0bd1',1,'nimble::graphics::IndexBuffer']]],
  ['projectiontype_5forthographic',['PROJECTIONTYPE_ORTHOGRAPHIC',['../classnimble_1_1graphics_1_1_camera.html#a0568476a7b73f60a7b183db0dbf15cd8aad9373950dfc820be2c1e0b67826f40f',1,'nimble::graphics::Camera']]],
  ['projectiontype_5fperspective',['PROJECTIONTYPE_PERSPECTIVE',['../classnimble_1_1graphics_1_1_camera.html#a0568476a7b73f60a7b183db0dbf15cd8a22d289e57eb01a558fd6228f9bfa5368',1,'nimble::graphics::Camera']]]
];
