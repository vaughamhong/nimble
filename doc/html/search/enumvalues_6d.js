var searchData=
[
  ['max_5fanalog_5fsticks',['MAX_ANALOG_STICKS',['../classnimble_1_1input_1_1_joystick_device.html#a2a934c38dfe6060ebf879a491d5e57caab1bf3cad974e2372c6400f739ab70809',1,'nimble::input::JoystickDevice']]],
  ['max_5faxis',['MAX_AXIS',['../classnimble_1_1input_1_1_joystick_device.html#a743d88c28c02d2589bbf9c6b04deb998a2e7ddd15de6033e6f998559bb642d763',1,'nimble::input::JoystickDevice::MAX_AXIS()'],['../classnimble_1_1input_1_1_mouse_device.html#ad59cdae26f286c66415be0e060655c87a97fe2d49087525eb38c7f8628b6a3c89',1,'nimble::input::MouseDevice::MAX_AXIS()']]],
  ['max_5fbuttons',['MAX_BUTTONS',['../classnimble_1_1input_1_1_joystick_device.html#a772aa1680b29d85142784e81fd419a4fa1b9ca5a874f158ed540d351960d70396',1,'nimble::input::JoystickDevice::MAX_BUTTONS()'],['../classnimble_1_1input_1_1_mouse_device.html#ab9497421f7d2259d3694c76511dd2d3ea2483f1b5eabd8822132fff6a1c421921',1,'nimble::input::MouseDevice::MAX_BUTTONS()']]],
  ['max_5fclasses',['MAX_CLASSES',['../uidevice_09detect_8h.html#a133e92597739340bac439d1b0916dcb6ae6eec8cf902778e07a61028d000d39c6',1,'uidevice+detect.h']]],
  ['max_5fdevicetypes',['MAX_DEVICETYPES',['../uidevice_09detect_8h.html#a4ec2e35c9447ccc1764ba80c1490a114a411795ea900c146adc4875dbb5cd32b0',1,'uidevice+detect.h']]],
  ['max_5fdirectional_5fpads',['MAX_DIRECTIONAL_PADS',['../classnimble_1_1input_1_1_joystick_device.html#a50087854b591ed759fbfbe6a41ba15f0ab45c938e4dfad9652ccf2aa6dc8cd595',1,'nimble::input::JoystickDevice']]],
  ['max_5fkeys',['MAX_KEYS',['../classnimble_1_1input_1_1_keyboard_device.html#a3be4d08dad142a3730f5acc1f6beae9fa66238bd4e2b9bebf0523b7ad5071273c',1,'nimble::input::KeyboardDevice']]],
  ['max_5fnumregions',['MAX_NUMREGIONS',['../classnimble_1_1graphics_1_1_texture_atlas.html#af32a424f83857825cf939b1cc1d929d8aa18cb83b37600b91d920bdbd0b6d2884',1,'nimble::graphics::TextureAtlas']]],
  ['max_5fregionnamelength',['MAX_REGIONNAMELENGTH',['../classnimble_1_1graphics_1_1_texture_atlas.html#af32a424f83857825cf939b1cc1d929d8ac955a5f09dc28b2973eb90becd0dde21',1,'nimble::graphics::TextureAtlas']]],
  ['max_5ftouchevents',['MAX_TOUCHEVENTS',['../classnimble_1_1input_1_1_touch_device.html#a4413645af9f409a9bd46578935fbbe12a6a38dfe7f928dcd4f2aecca5fbd47fce',1,'nimble::input::TouchDevice']]]
];
