ROOT=`pwd`
PROJ_DIR=$ROOT/projects/tests
BUILD_DIR=$ROOT/builds/tests
LIB_DIR=$ROOT/builds/tests/libs
BIN_DIR=$ROOT/builds/tests/bins
rm -rf $PROJ_DIR
mkdir -p $PROJ_DIR
mkdir -p $LIB_DIR
mkdir -p $BIN_DIR
pushd $PROJ_DIR
cmake \
-DCMAKE_BUILD_TYPE=Debug \
-DCMAKE_ARCHIVE_OUTPUT_DIRECTORY=$LIB_DIR \
-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=$LIB_DIR \
-DCMAKE_RUNTIME_OUTPUT_DIRECTORY=$BIN_DIR \
-DNIMBLE_TARGET_OSX=ON \
-DNIMBLE_GENPROJ_TESTS=ON \
-DNIMBLE_ENABLE_COVERAGEFLAGS=ON \
-DNIMBLE_DISABLE_ASSERTS=ON \
-G "Unix Makefiles" $ROOT
make -j `sysctl -n hw.logicalcpu_max`
popd
./builds/tests/bins/nimble-runtests