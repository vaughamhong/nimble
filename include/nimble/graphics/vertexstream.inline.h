//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

//////////////////////////////////////////////////////////////////////////

#include <nimble/graphics/vertexstream.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{

        //////////////////////////////////////////////////////////////////////////

        //! Constructor
        template <typename Allocator>
        VertexStream<Allocator>::VertexStream(AllocatorType *pAllocator)
        :m_buffer(pAllocator)
        ,m_numVertices(0){
        }
        //! Constructor
        template <typename Allocator>
        VertexStream<Allocator>::VertexStream(renderdevice::VertexFormat const &vertexFormat, AllocatorType *pAllocator)
        :m_vertexFormat(vertexFormat)
        ,m_buffer(pAllocator)
        ,m_numVertices(0){
        }
        //! Constructor
        template <typename Allocator>
        template <typename OtherAllocator>
        VertexStream<Allocator>::VertexStream(graphics::VertexStream<OtherAllocator> const &array, AllocatorType *pAllocator)
        :m_buffer(pAllocator){
            copy(array);
        }
        //! Destructor
        template <typename Allocator>
        VertexStream<Allocator>::~VertexStream(){
        }
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Assignment operator
        template <typename Allocator>
        template <typename OtherAllocator>
        void VertexStream<Allocator>::operator=(graphics::VertexStream<OtherAllocator> const &array){
            copy(array);
        }

        //////////////////////////////////////////////////////////////////////////

        //! returns the vertex format
        template <typename Allocator>
        const renderdevice::VertexFormat& VertexStream<Allocator>::getVertexFormat() const{
            return m_vertexFormat;
        }
        //! returns the vertex stride
        template <typename Allocator>
        size_t VertexStream<Allocator>::getVertexStride() const{
            return m_vertexFormat.getVertexStride();
        }
        //! returns the number of vertices
        //! \return the number of vertices
        template <typename Allocator>
        size_t VertexStream<Allocator>::getNumVertices() const{
            return m_numVertices;
        }
        //! returns the array pointer
        template <typename Allocator>
        char* VertexStream<Allocator>::getArrayPointer(size_t index) const{
            if(index < m_numVertices){
                size_t offset = (index * getVertexStride());
                return &m_buffer[(int32_t)offset];
            }else{
                return 0;
            }
        }

        //////////////////////////////////////////////////////////////////////////
        
        //! Copies into a range
        template <typename Allocator>
        void VertexStream<Allocator>::copy(size_t offset, char const *pVertices, size_t count){
            NIMBLE_ASSERT((offset + count) * getVertexStride() <= m_buffer.getSize());
            size_t byteOffset = (offset * getVertexStride());
            memcpy(&m_buffer[(int32_t)byteOffset], pVertices, getVertexStride() * count);
        }
        //! Resize
        template <typename Allocator>
        void VertexStream<Allocator>::resize(size_t count){
            core::check_print(count >= m_numVertices, __LINE__, __FILE__, "Losing data on vertex array resize");
            m_buffer.resize(count * getVertexStride());
            m_numVertices = count;
        }
        //! Clears vertices
        template <typename Allocator>
        void VertexStream<Allocator>::clear(){
            m_buffer.resize(0);
            m_numVertices = 0;
        }
        
        //////////////////////////////////////////////////////////////////////////

        //! copies this vertex array
        template <typename Allocator>
        template <typename OtherAllocator>
        void VertexStream<Allocator>::copy(graphics::VertexStream<OtherAllocator> const &array){
            m_buffer = array.m_buffer;
            m_vertexFormat = array.m_vertexFormat;
            m_numVertices = array.m_numVertices;
        }
            
        //////////////////////////////////////////////////////////////////////////
    };
};

//////////////////////////////////////////////////////////////////////////