//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

//////////////////////////////////////////////////////////////////////////

#include <nimble/graphics/mesh.h>
#include <nimble/core/assert.h>
#include <nimble/renderdevice/iindexbuffer.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Constructor
        template <typename Allocator>
        Mesh<Allocator>::Mesh(eMeshUsage usage, renderdevice::ePrimitiveType primitiveType, renderdevice::eIndexType indexType, int32_t numIndices, int32_t numVertexStreams, renderdevice::VertexFormat *pVertexFormats, int32_t numVertices)
        :m_usage(usage)
        ,m_primitiveType(primitiveType)
        ,m_indexArray(indexType)
        ,m_numVertexStreams(0){
            // make sure the number of requested vertex streams is valid
            NIMBLE_ASSERT(numVertexStreams <= kMaxVertexStreams);
            m_numVertexStreams = numVertexStreams;
            
            // initialize index array
            m_indexArray.resize(numIndices);
            
            // initialize vertex streams
            for(int32_t i = 0; i < numVertexStreams; i++){
                m_vertexStreams[i] = graphics::VertexStream<>(pVertexFormats[i]);
                m_vertexStreams[i].resize(numVertices);
            }
        }
        //! Constructor
        template <typename Allocator>
        template <typename OtherAllocator>
        Mesh<Allocator>::Mesh(graphics::Mesh<OtherAllocator> const &mesh)
        :m_usage(mesh.m_usage)
        ,m_primitiveType(mesh.m_primitiveType)
        ,m_indexArray(mesh.m_indexArray)
        ,m_numVertexStreams(mesh.m_numVertexStreams){
            for(int32_t i = 0; i < m_numVertexStreams; i++){
                m_vertexStreams[i] = mesh.m_vertexStreams[i];
            }
        }
        //! Destructor
        template <typename Allocator>
        Mesh<Allocator>::~Mesh(){
        }
        
        //////////////////////////////////////////////////////////////////////////

        //! Assignment operator
        template <typename Allocator>
        template <typename OtherAllocator>
        void Mesh<Allocator>::operator=(graphics::Mesh<OtherAllocator> const &mesh){
            m_usage = mesh.m_usage;
            m_primitiveType = mesh.m_primitiveType;
            m_indexArray = mesh.m_indexArray;
            m_numVertexStreams = mesh.m_numVertexStreams;
            for(int32_t i = 0; i < m_numVertexStreams; i++){
                m_vertexStreams[i] = mesh.m_vertexStreams[i];
            }
        }
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Returns the mesh usage
        template <typename Allocator>
        graphics::eMeshUsage Mesh<Allocator>::getUsage() const{
            return m_usage;
        }
        //! Returns primitive type
        template <typename Allocator>
        renderdevice::ePrimitiveType Mesh<Allocator>::getPrimitiveType() const{
            return m_primitiveType;
        }
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Returns the index array
        template <typename Allocator>
        graphics::IIndexArray& Mesh<Allocator>::getIndexArray(){
            return m_indexArray;
        }
        //! Returns the index array
        template <typename Allocator>
        graphics::IIndexArray const& Mesh<Allocator>::getIndexArray() const{
            return m_indexArray;
        }
        
        //! Returns the number of vertex streams
        template <typename Allocator>
        size_t Mesh<Allocator>::getNumVertexStreams() const{
            return m_numVertexStreams;
        }
        //! Returns the vertex stream
        template <typename Allocator>
        graphics::IVertexStream& Mesh<Allocator>::getVertexStream(int32_t streamIndex){
            NIMBLE_ASSERT(streamIndex < m_numVertexStreams);
            return m_vertexStreams[streamIndex];
        }
        //! Returns the vertex stream
        template <typename Allocator>
        graphics::IVertexStream const& Mesh<Allocator>::getVertexStream(int32_t streamIndex) const{
            NIMBLE_ASSERT(streamIndex < m_numVertexStreams);
            return m_vertexStreams[streamIndex];
        }
        //! Resize all vertex streams
        template <typename Allocator>
        void Mesh<Allocator>::resizeVertexStreams(size_t numVertices){
            for(int32_t i = 0; i < m_numVertexStreams; i++){
                m_vertexStreams[i].resize(numVertices);
            }
        }
    };
};

//////////////////////////////////////////////////////////////////////////