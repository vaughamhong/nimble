//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

//////////////////////////////////////////////////////////////////////////

#include <nimble/graphics/indexarray.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        
        //////////////////////////////////////////////////////////////////////////

        //! Constructor
        template <typename Allocator>
        IndexArray<Allocator>::IndexArray(renderdevice::eIndexType indexType, AllocatorType *pAllocator)
        :m_indexType(indexType)
        ,m_buffer(pAllocator)
        ,m_numIndices(0){
        }
        //! Constructor
        template <typename Allocator>
        template <typename OtherAllocator>
        IndexArray<Allocator>::IndexArray(graphics::IndexArray<OtherAllocator> const &array, AllocatorType *pAllocator)
        :m_buffer(pAllocator){
            copy(array);
        }
        //! Destructor
        template <typename Allocator>
        IndexArray<Allocator>::~IndexArray(){
        }
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Assignment operator
        template <typename Allocator>
        template <typename OtherAllocator>
        void IndexArray<Allocator>::operator=(graphics::IndexArray<OtherAllocator> const &array){
            copy(array);
        }
        
        //////////////////////////////////////////////////////////////////////////

        //! returns the index type
        template <typename Allocator>
        renderdevice::eIndexType IndexArray<Allocator>::getIndexType() const{
            return m_indexType;
        }
        //! returns the index stride
        template <typename Allocator>
        size_t IndexArray<Allocator>::getIndexSize() const{
            return renderdevice::getIndexTypeSize(m_indexType);
        }
        //! returns the number of indices
        //! \return the number of indices
        template <typename Allocator>
        size_t IndexArray<Allocator>::getNumIndices() const{
            return m_numIndices;
        }
        //! returns the array pointer
        template <typename Allocator>
        char* IndexArray<Allocator>::getArrayPointer(size_t index) const{
            if(index < m_numIndices){
                size_t offset = (index * getIndexSize());
                return &m_buffer[(int32_t)offset];
            }else{
                return 0;
            }
        }
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Copies into a range
        template <typename Allocator>
        void IndexArray<Allocator>::copy(size_t offset, char const *pIndices, size_t count){
            NIMBLE_ASSERT((offset + count) * getIndexSize() <= m_buffer.getSize());
            size_t byteOffset = (offset * getIndexSize());
            memcpy(&m_buffer[(int32_t)byteOffset], pIndices, getIndexSize() * count);
        }
        //! Resize
        template <typename Allocator>
        void IndexArray<Allocator>::resize(size_t count){
            core::check_print(count >= m_numIndices, __LINE__, __FILE__, "Losing data on index array resize");
            m_buffer.resize(count * getIndexSize());
            m_numIndices = count;
        }
        //! Clears vertices
        template <typename Allocator>
        void IndexArray<Allocator>::clear(){
            m_buffer.resize(0);
            m_numIndices = 0;
        }

        //////////////////////////////////////////////////////////////////////////

        //! copies this vertex array
        template <typename Allocator>
        template <typename OtherAllocator>
        void IndexArray<Allocator>::copy(graphics::IndexArray<OtherAllocator> const &array){
            m_buffer = array.m_buffer;
            m_indexType = array.m_indexType;
            m_numIndices = array.m_numIndices;
        }

        //////////////////////////////////////////////////////////////////////////
        
    };
};

//////////////////////////////////////////////////////////////////////////