//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/graphics/light.h>
#include <nimble/core/typemetadata.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Constructor
        template <typename T>
        CustomLight<T>::CustomLight(){
        }
        //! Destructor
        template <typename T>
        CustomLight<T>::~CustomLight(){
        }
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Returns true if parameter exists
        template <typename T>
        bool CustomLight<T>::exists(const char *name) const{
            // make sure we have meta data
            core::TypeMetaData *pType = this->getTypeMetaData();
            if(pType == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material TypeMetaData");
                return false;
            }
            
            // make sure the field is valid
            core::FieldMetaData *pField = pType->findField(name);
            return (pField != 0);
        }
        //! Gets a parameter
        template <typename T>
        void* CustomLight<T>::getParamPointer(const char *name){
            // make sure we have meta data
            core::TypeMetaData *pType = this->getTypeMetaData();
            if(pType == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material TypeMetaData");
                return 0;
            }
            
            // make sure the field is valid
            core::FieldMetaData *pField = pType->findField(name);
            if(pField == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material field named %s", name);
                return 0;
            }
            
            // return our parameter
            size_t offset = pField->getOffset();
            char *pData = (char*)dynamic_cast<T*>(this);
            return &pData[offset];
        }
        //! Gets a parameter
        template <typename T>
        void* CustomLight<T>::getParamPointer(const char *name) const{
            // make sure we have meta data
            core::TypeMetaData *pType = this->getTypeMetaData();
            if(pType == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material TypeMetaData");
                return 0;
            }
            
            // make sure the field is valid
            core::FieldMetaData *pField = pType->findField(name);
            if(pField == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material field named %s", name);
                return 0;
            }
            
            // return our parameter
            size_t offset = pField->getOffset();
            char *pData = (char*)dynamic_cast<T const*>(this);
            return &pData[offset];
        }
        //! Sets a parameter
        template <typename T>
        void CustomLight<T>::setParamPointer(const char *name, void* pParam){
            // make sure we have meta data
            core::TypeMetaData *pType = this->getTypeMetaData();
            if(pType == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material TypeMetaData");
                return;
            }
            
            // make sure the field is valid
            core::FieldMetaData *pField = pType->findField(name);
            if(pField == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material field named %s", name);
                return;
            }
            
            // set our parameter
            size_t offset = pField->getOffset();
            char *pData = (char*)dynamic_cast<T*>(this);
            memcpy(&pData[offset], (char*)pParam, sizeof(T));
        }
        //! Gets a parameter size
        template <typename T>
        size_t CustomLight<T>::getParamSize(const char *name) const{
            // make sure we have meta data
            core::TypeMetaData *pType = this->getTypeMetaData();
            if(pType == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material TypeMetaData");
                return 0;
            }
            
            // make sure the field is valid
            core::FieldMetaData *pField = pType->findField(name);
            if(pField == 0){
                NIMBLE_LOG_ERROR("material", "Invalid material field named %s", name);
                return 0;
            }
            
            // return our parameter
            return pField->getSize();
        }
        
        //////////////////////////////////////////////////////////////////////////
        
        //! Returns the type for this object
        //! \return the type for this object
        template <typename T>
        core::TypeMetaData* CustomLight<T>::getTypeMetaData() const{
            return graphics::getLightTypeMetaData<T>();
        }
        
        //////////////////////////////////////////////////////////////////////////
        
    };
};

//////////////////////////////////////////////////////////////////////////