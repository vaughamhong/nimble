//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_viewport_h__
#define __nimble_graphics_viewport_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/math/vector.h>
#include <nimble/math/vector.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace graphics{
        
        //! Viewport definition
        class Viewport{
        private:
            
            math::Vector2i m_position;
            math::Vector2i m_size;
            
        public:

            //! constructor
            Viewport();
            //! constructor
            Viewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height);
            //! destructor
            virtual ~Viewport();
            //! sets this camera's members to be equal with another camera
			virtual void operator=(Viewport const& viewport);
            
        public:
            
            //! sets the viewport information
			virtual void setViewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height);

        public:
            
			//! gets the camera viewport position
			virtual uint32_t getX() const;
			//! gets the camera viewport position
			virtual uint32_t getY() const;
            
			//! gets the camera viewport width
			virtual uint32_t getWidth() const;
			//! gets the camera viewport height
			virtual uint32_t getHeight() const;
        };
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////