//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_imaterial_h__
#define __nimble_graphics_imaterial_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <cstdlib>
#include <nimble/core/logger.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace graphics{
        
        //! Abstract material definition
		class IMaterial{
		public:
            
            //! Constructor
            IMaterial(){}
			//! Destructor
			virtual ~IMaterial(){}
            
        public:
            
            //! Returns the material hash
            virtual int64_t getHashIndex() const = 0;

        public:
            
            //! Returns the number of parameters
            virtual size_t getNumParams() const = 0;
            //! Returns the parameter name by index
            virtual const char* getParamNameByIndex(size_t index) const = 0;
            //! Returns the parameter name by index
            virtual void* getParamDataByIndex(size_t index) const = 0;
            
            //! Returns true if parameter exists
            virtual bool existsParamWithName(const char *name) const = 0;

        public:
            
            //! Gets a parameter
            template <typename T>
            T* getParam(const char *name){
                if(this->existsParamWithName(name)){
                    void *pParam = this->getParamPointer(name);
                    size_t size = this->getParamSize(name);
                    
                    if(pParam == 0){
                        NIMBLE_LOG_WARNING("material",
                                   "Failed to find material parameter named \"%s\"",
                                   name);
                        return 0;
                    }
                    if(size != sizeof(T)){
                        NIMBLE_LOG_WARNING("material",
                                   "Failed to get material param pointer - parameter \"%s\" expected size (%d) differs from internal size (%d)",
                                   name,
                                   sizeof(T),
                                   size);
                        return 0;
                    }
                    return static_cast<T*>(pParam);
                }else{
                    NIMBLE_LOG_WARNING("material",
                               "Failed to find material parameter named \"%s\"",
                               name);
                    return 0;
                }
            }
            //! Gets a parameter
            template <typename T>
            T const* getParam(const char *name) const{
                if(this->existsParamWithName(name)){
                    void *pParam = this->getParamPointer(name);
                    size_t size = this->getParamSize(name);
                    
                    if(pParam == 0){
                        NIMBLE_LOG_WARNING("material",
                                   "Failed to find material parameter named \"%s\"",
                                   name);
                        return 0;
                    }
                    if(size != sizeof(T)){
                        NIMBLE_LOG_WARNING("material",
                                   "Failed to get material param pointer - parameter \"%s\" expected size (%d) differs from internal size (%d)",
                                   name,
                                   sizeof(T),
                                   size);
                        return 0;
                    }
                    return static_cast<T const*>(pParam);
                }else{
                    NIMBLE_LOG_WARNING("material",
                               "Failed to find material parameter named \"%s\"",
                               name);
                    return 0;
                }
            }
            //! Sets a parameter
            template <typename T>
            void setParam(const char *name, T const &pValue){
                if(this->existsParamWithName(name)){
                    void *pParam = this->getParamPointer(name);
                    size_t size = this->getParamSize(name);
                    
                    if(pParam == 0){
                        NIMBLE_LOG_WARNING("material",
                                   "Failed to set material parameter named \"%s\" - not found",
                                   name);
                        return;
                    }
                    if(size != sizeof(T)){
                        NIMBLE_LOG_WARNING("material",
                                   "Failed to set material param pointer - parameter \"%s\" expected size (%d) differs from internal size (%d)",
                                   name,
                                   sizeof(T),
                                   size);
                        return;
                    }
                    *static_cast<T*>(pParam) = pValue;
                }else{
                    NIMBLE_LOG_WARNING("material",
                               "Failed to set material parameter named \"%s\" - not found",
                               name);
                }
            }
            
        protected:
            
            //! Gets a parameter
            virtual void* getParamPointer(const char *name) = 0;
            //! Gets a parameter
            virtual void* getParamPointer(const char *name) const = 0;
            //! Sets a parameter
            virtual void setParamPointer(const char *name, void* pParam) = 0;
            //! Gets a parameter size
            virtual size_t getParamSize(const char *name) const = 0;
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////