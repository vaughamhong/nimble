//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_renderable_h__
#define __nimble_graphics_renderable_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/graphics/irenderable.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        
        //! A renderable contains all information required to render a mesh
        class Renderable
        : public graphics::IRenderable{
        private:
            
            graphics::IMesh     *m_pMesh;
            graphics::IMesh     *m_pTransformedMesh;

            graphics::IMaterial *m_pMaterial;

        public:
            
            //! Constructor
            Renderable();
            //! Destructor
            virtual ~Renderable();
            
        public:
            
            //! Sets our mesh
            virtual void setMesh(graphics::IMesh *pMesh);
            //! Returns the mesh
            virtual graphics::IMesh* getMesh() const;
            
            //! Sets our mesh
            virtual void setTransformedMesh(graphics::IMesh *pMesh);
            //! Returns the transformed mesh
            virtual graphics::IMesh* getTransformedMesh() const;

            //! Sets our material
            virtual void setMaterial(graphics::IMaterial *pMaterial);
            //! Returns the material
            virtual graphics::IMaterial* getMaterial() const;
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////