//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_irenderable_h__
#define __nimble_graphics_irenderable_h__

//////////////////////////////////////////////////////////////////////////

#include <vector>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        class IMesh;
        class IMaterial;
        
        //! Abstract renderable
        class IRenderable{
        public:
            
            //! Constructor
            IRenderable(){}
            //! Destructor
            virtual ~IRenderable(){}
            
        public:
            
            //! Sets our mesh
            virtual void setMesh(graphics::IMesh *pMesh) = 0;
            //! Returns the mesh
            virtual graphics::IMesh* getMesh() const = 0;
            
            //! Sets our mesh
            virtual void setTransformedMesh(graphics::IMesh *pMesh) = 0;
            //! Returns the transformed mesh
            virtual graphics::IMesh* getTransformedMesh() const = 0;
            
            //! Sets our material
            virtual void setMaterial(graphics::IMaterial *pMaterial) = 0;
            //! Returns the material
            virtual graphics::IMaterial* getMaterial() const = 0;
        };
        
        //! A list of renderables
        typedef std::vector<graphics::IRenderable*> RenderableList;
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////