//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_imesh_h__
#define __nimble_graphics_imesh_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/renderdevice/iindexbuffer.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace graphics{
        class IIndexArray;
        class IVertexStream;
        
        //! Mesh usage
        enum eMeshUsage{
            kMeshUsageNull,         //!< unknown usage
            kMeshUsageStatic,       //!< this mesh is static
            kMeshUsageDynamic,      //!< this mesh is dynamic
        };

        //! Abstract mesh
		class IMesh{
		public:
            
			//! Constructor
			IMesh(){}
			//! Destructor
			virtual ~IMesh(){}
            
        public:
            
            //! Returns the mesh usage
            virtual graphics::eMeshUsage getUsage() const = 0;
            //! Returns primitive type
            virtual renderdevice::ePrimitiveType getPrimitiveType() const = 0;
                        
        public:
            
            //! Returns the index array
            virtual graphics::IIndexArray& getIndexArray() = 0;
            //! Returns the index array
            virtual graphics::IIndexArray const& getIndexArray() const = 0;

            //! Returns the number of vertex streams
            virtual size_t getNumVertexStreams() const = 0;
            //! Returns the vertex stream
            virtual graphics::IVertexStream& getVertexStream(int32_t streamIndex) = 0;
            //! Returns the vertex stream
            virtual graphics::IVertexStream const& getVertexStream(int32_t streamIndex) const = 0;
            //! Resize all vertex streams
            virtual void resizeVertexStreams(size_t numVertices) = 0;
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////