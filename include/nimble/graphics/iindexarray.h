//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_iindexarray_h__
#define __nimble_graphics_iindexarray_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/core/buffer.h>
#include <nimble/renderdevice/iindexbuffer.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace graphics{
    
    //! Manages an array of indices
    class IIndexArray{
    public:
        
        //! Constructor
        IIndexArray(){}
        //! Destructor
        virtual ~IIndexArray(){}
                
    public:
        
        //! returns the index type
        virtual renderdevice::eIndexType getIndexType() const = 0;
        //! returns the index stride
        virtual size_t getIndexSize() const = 0;
        //! returns the number of indices
        //! \return the number of indices
        virtual size_t getNumIndices() const = 0;
        //! returns the array pointer
        virtual char* getArrayPointer(size_t index = 0) const = 0;
        
    public:
        
        //! copies indices
        virtual void copy(size_t offset, char const *pIndices, size_t count) = 0;
        //! Resize
        virtual void resize(size_t count) = 0;
        //! Clears indices
        virtual void clear() = 0;
    };
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////