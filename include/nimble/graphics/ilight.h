//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_ilight_h__
#define __nimble_graphics_ilight_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <cstdlib>
#include <nimble/core/logger.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace graphics{

        //! Abstract light
		class ILight{
		public:
            
			//! Constructor
			ILight(){}
			//! Destructor
			virtual ~ILight(){}
            
        public:
            
            //! Gets a parameter
            template <typename T>
            T* getParam(const char *name){
                if(this->exists(name)){
                    void *pParam = this->getParamPointer(name);
                    size_t size = this->getParamSize(name);
                    
                    if(pParam == 0){
                        NIMBLE_LOG_WARNING("light",
                                   "Failed to find light parameter named \"%s\"",
                                   name);
                        return 0;
                    }
                    if(size != sizeof(T)){
                        NIMBLE_LOG_WARNING("light",
                                   "Failed to get light param pointer - parameter \"%s\" expected size (%d) differs from internal size (%d)",
                                   name,
                                   sizeof(T),
                                   size);
                        return 0;
                    }
                    return static_cast<T*>(pParam);
                }else{
                    NIMBLE_LOG_WARNING("light",
                               "Failed to find light parameter named \"%s\"",
                               name);
                    return 0;
                }
            }
            //! Gets a parameter
            template <typename T>
            T const* getParam(const char *name) const{
                if(this->exists(name)){
                    void *pParam = this->getParamPointer(name);
                    size_t size = this->getParamSize(name);
                    
                    if(pParam == 0){
                        NIMBLE_LOG_WARNING("light",
                                   "Failed to find light parameter named \"%s\"",
                                   name);
                        return 0;
                    }
                    if(size != sizeof(T)){
                        NIMBLE_LOG_WARNING("light",
                                   "Failed to get light param pointer - parameter \"%s\" expected size (%d) differs from internal size (%d)",
                                   name,
                                   sizeof(T),
                                   size);
                        return 0;
                    }
                    return static_cast<T const*>(pParam);
                }else{
                    NIMBLE_LOG_WARNING("light",
                               "Failed to find light parameter named \"%s\"",
                               name);
                    return 0;
                }
            }
            //! Sets a parameter
            template <typename T>
            void setParam(const char *name, T const &pValue){
                if(this->exists(name)){
                    void *pParam = this->getParamPointer(name);
                    size_t size = this->getParamSize(name);
                    
                    if(pParam == 0){
                        NIMBLE_LOG_WARNING("light",
                                   "Failed to set light parameter named \"%s\" - not found",
                                   name);
                        return;
                    }
                    if(size != sizeof(T)){
                        NIMBLE_LOG_WARNING("light",
                                   "Failed to set light param pointer - parameter \"%s\" expected size (%d) differs from internal size (%d)",
                                   name,
                                   sizeof(T),
                                   size);
                        return;
                    }
                    *static_cast<T*>(pParam) = pValue;
                }else{
                    NIMBLE_LOG_WARNING("light",
                               "Failed to set light parameter named \"%s\" - not found",
                               name);
                }
            }
            //! Returns true if parameter exists
            virtual bool exists(const char *name) const = 0;
            
        protected:
            
            //! Gets a parameter
            virtual void* getParamPointer(const char *name) = 0;
            //! Gets a parameter
            virtual void* getParamPointer(const char *name) const = 0;
            //! Sets a parameter
            virtual void setParamPointer(const char *name, void* pParam) = 0;
            //! Gets a parameter size
            virtual size_t getParamSize(const char *name) const = 0;
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////