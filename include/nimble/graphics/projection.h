//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_projection_h__
#define __nimble_graphics_projection_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/math/matrix.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace graphics{
    
    //! Calculates a perspective projection matrix
    //! \param aspect the aspect ratio of our view
    //! \param fov the field of view
    //! \param near the near clipping plane
    //! \param far the far clipping plane
    //! \return a perspective projection matrix
    math::Matrix4x4f calculatePerspectiveProjMatrix(size_t width, size_t height, float fov, float near, float far);
    
    //! Calculates a orthographic projection matrix
    //! \param width the width of our view
    //! \param height the width of our view
    //! \param fov the field of view
    //! \param near the near clipping plane
    //! \param far the far clipping plane
    //! \return a perspective projection matrix
    math::Matrix4x4f calculateOrthographicProjMatrix(size_t width, size_t height, float fov, float near, float far);
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////