//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_vertexstream_h__
#define __nimble_graphics_vertexstream_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/core/buffer.h>
#include <nimble/graphics/ivertexstream.h>
#include <nimble/renderdevice/vertexformat.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace graphics{
    
    //! Manages a stream of vertex data
    template <typename Allocator = core::Allocator>
    class VertexStream
    :public graphics::IVertexStream{
    public:
        
        typedef Allocator AllocatorType;
        
    private:
        
        renderdevice::VertexFormat  m_vertexFormat;
        core::Buffer<AllocatorType> m_buffer;
        size_t                      m_numVertices;
        
    public:
        
        //! Constructor
        VertexStream(AllocatorType *pAllocator = 0);
        //! Constructor
        VertexStream(renderdevice::VertexFormat const &vertexFormat, AllocatorType *pAllocator = 0);
        //! Constructor
        template <typename OtherAllocator>
        VertexStream(graphics::VertexStream<OtherAllocator> const &array, AllocatorType *pAllocator = 0);
        //! Destructor
        virtual ~VertexStream();
        
    public:
        
        //! Assignment operator
        template <typename OtherAllocator>
        void operator=(graphics::VertexStream<OtherAllocator> const &array);
        
    public:
        
        //! returns the vertex format
        virtual const renderdevice::VertexFormat& getVertexFormat() const;
        //! returns the vertex stride
        virtual size_t getVertexStride() const;
        //! returns the number of vertices
        //! \return the number of vertices
        virtual size_t getNumVertices() const;
        //! returns the array pointer
        virtual char* getArrayPointer(size_t index = 0) const;
        
    public:
        
        //! Copies into a range
        virtual void copy(size_t offset, char const *pVertices, size_t count);
        //! Resize
        virtual void resize(size_t count);
        //! Clears vertices
        virtual void clear();
    
    private:
        
        //! copies this vertex array
        template <typename OtherAllocator>
        void copy(graphics::VertexStream<OtherAllocator> const &array);
    };
};};

//////////////////////////////////////////////////////////////////////////

#include "vertexstream.inline.h"

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////