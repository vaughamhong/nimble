//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_mesh_h__
#define __nimble_graphics_mesh_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/graphics/imesh.h>
#include <nimble/graphics/vertexstream.h>
#include <nimble/graphics/indexarray.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        
        //! The number of maximum mesh vertex streams
        static const int32_t kMaxVertexStreams = 8;
        
        //! A mesh manages an index buffer and a set of vertex streams
        template <typename Allocator = core::Allocator>
        class Mesh
        : public graphics::IMesh{
        private:
            
            //! how this mesh will be used
            eMeshUsage                          m_usage;
            
            //! the type of index primitives this mesh defines
            renderdevice::ePrimitiveType        m_primitiveType;
            
            //! the array of indices that define this mesh
            graphics::IndexArray<Allocator>     m_indexArray;
            
            //! the vertex streams that define this mesh
            graphics::VertexStream<Allocator>   m_vertexStreams[kMaxVertexStreams];
            int32_t                             m_numVertexStreams;
            
        public:
            
            //! Constructor
            Mesh(eMeshUsage usage, renderdevice::ePrimitiveType primitiveType, renderdevice::eIndexType indexType, int32_t numIndices, int32_t numVertexStreams, renderdevice::VertexFormat *pVertexFormats, int32_t numVertices);
            //! Constructor
            template <typename OtherAllocator>
            Mesh(graphics::Mesh<OtherAllocator> const &mesh);
            //! Destructor
            virtual ~Mesh();
            
        public:
            
            //! Assignment operator
            template <typename OtherAllocator>
            void operator=(graphics::Mesh<OtherAllocator> const &mesh);
            
        public:
            
            //! Returns the mesh usage
            graphics::eMeshUsage getUsage() const;
            //! Returns primitive type
            virtual renderdevice::ePrimitiveType getPrimitiveType() const;
            
        public:
            
            //! Returns the index array
            virtual graphics::IIndexArray& getIndexArray();
            //! Returns the index array
            virtual graphics::IIndexArray const& getIndexArray() const;
            
            //! Returns the number of vertex streams
            virtual size_t getNumVertexStreams() const;
            //! Returns the vertex stream
            virtual graphics::IVertexStream& getVertexStream(int32_t streamIndex);
            //! Returns the vertex stream
            virtual graphics::IVertexStream const& getVertexStream(int32_t streamIndex) const;
            //! Resize all vertex streams
            virtual void resizeVertexStreams(size_t numVertices);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#include "mesh.inline.h"

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////