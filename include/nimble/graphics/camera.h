//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_camera_h__
#define __nimble_graphics_camera_h__

///////////////////////////////////////////////////////////////////////////////////////////////

#include <nimble/math/vector.h>
#include <nimble/math/quaternion.h>

///////////////////////////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
  
        //! projection types
        enum eProjectionType{
            kProjectionOrthographic,
            kProjectionPerspective,
            kMaxProjectionTypes
        };

        //! Contains all information required to describe a camera
        class Camera{
        private:
            
            nimble::math::Vector3f      m_position;
            nimble::math::Quaternion4f  m_orientation;

            float                       m_fieldOfView, m_nearPlane, m_farPlane;
            eProjectionType             m_projectionType;
            
        public:
            
            //! Constructor
            Camera();
            //! Destructor
            virtual ~Camera();
            
        public:
            
            //! sets the camera position
			virtual void setPosition(math::Vector3f const &position);
            //! sets our orientation
            virtual void setOrientation(math::Quaternion4f const &orientation);
            //! sets our orientation
            virtual void setOrientation(math::Vector3f const &forward);
            
            //! gets the camera position
            virtual math::Vector3f& getPosition();
            //! gets the camera orientation
            virtual math::Quaternion4f& getOrientation();
            //! gets the camera position
            virtual math::Vector3f getPosition() const;
            //! gets the camera orientation
            virtual math::Quaternion4f getOrientation() const;
            
        public:
            
            //! sets the projection type
			virtual void setProjectionType(graphics::eProjectionType projectionType);
            //! sets the camera field of view
			virtual void setFieldOfView(float fieldOfView);
            //! sets the camera near plane
			virtual void setNearPlane(float nearPlane);
            //! sets the camera far plane
			virtual void setFarPlane(float farPlane);
            
            //! returns the projection type
            virtual graphics::eProjectionType getProjectionType() const;
			//! gets the camera field of view
			virtual float getFieldOfView() const;
			//! gets the camera near plane
			virtual float getNearPlane() const;
			//! gets the camera far plane
			virtual float getFarPlane() const;
        };
    };
};

///////////////////////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////////////////////