//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_materiallist_h__
#define __nimble_graphics_materiallist_h__

//////////////////////////////////////////////////////////////////////////

#include <vector>
#include <cstdlib>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        class IMaterial;
        
        //! Contains a list of materials
        class MaterialList{
        private:
            
            typedef std::vector<graphics::IMaterial*> ListType;
            ListType m_list;
            
        public:
            
            //! Constructor
            MaterialList();
            //! Destructor
            virtual ~MaterialList();
            
        public:
            
            //! Add a material
            size_t addMaterial(IMaterial *pMaterial);
            //! Removes a material
            void removeMaterial(size_t index);
            //! Remove all materiales
            void removeAllMaterials();
            //! Returns a material by index
            IMaterial* getMaterial(size_t index);
            //! Returns the number of materiales
            size_t getNumMaterials() const;
            
        private:
            
            //! Check if material already exists
            bool exists(IMaterial *pMaterial) const;
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////