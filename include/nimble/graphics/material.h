//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_material_h__
#define __nimble_graphics_material_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/graphics/imaterial.h>
#include <nimble/math/vector.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class TypeMetaData;
    };
    namespace renderdevice{
        class ITexture;
    };
    namespace graphics{
        
        //! Custom material manages the material format and maps names to
        //! pointers into our material structure from its metadata
        template <typename T>
        class MaterialWrapper
        : public graphics::IMaterial
        , public T{
        public:
            
            //! Constructor
            MaterialWrapper();
            //! Destructor
            virtual ~MaterialWrapper();
            
        public:

            //! Returns the material hash
            virtual int64_t getHashIndex() const;

        public:
            
            //! Returns the number of parameters
            virtual size_t getNumParams() const;
            //! Returns the parameter name by index
            virtual const char* getParamNameByIndex(size_t index) const;
            //! Returns the parameter name by index
            virtual void* getParamDataByIndex(size_t index) const;

            //! Returns true if parameter exists
            virtual bool existsParamWithName(const char *name) const;

        protected:
            
            //! Gets a parameter
            virtual void* getParamPointer(const char *name);
            //! Gets a parameter
            virtual void* getParamPointer(const char *name) const;
            //! Sets a parameter
            virtual void setParamPointer(const char *name, void* pParam);
            //! Gets a parameter size
            virtual size_t getParamSize(const char *name) const;
            
        protected:
                    
            //! Returns the type for this object
            //! \return the type for this object
            core::TypeMetaData* getTypeMetaData() const;
        };
        
        //! \return the material type metadata
        template <typename T>
        inline core::TypeMetaData* getMaterialTypeMetaData(){
            return T::getTypeMetaData();
        }
    };
};

//////////////////////////////////////////////////////////////////////////

#include "material.inline.h"

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////