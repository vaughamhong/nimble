//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_meshlist_h__
#define __nimble_graphics_meshlist_h__

//////////////////////////////////////////////////////////////////////////

#include <vector>
#include <cstdlib>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        class IMesh;
                
        //! Contains a list of meshes
        class MeshList{
        private:
            
            typedef std::vector<graphics::IMesh*> ListType;
            ListType m_list;
            
        public:
            
            //! Constructor
            MeshList();
            //! Destructor
            virtual ~MeshList();
            
        public:
            
            //! Add a mesh
            size_t addMesh(IMesh *pMesh);
            //! Removes a mesh
            void removeMesh(size_t index);
            //! Remove all meshes
            void removeAllMeshes();
            //! Returns a mesh by index
            IMesh* getMesh(size_t index);
            //! Returns the number of meshes
            size_t getNumMeshes() const;
            
        private:
            
            //! Check if mesh already exists
            bool exists(IMesh *pMesh) const;
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////