//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_ivertexarray_h__
#define __nimble_graphics_ivertexarray_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/renderdevice/vertexformat.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace graphics{
    
    //! Abstract vertex array
    class IVertexArray{
    public:
        
        //! Constructor
        IVertexArray(){}
        //! Destructor
        virtual ~IVertexArray(){}
        
    public:
                
        //! returns the vertex format
        virtual const renderdevice::VertexFormat& getVertexFormat() const = 0;
        //! returns the vertex stride
        virtual size_t getVertexStride() const = 0;
        //! returns the number of vertices
        //! \return the number of vertices
        virtual size_t getNumVertices() const = 0;
        //! returns the array pointer
        virtual char* getArrayPointer(size_t index = 0) const = 0;
        //! Resize
        virtual void resize(size_t count) = 0;
    };
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////