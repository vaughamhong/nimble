//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_indexarray_h__
#define __nimble_graphics_indexarray_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <cstdlib>
#include <nimble/core/buffer.h>
#include <nimble/graphics/iindexarray.h>
#include <nimble/renderdevice/iindexbuffer.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace graphics{
    
    //! Manages an array of indices
    template <typename Allocator = core::Allocator>
    class IndexArray
    : public graphics::IIndexArray{
    public:
        
        typedef Allocator AllocatorType;
        
    private:
        
        renderdevice::eIndexType        m_indexType;
        core::Buffer<AllocatorType>     m_buffer;
        size_t                          m_numIndices;
        
    public:
        
        //! Constructor
        IndexArray(renderdevice::eIndexType indexType, AllocatorType *pAllocator = 0);
        //! Constructor
        template <typename OtherAllocator>
        IndexArray(graphics::IndexArray<OtherAllocator> const &array, AllocatorType *pAllocator = 0);
        //! Destructor
        virtual ~IndexArray();
        
    public:
        
        //! Assignment operator
        template <typename OtherAllocator>
        void operator=(graphics::IndexArray<OtherAllocator> const &array);
        
    public:
        
        //! returns the index type
        virtual renderdevice::eIndexType getIndexType() const;
        //! returns the index stride
        virtual size_t getIndexSize() const;
        //! returns the number of indices
        //! \return the number of indices
        virtual size_t getNumIndices() const;
        //! returns the array pointer
        virtual char* getArrayPointer(size_t index = 0) const;
        
    public:
        
        //! Copies into a range
        virtual void copy(size_t offset, char const *pIndices, size_t count);
        //! Resize
        virtual void resize(size_t count);
        //! Clears indices
        virtual void clear();
    
    private:
        
        //! copies this vertex array
        template <typename OtherAllocator>
        void copy(graphics::IndexArray<OtherAllocator> const &array);
    };
};};

//////////////////////////////////////////////////////////////////////////

#include "indexarray.inline.h"

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////