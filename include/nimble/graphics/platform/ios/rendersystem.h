//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_renderer_ios_rendersystem_h__
#define __nimble_renderer_ios_rendersystem_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/renderer/rendersystem.h>

//////////////////////////////////////////////////////////////////////////

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace renderer{
        namespace ios{

            //! iOS RenderSystem
            class RenderSystem
            : public renderer::RenderSystem{
            private:
                
                EAGLContext*                m_context;
                renderdevice::IFrameBuffer* m_pMainFrameBuffer;
                GLuint                      m_colorBufferHandle;
                
            public:
                
                //! Constructor
                RenderSystem();
                //! Destructor
                virtual ~RenderSystem();
                
            protected:
                
                //! Initialize render data
                virtual void initializeRenderData(core::PropertyList &propertyList);
                //! Destroy render data
                virtual void destroyRenderData();
                
            public:
                
                //! the frame has begun
                virtual void frameHasBegun();
                //! the frame has ended
                virtual void frameHasEnded();
            };
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////