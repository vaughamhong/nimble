//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_graphics_light_h__
#define __nimble_graphics_light_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/typemetadata.h>
#include <nimble/graphics/ilight.h>
#include <nimble/math/vector.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class TypeMetaData;
    };
    namespace graphics{
        
        //! Custom light
        template <typename T>
        class CustomLight
        : public graphics::ILight
        , public T{
        public:
            
            //! Constructor
            CustomLight();
            //! Destructor
            virtual ~CustomLight();
            
        public:
            
            //! Returns true if parameter exists
            virtual bool exists(const char *name) const;
            //! Gets a parameter
            virtual void* getParamPointer(const char *name);
            //! Gets a parameter
            virtual void* getParamPointer(const char *name) const;
            //! Sets a parameter
            virtual void setParamPointer(const char *name, void* pParam);
            //! Gets a parameter size
            virtual size_t getParamSize(const char *name) const;
            
        protected:
            
            //! Returns the type for this object
            //! \return the type for this object
            core::TypeMetaData* getTypeMetaData() const;
        };
        
        //! \return the light type metadata
        template <typename T>
        inline core::TypeMetaData* getLightTypeMetaData(){
            return T::getTypeMetaData();
        }
        
        //! Basic light
        struct light_t{
        public:
            math::Vector3f diffuse;
            math::Vector3f ambient;
            math::Vector3f specular;
            math::Vector3f emissive;
            
            //! Constructor
            light_t();
            //! Destructor
            ~light_t();
            //! Returns the metatype data
            static core::TypeMetaData* getTypeMetaData();
        };
        
        //! Basic light
        class Light
        : public graphics::CustomLight<light_t>{
        public:
            
            //! Constructor
            Light();
            //! Destructor
            virtual ~Light();
            
        public:
            
            //! Returns the diffuse color
            math::Vector3f& diffuseColor();
            //! Returns the ambient color
            math::Vector3f& ambientColor();
            //! Returns the specular color
            math::Vector3f& specularColor();
            //! Returns the emissive color
            math::Vector3f& emissiveColor();
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#include "light.inline.h"

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////