//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_input_messages_h__
#define __nimble_input_messages_h__

#include <nimble/message/message.h>
#include <nimble/input/touch.h>

namespace nimble{
	namespace input{
        
		//! on a simulation update
		class OnTouchUpdate
        : public nimble::message::MessageWrapper<OnTouchUpdate>
        , public nimble::input::TouchDevice::TouchEvent{
        public:
            
			//! Constructor
			OnTouchUpdate(){
            }//! Constructor
			OnTouchUpdate(nimble::input::TouchDevice::TouchEvent const &event){
                m_position = event.getPosition();
                m_position = event.getPosition();
                m_touchEvent = event.getTouchEvent();
            }
		};
	};
};

#endif // __nimble_input_messages_h__
