//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_input_touch_h__
#define __nimble_input_touch_h__

#include <nimble/math/vector.h>
#include <vector>

namespace nimble{
	namespace input{
        
		//! touch device
		class TouchDevice{
		public:
                        
            //! touch event
            enum eTouchEvent{
                kTouchEventBegin,
                kTouchEventEnd,
                kTouchEventMove,
                MAX_TOUCHEVENTS
            };
            
			//! event
			class TouchEvent{
			protected:
                
                math::Vector2f m_position;   //!< position
                eTouchEvent m_touchEvent;   //!< touch event
				
			public:
                
				//! Constructor
				TouchEvent()
                :m_touchEvent(kTouchEventBegin){
					m_position[0] = 0.0f; 
                    m_position[1] = 0.0f;
				}
                TouchEvent(int x, int y, eTouchEvent touchEvent)
                :m_touchEvent(touchEvent){
					m_position[0] = (float)x; 
                    m_position[1] = (float)y;
				}
				
				//! returns the position
				math::Vector2f getPosition() const{return m_position;}
				//! returns the position
				math::Vector2f& getPosition(){return m_position;}
                
				//! returns the touch event
                eTouchEvent getTouchEvent() const{return m_touchEvent;}
			};
            
			//! event handler
			class EventHandler{
			public:
				//! invoked when touch move
				virtual void handleTouchMove(const TouchEvent& arg) = 0;
                //! invoked when touch move
				virtual void handleTouchBegin(const TouchEvent& arg) = 0;
                //! invoked when touch move
				virtual void handleTouchEnd(const TouchEvent& arg) = 0;
			};
            
			typedef std::vector<EventHandler*>  ObserverList;
			typedef ObserverList::iterator      ObserverIterator;
            
		private:
            
			ObserverList m_observersList;
            
		public:
            
			//! Constructor
			TouchDevice(){}
			//! Destructor
			virtual ~TouchDevice(){}
            
		public:
            
			//! subscribe
			virtual void subscribe(EventHandler* pEventHandler);
			//! unsubscribe
			virtual void unsubscribe(EventHandler* pEventHandler);
        
        public:
            
            //! Update device
            virtual void update();
            //! dispatch input into the device
            virtual void injectInput(input::TouchDevice::eTouchEvent event, int x, int y);
            
		protected:
            
			//! returns the first iterator in observer list
			ObserverIterator observersListBegin(){return m_observersList.begin();}
			//! returns the last iterator in observer list
			ObserverIterator observersListEnd(){return m_observersList.end();}
		};
	};
};

#endif // __nimble_input_touch_h__
