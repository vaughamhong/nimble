//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_input_mouse_h__
#define __nimble_input_mouse_h__

#include <vector>

namespace nimble{
	namespace input{

		//! mouse device
		class MouseDevice{
		public:

			//! mouse buttons
			enum eButtonCode{
				BUTTON_1 = 1,
				BUTTON_2 = 2,
				BUTTON_3 = 3,
				BUTTON_4 = 4,

				MAX_BUTTONS = 4
			};

			//! axis
			enum eAxisCode{
				AXIS_X = 1,
				AXIS_Y = 2,
				AXIS_Z = 3,

				MAX_AXIS = 3
			};

			//! event
			class MouseEvent{
			private:

				int m_position[3];		//!< position
				int m_difference[3];	//!< difference
				int m_numButtons;		//!< the number of buttons
				int m_buttonStates;		//!< button states

			public:

				//! Constructor
				MouseEvent(int x, int y, int z, int dx, int dy, int dz, int numButtons, int buttonStates)
					:m_numButtons(numButtons), m_buttonStates(buttonStates){
					m_position[0] = x; m_position[1] = y; m_position[2] = z;
					m_difference[0] = dx; m_difference[1] = dy; m_difference[2] = dz;
				}
				
				//! position
				int getPosition(eAxisCode axisCode) const{return m_position[((int)axisCode) - 1];}
				//! difference
				int getDifference(eAxisCode axisCode) const{return m_difference[((int)axisCode) - 1];}
				//! returns the button state
				bool isButtonPressed(eButtonCode buttonCode) const{return 0 != (m_buttonStates & (1 << ((int)buttonCode - 1)));}
				//! returns the button state
				bool isButtonReleased(eButtonCode buttonCode) const{return 0 == (m_buttonStates & (1 << ((int)buttonCode - 1)));}
				//! returns the number of buttons
				int getNumButtons() const{return m_numButtons;}
			};

			//! event handler
			class EventHandler{
			public:

				//! invoked when mouse is moved
				virtual void handleMouseMoved(const MouseEvent& arg) = 0;
				//! invoked when button is pressed
				virtual void handleMouseButtonPressed(const MouseEvent& arg) = 0;
				//! invoked when button is released
				virtual void handleMouseButtonReleased(const MouseEvent& arg) = 0;
			};

			typedef std::vector<EventHandler*>  ObserverList;
			typedef ObserverList::iterator      ObserverIterator;

		private:

			ObserverList m_observersList;
			
		public:

			//! Constructor
			MouseDevice(){}
			//! Destructor
			virtual ~MouseDevice(){}

		public:

			//! Updates the internal state
			virtual void update() = 0;

			//! position
			virtual int getPosition(eAxisCode axisCode) const = 0;
			//! difference
			virtual int getDifference(eAxisCode axisCode) const = 0;

			//! returns the button state
			virtual bool isButtonPressed(eButtonCode buttonCode) const = 0;
			//! returns the button state
			virtual bool isButtonReleased(eButtonCode buttonCode) const = 0;
			//! returns the number of buttons
			virtual unsigned int getNumButtons() const = 0;

			//! dispatch
			virtual void dispatchEvents() = 0;
			//! subscribe
			virtual void subscribe(EventHandler* pEventHandler);
			//! unsubscribe
			virtual void unsubscribe(EventHandler* pEventHandler);

		protected:

			//! returns the first iterator in observer list
			ObserverIterator observersListBegin(){return m_observersList.begin();}
			//! returns the last iterator in observer list
			ObserverIterator observersListEnd(){return m_observersList.end();}
		};
	};
};

#endif // __nimble_input_mouse_h__
