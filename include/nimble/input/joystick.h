//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_input_joystick_h__
#define __nimble_input_joystick_h__

#include <vector>

namespace nimble{
	namespace input{

		//! joystick device
		class JoystickDevice{
		public:

			//! joystick buttons
			enum eButtonCode{
				BUTTON_1,
				BUTTON_2,
				BUTTON_3,
				BUTTON_4,
				BUTTON_5,
				BUTTON_6,
				BUTTON_7,
				BUTTON_8,
				BUTTON_9,
				BUTTON_10,
				BUTTON_11,
				BUTTON_12,
				BUTTON_13,
				BUTTON_14,
				BUTTON_15,
				BUTTON_16,

				MAX_BUTTONS
			};

			//! axis
			enum eAxisCode{
				AXIS_X,
				AXIS_Y,
				AXIS_Z,

				MAX_AXIS
			};

			//! analog sticks
			enum eAnalogStick{
				ANALOG_STICK_1,
				ANALOG_STICK_2,

				MAX_ANALOG_STICKS
			};

			//! directional pad
			enum eDirectionalPad{
				DIRECTIONAL_PAD_1,

				MAX_DIRECTIONAL_PADS
			};

			//! analog stick
			struct analogStick_t{
				int stick[MAX_AXIS];
			};

			//! directional pad
			struct directionalPad_t{
				char pad[MAX_AXIS];
			};

			//! event
			class JoystickEvent{
			private:

				char m_buttonStates[MAX_BUTTONS];					//!< button states

			public:

				//! Constructor
				JoystickEvent(){
				}
				
				//! returns the button state
				bool isButtonPressed(eButtonCode buttonCode) const{return 0 != m_buttonStates[buttonCode];}
				//! returns the button state
				bool isButtonReleased(eButtonCode buttonCode) const{return 0 == m_buttonStates[buttonCode];}
				//! returns the number of buttons
				int getNumButtons() const{return MAX_BUTTONS;}
			};

			//! event handler
			class EventHandler{
			public:

				//! invoked when button is pressed
				virtual void handleJoystickButtonPressed(const JoystickEvent& arg) = 0;
				//! invoked when button is released
				virtual void handleJoystickButtonReleased(const JoystickEvent& arg) = 0;
			};

			typedef std::vector<EventHandler*>	ObserverList;
			typedef ObserverList::iterator      ObserverIterator;

		private:

			ObserverList m_observersList;
			
		public:

			//! Constructor
			JoystickDevice(){}
			//! Destructor
			virtual ~JoystickDevice(){}

		public:

			//! Updates the internal state
			virtual void update() = 0;

			//! returns the button state
			virtual bool isButtonPressed(eButtonCode buttonCode) const = 0;
			//! returns the button state
			virtual bool isButtonReleased(eButtonCode buttonCode) const = 0;
			//! returns the number of buttons
			virtual unsigned int getNumButtons() const = 0;

			//! dispatch
			virtual void dispatchEvents() = 0;
			//! subscribe
			virtual void subscribe(EventHandler* pEventHandler);
			//! unsubscribe
			virtual void unsubscribe(EventHandler* pEventHandler);

		protected:

			//! returns the first iterator in observer list
			ObserverIterator observersListBegin(){return m_observersList.begin();}
			//! returns the last iterator in observer list
			ObserverIterator observersListEnd(){return m_observersList.end();}
		};
	};
};

#endif // __nimble_input_joystick_h__
