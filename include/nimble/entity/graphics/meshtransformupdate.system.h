//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_meshtransformupdatesystem_h__
#define __nimble_entity_meshtransformupdatesystem_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/system.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace scene{
        class SceneTransform;
    };
    namespace graphics{
        class IRenderable;
        class IMesh;
    };
    namespace entity{
        class RenderableListComponent;
        class Component;
    
        //! Updates mesh skinning
        class MeshTransformUpdateSystem
        : public entity::System{
        private:
            
            //! Contains all information we need to know to transform
            struct transformInfo_t{
                scene::SceneTransform   *pSceneTransform;
                graphics::IMesh         *pOriginalMesh;
                graphics::IMesh         *pTransformedMesh;
            };
            //! A mapping from renderable to transform info
            typedef std::map<graphics::IRenderable*, transformInfo_t> RenderableToTransformInfoIndex;
            
            //! A lock for our renderable to transform info
            core::Mutex m_lock;
            //! Our renderable to transform info index
            RenderableToTransformInfoIndex m_renderableToTransformInfoIndex;
            
        public:
            
            //! Constructor
            MeshTransformUpdateSystem();
            //! Destructor
            virtual ~MeshTransformUpdateSystem();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();

        public:
            
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();
            
            //! Called when component was added to an entity
            virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent);
            //! Called when component was removed from an entity
            virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent);
            
            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval);
            
        private:
            
            //! Called when renderable was added
            virtual void renderableListDidAddRenderable(entity::RenderableListComponent *pRenderableList, graphics::IRenderable *pRenderable);
            //! Called when renderable was removed
            virtual void renderableListWillRemoveRenderable(entity::RenderableListComponent *pRenderableList, graphics::IRenderable *pRenderable);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////