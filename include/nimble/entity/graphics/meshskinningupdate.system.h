//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_meshskinningupdatesystem_h__
#define __nimble_entity_meshskinningupdatesystem_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/system.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace graphics{
        class IRenderable;
    };
    namespace entity{
        class Component;

        //! Updates mesh skinning
        class MeshSkinningUpdateSystem
        : public entity::System{
        private:
            
            typedef std::vector<graphics::IRenderable*> RenderableList;
            
            core::Mutex m_lock;
            RenderableList m_renderableList;
            
        public:
            
            //! Constructor
            MeshSkinningUpdateSystem();
            //! Destructor
            virtual ~MeshSkinningUpdateSystem();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();

        public:
            
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();
            
            //! Called when component was created
            virtual void componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent);
            //! Called when component will be destroyed
            virtual void componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent);

            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval);            
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////