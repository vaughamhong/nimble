//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_renderablelistcomponent_h__
#define __nimble_entity_renderablelistcomponent_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/component.h>
#include <nimble/graphics/irenderable.h>
#include <nimble/core/functor.h>
#include <vector>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace graphics{
        class IRenderable;
    };
    namespace entity{
        class RenderableListComponent;
        
        // note: RenderableList tracks a set of callbacks for when a renderable is added / removed.
        // this is done so systems are able to initialize / cleanup when renderables become active.
        // we do callbacks this way (in favor of generic component did change) because systems may need
        // to know about changes on the sub resource level. A component did change call on a remove all
        // renderables may not have all the information necessary (eg. the removed renderables) to
        // do a cleanup - however, a fine grained renderableListWillRemoveRenderable callback will provide all the
        // information required for cleanup.
        
        //! Callback when renderable was added
        typedef core::Functor<void, TLIST_2(entity::RenderableListComponent*, graphics::IRenderable*)> RenderableListDidAddRenderableCallback;
        //! Callback when renderable was removed
        typedef core::Functor<void, TLIST_2(entity::RenderableListComponent*, graphics::IRenderable*)> RenderableListWillRemoveRenderableCallback;
        
        //! Contains a list of renderables
        class RenderableListComponent
        : public entity::Component{
        private:
            
            typedef std::vector<RenderableListDidAddRenderableCallback> RenderableListDidAddRenderableCallbackList;
            typedef std::vector<RenderableListWillRemoveRenderableCallback> RenderableListWillRemoveRenderableCallbackList;
            
            // callback lists
            RenderableListDidAddRenderableCallbackList m_renderableListDidAddRenderableCallbacks;
            RenderableListWillRemoveRenderableCallbackList m_renderableListWillRemoveRenderableCallbacks;
            
            // the list of renderables
            graphics::RenderableList m_renderables;

        public:
            
            //! Constructor
            RenderableListComponent();
            //! Destructor
            virtual ~RenderableListComponent();
                        
        public:
            
            //! Registers for renderable was added callback
            virtual void registerCallbackOnRenderableListDidAddRenderable(RenderableListDidAddRenderableCallback callback);
            //! Unregisters for renderable was added callback
            virtual void unregisterCallbackOnRenderableListDidAddRenderable(RenderableListDidAddRenderableCallback callback);

            //! Registers for renderable was removed callback
            virtual void registerCallbackOnRenderableListWillRemoveRenderable(RenderableListWillRemoveRenderableCallback callback);
            //! Unregisters for renderable was removed callback
            virtual void unregisterCallbackOnRenderableListWillRemoveRenderable(RenderableListWillRemoveRenderableCallback callback);
            
        public:
            
            //! Add a renderable
            virtual size_t addRenderable(graphics::IRenderable *pRenderable);
            
            //! Removes a renderable
            virtual void removeRenderable(size_t index);
            //! Remove all renderables
            virtual void removeAllRenderables();
            
            //! Returns all renderables
            virtual const graphics::RenderableList& getRenderables() const;
            //! Returns a renderable by index
            virtual graphics::IRenderable* getRenderable(size_t index) const;
            //! Returns the number of renderables
            virtual size_t getNumRenderables() const;
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////