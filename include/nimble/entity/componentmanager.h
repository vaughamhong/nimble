//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_componentmanager_h__
#define __nimble_entity_componentmanager_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/thread.mutex.h>
#include <nimble/core/objectfactory.h>
#include <nimble/core/propertytree.h>
#include <nimble/resource/resourcecache.h>
#include <nimble/entity/component.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace entity{
        class Entity;
        class Component;
        class ComponentManager;

        typedef std::vector<entity::Component*> ComponentList;

        //! Delegate callbacks on entity manager state change
        class ComponentManagerDelegate{
        public:
            //! Called when component was created
            virtual void componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent) = 0;
            //! Called when component will be destroyed
            virtual void componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent) = 0;
        };

		//! Manages the creation, storage and fetching of components
		class ComponentManager{
        public:
            
            typedef std::map<std::string, ComponentList> TypeNameToComponentsIndex;
            
        private:
                        
            core::Mutex                     m_lock;
            
            TypeNameToComponentsIndex       m_typeNameToComponentsIndex;
            
            core::ObjectFactory             m_factory;
            resource::ResourceCache         m_cache;
            
            ComponentManagerDelegate        *m_pDelegate;
            
        public:
            
            //! Gets our component manager
            static ComponentManager* get();
            
            //! Constructor
            ComponentManager();
            //! Destructor
            ~ComponentManager();
            
        public:
            
            //! Updates manager
            virtual void update(double interval);
            
        public:
            
            //! Sets our delegate
            virtual void setDelegate(ComponentManagerDelegate *pDelegate);
            
        public:
            
			//! Registers a component to our factory
            //! \param type the component type
            template <typename T>
            void registerComponent(const char *name){
                core::ScopedLock lock(&m_lock);
				core::ObjectFactory::ConstructorFunc constructor = core::ObjectFactory::defaultConstructor<T>;
				core::ObjectFactory::DestructorFunc destructor = core::ObjectFactory::defaultDestructor<T>;
				registerComponent<T>(name, constructor, destructor);
            }
			//! Registers a component to our factory
            //! \param type the component type
            template <typename T>
            void registerComponent(const char *name, core::ObjectFactory::ConstructorFunc constructor){
                core::ScopedLock lock(&m_lock);
				core::ObjectFactory::DestructorFunc destructor = core::ObjectFactory::defaultDestructor<T>;
				registerComponent<T>(name, constructor, destructor);
            }
			//! Registers a component to our factory
            //! \param type the component type
            template <typename T>
            void registerComponent(const char *name, core::ObjectFactory::ConstructorFunc constructor, core::ObjectFactory::DestructorFunc destructor){
                core::ScopedLock lock(&m_lock);
                m_factory.registerType<T>(name, constructor, destructor);
            }
            //! Unregisters a component from our factory
            void unregisterComponent(const char *name){
                core::ScopedLock lock(&m_lock);
                m_factory.unregisterType(name);
            }
            
        public:
            
            //! Creates and returns a concrete component
            template <typename T>
            T* createComponent(const char *name){
                entity::Component *pComponent = this->createComponent(name);
                if(pComponent){
                    T *pConcreteComponent = dynamic_cast<T*>(pComponent);
                    if(pConcreteComponent){
                        return pConcreteComponent;
                    }else{
                        this->destroyComponent(pComponent);
                    }
                }
                return 0;
            }
            
            //! Creates an component from file
            //! \param name the name of the component to create
            //! \return a pointer to the component
            entity::Component* createComponent(const char *name);
            //! Creates an component from file
            //! \param name the name of the component to create
            //! \param properties the properties to create this component with
            //! \return a pointer to the component
            entity::Component* createComponent(const char *name, core::PropertyTree &properties);
            //! Destroys an component
            //! \param pComponent the component to destroy
            void destroyComponent(entity::Component *pComponent);
            //! Destroys all components
            void destroyAllComponents();
            
            //! Fetches an component by id
            //! \param id the unique id to fetch with
            //! \return a pointer to the component
            entity::Component* getComponentByResourceId(resource::ResourceId resourceId);
            //! Returns true if this manage owns this component
            bool ownsComponent(entity::Component *pComponent);
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////