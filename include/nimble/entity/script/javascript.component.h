//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_javascriptcomponent_h__
#define __nimble_entity_javascriptcomponent_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/component.h>
#include <nimble/script/javascriptenvironment.h>
#include "v8.h"

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace entity{
        
        //! Javascript entity component
        class JavascriptComponent
        : public entity::Component
        , public script::JavascriptEnvironment{
        public:
            
            //! Constructor
            JavascriptComponent(v8::Isolate *pIsolate);
            //! Destructor
            virtual ~JavascriptComponent();
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////