//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_javascriptupdatesystem_h__
#define __nimble_entity_javascriptupdatesystem_h__

//////////////////////////////////////////////////////////////////////////

#include <unordered_map>
#include <nimble/entity/system.h>
#include <nimble/script/javascriptenvironment.h>
#include "v8.h"

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace entity{
        class Component;
    };
    namespace script{
        class JavascriptEnvironment;
    };
    namespace entity{
        
        //! Updates javascript
        class JavascriptUpdateSystem
        : public entity::System
        , public script::JavascriptEnvironmentDelegate{
        private:
            
            // platform abstraction layer
            v8::Platform *m_pPlatform;
            // an isolated instance of the V8 engine
            v8::Isolate *m_pIsolate;
            
        private:
            
            //! Contains all information we need to know about our script environment
            struct environmentInfo_t{
                script::JavascriptEnvironment *pEnvironment;
            };
            //! A mapping from environment to info
            typedef std::unordered_map<script::JavascriptEnvironment*, environmentInfo_t> EnvironmentToInfoIndex;
            
            //! A lock for our environment to info
            core::Mutex m_lock;
            //! Our scene to sene info index
            EnvironmentToInfoIndex m_environmentToInfoIndex;
            
        public:
            
            //! Constructor
            JavascriptUpdateSystem();
            //! Destructor
            virtual ~JavascriptUpdateSystem();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();

        public:
            
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();
            
            //! Called when component was added to an entity
            virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent);
            //! Called when component was removed from an entity
            virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent);

            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval);
            
        public:
            
            //! Builds an javascript component
            core::Object* buildJavascriptComponent();
            
        public:
            
            //! Called when a javascript environment script will load
            virtual void javascriptEnvironmentScriptWillLoad(script::JavascriptEnvironment *pEnvironment);
            //! Called when a javascript environment script failed to load
            virtual void javascriptEnvironmentScriptLoadDidFail(script::JavascriptEnvironment *pEnvironment);
            //! Called when a javascript environment script was loaded
            virtual void javascriptEnvironmentScriptDidLoad(script::JavascriptEnvironment *pEnvironment);
            //! Called when a javascript environment script will unload
            virtual void javascriptEnvironmentScriptWillUnload(script::JavascriptEnvironment *pEnvironment);
            
        protected:
            
            //! Called to setup global template
            virtual void setupGlobalTemplate(script::JavascriptEnvironment *pEnvironment);
            
            //! Returns the platform
            virtual v8::Platform* getPlatform() const;
            //! Returns the isolate
            virtual v8::Isolate* getIsolate() const;
            
        private:
                        
            //! Adds event listener
            static void nativeAddEventListener(const v8::FunctionCallbackInfo<v8::Value>& args);
            //! Removes event listener
            static void nativeRemoveEventListener(const v8::FunctionCallbackInfo<v8::Value>& args);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////