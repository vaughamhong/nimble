//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_entitymanager_h__
#define __nimble_entity_entitymanager_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/thread.mutex.h>
#include <nimble/entity/entity.h>
#include <nimble/resource/resourcecache.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class SearchPaths;
        class PropertyTree;
    };
    namespace resource{
        class ResourceManager;
        class ResourceLoader;
    };
	namespace entity{
        class Entity;
        class EntityManager;
        class Component;
        class ComponentManager;
        
        //! Delegate callbacks on entity manager state change
        class EntityManagerDelegate{
        public:
            //! Called when entity was created
            virtual void entityManagerDidCreateEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity) = 0;
            //! Called when entity was destroyed
            virtual void entityManagerWillDestroyEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity) = 0;
            
            //! Called when component was added to an entity
            virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent) = 0;
            //! Called when component was removed from an entity
            virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent) = 0;
        };
        
		//! Manages the creation, storage and fetching of entities
		class EntityManager
        : public entity::EntityDelegate{
        private:
            
            entity::ComponentManager    *m_pComponentManager;

            core::Mutex                 m_lock;
            resource::ResourceCache     m_cache;
            
            EntityManagerDelegate       *m_pDelegate;
            
		public:
            
            //! Gets our entity manager
            static EntityManager* get();
            
        public:
            
            //! Constructor
            EntityManager(entity::ComponentManager *pComponentManager);
            //! Destructor
            ~EntityManager();
            
        public:
            
            //! Updates manager
            virtual void update(double interval);
            
        public:
            
            //! Sets our delegate
            virtual void setDelegate(EntityManagerDelegate *pDelegate);
            
        public:

            //! Create a entity
            //! \return a pointer to the entity
            entity::Entity* createEntity();
            //! Create a entity with properties
            //! \param property the property to configure with
            //! \return a pointer to the entity
            entity::Entity* createEntityWithProperties(core::PropertyTree &properties);
            //! Creates an entity from file
            //! \param filename the filename to configure from
            //! \return a pointer to the entity
            entity::Entity* createEntityFromFile(const char *filepath, core::SearchPaths *pPaths = 0);
            //! Destroys an entity
            //! \param entity the entity to destroy
            void destroyEntity(entity::Entity* entity);
            //! Destroys all entities
            void destroyAllEntities();
            //! Fetches an entity by id
            //! \param id the unique id to fetch with
            //! \return a pointer to the entity
            entity::Entity* getEntityByResourceId(resource::ResourceId id);
            //! Returns true if this manage owns this entity
            bool ownsEntity(entity::Entity *pEntity);
            
        private:
            
            //! Called when component was added to an entity
            virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent);
            //! Called when component was removed from an entity
            virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent);
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////