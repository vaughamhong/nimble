//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_MediaPlayercomponent_h__
#define __nimble_entity_MediaPlayercomponent_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/component.h>
#include <nimble/mediaplayer/mediaplayer.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace media{
        class MediaPlayer;
    };
    namespace entity{

        //! MediaPlayer entity component
        class MediaPlayerComponent
        : public entity::Component
        , public media::MediaPlayer{
        public:
            
            //! Constructor
            MediaPlayerComponent();
            //! Destructor
            virtual ~MediaPlayerComponent();            
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////