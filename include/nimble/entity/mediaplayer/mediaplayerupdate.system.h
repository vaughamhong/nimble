//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_mediaplayerupdatesystem_h__
#define __nimble_entity_mediaplayerupdatesystem_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/system.h>
#include <unordered_map>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace media{
		class MediaPlayer;
    };
    namespace entity{
        class Component;

        //! Updates world simulations
        class MediaPlayerUpdateSystem
        : public entity::System{
		private:

			//! Contains all information we need to know to update video player
			struct MediaPlayerInfo_t{
				media::MediaPlayer *pMediaPlayer;
			};
			//! A mapping from vide player to info
			typedef std::unordered_map<media::MediaPlayer*, MediaPlayerInfo_t> MediaPlayerToMediaPlayerInfoIndex;

			//! A lock for our scene to video player info
			core::Mutex m_lock;
			//! Our video player to info index
			MediaPlayerToMediaPlayerInfoIndex m_MediaPlayerToMediaPlayerInfoIndex;

        public:
            
            //! Constructor
            MediaPlayerUpdateSystem();
            //! Destructor
            virtual ~MediaPlayerUpdateSystem();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();

        public:
            
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();

			//! Called when component was added to an entity
			virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent);
			//! Called when component was removed from an entity
			virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent);

            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval);
            
        public:
            
            //! Builds an MediaPlayer component
            core::Object* buildMediaPlayerComponent();
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////