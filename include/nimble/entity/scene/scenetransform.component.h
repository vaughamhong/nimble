//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_scenetransformcomponent_h__
#define __nimble_entity_scenetransformcomponent_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/component.h>
#include <nimble/scene/scenetransform.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace core{
        class PropertyTree;
    };
    namespace entity{
        
        //! Holds scene transformation data
		class SceneTransformComponent
        : public entity::Component
        , public scene::SceneTransform{
        public:
            
            //! Constructor
            SceneTransformComponent();
            //! Destructor
            virtual ~SceneTransformComponent();
            
        public:
            
            //! Deserialize from a property tree
            //! Once this call is completed, the component should be ready to use
            virtual void deserialize(core::PropertyTree &properties);
            //! Serailize into a property tree
            virtual void serialize(core::PropertyTree &properties);
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////