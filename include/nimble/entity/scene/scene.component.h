//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_scenecomponent_h__
#define __nimble_entity_scenecomponent_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/component.h>
#include <nimble/scene/scene.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace core{
        class PropertyTree;
    };
    namespace entity{
        
        //! Holds scene transformation data
		class SceneComponent
        : public entity::Component
        , public scene::Scene{
        public:
            
            //! Constructor
            SceneComponent();
            //! Destructor
            virtual ~SceneComponent();
            
        public:
            
            //! Deserialize from a property tree
            //! Once this call is completed, the component should be ready to use
            virtual void deserialize(core::PropertyTree &properties);
            //! Serailize into a property tree
            virtual void serialize(core::PropertyTree &properties);
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////