//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_system_h__
#define __nimble_entity_system_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/message/messagereceiver.h>
#include <nimble/core/propertytree.h>
#include <nimble/resource/resource.h>
#include <stdint.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace entity{
        class Entity;
        class EntityManager;
        class Component;
        class ComponentManager;
        
        typedef int32_t SystemId;

        //! A System that runs updates on a set of entities with the right components
        //! Systems are notified when entities are created / destroyed
        class System
        : public resource::Resource{
        private:
            
			std::string m_name;
            bool m_enabled;
            
        public:
            
            //! Constructor
            System();
            //! Destructor
            virtual ~System();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();

        public:
                        
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();
            
            //! Called when entity was created
            virtual void entityManagerDidCreateEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity);
            //! Called when entity was destroyed
            virtual void entityManagerWillDestroyEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity);
            
            //! Called when component was added to an entity
            virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent);
            //! Called when component was removed from an entity
            virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent);
            
            //! Called when component was created
            virtual void componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent);
            //! Called when component will be destroyed
            virtual void componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent);
            
        public:
            
            //! Returns our systemId
            virtual entity::SystemId getSystemId() const;

            //! Enables the system
            virtual void enable();
            //! Disables the system
            virtual void disable();
            //! Returns true if enabled
            //! \return true if enabled
            virtual bool isEnabled() const;

			//! Sets the name of this system
			virtual void setName(const char *name);
			//! Returns the name of this system
			virtual const char* getName() const;
            
            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval) = 0;
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif