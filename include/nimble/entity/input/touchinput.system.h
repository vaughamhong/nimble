//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_touchinputsystem_h__
#define __nimble_entity_touchinputsystem_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/input/touch.h>
#include <nimble/entity/system.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace entity{
        
        //! Input system
        class TouchInputSystem
        : public entity::System
        , public input::TouchDevice::EventHandler{
        private:
            
            input::TouchDevice *m_pTouchDevice;
            
        public:
            
            //! Constructor
            TouchInputSystem();
            //! Destructor
            virtual ~TouchInputSystem();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();

        public:
            
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();
            
            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval);
                        
        public:
            
            //! inject touch input
            virtual void injectInput(nimble::input::TouchDevice::eTouchEvent event, unsigned int x, unsigned int y);
            //! invoked when touch move
            virtual void handleTouchMove(const nimble::input::TouchDevice::TouchEvent& event);
            //! invoked when touch move
            virtual void handleTouchBegin(const nimble::input::TouchDevice::TouchEvent& event);
            //! invoked when touch move
            virtual void handleTouchEnd(const nimble::input::TouchDevice::TouchEvent& event);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////