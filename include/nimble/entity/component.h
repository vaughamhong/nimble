//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_component_h__
#define __nimble_entity_component_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/propertytree.h>
#include <nimble/resource/resource.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace entity{
        class Entity;
        class Component;
        class System;
        
		//! components are the building blocks for entities
        //! + responsible for a single concept (eg. Renderable, Actionable, Health)
        //! + hold data to be processed by entity systems
        //! + components (types) are registered with the component manager
        //! + components can be created by name by the component manager
        //! + the component manager owns the lifetime of a component
		class Component
        : public resource::Resource{
        friend class entity::Entity;
        private:
            
            entity::Entity *m_pEntity;
            
		public:
            
            //! Constructor
			Component();
			//! Destructor
			virtual ~Component();
            
        public:
            
            //! Deserialize from a property tree
            //! Once this call is completed, the component should be ready to use
            virtual void deserialize(core::PropertyTree &properties);
            //! Serailize into a property tree
            virtual void serialize(core::PropertyTree &properties);
            
        public:

            //! Called when component was added to an entity
            virtual void didAddParentEntity(entity::Entity *pEntity);
            //! Called when component will be removed from entity
            virtual void willRemoveParentEntity(entity::Entity *pEntity);
            
            //! returns our owner
            virtual entity::Entity* getEntity() const;
            
        private:
            
            //! sets our owner
            virtual void setEntity(entity::Entity *pEntity);        
        };
        
        //! Helpe wrapper for a component
        template <typename T>
        class ComponentWrapper
        : public entity::Component
        , public T{
        public:
            
            //! Constructor
            ComponentWrapper(){}
            //! Destructor
            virtual ~ComponentWrapper(){}
        };
	};
};

//////////////////////////////////////////////////////////////////////////


#endif