//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_SystemsManager_h__
#define __nimble_entity_SystemsManager_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/locator.h>
#include <nimble/core/factory.h>
#include <nimble/core/thread.mutex.h>
#include <nimble/core/objectfactory.h>
#include <nimble/core/propertytree.h>
#include <nimble/resource/resourcecache.h>
#include <nimble/entity/entitymanager.h>
#include <nimble/entity/componentmanager.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace entity{
        class System;
        class SystemsManager;
        class EntityManager;
        class ComponentManager;
        
        //! Handles ScenesManager delegate callbacks
        class SystemsManagerDelegate{
        public:
            //! Called when system was added
            virtual void systemWasAdded(entity::SystemsManager *pSystemsManager, entity::System *pSystem){};
            //! Called when system was removed
            virtual void systemWasRemoved(entity::SystemsManager *pSystemsManager, entity::System *pSystem){};
        };
        
		//! Manages the creation, storage and fetching of systems
		class SystemsManager
        : public EntityManagerDelegate
        , public ComponentManagerDelegate{
        public:

            typedef resource::ResourceCache                 SystemsDatabase;
            typedef core::ObjectFactory                     SystemsFactory;
            typedef std::map<std::string, entity::System*>  NameToSystemIndex;
            
        private:
            
            core::Mutex                 m_lock;

            NameToSystemIndex           m_nameToSystem;
            SystemsFactory              m_factory;
            SystemsDatabase             m_database;
            
            SystemsManagerDelegate      *m_pDelegate;
            
            entity::EntityManager       *m_pEntityManager;
            entity::ComponentManager    *m_pComponentManager;
            
		public:
            
            //! Gets our systems manager
            static SystemsManager* get();

        public:

            //! Constructor
            SystemsManager(entity::EntityManager *pEntityManager, entity::ComponentManager *pComponentManager);
            //! Destructor
            ~SystemsManager();
            
        public:
            
            //! Registers a system to our factory
            //! \param builder the system builder
            //! \param type the system type
            template <typename T>
            void registerSystem(const char *name, bool replace = false){
                core::ScopedLock lock(&m_lock);
				core::ObjectFactory::ConstructorFunc constructor = core::ObjectFactory::defaultConstructor<T>;
				core::ObjectFactory::DestructorFunc destructor = core::ObjectFactory::defaultDestructor<T>;
                m_factory.registerType<T>(name, constructor, destructor, replace);
                NIMBLE_LOG_INFO("system", "[Register][name:%s]", name);
            }
			//! Registers a system to our factory
            //! \param builder the system builder
            //! \param type the system type
            template <typename T>
            void registerSystem(const char *name, core::ObjectFactory::ConstructorFunc constructor, core::ObjectFactory::DestructorFunc destructor){
                core::ScopedLock lock(&m_lock);
                m_factory.registerType<T>(name, constructor, destructor);
                NIMBLE_LOG_INFO("system", "[Register][name:%s]", name);
            }
            //! Unregisters a system from our factory
            void unregisterSystem(const char *name){
                core::ScopedLock lock(&m_lock);
                m_factory.unregisterType(name);
                NIMBLE_LOG_INFO("system", "[Unregister][name:%s]", name);
            }
            
        public:
            
            //! Configures systems
            void initialize(core::PropertyTree &properties);
            //! terminate
            void terminate();
            
            //! Sets our delegate
            void setDelegate(SystemsManagerDelegate *pDelegate);

        public:
            
            //! Create a system by class
            //! \param name a unique name to reference the system
            //! \param className the class name of the system
            //! \return a pointer to the entity
            entity::System* createSystemByClass(const char *name, const char *className);
            //! Destroys an system
            //! \param pSystem the system to destroy
            void destroySystem(entity::System *pSystem);
            //! Destroys all systems
            void destroyAllSystems();
            //! Returns the system by name
            //! \param name the name of the system
            //! \return the system by name
            entity::System* getSystem(const char *name);
            
            //! Enables a system by name
            //! \param name the name of the system to enable
            void enableSystem(const char *name);
            //! Disables a system by name
            //! \param name the name of the system to disable
            void disableSystem(const char *name);
            
            //! Updates relative to an interval
            //! \param interval the elapsed time since the last call
            void updateFrameWithInterval(uint64_t frameIndex, double interval);
            
        private:
            
            //! Called when entity was created
            virtual void entityManagerDidCreateEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity);
            //! Called when entity was destroyed
            virtual void entityManagerWillDestroyEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity);
            //! Called when component was added to an entity
            virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent);
            //! Called when component was removed from an entity
            virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent);
            
            //! Called when component was created
            virtual void componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent);
            //! Called when component will be destroyed
            virtual void componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent);
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////