//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_worldsimupdatesystem_h__
#define __nimble_entity_worldsimupdatesystem_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/system.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace physics{
        class WorldSim;
    };
    namespace entity{
        class Component;

        //! Updates world simulations
        class WorldSimUpdateSystem
        : public entity::System{
        public:
            
            static const int32_t kDefaultMaxSubSteps = 5;
            
        private:
            
            typedef std::vector<physics::WorldSim*> WorldSimList;
            
            core::Mutex m_lock;
            WorldSimList m_worldSimList;
            int32_t m_maxSubSteps;
            
        public:
            
            //! Constructor
            WorldSimUpdateSystem();
            //! Destructor
            virtual ~WorldSimUpdateSystem();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();

        public:
            
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();
            
            //! Called when component was created
            virtual void componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent);
            //! Called when component will be destroyed
            virtual void componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent);

            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval);
            
        public:
            
            //! Sets the max sub steps
            virtual void setMaxSubSteps(int32_t steps);
            
        public:
            
            //! Builds an rigid body component
            core::Object* buildRigidBody();
            //! Builds an world simulation component
            core::Object* buildWorldSim();
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////