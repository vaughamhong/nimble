//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_rigidbodycomponent_h__
#define __nimble_entity_rigidbodycomponent_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/component.h>
#include <nimble/physics/rigidbody.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace entity{

        //! RigidBody entity component
        class RigidBodyComponent
        : public entity::Component
		, public physics::rigidBody{
        public:
            
            //! Constructor
            RigidBodyComponent();
            //! Destructor
            virtual ~RigidBodyComponent();
            
        public:
            
            //! Deserialize from a property tree
            //! Once this call is completed, the component should be ready to use
            virtual void deserialize(core::PropertyTree &properties);
            //! Serailize into a property tree
            virtual void serialize(core::PropertyTree &properties);
            
        public:
            
            //! Called when component was added to an entity
            virtual void didAddParentEntity(entity::Entity *pEntity);
            //! Called when component will be removed from entity
            virtual void willRemoveParentEntity(entity::Entity *pEntity);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////