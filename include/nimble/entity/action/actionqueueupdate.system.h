//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_actionqueueupdatesystem_h__
#define __nimble_entity_actionqueueupdatesystem_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/system.h>
#include <unordered_map>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace action{
        class ActionQueue;
    };
    namespace entity{
        
        //! Action queue update system
        class ActionQueueUpdateSystem
        : public entity::System{
        private:
            
            //! Contains all information we need to know to transform
            struct actionQueueInfo_t{
                action::ActionQueue *pActionQueue;
            };
            //! A mapping from scene to scene info
            typedef std::unordered_map<action::ActionQueue*, actionQueueInfo_t> ActionQueueToActionQueueInfoIndex;
            
            //! A lock for our scene to scene info
            core::Mutex m_lock;
            //! Our scene to sene info index
            ActionQueueToActionQueueInfoIndex m_actionQueueToActionQueueInfoIndex;

        public:
            
            //! Constructor
            ActionQueueUpdateSystem();
            //! Destructor
            virtual ~ActionQueueUpdateSystem();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();
            
        public:
            
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();
            
            //! Called when component was added to an entity
            virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent);
            //! Called when component was removed from an entity
            virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent);

            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////