//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_actionqueue_component_h__
#define __nimble_entity_actionqueue_component_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/component.h>
#include <nimble/action/actionqueue.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace entity{
        
		//! a actions component
		class ActionQueueComponent
        : public entity::Component
        , public action::ActionQueue{
		public:
            
            //! Constructor
			ActionQueueComponent();
			//! Destructor
			virtual ~ActionQueueComponent();
            
        public:
            
            //! Enqueue an action
            virtual void enqueue(action::ActionPtr action);
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////