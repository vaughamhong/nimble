//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_webviewupdatesystem_h__
#define __nimble_entity_webviewupdatesystem_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/system.h>
#include <unordered_map>

//////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
#include <Awesomium/WebCore.h>
#endif

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace web{
        class WebView;
    };
    namespace entity{
        class Component;
        
        //! Updates world simulations
        class WebViewUpdateSystem
        : public entity::System{
		private:

#if !defined(NIMBLE_TARGET_ANDROID)
            Awesomium::WebCore *m_pWebCore;
#endif
            
			//! Contains all information we need to know to update web view
			struct webViewInfo_t{
                web::WebView *pWebView;
			};
			//! A mapping from web view to info
			typedef std::unordered_map<web::WebView*, webViewInfo_t> WebViewToWebViewInfoIndex;

			//! A lock for our scene to web view info
			core::Mutex m_lock;
			//! Our web view to info index
			WebViewToWebViewInfoIndex m_webViewToWebViewInfoIndex;

        public:
            
            //! Constructor
            WebViewUpdateSystem();
            //! Destructor
            virtual ~WebViewUpdateSystem();
            
        public:
            
            //! Initialize from a property tree
            //! Once this call is completed, the system should be ready to use
            virtual void initialize(core::PropertyTree &properties);
            //! Terminate this system
            virtual void terminate();

        public:
            
            //! Register the varius components associate with this system
            virtual void registerComponents();
            //! Unregister the varius components associate with this system
            virtual void unregisterComponents();

			//! Called when component was added to an entity
			virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent);
			//! Called when component was removed from an entity
			virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent);

            //! Updates relative to an interval
            virtual void update(uint64_t frameIndex, double interval);
            
        public:
            
            //! Builds an web view component
            core::Object* buildWebViewComponent();
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////