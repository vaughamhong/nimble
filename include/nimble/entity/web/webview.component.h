//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_webviewcomponent_h__
#define __nimble_entity_webviewcomponent_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/component.h>
#include <nimble/web/webview.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace entity{
        class WebView;

        //! WebView entity component
        class WebViewComponent
        : public entity::Component
        , public web::WebView{
        public:
            
#if !defined(NIMBLE_TARGET_ANDROID)
            //! Constructor
            WebViewComponent(Awesomium::WebCore *pWebCore);
#else
            //! Constructor
            WebViewComponent();
#endif
            //! Destructor
            virtual ~WebViewComponent();
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////