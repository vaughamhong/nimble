//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_entity_entity_h__
#define __nimble_entity_entity_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/propertytree.h>
#include <nimble/resource/resource.h>
#include <nimble/entity/component.h>

//////////////////////////////////////////////////////////////////////////

#include <unordered_map>
#include <vector>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace entity{
        class Component;
        class ComponentManager;
        class Entity;
        class EntityManager;
        
        //! Delegate callbacks on entity state change
        class EntityDelegate{
        public:
            //! Called when component was added to an entity
            virtual void entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent) = 0;
            //! Called when component was removed from an entity
            virtual void entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent) = 0;
        };
        
        typedef std::unordered_map<core::TypeId, entity::Component*> TypeToComponentIndex;
        typedef std::vector<entity::Component*> ComponentList;

        //! Entities contains a list of components that describe the capabilities
        //! of the entity. Subsystems react on the type of components present in an entity.
        class Entity
        : public resource::Resource{
        friend class EntityManager;
        private:
            
            TypeToComponentIndex        m_typeToComponentIndex;
            ComponentList               m_components;
            entity::ComponentManager    *m_pComponentManager;
            EntityDelegate              *m_pDelegate;
            
		private:

            //! Constructor
			Entity(entity::ComponentManager *pComponentManager, core::PropertyTree &properties, EntityDelegate *pDelegate = 0);
			//! Destructor
			~Entity();
            
        public:
            
            //! Sets our delegate
            virtual void setDelegate(EntityDelegate *pDelegate);
            
        public:
            
            //! Adds a component
            //! \param pComponent the component to add
            void addComponent(entity::Component *pComponent);
            //! Removes a component
            //! \param type the component with type to remove
            void removeComponent(core::TypeId type);
            //! Removes all components from this entity
            void removeAllComponents();
            //! Gets a component
            //! \param type the component with type to get
            entity::Component* getComponent(core::TypeId type);
            //! Gets a component
            //! \param type the component with type to get
            entity::Component const* getComponent(core::TypeId type) const;
            //! Returns all components
            const ComponentList getComponents() const;
            //! Returns true if the component exists
            //! \param pComponent the component to check for existence
            //! \return true if the component exists
            bool componentExists(entity::Component *pComponent);
            //! Returns true if the component with type exists
            //! \param type the type to check for existence
            //! \return true if the component with type exists
            bool componentWithTypeExists(core::TypeId type);
            //! Gets a component by type
            //! \return the concrete component
            template <typename T> T* getComponent(){
                return dynamic_cast<T*>(getComponent(core::getTypeId<T>()));
            }
            //! Gets a component by type
            //! \return the concrete component
            template <typename T> T const* getComponent() const{
                return dynamic_cast<T const*>(getComponent(core::getTypeId<T>()));
            }
            
            //! Gets a component by type
            //! \return the concrete component
            template <typename T> T* findFirstComponentWithType(){
                for(TypeToComponentIndex::iterator it = m_typeToComponentIndex.begin(); it != m_typeToComponentIndex.end(); it++){
                    T *pConcreteComponent = dynamic_cast<T*>(it->second);
                    if(pConcreteComponent != 0){
                        return pConcreteComponent;
                    }
                }
                return 0;
            }
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////