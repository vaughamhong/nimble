//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_audio_win32_audiobufferloader_h__
#define __nimble_audio_win32_audiobufferloader_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/resource/resourceloader.h>
#include <nimble/core/propertytree.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace audio{
        
		//! Audio buffer loader
        class AudioBufferLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            AudioBufferLoader();
            //! Destructor
            virtual ~AudioBufferLoader();
                        
            //! loads a resource
            //! \param filePath the name of the file we want to load
            //! \param pStreamProvider the data provider to use
            virtual resource::IResource* load(const char *path);
        };
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////