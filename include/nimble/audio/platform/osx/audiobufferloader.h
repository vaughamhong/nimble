//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_audio_osx_audiobufferloader_h__
#define __nimble_audio_osx_audiobufferloader_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/resource/resourceloader.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace audio{
        
		//! Audio buffer loader
        class AudioBufferLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            AudioBufferLoader();
            //! Destructor
            virtual ~AudioBufferLoader();
                        
            //! loads a resource
            //! \param path the path of the file we want to load
            virtual resource::IResource* loadResource(const char* path);
        };
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////