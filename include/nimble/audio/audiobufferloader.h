//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_audio_audiobufferloader_h__
#define __nimble_audio_audiobufferloader_h__

//////////////////////////////////////////////////////////////////////////

#if defined(NIMBLE_TARGET_WIN32)
    #include <nimble/audio/platform/win32/audiobufferloader.h>
#elif defined(NIMBLE_TARGET_OSX)
    #include <nimble/audio/platform/osx/audiobufferloader.h>
#endif

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////