//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_gui_text_h__
#define __nimble_gui_text_h__

////////////////////////////////////////////////////////////////////////////
//
//#include <string>
//#include <nimble/math/vector.h>
//#include <nimble/graphics/mesh.h>
//#include <nimble/graphics/material.h>
//
////////////////////////////////////////////////////////////////////////////
//
//namespace nimble{
//    namespace renderdevice{
//        class IRenderDevice;
//        class ITexture;
//    };
//	namespace text{
//        class Font;
//        
//        // text vertex definition
//        struct vertex_t{
//            math::Vector3f  position;
//            math::Vector4c  color;
//            math::Vector2f  texCoord;
//            
//            //! Constructor
//            vertex_t()
//            :position(math::Vector3f::zero())
//            ,color(math::Vector4c::zero())
//            ,texCoord(math::Vector2f::zero()){
//            }
//            //! Destructor
//            ~vertex_t(){
//            }
//            //! Returns the format of this vertex
//            static nimble::renderdevice::VertexFormat getFormat(){
//                nimble::renderdevice::VertexFormat format;
//                format.addAttribute(nimble::renderdevice::VertexFormat::kSemanticPosition,
//                                    nimble::renderdevice::VertexFormat::kTypeFloat3);
//                format.addAttribute(nimble::renderdevice::VertexFormat::kSemanticColor,
//                                    nimble::renderdevice::VertexFormat::kTypeByte4);
//                format.addAttribute(nimble::renderdevice::VertexFormat::kSemanticTexCoord1,
//                                    nimble::renderdevice::VertexFormat::kTypeFloat2);
//                return format;
//            }
//        };
//        // text material definition
//        struct material_t{
//            renderdevice::ITexture *pColormap;
//            
//            //! Returns the hash index for this material
//            int64_t getHashIndex() const{
//                return 0;
//            }
//            //! Returns the material format metadata
//            static nimble::core::TypeMetaData* getTypeMetaData(){
//                static nimble::core::TypeMetaData *pMaterialTypeMetaData = 0;
//                if(pMaterialTypeMetaData == 0){
//                    pMaterialTypeMetaData = nimble::core::createTypeMetaData<material_t>("Material");
//                    nimble::core::FieldMetaData fields[] = {
//                        nimble::core::FieldMetaData("colormap", &material_t::pColormap),
//                    };
//                    pMaterialTypeMetaData->fields(fields);
//                }
//                return pMaterialTypeMetaData;
//            }
//        };
//        
//        //! text
//        class Text{
//        private:
//
//            text::Font                                  *m_pFont;
//            std::string                                 m_text;
//            
////            graphics::Mesh<>                            m_mesh;
////            graphics::MaterialWrapper<text::material_t>  m_material;
//            
//        public:
//            
//            //! Constructor
//            Text(text::Font *pFont, const char *text);
//            //! Destructor
//            virtual ~Text();
//            
//        public:
//            
//            //! Set our text
//            void setText(const char *text);
//            //! Get our text
//            const char* getText() const;
//            
//            //! Get our font
//            text::Font* getFont() const;
//
//        private:
//            
//            //! Regenerate mesh
//            void regenerateMesh();
//        };
//    };
//};
//
////////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////