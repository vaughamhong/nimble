//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_model_chumbalumsoftms3d_h__
#define __nimble_model_chumbalumsoftms3d_h__

///////////////////////////////////////////////////////////////////////////////////////////////

#include <nimble/resource/resource.h>
#include <nimble/resource/resourceloader.h>
#include <nimble/math/vector.h>

///////////////////////////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace model{
        
        //! Chumbalumsoft MS3D file
        class ChumbalumsoftMS3D{
        public:
            
            const static unsigned int MS3D_MAXVERTICES		= 8192;		//!< max number of vertices
            const static unsigned int MS3D_MAXTRIANGLES		= 16384;	//!< max number of triangles
            const static unsigned int MS3D_MAXGROUPS		= 128;		//!< max number of geometry groups
            const static unsigned int MS3D_MAXMATERIALS		= 128;		//!< max number of materials
            const static unsigned int MS3D_MAXJOintS		= 128;		//!< max number of joints
            const static unsigned int MS3D_MAXKEYFRAMES		= 216;		//!< max number of keyframes
            
            const static unsigned int MS3D_SELECTED			= 1;		//!< flag
            const static unsigned int MS3D_HIDDEN			= 2;		//!< flag
            const static unsigned int MS3D_SELECTED2		= 4;		//!< flag
            const static unsigned int MS3D_DIRTY			= 8;		//!< flag
            
            //! ms3d Header
            typedef struct{
                char cID[10];
                int iVersion;
            }ms3dHeader;
            
            //! ms3d Vertex
            typedef struct{
                unsigned char ucFlags;
                unsigned char ucRefCount;
                char cBoneID;
                math::Vector3f point;
            }ms3dVertex;
            
            //! ms3d Triangle
            typedef struct{
                short wFlags;
                short waVertexIndices[3];
                math::Vector3f vertexNormals[3];
                float faU[3];
                float faV[3];
                char bySmoothingGroup;
                char byGroupIndex;
            }ms3dTriangle;
            
            //! ms3d Groups
            typedef struct{
                char byFlags;
                char caName[32];
                short wNumIndices;
                short* wpaTriangleIndices;
                char cMaterialIndex;
            }ms3dGroup;
            
            //! ms3d Materials
            typedef struct{
                char cName[32];
                float fAmbient[4];
                float fDiffuse[4];
                float fSpecular[4];
                float fEmissive[4];
                float fShininess;
                float fTransparency;
                char cMode;
                char caTexture[128];
                char caAlphamap[128];
            }ms3dMaterial;
            
            //! ms3d Animation
            typedef struct{
                float fTime;
                float fPosition[3];
            }ms3dKeyFrameRotation;
            typedef struct{
                float fTime;
                float fPosition[3];
            }ms3dKeyFramePosition;
            
            //! ms3d Joints
            typedef struct{
                char byFlags;
                char caName[32];
                char caParentName[32];
                float fRotation[3];
                float fPosition[3];
                
                short wNumKeyFrameRot;
                short wNumKeyFrameTran;
                
                ms3dKeyFrameRotation* paKeyFrameRotations;
                ms3dKeyFramePosition* paKeyFrameTranslations;
            }ms3dJoint;
            
            ms3dHeader header;
            short wNumVertices;
            ms3dVertex* paVertices;
            short wNumTriangles;
            ms3dTriangle* paTriangles;
            short wNumGroups;
            ms3dGroup* paGroups;
            short wNumMaterials;
            ms3dMaterial* paMaterials;
            short wNumJoints;
            ms3dJoint* paJoints;
            float fAnimationFPS;
            float fCurrentTime;
            int iTotalFrames;
            
        public:
            
            //! Constructor
            ChumbalumsoftMS3D();
            //! Constructor
            ChumbalumsoftMS3D(const char *path);
            //! Destructor
            ~ChumbalumsoftMS3D();
            
        public:
            
            //! Load from file
            void loadFromFile(const char *path);
            
        public:
            
            //! returns the group by name
            virtual model::ChumbalumsoftMS3D::ms3dGroup* getGroupByName(const char *name);
            //! returns the material by name
            virtual model::ChumbalumsoftMS3D::ms3dMaterial* getMaterialByName(const char *name);
        };
        
        //! Chumbalumsoft MS3D file resource loader
        class ChumbalumsoftMS3DLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            ChumbalumsoftMS3DLoader();
            //! Destructor
            virtual ~ChumbalumsoftMS3DLoader();
            
            //! loads a resource
            //! \param path the path of the file we want to load
            virtual resource::IResource* loadResource(const char* path);
        };
    };
};

///////////////////////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////////////////////