//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_model_assimpscene_h__
#define __nimble_model_assimpscene_h__
#if 0

///////////////////////////////////////////////////////////////////////////////////////////////

#include <nimble/resource/resource.h>
#include <nimble/resource/resourceloader.h>

///////////////////////////////////////////////////////////////////////////////////////////////

struct aiScene;

///////////////////////////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace model{
        
        //! AssImpScene file
        struct AssImpScene{
        public:
            
            const aiScene* m_scene;

        public:
            
            //! Constructor
            AssImpScene();
            //! Constructor
            AssImpScene(const char *path);
            //! Destructor
            ~AssImpScene();
            
        public:
            
            //! Load from file
            void loadFromFile(const char *path);
        };
        
        //! AssImp file resource loader
        class AssImpSceneLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            AssImpSceneLoader();
            //! Destructor
            virtual ~AssImpSceneLoader();
            
            //! loads a resource
            //! \param path the path of the file we want to load
            virtual resource::IResource* loadResource(const char* path);
        };
    };
};

///////////////////////////////////////////////////////////////////////////////////////////////

#endif
#endif

///////////////////////////////////////////////////////////////////////////////////////////////