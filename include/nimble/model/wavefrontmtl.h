//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_model_wavefrontmtl_h__
#define __nimble_model_wavefrontmtl_h__

///////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <vector>
#include <map>
#include <string>
#include <nimble/resource/resourceloader.h>
#include <nimble/model/obj_parser.h>

///////////////////////////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace model{
        
        //! Wavefront MTL file
        struct WavefrontMTL{
        public:
            
            typedef tinyobj::material_t material_t;
            
            tinyobj::mtl_data_t m_data;
            
        public:
            
            //! Constructor
            WavefrontMTL();
            //! Constructor
            WavefrontMTL(const char *path);
            //! Destructor
            ~WavefrontMTL();
            
        public:
            
            //! Load from file
            void loadFromFile(const char *path);
            
            //! Finds material by name
            material_t* findMaterialByName(const char *name);
        };
        
        //! Wavefront MTL file resource loader
        class WavefrontMTLLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            WavefrontMTLLoader();
            //! Destructor
            virtual ~WavefrontMTLLoader();
            
            //! loads a resource
            //! \param path the path of the file we want to load
            virtual resource::IResource* loadResource(const char* path);
        };
    };
};

///////////////////////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////////////////////