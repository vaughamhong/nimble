//
// Copyright 2012-2013, Syoyo Fujita.
//
// Licensed under 2-clause BSD liecense.
//
#ifndef _TINY_OBJ_LOADER_H
#define _TINY_OBJ_LOADER_H

#include <string>
#include <vector>
#include <map>

namespace tinyobj {
    
    struct material_t{
        std::string name;
        
        float ambient[3];
        float diffuse[3];
        float specular[3];
        float transmittance[3];
        float emission[3];
        float shininess;
        float ior;              // index of refraction
        float dissolve;         // 1 == opaque; 0 == fully transparent
        int illum;              // illumination model (see http://www.fileformat.info/format/material/)
        
        std::string ambient_texname;
        std::string diffuse_texname;
        std::string specular_texname;
        std::string normal_texname;
        std::map<std::string, std::string> unknown_parameter;
    };
    
    struct mesh_t{
        std::string                 name;
        std::vector<float>          positions;
        std::vector<float>          normals;
        std::vector<float>          texcoords;
        std::vector<unsigned int>   indices;
        std::vector<int>            material_ids; // per-mesh material ID
    };
    struct obj_data_t{
        std::vector<std::string>    material_lib_ref;   // material lib reference
        std::vector<std::string>    material_ref;       // material references
        std::vector<mesh_t>         meshes;
    };
    struct mtl_data_t{
        std::vector<material_t>     materials;
    };
    
    /// Loads object from a std::istream, uses GetMtlIStreamFn to retrieve
    std::string parse_obj(obj_data_t &obj, std::istream& inStream);
    /// Loads materials into std::map
    std::string parse_mtl(mtl_data_t &mtl, std::istream& inStream);
}

#endif  // _TINY_OBJ_LOADER_H
