//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_model_wavefrontobj_h__
#define __nimble_model_wavefrontobj_h__

///////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <vector>
#include <string>
#include <nimble/resource/resourceloader.h>
#include <nimble/model/obj_parser.h>

///////////////////////////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace model{
        
        //! Wavefront OBJ file
        struct WavefrontOBJ{
        public:
            
            typedef tinyobj::mesh_t mesh_t;
            
            tinyobj::obj_data_t m_data;
            
        public:
            
            //! Constructor
            WavefrontOBJ();
            //! Constructor
            WavefrontOBJ(const char *path);
            //! Destructor
            ~WavefrontOBJ();
            
        public:
            
            //! Load from file
            void loadFromFile(const char *path);
            
        public:
            
            //! Find face lists that belongs to a group
            mesh_t* findFaceListsByGroup(const char *group);
        };
        
        //! Wavefront OBJ file resource loader
        class WavefrontOBJLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            WavefrontOBJLoader();
            //! Destructor
            virtual ~WavefrontOBJLoader();
            
            //! loads a resource
            //! \param path the path of the file we want to load
            virtual resource::IResource* loadResource(const char* path);
        };
    };
};

///////////////////////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////////////////////