//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_model_tuscanyxml__
#define __nimble_model_tuscanyxml__

///////////////////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <nimble/resource/resourceloader.h>

///////////////////////////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace model{

        //! Tuscany XML file
        struct TuscanyXML{
            
            struct TTexture{
                char mFilename[32];
            };
            
            struct TVertex{
                float mPosition[3];
                float mNormal[3];
                float mTexCoord[2][2];
            };
            
            struct TIndex{
                int mIndex;
            };
            
            struct TModel{
                char mName[32];
                int mNumVertices;
                int startVertex;
                int mNumIndices;
                int startIndex;
                int textureIndex;
                int lightmapIndex;
            };
            
            struct TPlane{
                float mNormal[3];
                float mDistance;
            };
            struct TCollisionModel{
                int mPlaneIndex;
                int mNumPlanes;
            };
            
            std::vector<TTexture>           mTextures;
            std::vector<TVertex>            mVertices;
            std::vector<TIndex>             mIndices;
            std::vector<TModel>             mModels;
            std::vector<TPlane>             mPlanes;
            std::vector<TCollisionModel>    mCollisionModels;
            
        public:
            
            //! Constructor
            TuscanyXML();
            //! Constructor
            TuscanyXML(const char *path);
            //! Destructor
            ~TuscanyXML();
            
        public:
            
            //! Load from file
            void loadFromFile(const char *path);
        };
        
        //! Tuscany XML file resource loader
        class TuscanyXMLLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            TuscanyXMLLoader();
            //! Destructor
            virtual ~TuscanyXMLLoader();
            
            //! loads a resource
            //! \param path the path of the file we want to load
            virtual resource::IResource* loadResource(const char* path);
        };
    };
};

///////////////////////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////////////////////