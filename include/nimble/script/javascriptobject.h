//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_script_javascriptobject_h__
#define __nimble_script_javascriptobject_h__

//////////////////////////////////////////////////////////////////////////

#include "v8.h"

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace script{
        class JavascriptEnvironment;
        
        //! A javascript object knows how to wrap a native object
        //! into a v8 object, and does native object cleanup by tracking a
        //! weak (persistent) pointer - invoked when garbage
        //! collected (finalized).
        class JavascriptObject{
        private:
            
            // this object shall not be destroyed once out of a handle scope.
            // we manage a persistent handle to the object and clean up manually.
            v8::Persistent<v8::Object> m_persistent;
            
            // the environment this object belongs to
            script::JavascriptEnvironment *m_pEnvironment;
            
        public:
            
            //! Constructor
            JavascriptObject(script::JavascriptEnvironment *pEnvironment);
            //! Destructor
            virtual ~JavascriptObject();
            
        public:
            
            //! Wrap into javascript object
            void wrap(v8::Handle<v8::Object> object);
            
            //! Returns the persistent object
            v8::Persistent<v8::Object>& getPersistent();
            //! Returns the local object
            v8::Local<v8::Object> getObject();
            //! Returns the environment
            script::JavascriptEnvironment* getEnvironment() const;
            
        private:
            
            //! Cleans up native object when invoked for garbage collection
            static void cleanupJavascriptObject(const v8::WeakCallbackData<v8::Object, JavascriptObject>& data);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////