//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_script_javascriptenvironment_h__
#define __nimble_script_javascriptenvironment_h__

//////////////////////////////////////////////////////////////////////////

#include <unordered_map>
#include <string>
#include "v8.h"

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class PropertyTree;
    };
    namespace script{
        class JavascriptFunction;
        class JavascriptEnvironment;
        
        //! The maximum number of parameters our method calls can use
        static const int kCallMethodMaxParameters = 8;
        
        //! Delegates
        class JavascriptEnvironmentDelegate{
        public:
            //! Called when a javascript environment script will load
            virtual void javascriptEnvironmentScriptWillLoad(JavascriptEnvironment *pEnvironment) = 0;
            //! Called when a javascript environment script failed to load
            virtual void javascriptEnvironmentScriptLoadDidFail(JavascriptEnvironment *pEnvironment) = 0;
            //! Called when a javascript environment script was loaded
            virtual void javascriptEnvironmentScriptDidLoad(JavascriptEnvironment *pEnvironment) = 0;
            //! Called when a javascript environment script will unload
            virtual void javascriptEnvironmentScriptWillUnload(JavascriptEnvironment *pEnvironment) = 0;
        };
        
        //! A wrapper for a javascript variable
        class JSVar{
        public:
            
            enum eType{
                kTypeVoid,
                kTypeInt,
                kTypeBool,
                kTypeString,
            }m_type;

        private:
            
            union{
                int i;
                bool b;
            }m_var;
            std::string m_string;
            
        public:
            
            //! Constructor
            JSVar();
            //! Constructor
            JSVar(JSVar const &var);
            //! Constructor
            JSVar(int value);
            //! Constructor
            JSVar(bool value);
            //! Constructor
            JSVar(const char *value);
            //! Destructor
            ~JSVar();
            
        public:
            
            //! Returns the type
            eType getType() const;
            //! Returns the int
            int getInt() const;
            //! Returns the bool
            bool getBool() const;
            //! Returns the string
            const char* getString() const;
            
        public:
            
            //! Copies variable
            JSVar& operator=(const JSVar& var);
        };
        
        //! Javascript execution environment
        class JavascriptEnvironment{
        private:
            
            JavascriptEnvironmentDelegate       *m_pDelegate;
            
            // an isolated instance of the V8 engine
            v8::Isolate                         *m_pIsolate;
            
            // persistent handles to objects that have the lifetime of this object
            v8::Persistent<v8::ObjectTemplate>  m_globalTemplate;
            v8::Persistent<v8::Context>         m_context;
            v8::Persistent<v8::Script>          m_script;
            
            // tracks if our environment has been initialized
            bool                                m_initialized;
                        
        private:
            
            //! Contains all information needed to handle an event
            struct eventInfo_t{
                script::JavascriptFunction *pFunction;
            };
            //! A mapping from event hash to info
            typedef std::unordered_map<int64_t, eventInfo_t> EventHashToInfoIndex;
            EventHashToInfoIndex m_eventHashToInfoIndex;
            
        public:
            
            //! Constructor
            JavascriptEnvironment(v8::Isolate *pIsolate, JavascriptEnvironmentDelegate *pDelegate = 0);
            //! Destructor
            virtual ~JavascriptEnvironment();
            
        public:
            
            //! Sets delegate
            virtual void setDelegate(JavascriptEnvironmentDelegate *pDelegate);
                        
        public:
                        
            //! Returns the isolate
            virtual v8::Isolate* getIsolate();
            //! Returns the global template
            virtual v8::Persistent<v8::ObjectTemplate>& getGlobalTemplate();
            //! Returns the context
            virtual v8::Persistent<v8::Context>& getContext();
            //! Returns the script
            virtual v8::Persistent<v8::Script>& getScript();
            
        public:
            
            //! Returns true if loaded
            virtual bool isLoaded() const;
            
            //! Load script
            virtual void loadWithFilePath(const char *filePath);
            //! Load script
            virtual void loadWithBuffer(const char *pBuffer);
            //! Unload script
            virtual void unload();
            
        private:
            
            //! initializes our environment
            virtual void initializeEnvironment();
            //! destroys our environment
            virtual void destroyEnvironment();
            
        public:
            
            //! Run script
            virtual void run();
            
        public:
            
            //! Returns true if method exists
            virtual bool existsMethodWithName(const char *name);
            //! Calls a javascript method and returns a value
            virtual script::JSVar callMethod(const char *name, int numParams = 0, ...);
            //! Calls a javascript method and returns a value
            virtual script::JSVar callMethod(const char *name, int numParams, va_list va);
            //! Calls a javascript method and returns a value
            virtual script::JSVar callMethod(v8::Handle<v8::Function> &function, int numParams, va_list va);
            
        public:
            
            //! Add event listener
            virtual void addEventListener(const char *eventName, v8::Handle<v8::Function> &function);
            //! Remove event listener
            virtual void removeEventListener(const char *eventName);
            //! Removes all listeners
            virtual void removeAllEventListeners();
            
        public:
            
            //! Dispatch event
            virtual void dispatchEvent(const char *eventName, int numParams = 0, ...);
            //! Dispatch event
            virtual void dispatchEvent(const char *eventName, int numParams, va_list va);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////