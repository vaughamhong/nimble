//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_script_httpjavascriptmodule_h__
#define __nimble_script_httpjavascriptmodule_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/script/javascriptobject.h>
#include <nimble/network/http.response.h>
#include <nimble/network/http.request.h>
#include <nimble/core/future.h>
#include "v8.h"

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class ThreadPool;
    };
    namespace network{
        class HttpRequest;
        class HttpResponse;
    };
    namespace script{
        class JavascriptFunction;
        class JavascriptEnvironment;
        
        //! A javascript http object
        class HttpJavascriptModule
        : public script::JavascriptObject{
        private:
            
            //! Contains all information needed to handle a dispatch
            struct dispatchInfo_t{
                network::HttpRequest request;
                network::HttpResponse response;
                script::JavascriptFunction *pCallback;
                core::Future<dispatchInfo_t*> future;
            };
            typedef std::vector<dispatchInfo_t*> CleanupList;
            static CleanupList m_cleanupList;
                        
        private:
            
            //! Constructor
            HttpJavascriptModule(script::JavascriptEnvironment *pEnvironment);
            //! Destructor
            virtual ~HttpJavascriptModule();
            
        public:
            
            //! Update
            static void cleanup();
            
        public:
            
            //! Exports module
            static void exportModule(v8::Local<v8::Object> &object, script::JavascriptEnvironment *pEnvironment);

        private:
                        
            //! Dispatches a request
            static void nativeRequest(const v8::FunctionCallbackInfo<v8::Value>& args);
            //! Dispatches a request
            static dispatchInfo_t* dispatchRequest(dispatchInfo_t *pDispatchInfo);
            //! Called when request was completed
            static void onRequestCompleted(script::JavascriptEnvironment *pEnvironment, core::Future<dispatchInfo_t*> &future);
            
        private:
            
            //! Allocates a dispatch info
            static dispatchInfo_t* allocateDispatchInfo();
            //! Deallocates a dispatch info
            static void deallocateDispatchInfo(dispatchInfo_t *pInfo);
        };
        
        //! A javascript http response object
        //! This class can only be instatiated using the static createInstance method
        class HttpResponseJavascriptObject
        : public script::JavascriptObject{
        private:
            
            network::HttpResponse m_response;
            
        private:
            
            //! Constructor
            HttpResponseJavascriptObject(script::JavascriptEnvironment *pEnvironment, network::HttpResponse const &response);
            //! Destructor
            virtual ~HttpResponseJavascriptObject();
            
        public:
            
            //! Exports object template to a target
            //! \param target the target to export to
            static void exportObjectTemplate(v8::Handle<v8::ObjectTemplate> &target);
            //! Creates an instance
            static HttpResponseJavascriptObject* createInstance(script::JavascriptEnvironment *pEnvironment, network::HttpResponse response);
            
        private:
            
            //! Creates an instance
            static void construct(const v8::FunctionCallbackInfo<v8::Value>& args);
            //! Gets our response code
            //! \param property the property name of this operation
            //! \param info extra information about this operation
            static void getCode(v8::Local<v8::String> property, const v8::PropertyCallbackInfo<v8::Value>& info);
            //! Gets our response body
            //! \param property the property name of this operation
            //! \param info extra information about this operation
            static void getBody(v8::Local<v8::String> property, const v8::PropertyCallbackInfo<v8::Value>& info);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////