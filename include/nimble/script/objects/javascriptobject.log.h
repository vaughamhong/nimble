//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_script_logjavascriptmodule_h__
#define __nimble_script_logjavascriptmodule_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/script/javascriptobject.h>
#include <string>
#include "v8.h"

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace script{
        
        //! A javascript log object
        class LogJavascriptModule{
        private:
            
            //! Constructor
            LogJavascriptModule();
            //! Destructor
            virtual ~LogJavascriptModule();
            
        public:
            
            //! Exports module
            static void exportModule(v8::Local<v8::Object> &object, script::JavascriptEnvironment *pEnvironment);

        private:

            //! Native log info implementation
            static void logInfo(const v8::FunctionCallbackInfo<v8::Value>& args);
            //! Native log error implementation
            static void logError(const v8::FunctionCallbackInfo<v8::Value>& args);
            //! Native log warning implementation
            static void logWarning(const v8::FunctionCallbackInfo<v8::Value>& args);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////