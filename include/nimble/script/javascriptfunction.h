//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_script_javascriptfunction_h__
#define __nimble_script_javascriptfunction_h__

//////////////////////////////////////////////////////////////////////////

#include "v8.h"

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace script{
        
        //! A javascript object knows how to wrap a native object
        //! into a v8 object, and does native object cleanup by tracking a
        //! weak (persistent) pointer - invoked when garbage
        //! collected (finalized).
        class JavascriptFunction{
        private:
            
            // this object shall not be destroyed once out of a handle scope.
            // we manage a persistent handle to the object and clean up manually.
            v8::Persistent<v8::Function> m_persistent;
            
        public:
            
            //! Constructor
            JavascriptFunction();
            //! Destructor
            virtual ~JavascriptFunction();
            
        public:
            
            //! Wrap into javascript object
            void wrap(v8::Handle<v8::Function> function);
            //! Returns the persistent object
            v8::Persistent<v8::Function>& getPersistent();
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////