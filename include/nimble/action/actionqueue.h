//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_action_actionqueueaction_h__
#define __nimble_action_actionqueueaction_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/action/action.h>
#include <queue>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace action{
        
		//! a actions component
		class ActionQueue{
        private:
            
            typedef std::queue<action::ActionPtr> ActionList;
            ActionList m_actionQueue;
            
		public:
            
            //! Constructor
			ActionQueue();
			//! Destructor
			virtual ~ActionQueue();
            
        public:
            
            //! Enqueue an action
            virtual void enqueue(action::ActionPtr action);
            //! Dequeue an action
            virtual void dequeue();
            //! Returns the size of the action list
            virtual uint32_t getQueueSize() const;
            //! Returns the front action
            virtual action::ActionPtr getFrontAction();
            //! Clears queue
            virtual void clear();
		};
	};
};

//////////////////////////////////////////////////////////////////////////


#endif