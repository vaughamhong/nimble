//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_action_soundfxaction_h__
#define __nimble_action_soundfxaction_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/action/action.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace audiodevice{
        class IAudioDevice;
        class IAudioBuffer;
        class IAudioPlayer;
    };
	namespace action{
        
		//! Plays a sound fx
		class SoundFxAction
        : public action::Action{
        private:
            
            audiodevice::IAudioDevice *m_pAudioDevice;
            audiodevice::IAudioBuffer *m_pAudioBuffer;
            audiodevice::IAudioPlayer *m_pAudioPlayer;
            float         m_pitch;
            float         m_gain;
            
		public:
            
			//! Constructor
			SoundFxAction(audiodevice::IAudioBuffer *pAudioBuffer, audiodevice::IAudioDevice *pAudioDevice = 0, float pitch = 1.0f, float gain = 1.0f);
			//! Constructor
            virtual ~SoundFxAction();
            
        public:
            
            //! Initializes this action
            virtual void initialize(action::ActionQueue *pActionQueue);
            //! Destroys this action
            virtual void destroy();
            
        public:
            
            //! Starts this action
            virtual void start();
            //! Stops this action
            virtual void stop();
            
        public:
            
            //! \return true if something has changed
            virtual void update(double elapsedTime);
            //! returns true if this action has completed
            virtual bool isFinished();
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////