//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_action_waitaction_h__
#define __nimble_action_waitaction_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/action/action.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace action{
        
		//! Waits for a duration of time
		class WaitAction
        : public action::Action{
        private:
            
            float   m_waitDuration;
            double  m_currentWaitDuration;
            
		public:
            
			//! Constructor
			WaitAction(float duration);
			//! Constructor
            virtual ~WaitAction();
            
        public:
            
            //! Initializes this action
            virtual void initialize(action::ActionQueue *pActionQueue);
            //! Destroys this action
            virtual void destroy();
            
        public:
            
            //! Starts this action
            virtual void start();
            //! Stops this action
            virtual void stop();
            
        public:
            
            //! \return true if something has changed
            virtual void update(double elapsedTime);
            //! returns true if this action has completed
            virtual bool isFinished();
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////