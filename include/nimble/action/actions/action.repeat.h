//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_action_repeataction_h__
#define __nimble_action_repeataction_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/action/action.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace action{
        
		//! Repeats an action N times
		class RepeatAction
        : public action::Action{
        private:
            
            int32_t             m_count;
            int32_t             m_currentCount;
            action::ActionPtr   m_action;
            
		public:
            
			//! Constructor
			RepeatAction(action::Action *pAction, int32_t count = -1);
			//! Constructor
			RepeatAction(action::ActionPtr action, int32_t count = -1);
			//! Constructor
            virtual ~RepeatAction();
            
        public:
            
            //! Initializes this action
            virtual void initialize(action::ActionQueue *pActionQueue);
            //! Destroys this action
            virtual void destroy();
            
        public:
            
            //! Starts this action
            virtual void start();
            //! Stops this action
            virtual void stop();
            
        public:
            
            //! \return true if something has changed
            virtual void update(double elapsedTime);
            //! returns true if this action has completed
            virtual bool isFinished();
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////