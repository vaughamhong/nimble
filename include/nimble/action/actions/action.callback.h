//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_action_callbackaction_h__
#define __nimble_action_callbackaction_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/action/action.h>
#include <nimble/core/functor.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace action{
        
		//! Executes a callback action
        template <typename Func>
		class CallbackAction
        : public action::Action{
        public:
            
            typedef core::BindAllFunctor<Func> Callback;
            
        private:
            
            bool m_isFinished;
            Callback m_callback;
            
		public:
            
			//! Constructor
			CallbackAction(Callback callback)
            :action::Action("CallbackAction")
            ,m_isFinished(false)
            ,m_callback(callback){
            }
			//! Constructor
            virtual ~CallbackAction(){
            }
            
        public:
            
            //! Initializes this action
            virtual void initialize(action::ActionQueue *pActionQueue){
            }
            //! Destroys this action
            virtual void destroy(){
            }
            
        public:
            
            //! Starts this action
            virtual void start(){
                action::Action::start();
            }
            //! Stops this action
            virtual void stop(){
                action::Action::stop();
            }
            
        public:
            
            //! \return true if something has changed
            virtual void update(double elapsedTime){
                if(!m_isFinished){
                    m_callback();
                    m_isFinished = true;
                }
            }
            //! returns true if this action has completed
            virtual bool isFinished(){
                return m_isFinished;
            }
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////