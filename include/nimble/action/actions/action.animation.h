//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_action_animationaction_h__
#define __nimble_action_animationaction_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/action/action.h>
#include <nimble/math/curve.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace scene{
        class SceneTransform;
    };
	namespace action{
                
		//! Executes an animation for a duration of time
		class AnimationAction
        : public action::Action{
        private:
            
            scene::SceneTransform   *m_pSceneTransform;

            float                   m_duration;
            math::eCurve            m_curve;
            
		public:
            
			//! Constructor
			AnimationAction(const char *name, float duration = 0.0f, math::eCurve curve = math::kCurveLinear);
			//! Constructor
            virtual ~AnimationAction();
            
        public:
            
            //! Initializes this action
            virtual void initialize(action::ActionQueue *pActionQueue);
            //! Destroys this action
            virtual void destroy();
            
        public:
            
            //! Starts this action
            virtual void start();
            //! Stops this action
            virtual void stop();
            
        public:
            
            //! \return true if something has changed
            virtual void update(double elapsedTime) = 0;
            //! returns true if this action has completed
            virtual bool isFinished() = 0;
            
        public:
            
            //! sets our scene transform
            virtual void setSceneTransform(scene::SceneTransform *pTransform);
            //! gets our scene transform
            virtual scene::SceneTransform* getSceneTransform() const;
            
        public:
            
            //! returns the curve type
            virtual math::eCurve getCurve();
            //! returns the duration
            virtual float getDuration();
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////