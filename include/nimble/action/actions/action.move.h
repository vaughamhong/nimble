//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_action_moveaction_h__
#define __nimble_action_moveaction_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/action/actions/action.animation.h>
#include <nimble/scene/scenetransform.h>
#include <nimble/math/vector.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace action{
        
		//! Moves a scene transform
		class MoveAction
        : public action::AnimationAction{
        private:
                        
            double                  m_elapsedTime;
            
            math::Vector3f          m_startPosition;
            math::Vector3f          m_targetPosition;
            
		public:
            
			//! Constructor
			MoveAction(math::Vector3f destination, float duration = 0.0f, math::eCurve curve = math::kCurveLinear);
			//! Constructor
            virtual ~MoveAction();
            
        public:
            
            //! Initializes this action
            virtual void initialize(action::ActionQueue *pActionQueue);
            //! Destroys this action
            virtual void destroy();
            
        public:
            
            //! Starts this action
            virtual void start();
            //! Stops this action
            virtual void stop();
            
        public:
            
            //! \return true if something has changed
            virtual void update(double elapsedTime);
            //! returns true if this action has completed
            virtual bool isFinished();
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////