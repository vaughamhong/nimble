//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_action_action_h__
#define __nimble_action_action_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/core/smartptr.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace action{
        class Action;
        class ActionQueue;
        class ParallelAction;
        class RepeatAction;
        class SequenceAction;
        
        typedef core::SmartPtr<Action> ActionPtr;
        
        //! An action
        class Action{
        private:
            
            std::string             m_name;
            bool                    m_hasStarted;
            action::ActionQueue     *m_pActionQueue;
            
        public:
            
            //! Constructor
            Action(const char *name);
            //! Destructor
            virtual ~Action();
            
        public:
            
            //! Initializes this action
            virtual void initialize(action::ActionQueue *pActionQueue);
            //! Destroys this action
            virtual void destroy();

        public:
            
            //! Starts this action
            virtual void start();
            //! Stops this action
            virtual void stop();
            
        public:
            
            //! Updates this action
            virtual void update(double elapsedTime) = 0;
            //! returns true if this action has completed
            virtual bool isFinished() = 0;
            //! returns if we have started
            virtual bool hasStarted();
            //! gets the message queue
            virtual action::ActionQueue* getActionQueue();
        };
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////