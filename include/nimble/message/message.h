//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_message_message_h__
#define __nimble_message_message_h__

//////////////////////////////////////////////////////////////////////////

#include <typeinfo>
#include <string>
#include <nimble/core/serializable.h>
#include <nimble/core/hash.h>
#include <nimble/core/functor.h>
#include <nimble/core/factory.h>
#include <nimble/core/stream.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace message{
    typedef int32_t MessageId;

    //! Message base type
    class Message
    : public core::ISerializable{
    public:
        
        //! Constructor
        Message(){}
        //! Destructor
        virtual ~Message(){}
        
    public:
        
        //! returns a id of this message type
        virtual MessageId getId() const = 0;
        //! returns a id generated from the type name of the message
        template <typename T> static MessageId getId(){
            return core::hash(typeid(T).name());
        }
    };
    
    //! Wraps our message
    //! Usage: class CustomMessage: public MessageWrapper<CustomMessage>{...}
    //! Messages that use the MessageWrapper must have single inheritence trees (eg. A > B > C > ...)
    template <class T>
    class MessageWrapper
    : public message::Message{
    public:
        
        //! Constructor
        MessageWrapper(){
        }
        //! Destructor
        virtual ~MessageWrapper(){
        }
        
    public:
        
        // *** This is not portable ***
        // Assuming single inheritence tree (eg. A > B > C > ...)
        // Assuming we are using a standard GCC compiler
        // We can directly serialize our internal data by copying 
        // relative to an offset of 4 bytes from our this pointer.
        // Our memory frame looks like the following.
        //
        // +0: pointer to our vtable
        // +4: start of our data members
        // *** This is not portable ***
        
        //! Writes to this stream
        //! \param pStream the stream to decode to
        virtual void encode(core::Stream *pStream) const{
            int bufferSize = sizeof(T) - 0x04;          // subtract 0x04 - do not include vtable pointer
            char *pDataStart = (char*)this + 0x04;      // offset 0x04 - where our data members start
            pStream->write(pDataStart, bufferSize);
        }
        //! Reads from this stream
        //! \param pStream the stream to decode from
        virtual void decode(core::Stream *pStream){
            unsigned int bufferSize = sizeof(T) - 0x04; // subtract 0x04 - do not include vtable pointer
            char *pDataStart = (char*)this + 0x04;      // offset 0x04 - where our data members start
            pStream->read(pDataStart, bufferSize);
        }
        //! returns the type
        virtual message::MessageId getId() const{
            return Message::getId<T>();
        }
    };
    
    // creates our message
    template <typename T> T* createMessage(){
        message::Message *pMessage = new /*( dynamic )*/ T();
        return dynamic_cast<T*>(pMessage);
    }
    
    // helper typdefs
    typedef core::Functor<void, TLIST_1(message::Message*)> MessageHandler;
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////