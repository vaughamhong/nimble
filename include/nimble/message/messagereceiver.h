//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_message_messagereceiver_h__
#define __nimble_message_messagereceiver_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/message/message.h>
#include <nimble/message/messagesubscriber.h>
#include <nimble/core/thread.mutex.h>
#include <nimble/core/assert.h>
#include <map>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace message{

    //! Routes messages to callback handlers
    class MessageReceiver
    : public message::MessageSubscriber{
    public:
        
        typedef std::map<MessageId, MessageHandler> MessageHandlerIndex;
        MessageHandlerIndex                         m_messageHandlerIndex;
        mutable core::Mutex                         m_lock;
        
    public:

        //! Constructor
        MessageReceiver();
        //! Constructor
        //! \param bufferSize the buffer size for our message queue
        MessageReceiver(uint32_t bufferSize);
        //! Destructor
        virtual ~MessageReceiver();
    
    public:
        
        //! adds a message type
        //! \param handler the handler to be used for a specific message type
        template <typename T, typename ObjType, typename Func>
        void addMessageHandler(const ObjType &obj, const Func &func){
            core::ScopedLock lock(&m_lock);
            message::MessageSubscriber::addMessageType<T>();
            
            MessageId messageId = Message::getId<T>();
            m_messageHandlerIndex.insert(std::pair<MessageId, MessageHandler>(messageId, MessageHandler(obj, func)));
        }
        //! adds a message type
        //! \param handler the handler to be used for a specific message type
        template <typename T>
        void addMessageHandler(MessageHandler handler){
            core::ScopedLock lock(&m_lock);
            
            MessageId messageId = Message::getId<T>();
            m_messageHandlerIndex.insert(std::pair<MessageId, MessageHandler>(messageId, handler));
        }
        //! removes a message type
        template <typename T>
        void removeMessageHandler(){
            core::ScopedLock lock(&m_lock);
            message::MessageSubscriber::removeMessageType<T>();

            MessageId messageId = Message::getId<T>();
            MessageHandlerIndex::iterator it = m_messageHandlerIndex.find(messageId);
            if(it != m_messageHandlerIndex.end()){
                m_messageHandlerIndex.erase(it);
            }
        }

    public:

        //! Process and invoke callbacks (handlers) on received messages
        virtual void processMessages();
        //! Callback when a message has been received
        //! \param pMessage the message to handle
        virtual void onMessageReceived(message::Message* pMessage);
    };
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////