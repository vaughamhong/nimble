//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_message_messagesubscriber_h__
#define __nimble_message_messagesubscriber_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/message/message.h>
#include <nimble/message/messagequeue.h>
#include <nimble/core/thread.mutex.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace message{
    class MessagePublisher;
    
    //! Message subscriber interface
    class MessageSubscriber{
    friend class nimble::message::MessagePublisher;
    private:
        
        mutable core::Mutex     m_lock;
        MessageQueue*           m_pMessageQueue;
        
    public:

        //! Constructor
        MessageSubscriber();
        //! Constructor
        //! \param bufferSize the buffer size for our message queue
        MessageSubscriber(uint32_t bufferSize);
        //! Destructor
        virtual ~MessageSubscriber();
        
    public:
        
        //! Returns the next message
        message::Message* getNextMessage();
        //! Returns true if there are no more messages left
        bool isEmpty() const;
    
    public:
        
        //! adds a message type
        template <typename T>
        void addMessageType(){
            core::ScopedLock lock(&m_lock);
            NIMBLE_ASSERT(m_pMessageQueue != 0);

            MessageId messageId = Message::getId<T>();
            m_pMessageQueue->registerMessageType<T>(messageId);
        }
        //! removes a message type
        template <typename T>
        void removeMessageType(){
            core::ScopedLock lock(&m_lock);
            NIMBLE_ASSERT(m_pMessageQueue != 0);

            MessageId messageId = Message::getId<T>();
            m_pMessageQueue->unregisterMessageType(messageId);
        }
        
    protected:
        
        //! adds a message to this subscriber
        //! \param pMessage the message to add
        virtual void addMessage(const Message* pMessage);
        //! adds a message to this subscriber
        //! \param pMessage the message to add
        template <typename T> void addMessage(){
            core::ScopedLock lock(&m_lock);
            message::Message *pMessage = message::createMessage<T>();
            this->addMessage(pMessage);
            delete pMessage;
        }
    };
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////