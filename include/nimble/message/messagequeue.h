//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_message_messagequeue_h__
#define __nimble_message_messagequeue_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/message/message.h>
#include <nimble/core/factory.h>
#include <nimble/core/stream.ringbuffer.h>
#include <nimble/core/thread.mutex.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace message{

    //! Thread-safe message queue
    //! Note: Message types that are not registered will not be serialized into the queue and will be lost
    class MessageQueue{
    public:
        
        static const unsigned int kDefaultQueueSize = 128;
        
    private:
        
        mutable core::Mutex                     m_lock;
        core::RingBufferStream                  m_ringBuffer;
        core::Factory<Message, MessageId>       m_factory;
        
    public:

        //! Constructor
        //! \param bufferSize the size of our internal buffer
        MessageQueue(unsigned int bufferSize = kDefaultQueueSize);
        //! Destructor
        virtual ~MessageQueue();

    public:
        
        //! Adds a message type
        //! \param messageId the message id to register
        template <typename T>
        void registerMessageType(MessageId messageId){
            core::ScopedLock lock(&m_lock);
            m_factory.registerBuilder<T>(messageId);
        }
        //! Removes a message type
        void unregisterMessageType(MessageId messageId){
            core::ScopedLock lock(&m_lock);
            m_factory.unregister(messageId);
        }
        
    public:

        //! Adds a message to the queue
        //! \param pMessage the message to add
        //! \return the message in the ring buffer
        virtual void push(const Message* pMessage);
        //! Returns an error code
        //! \return the next message
        virtual Message* pop();
        
        //! Returns true when queue is empty
        //! \return true when queue is empty
        virtual bool isEmpty() const;
    };
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////