//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_message_messagepublisher_h__
#define __nimble_message_messagepublisher_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/message/message.h>
#include <nimble/core/thread.mutex.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace message{
    class MessageSubscriber;
    
    //! Message publishing interface
    //! MessageSubscribers subscribes to message types
    //! Messages posted to the MessagePublisher are pushed to subscribers
    class MessagePublisher{
    private:
        
        typedef struct{
            message::MessageSubscriber      *m_pSubscriber;
            message::MessageId              m_messageId;
        }subscriber_t;
        
        typedef std::vector<subscriber_t>   SubscriberList;
        SubscriberList                      m_subscribers;

        mutable core::Mutex                 m_lock;
        
    public:
        
        //! Constructor
        MessagePublisher();
        //! Destructor
        virtual ~MessagePublisher();
        
    public:
        
        //! Sends a message
        //! \param pMessage the message to send
        void send(message::Message* pMessage);
        //! posts an event (message without any data)
        template <typename T>
        void send(){
            message::Message *pMessage = message::createMessage<T>();
            this->send(pMessage);
            delete pMessage;
        }
    
    public:
        
        //! adds a message subscriber
        //! \param pObserver the message subscriber
        template <typename T>
        void addSubscriber(message::MessageSubscriber *pSubscriber){
            this->addSubscriber(pSubscriber, Message::getId<T>());
        }
        //! removes a message subscriber
        //! \param pSubscriber the message subscriber
        template <typename T>
        void removeSubscriber(message::MessageSubscriber *pSubscriber){
            this->removeSubscriber(pSubscriber, Message::getId<T>());
        }
        //! removes message observers
        //! \param pSubscriber the message subscriber
        void removeSubscriber(message::MessageSubscriber *pSubscriber);
        //! removes all message observers
        void removeAllSubscribers();
        
    private:
        
        //! adds a message subscriber
        //! \param pSubscriber the message subscriber
        //! \param messageId the message id we are observing on
        void addSubscriber(message::MessageSubscriber *pSubscriber, message::MessageId messageId);
        //! removes a message subscriber
        //! \param pSubscriber the message subscriber
        //! \param messageId the message id we are observing on
        void removeSubscriber(message::MessageSubscriber *pSubscriber, message::MessageId messageId);
        //! exists an subscriber for a message
        bool existsSubscriber(message::MessageSubscriber *pSubscriber, message::MessageId messageId);
    };
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////