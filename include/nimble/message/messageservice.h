//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_message_messageservice_h__
#define __nimble_message_messageservice_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/message/message.h>
#include <nimble/message/messagepublisher.h>
#include <nimble/core/thread.mutex.h>
#include <map>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace message{
    class MessageSubscriber;
            
    //! A high level interface for binding subscribers to publishers through channels
    class MessageService{
    private:
        
        typedef std::map<std::string, message::MessagePublisher*> ChannelToPublisherIndex;
        ChannelToPublisherIndex     m_channelToPublisherIndex;
        mutable core::Mutex         m_lock;

    public:
        
        //! Constructor
        MessageService();
        //! Destructor
        virtual ~MessageService();
        
        //! Returns the default message service
        static MessageService* get();
        
    public:
        
        //! Sends a message
        //! \param channel the channel to send the message
        //! \param pMessage the message to send
        void send(message::Message* pMessage, const char *channel = "");
        //! posts an event (message without any data)
        //! \param channel the channel to send the message
        template <typename T>
        void send(const char *channel = ""){
            message::Message *pMessage = message::createMessage<T>();
            this->send(pMessage, channel);
            delete pMessage;
        }
        
    public:
        
        //! adds a message subscriber
        //! \param pObserver the message subscriber
        //! \param channel the channel to add subscriber
        template <typename T>
        void addSubscriber(message::MessageSubscriber *pSubscriber, const char *channel = ""){
            core::ScopedLock lock(&m_lock);
            message::MessagePublisher *pPublisher = getMessagePublisherByChannel(channel);
            pPublisher->addSubscriber<T>(pSubscriber);
        }
        //! removes a message subscriber
        //! \param pSubscriber the message subscriber
        //! \param channel the channel to remove subscriber
        template <typename T>
        void removeSubscriber(message::MessageSubscriber *pSubscriber, const char *channel = ""){
            core::ScopedLock lock(&m_lock);
            message::MessagePublisher *pPublisher = getMessagePublisherByChannel(channel);
            pPublisher->removeSubscriber<T>(pSubscriber);
        }
        //! removes message observers
        //! \param pSubscriber the message subscriber
        //! \param channel the channel to remove subscriber
        void removeSubscriber(message::MessageSubscriber *pSubscriber, const char *channel = "");
        //! removes all message observers
        //! \param channel the channel to remove subscriber
        void removeAllSubscribers(const char *channel);
        
    private:
        
        //! returns the publisher by channel
        //! \param channel the channel reference to our publisher
        //! \param createIfNotExist create the publisher if it does not exist
        message::MessagePublisher* getMessagePublisherByChannel(const char *channel, bool createIfNotExist = true);
    };
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////