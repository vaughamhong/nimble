//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_font_glyph_h__
#define __nimble_font_glyph_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace text{
        
        //! Describes texture position / size for a glyph
        struct glyphInfo_t{
            int32_t x, y;
            int32_t width, height;
            
            //! Returns our zero glyph info
            static glyphInfo_t zero(){
                glyphInfo_t info;
                info.x = 0;
                info.y = 0;
                info.width = 0;
                info.height = 0;
                return info;
            }
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////