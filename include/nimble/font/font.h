//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_font_font_h__
#define __nimble_font_font_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/font/glyph.h>
#include <nimble/resource/resourceloader.h>
#include <string>
#include <unordered_map>

//////////////////////////////////////////////////////////////////////////

#include "ft2build.h"
#include FT_FREETYPE_H

//////////////////////////////////////////////////////////////////////////

// TODO
// + Support UTF-8

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace renderdevice{
        class ITexture;
    };
	namespace text{
        
        //! Contains all information needed for a font used to create text
        class Font{
        public:
            
            static const uint32_t kDefaultFontSize = 12;
            static const uint32_t kDefaultTextureWidth = 256;
            static const uint32_t kDefaultTextureHeight = 256;

        private:
            
            typedef std::unordered_map<char, glyphInfo_t> KeyToGlyphInfoIndex;
            
            std::string             m_filepath;
            size_t                  m_size;
            FT_Face                 m_face;
            
            renderdevice::ITexture  *m_pTexture;
            KeyToGlyphInfoIndex     m_glyphCache;
            
        public:
            
            //! Constructor
            Font(size_t size = kDefaultFontSize, uint32_t textureWidth = kDefaultTextureWidth, uint32_t textureHeight = kDefaultTextureHeight);
            //! Constructor
            Font(const char *path, size_t size = kDefaultFontSize, uint32_t textureWidth = kDefaultTextureWidth, uint32_t textureHeight = kDefaultTextureHeight);
            //! Destructor
            virtual ~Font();
            
        public:
            
            //! Load from file
            void loadFromFile(const char *path);
            
        public:
            
            //! Sets font size
            void setFontSize(size_t size);
            //! Returns the font size
            size_t getFontSize() const;
                        
            //! Returns the family name
            const char* getFamilyName() const;
            //! Returns the style name
            const char *getStyleName() const;
            
        public:
            
            //! Loads glyphs
            void loadGlyphs(const char *characters = 0);
            
            //! Returns our texture
            renderdevice::ITexture* getTexture() const;
            //! Returns glyph location
            glyphInfo_t getGlyphInfo(char character) const;
        };
        
        //! Font resource loader
        class FontLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            FontLoader();
            //! Destructor
            virtual ~FontLoader();
            
            //! loads a resource
            //! \param path the path of the file we want to load
            virtual resource::IResource* loadResource(const char* path);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////