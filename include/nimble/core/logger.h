//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_core_logger_h__
#define __nimble_core_logger_h__

//////////////////////////////////////////////////////////////////////////

#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace core{
        class Stream;
            
        //! creates a log category
        void logger_createCategory(const char *category, bool enabled = true, core::Stream *pStream = 0);
        //! destroys a log category
        void logger_destroyCategory(const char *category);
        //! enables a log category
        void logger_enableCategory(const char *category);
        //! returns true if category is enabled
        bool logger_isCategoryEnabled(const char *category);
        //! returns true if category exists
        bool logger_categoryExists(const char *category);
        //! disables a log category
        void logger_disableCategory(const char *category);
        //! sets a category stream
        void logger_setCategoryStream(const char *category, core::Stream *pStream);
        //! returns the category stream
        core::Stream* logger_getCategoryStream(const char *category);
        
        //! prints to a category
        void log_info(const char *category, int line, const char *file, const char *format, ...);
        //! prints warning to a category
        void log_warning(const char *category, int line, const char *file, const char *format, ...);
        //! prints error to a category
        void log_error(const char *category, int line, const char *file, const char *format, ...);
        
        //! prints to a category
        void log_info(const char *category, int line, const char *file, const char *format, va_list args);
        //! prints warning to a category
        void log_warning(const char *category, int line, const char *file, const char *format, va_list args);
        //! prints error to a category
        void log_error(const char *category, int line, const char *file, const char *format, va_list args);
    };
};

//////////////////////////////////////////////////////////////////////////

#if defined(NIMBLE_DEBUG) || defined(NIMBLE_PROFILE)
    #define NIMBLE_LOG_INFO(category, format, ...) nimble::core::log_info(category, __LINE__, __FILE__, format, ##__VA_ARGS__)
    #define NIMBLE_LOG_WARNING(category, format, ...) nimble::core::log_warning(category, __LINE__, __FILE__, format, ##__VA_ARGS__)
    #define NIMBLE_LOG_ERROR(category, format, ...) nimble::core::log_error(category, __LINE__, __FILE__, format, ##__VA_ARGS__)
#else
    #define NIMBLE_LOG_INFO(category, format, ...)
    #define NIMBLE_LOG_WARNING(category, format, ...)
    #define NIMBLE_LOG_ERROR(category, format, ...)
#endif

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////