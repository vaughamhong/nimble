//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_core_assert_h__
#define __nimble_core_assert_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace core{
    
    //! asserts on expresssion and halts
    //! \param expression the expression to assert on
    //! \param line the assert line
    //! \param file the assert file
    //! \param format the message format
    //! \param ... variable argument list
    void assert_print(bool expression, int line = 0, const char *file = 0, const char *format = 0, ...);
    
    //! asserts on expresssion and halts
    //! \param expression the expression to assert on
    //! \param line the check line
    //! \param file the check file
    //! \param format the message format
    //! \param ... variable argument list
    void check_print(bool expression, int line = 0, const char *file = 0, const char *format = 0, ...);
};};

//////////////////////////////////////////////////////////////////////////

#define NIMBLE_ASSERT(expr) nimble::core::assert_print(expr, __LINE__, __FILE__);
#define NIMBLE_ASSERT_PRINT(expr, format, ...) nimble::core::assert_print(expr, __LINE__, __FILE__, format, ##__VA_ARGS__);
#define NIMBLE_CHECK_PRINT(expr, format, ...) nimble::core::check_print(expr, __LINE__, __FILE__, format, ##__VA_ARGS__);

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////