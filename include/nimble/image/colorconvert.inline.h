//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

//////////////////////////////////////////////////////////////////////////

// TODO: wow color conversion, such optimization candidate

//////////////////////////////////////////////////////////////////////////

// converts R5G6B5 to R8G8B8A8
template <>
inline void convertFormat<colorR5G6B5_t, colorR8G8B8A8_t>(colorR5G6B5_t const *src, colorR8G8B8A8_t *dest, int32_t width, int32_t height){
    for(int32_t i = 0; i < width * height; i++){
        image::colorR5G6B5_t const &in = src[i];
        image::colorR8G8B8A8_t &out = dest[i];
        uint8_t r5 = (uint8_t)((in.value & 0xF800) >> 11);
        uint8_t g6 = (uint8_t)((in.value & 0x07E0) >> 5);
        uint8_t b5 = (uint8_t)((in.value & 0x001F) >> 0);
        out.rgba.r = (r5 << 3) | (r5 >> 2);    //!< map the top 3 bits to the bottom 3
        out.rgba.g = (g6 << 2) | (g6 >> 4);    //!< map the top 2 bits to the bottom 2
        out.rgba.b = (b5 << 3) | (b5 >> 2);    //!< map the top 3 bits to the bottom 3
        out.rgba.a = 255;
    }
}
// converts R8G8B8A8 to R5G6B5
template <>
inline void convertFormat<colorR8G8B8A8_t, colorR5G6B5_t>(colorR8G8B8A8_t const *src, colorR5G6B5_t *dest, int32_t width, int32_t height){
    for(int32_t i = 0; i < width * height; i++){
        image::colorR8G8B8A8_t const &in = src[i];
        image::colorR5G6B5_t &out = dest[i];
        uint8_t r = in.rgba.r;
        uint8_t g = in.rgba.g;
        uint8_t b = in.rgba.b;
        out.rgb.r = (r >> 3);       //!< chop off lower bits
        out.rgb.g = (g >> 2);       //!< chop off lower bits
        out.rgb.b = (b >> 3);       //!< chop off lower bits
    }
}

//////////////////////////////////////////////////////////////////////////

// converts R5G6B5 to B5G6R5
template <>
inline void convertFormat<colorR5G6B5_t, colorB5G6R5_t>(colorR5G6B5_t const *src, colorB5G6R5_t *dest, int32_t width, int32_t height){
    for(int32_t i = 0; i < width * height; i++){
        dest[i].bgr.r = src[i].rgb.r;
        dest[i].bgr.g = src[i].rgb.g;
        dest[i].bgr.b = src[i].rgb.b;
    }
}
// converts B5G6R5 to R5G6B5
template <>
inline void convertFormat<colorB5G6R5_t, colorR5G6B5_t>(colorB5G6R5_t const *src, colorR5G6B5_t *dest, int32_t width, int32_t height){
    for(int32_t i = 0; i < width * height; i++){
        dest[i].rgb.r = src[i].bgr.r;
        dest[i].rgb.g = src[i].bgr.g;
        dest[i].rgb.b = src[i].bgr.b;
    }
}

//////////////////////////////////////////////////////////////////////////

// converts R8G8B8 to B8G8R8
template <>
inline void convertFormat<colorR8G8B8_t, colorB8G8R8_t>(colorR8G8B8_t const *src, colorB8G8R8_t *dest, int32_t width, int32_t height){
    for(int32_t i = 0; i < width * height; i++){
        dest[i].bgr.r = src[i].rgb.r;
        dest[i].bgr.g = src[i].rgb.g;
        dest[i].bgr.b = src[i].rgb.b;
    }
}
// converts B8G8R8 to R8G8B8
template <>
inline void convertFormat<colorB8G8R8_t, colorR8G8B8_t>(colorB8G8R8_t const *src, colorR8G8B8_t *dest, int32_t width, int32_t height){
    for(int32_t i = 0; i < width * height; i++){
        dest[i].rgb.r = src[i].bgr.r;
        dest[i].rgb.g = src[i].bgr.g;
        dest[i].rgb.b = src[i].bgr.b;
    }
}

//////////////////////////////////////////////////////////////////////////

// converts R8G8B8A8 to B8G8R8A8
template <>
inline void convertFormat<colorR8G8B8A8_t, colorB8G8R8A8_t>(colorR8G8B8A8_t const *src, colorB8G8R8A8_t *dest, int32_t width, int32_t height){
    for(int32_t i = 0; i < width * height; i++){
        dest[i].bgra.r = src[i].rgba.r;
        dest[i].bgra.g = src[i].rgba.g;
        dest[i].bgra.b = src[i].rgba.b;
        dest[i].bgra.a = src[i].rgba.a;
    }
}
// converts B8G8R8A8 to R8G8B8A8
template <>
inline void convertFormat<colorB8G8R8A8_t, colorR8G8B8A8_t>(colorB8G8R8A8_t const *src, colorR8G8B8A8_t *dest, int32_t width, int32_t height){
    for(int32_t i = 0; i < width * height; i++){
        dest[i].rgba.r = src[i].bgra.r;
        dest[i].rgba.g = src[i].bgra.g;
        dest[i].rgba.b = src[i].bgra.b;
        dest[i].rgba.a = src[i].bgra.a;
    }
}

//////////////////////////////////////////////////////////////////////////