//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_image_image_h__
#define __nimble_image_image_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/resource/resource.h>
#include <nimble/resource/resourceloader.h>
#include <nimble/core/Lockable.h>
#include <nimble/core/buffer.h>

//////////////////////////////////////////////////////////////////////////

#define IMAGE_TUPLESET \
IMAGE_TUPLE(kImageFormatR8G8B8,         sizeof(char) * 3) \
IMAGE_TUPLE(kImageFormatR16G16B16,      sizeof(short) * 3) \
IMAGE_TUPLE(kImageFormatRGBF,           sizeof(float) * 3) \
\
IMAGE_TUPLE(kImageFormatR8G8B8A8,       sizeof(char) * 4) \
IMAGE_TUPLE(kImageFormatR16G16B16A16,   sizeof(short) * 4) \
IMAGE_TUPLE(kImageFormatRGBAF,          sizeof(float) * 4) \
\
IMAGE_TUPLE(kImageFormatB8G8R8,         sizeof(char) * 3) \
IMAGE_TUPLE(kImageFormatB8G8R8A8,       sizeof(char) * 4) \
\
IMAGE_TUPLE(kImageFormatDepth16,        sizeof(short) * 2) \
IMAGE_TUPLE(kImageFormatDepth24,        sizeof(short) * 3) \
IMAGE_TUPLE(kImageFormatDepth32,        sizeof(short) * 4)

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace image{
        
        //! Image formats
        #define IMAGE_TUPLE(ENUM, PIXEL_SIZE) ENUM,
            enum eImageFormat{
                IMAGE_TUPLESET
                
                kMaxImageFormats,
                kImageFormatUnknown = -1,
            };
        #undef IMAGE_TUPLE

		//! image definition
		class Image{
        private:
            
            uint32_t            m_width;
            uint32_t            m_height;
            image::eImageFormat m_format;
            
            core::Buffer<>      m_buffer;
            
		public:

            //! Constructor
            Image();
            //! Cosntructor
            Image(uint32_t width, uint32_t height, image::eImageFormat format);
            //! Constructor
            Image(const char *path);
            //! Construtor
            Image(Image const &image);
            //! Destructor
			virtual ~Image();
            
        public:
            
            //! Load from file
            virtual void loadFromFile(const char *path);
            
		public:

            //! returns true if image is empty
            virtual bool isEmpty();
            
            //! gets the format of this image
			virtual image::eImageFormat getFormat() const;
			//! gets the width of the image
			virtual uint32_t getWidth() const;
			//! gets the height of the image
			virtual uint32_t getHeight() const;
            
            //!	gets the size in bytes for a single pixel
			virtual uint32_t getBytesPerPixel() const;
            //! gets the size of our image in bytes
            virtual uint32_t getSize() const;
            
            //! gets the image buffer
            virtual char* getBuffer() const;
            
        public:
            
            //! Copies an image
            virtual void copy(const char *pData, size_t size);
            //! Copies an image
            void operator=(image::Image const &image);            
		};
        
        //! Image file resource loader
        class ImageLoader
        : public resource::IResourceLoader{
        public:
            
            //! Constructor
            ImageLoader();
            //! Destructor
            virtual ~ImageLoader();
            
            //! loads a resource
            //! \param path the path of the file we want to load
            virtual resource::IResource* loadResource(const char* path);
        };
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////