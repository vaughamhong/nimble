//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_physics_rigidBody_h__
#define __nimble_physics_rigidBody_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>

//////////////////////////////////////////////////////////////////////////

class btRigidBody;
class btCollisionShape;
struct btDefaultMotionState;

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace scene{
        class SceneTransform;
    };
    namespace physics{
        
        //! Contains data needed for a rigid body
        class rigidBody{
        public:
            
            scene::SceneTransform       *m_pSceneTransform;
            
            float                       m_mass;
            btRigidBody                 *m_pRigidBody;
            btDefaultMotionState        *m_pMotionState;
            btCollisionShape            *m_pCollisionShape;
            
            int16_t                     m_groupMask;
            int16_t                     m_collisionMask;
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////