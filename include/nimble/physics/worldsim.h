//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_physics_worldsim_h__
#define __nimble_physics_worldsim_h__

//////////////////////////////////////////////////////////////////////////

#include <vector>
#include <nimble/core/thread.mutex.h>

//////////////////////////////////////////////////////////////////////////

class btBroadphaseInterface;
class btDefaultCollisionConfiguration;
class btCollisionDispatcher;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace physics{
		class rigidBody;

        //! Contains data needed for world simulations
        class WorldSim{
        public:
            
            core::Mutex                         m_lock;
            
            btBroadphaseInterface               *m_pBroadphaseInterface;
            btDefaultCollisionConfiguration     *m_pCollisionConfiguration;
            btCollisionDispatcher               *m_pCollisionDispatcher;
            btSequentialImpulseConstraintSolver *m_pSequentialImpulseConstraintSolver;
            btDiscreteDynamicsWorld             *m_pDiscreteDynamicsWorld;
            
			std::vector<rigidBody*>             m_rigidBodies;
            
        public:
            
            //! Constructor
            WorldSim();
            //! Destructor
            ~WorldSim();
            
            //! Adds a rigid body
			void addRigidBody(physics::rigidBody *pRigidBody);
            //! Removes a rigid body
			void removeRigidBody(physics::rigidBody *pRigidBody);
        };
    };
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////