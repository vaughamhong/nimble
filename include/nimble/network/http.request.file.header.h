//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_httpfileheaderrequest_h__
#define __nimble_network_httpfileheaderrequest_h__

///////////////////////////////////////////////////////////////////////////////

#include <nimble/network/http.request.h>

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{

        //! Manages data associated with a http file header request
        class HttpFileHeaderRequest
        : public HttpRequest{
        public:
            
            //! Constructor
            HttpFileHeaderRequest(Url const &url);
            //! Constructor
            HttpFileHeaderRequest(HttpFileHeaderRequest const &rhs);
            //! Destructor
            virtual ~HttpFileHeaderRequest();
            
        public:
            
            //! Assignment operator
            void operator=(HttpFileHeaderRequest const &rhs);
        };
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////