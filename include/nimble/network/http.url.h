//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_url_h__
#define __nimble_network_url_h__

///////////////////////////////////////////////////////////////////////////////

#include <string>

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{
        
        //! Manages data associated with a URL
        class Url{
        public:
            
            typedef std::string LocationType;
            
        private:
            
            LocationType m_location;
            std::string m_host;
            std::string m_port;
            std::string m_path;
            std::string m_protocol;
            
        public:
            
            //! Constructor
            Url();
            //! Constructor
            Url(LocationType location);
            //! Destructor
            virtual ~Url();
            
        public:
            
            //! Returns the location
            LocationType getLocation() const;
            
            //! Returns the host
            std::string getHost() const;
            //! Returns the port
            std::string getPort() const;
            //! Returns the path
            std::string getPath() const;
            //! Returns the protocol
            std::string getProtocol() const;
        };
        
        //! parses url into components
        void parseUrl(const char *url, std::string &protocol, std::string &host, std::string &port, std::string &path);
        //! resolves absolute path
        void resolveAbsolutePath(const char *pRemoteRootDir, const char *pPath, std::string &absolutePath);
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////