//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_httprequest_h__
#define __nimble_network_httprequest_h__

///////////////////////////////////////////////////////////////////////////////

#include <nimble/network/http.url.h>
#include <nimble/network/http.message.h>

///////////////////////////////////////////////////////////////////////////////

#define HTTPREQUEST_METHOD_TUPLESET \
HTTPREQUEST_METHOD_TUPLE(kGet) \
HTTPREQUEST_METHOD_TUPLE(kPost) \
HTTPREQUEST_METHOD_TUPLE(kPut) \
HTTPREQUEST_METHOD_TUPLE(kDelete)

#define HTTPREQUEST_OPTION_TUPLESET \
HTTPREQUEST_OPTION_TUPLE(kOptionDebugPrint) \
HTTPREQUEST_OPTION_TUPLE(kOptionFollowRedirect) \
HTTPREQUEST_OPTION_TUPLE(kOptionSSLVerifyPeer) \
HTTPREQUEST_OPTION_TUPLE(kOptionSSLVerifyHost) \
HTTPREQUEST_OPTION_TUPLE(kOptionEnableGZipEncoding)

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{

        //! Manages data associated with a http request
        class HttpRequest
        : public HttpMessage{
        public:
            
            enum eMethod{
            #define HTTPREQUEST_METHOD_TUPLE(ENUM) ENUM,
                HTTPREQUEST_METHOD_TUPLESET
            #undef HTTPREQUEST_METHOD_TUPLE
            };
            
            enum eOption{
            #define HTTPREQUEST_OPTION_TUPLE(ENUM) ENUM,
                HTTPREQUEST_OPTION_TUPLESET
            #undef HTTPREQUEST_OPTION_TUPLE
                kNumOptions
            };
            
        private:
            
            eMethod     m_method;
            Url         m_url;
            bool        m_options[kNumOptions];
            int32_t     m_timeoutInSeconds;
            
        public:
            
            //! Constructor
            HttpRequest();
            //! Constructor
            HttpRequest(Url const &url);
            //! Constructor
            HttpRequest(eMethod method, Url const &url);
            //! Constructor
            HttpRequest(HttpRequest const &rhs);
            //! Destructor
            virtual ~HttpRequest();
            
        public:
            
            //! Assignment operator
            void operator=(HttpRequest const &rhs);
            
        public:
            
            //! Sets timeout in seconds
            void setTimeout(int32_t seconds);
            //! Gets timeout in seconds
            int32_t getTimeout() const;
            
            //! Sets the url for this request
            void setUrl(Url const &url);
            //! Gets the url for this request
            Url getUrl() const;
            
            //! Sets the method for this request
            void setMethod(eMethod const &method);
            //! Gets the method for this request
            eMethod getMethod() const;
            
            //! Enable an option
            void enableOption(eOption const &option);
            //! Disable an option
            void disableOption(eOption const &option);
            //! Returns option value
            bool getOption(eOption const &option) const;
        };
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////