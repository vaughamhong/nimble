//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_httpfiledownloadrequest_h__
#define __nimble_network_httpfiledownloadrequest_h__

///////////////////////////////////////////////////////////////////////////////

#include <nimble/network/http.request.h>

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{

        //! Manages data associated with a http file download request
        class HttpFileDownloadRequest
        : public HttpRequest{
        public:
            
            typedef std::string PathType;
            
        private:
            
            PathType m_localPath;
            
        public:
            
            //! Constructor
            HttpFileDownloadRequest(PathType const &localPath, Url const &url);
            //! Constructor
            HttpFileDownloadRequest(HttpFileDownloadRequest const &rhs);
            //! Destructor
            virtual ~HttpFileDownloadRequest();
            
        public:
            
            //! Assignment operator
            void operator=(HttpFileDownloadRequest const &rhs);
            
        public:
            
            //! Sets the local path for this request
            void setLocalPath(PathType const &localPath);
            //! Gets the local path for this request
            PathType getLocalPath() const;
        };
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////