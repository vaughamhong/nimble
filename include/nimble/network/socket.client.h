//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_socket_client_h__
#define __nimble_network_socket_client_h__

///////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <string>

#if defined(NIMBLE_TARGET_WIN32)
#pragma message("TODO - IMPL network socket client")
#else
#include <sys/socket.h>
#endif

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{
        
        //! Manages a socket client
        class SocketClient{
        private:
            
            std::string m_host;
            int32_t m_port;
            int32_t m_socketFileDescriptor;
            int32_t m_network;
            int32_t m_type;
            bool  m_isConnected;
            
        public:
            
            //! Constructor
            SocketClient(int network, int type);
            //! Destructor
            virtual ~SocketClient();
            
        public:
            
            //! Connects to a host
            void connect(const char *host, int port);
            //! Disconnect
            void disconnect();
            
            //! Receives data from a host
            void read(char *pBuffer, uint32_t size) const;
            //! Sends data to a host
            void write(const char *pBuffer) const;
            
            //! Check how many bytes are ready to be read
            int32_t bytesReady() const;
            //! Returns true if we are connected
            bool isConnected() const;
        };
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////