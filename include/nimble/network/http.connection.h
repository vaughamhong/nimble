//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_httpconnection_h__
#define __nimble_network_httpconnection_h__

///////////////////////////////////////////////////////////////////////////////

#include <nimble/network/http.request.h>
#include <nimble/network/http.response.h>

///////////////////////////////////////////////////////////////////////////////

typedef void CURLM;
typedef void CURL;

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{
        
        class HttpConnection{
		private:

			CURL *m_pCurlHandle;

        public:
            
            //! Constructor
            HttpConnection();
            //! Destructor
            virtual ~HttpConnection();
            
        public:
            
            //! Dispatches a request
            HttpResponse dispatch(HttpRequest const &request);
        };
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////