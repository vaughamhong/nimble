//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_httpresponse_h__
#define __nimble_network_httpresponse_h__

///////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/network/http.message.h>

///////////////////////////////////////////////////////////////////////////////

#define HTTPRESPONSE_CODE_TUPLESET \
HTTPRESPONSE_CODE_TUPLE(kResponseCodeSuccess, 200) \
HTTPRESPONSE_CODE_TUPLE(kResponseCodeFailure, 404) \
HTTPRESPONSE_CODE_TUPLE(kResponseCodeRedirect, 302)

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{

        //! Response code
        enum eResponseCode{
        #define HTTPRESPONSE_CODE_TUPLE(ENUM, VALUE) ENUM = VALUE,
            HTTPRESPONSE_CODE_TUPLESET
        #undef HTTPRESPONSE_CODE_TUPLE
        };

        //! Manages data associated with a http response
        class HttpResponse
        : public HttpMessage{
        private:
            
            eResponseCode m_code;
            
        public:
            
            //! Constructor
            HttpResponse();
            //! Constructor
            HttpResponse(eResponseCode code);
            //! Constructor
            HttpResponse(HttpResponse const &rhs);
            //! Destructor
            virtual ~HttpResponse();
            
        public:
            
            //! Assignment operator
            void operator=(HttpResponse const &rhs);
            
        public:
            
            //! Sets our response code
            void setCode(eResponseCode const &code);
            //! Returns our response code
            eResponseCode getCode() const;
        };
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////