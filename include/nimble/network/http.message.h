//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_httpmessage_h__
#define __nimble_network_httpmessage_h__

///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/network/http.headeritem.h>

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{

        //! Manages data associated with a http message
        class HttpMessage{
        public:
            
            typedef std::string                 BodyType;
            typedef std::vector<HttpHeaderItem> Headers;
            typedef Headers::iterator           HeaderIterator;
            typedef Headers::const_iterator     HeaderConstIterator;
            
        private:
            
            Headers     m_headers;
            BodyType    m_body;
            
        public:
            
            //! Constructor
            HttpMessage();
            //! Constructor
            HttpMessage(HttpMessage const &rhs);
            //! Destructor
            virtual ~HttpMessage();
            
        public:
            
            //! Assignment operator
            void operator=(HttpMessage const &rhs);
            
        public:
            
            //! Sets body
            void setBody(BodyType body);
            //! Appends to our body
            void appendBody(BodyType append);
            //! Gets body
            BodyType getBody() const;
            
        public:
            
            //! Adds a header item
            void addHeaderItem(HttpHeaderItem const &item);
            //! Returns the number of header items
            uint32_t getNumHeaderItems() const;
            //! Returns the start iterator for our headers
            HeaderIterator findHeaderWithKey(network::HttpHeaderItem::KeyType const &key);
            //! Returns the start iterator for our headers
            HeaderIterator headerItemsBegin();
            //! Returns the end iterator for our headers
            HeaderIterator headerItemsEnd();
            //! Returns the start iterator for our headers
            HeaderConstIterator headerItemsBegin() const;
            //! Returns the end iterator for our headers
            HeaderConstIterator headerItemsEnd() const;
        };
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////