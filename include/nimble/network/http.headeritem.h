//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_httpheader_h__
#define __nimble_network_httpheader_h__

///////////////////////////////////////////////////////////////////////////////

#include <string>

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{
        
        //! Represents a single http header item
        class HttpHeaderItem{
        public:
            
            typedef std::string KeyType;
            typedef std::string ValueType;
            
        private:
            
            KeyType m_key;
            ValueType m_value;
            
        public:
            
            //! Constructor
            HttpHeaderItem();
            //! Constructor
            HttpHeaderItem(KeyType key, ValueType value);
            //! Constructor
            HttpHeaderItem(HttpHeaderItem const &rhs);
            //! Destructor
            virtual ~HttpHeaderItem();
            
        public:
            
            //! Assignment operator
            void operator=(HttpHeaderItem const &rhs);
            
        public:
            
            //! Sets our key
            void setKey(KeyType key);
            //! Gets a key
            KeyType getKey() const;
            
            //! Sets our value
            void setValue(ValueType value);
            //! Gets a value
            ValueType getValue() const;
        };
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////