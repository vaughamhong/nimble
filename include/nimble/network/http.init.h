//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_network_httpinit_h__
#define __nimble_network_httpinit_h__

///////////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace network{

		//! initialize
		void http_init();
		//! terminate
		void http_terminate();
	};
};

///////////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////