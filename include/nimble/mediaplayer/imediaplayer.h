//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_media_imediaplayer_h__
#define __nimble_media_imediaplayer_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
    namespace renderdevice{
        class ITexture;
    };
    namespace core{
        class ILockable;
    };
	namespace media{

		//! An abstract media player
		class IMediaPlayer{
        public:
            
            //! Constructor
            IMediaPlayer(){}
            //! Destructor
            virtual ~IMediaPlayer(){}
            
        public:
            
            //! Initialize
            virtual void initialize(const char *pPath, renderdevice::ITexture *pRenderTarget, size_t argc, const char *argv[]) = 0;
            //! Destroy
            virtual void destroy() = 0;
            
        public:
            
            //! Play video
            virtual void play() = 0;
            //! Pause video
            virtual void pause() = 0;
            //! Stop video
            virtual void stop() = 0;
            //! Restart video
            virtual void restart() = 0;
			//! Returns true if valid
			virtual bool isValid() const = 0;
            //! Returns true if playing
            virtual bool isPlaying() const = 0;
            
		public:

            //! Returns our path
            virtual const char* getPath() const = 0;
            
            //! Returns our lock target
            virtual core::ILockable* getLockTarget() const = 0;

            //! Gets our buffer
            virtual char* getBuffer() const = 0;
            //! Gets the width of the buffer
			virtual size_t getBufferWidth() const = 0;
			//! Gets the height of the buffer
			virtual size_t getBufferHeight() const = 0;
            //! Gets our bytes per pixel
            virtual size_t getBytesPerPixel() const = 0;
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////