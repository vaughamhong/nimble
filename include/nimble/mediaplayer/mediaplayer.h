//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_media_mediaplayer_h__
#define __nimble_media_mediaplayer_h__

//////////////////////////////////////////////////////////////////////////

#include <string>
#include <stdint.h>
#include <nimble/mediaplayer/imediaplayer.h>
#include <nimble/core/ilockable.h>
#include <nimble/core/buffer.h>

#if !defined(NIMBLE_TARGET_ANDROID)
#include <vlc/vlc.h>
#endif

//////////////////////////////////////////////////////////////////////////

// TODO
// + Impl MediaPlayer pixel format settings
// + Consider using YUV internal format (which matches VLC) and convert on GPU shader to save additional CPU
// + Buffer (below) may want to come from a shared scratch pad of some sort

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace media{

		//! A media player
		class MediaPlayer
        : public media::IMediaPlayer{
        public:
            
#if !defined(NIMBLE_TARGET_ANDROID)
            libvlc_instance_t       *m_pVLCHandle;
            libvlc_media_player_t   *m_pMediaPlayerHandle;
#endif
			core::ILockable         *m_pLockTarget;

            core::Buffer<>          m_buffer;
            std::string             m_path;
            size_t                  m_bufferWidth;
            size_t                  m_bufferHeight;
            size_t                  m_bytesPerPixel;
            
		public:

            //! Cosntructor
            MediaPlayer();
            //! Destructor
			virtual ~MediaPlayer();
            
        public:
            
            //! Initialize
            virtual void initialize(const char *pPath, renderdevice::ITexture *pRenderTarget, size_t argc, const char *argv[]);
            //! Destroy
            virtual void destroy();
            
        public:
            
            //! Play video
            virtual void play();
            //! Pause video
            virtual void pause();
            //! Stop video
            virtual void stop();
            //! Restart video
            virtual void restart();
			//! Returns true if valid
			virtual bool isValid() const;
            //! Returns true if playing
            virtual bool isPlaying() const;
            
		public:

            //! Returns our path
            virtual const char* getPath() const;            
            //! Returns our lock target
            virtual core::ILockable* getLockTarget() const;
            
            //! Gets our buffer
            virtual char* getBuffer() const;
            //! Gets the width of the buffer
			virtual size_t getBufferWidth() const;
			//! Gets the height of the buffer
			virtual size_t getBufferHeight() const;
            //! Gets our bytes per pixel
            virtual size_t getBytesPerPixel() const;
            
		private:

#if !defined(NIMBLE_TARGET_ANDROID)
			//! Called when we receive an event regarding the media we are playing
			static void mediaEventCallback(const struct libvlc_event_t *event, void *data);
			//! Called when we receive an event regarding the media player
			static void mediaPlayerEventCallback(const struct libvlc_event_t *event, void *data);
			//! A media sub item was added
			virtual void mediaSubItemWasAdded();
#endif
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////