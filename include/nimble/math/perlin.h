//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_math_perlin_h__
#define __nimble_math_perlin_h__

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace math{

        // generates a perlin noise
        double pnoise(double x, double y, double z);
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////