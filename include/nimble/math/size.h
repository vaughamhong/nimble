//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_math_size_h__
#define __nimble_math_size_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/math/vector.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace math{
        
        //! size types
        typedef Vector2<int> Size2i;
        typedef Vector3<int> Size3i;
        typedef Vector2<float> Size2f;
        typedef Vector3<float> Size3f;
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

///////////////////////////////////////////////////////////////////////////////