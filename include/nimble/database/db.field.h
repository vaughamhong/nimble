//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_db_db_field_h__
#define __nimble_db_db_field_h__

////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/core/stringutil.h>
#include <nimble/core/error.h>
#include <nimble/core/assert.h>
#include <string>

////////////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace db{
    
    //! a database record field
    class Field{
    public:
        
        //! field types
        enum eType{
            kTypeNull,
            kTypeInteger,
            kTypeReal,
            kTypeText,
            kTypeBlob
        };
        
    private:
        
        eType m_type;
        std::string m_data;
        
    public:
        
        //! Constructor
        Field();
        //! Constructor
        Field(eType type, std::string const &data);
        //! Constructor
        Field(const Field &field);
        //! Constructor
        Field(const int32_t &value);
        //! Constructor
        Field(const double &value);
        //! Constructor
        Field(const char *value, bool isText);
        //! Destructor
        ~Field();
        
    public:
        
        //! returns the field type
        eType getType() const;
        //! returns true if null
        bool isNull() const;
        //! returns our integer value
        int32_t getInteger() const;
        //! returns our real value
        double getReal() const;
        //! returns our text value
        const char* getText() const;
        //! returns our blob value
        const char* getBlob() const;
    
    public:
    
        //! sets our value
        template <typename T>
        Field& operator=(T &value){
            NIMBLE_ASSERT(getType() != Field::kTypeNull);
            m_data = core::lexical_cast<T>(value);
            return *this;
        }
    };
    static const Field kNull;
}
}

////////////////////////////////////////////////////////////////////////////////

#endif
