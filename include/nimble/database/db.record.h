//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_db_db_record_h__
#define __nimble_db_db_record_h__

////////////////////////////////////////////////////////////////////////////////

#include <nimble/database/db.field.h>
#include <map>

////////////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace db{
    
    //! a database record
    class Record{
    public:
        
        static db::Field m_nullField;
        typedef std::map<std::string, db::Field> RowData;
        RowData m_rowData;
        
    public:
        
        //! Constructor
        Record();
        //! Constructor
        Record(const Record &record);
        //! Destructor
        ~Record();
        
    public:
        
        //! clears this record
        void clear();
        
    public:
        
        //! returns the starting iterator
        RowData::iterator begin();
        //! returns the end iterator
        RowData::iterator end();
        
        //! returns the number of columns
        uint32_t getNumColumns() const;
        //! returns a field from a column name
        db::Field& operator[](std::string columnName);
    };
}
}

////////////////////////////////////////////////////////////////////////////////

#endif
