//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_db_sqlite_connection_h__
#define __nimble_db_sqlite_connection_h__

////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/core/error.h>
#include <nimble/database/db.record.h>
#include <nimble/database/sqlite3/sqlite3.h>
#include <vector>
#include <map>
#include <string>

////////////////////////////////////////////////////////////////////////////////

#define DBERRORS_TUPLESET \
DBERRORS_TUPLE(kBeginTransactionFail,       200,     "db",      "Begin transaction failed") \
DBERRORS_TUPLE(kCommitTransactionFail,      201,     "db",      "Commit transaction failed") \
DBERRORS_TUPLE(kRollbackTransactionFail,    202,     "db",      "Rollback transaction failed") \
DBERRORS_TUPLE(kMultipleRecordsFound,       203,     "db",      "Multiple records found") \
DBERRORS_TUPLE(kInvalidDBConnection,        204,     "db",      "Invalid database connection")

////////////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace db{
    class ConnectionPool;
    
    //! various helpful types
    typedef sqlite_int64 PrimaryKey;
    typedef std::vector<PrimaryKey> PrimaryKeys;
    typedef std::vector<db::Record> Records;
    
    //! a connection abstraction
    class Connection {
    friend class ConnectionPool;
    private:
        
        static const int32_t kBusyMaxRetries = 100;
        static const int32_t kBusyWaitMs = 20;
        
        sqlite3 *m_pHandle;
        std::string m_dbPath;
        bool m_isWritable;
        bool m_inTransaction;
        int32_t m_busyMaxRetries;
        int32_t m_busyWaitMs;
        
    public:
    
        //! static creation method
        static core::Error create(Connection **ppConnection, const char *dbPath, bool isWritable, int32_t busyMaxRetries = kBusyMaxRetries, int32_t busyWaitMs = kBusyWaitMs);
        //! Destructor
        virtual ~Connection();
        
    private:
        
        //! Constructor
        Connection(const char *dbPath, bool isWritable, int32_t busyMaxRetries = kBusyMaxRetries, int32_t busyWaitMs = kBusyWaitMs);
        
    public:
        
        //! returns the busy max retries
        int32_t getBusyMaxRetries() const;
        //! returns the busy wait ms
        int32_t getBusyWaitMs() const;
        //! returns true if this is a write connection
        bool isWritable() const;
        
    private:
        
        //! validates connection
        bool isConnectionValid();
        //! called when trying to access locked table
        static int32_t sqliteBusyHandler(void *data, int32_t retry);
        
    public:
        
        //! runs a database script on the current database
        core::Error runScript(const char *scriptPath);
        
        //! starts a transaction
        core::Error beginTransaction();
        //! commits a transaction
        core::Error commitTransaction();
        //! rolls back a transaction
        core::Error rollbackTransaction();
        
        //! finds a record given a query
        core::Error findRecord(Record &record, const char *table, PrimaryKey pk);
        //! returns a record that matches a set of conditions
        core::Error match(Records &records, const char *table, ...);
        //! inserts a record
        core::Error insert(PrimaryKey &pk, const char *table, ...);
        //! removes a record
        core::Error remove(const char *table, PrimaryKey pk);
        //! Updates a record
        core::Error update(const char *table, PrimaryKey pk, ...);
        //! inserts or updates a record
        core::Error insertOrUpdate(bool &inserted, PrimaryKey &pk, const char *table, ...);
    };
    
#define DBERRORS_TUPLE(VAR, CODE, CATEGORY, STRING) extern nimble::core::Error VAR;
    DBERRORS_TUPLESET
#undef DBERRORS_TUPLE
}
}

////////////////////////////////////////////////////////////////////////////////

#endif
