//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_db_sqlite_connectionpool_h__
#define __nimble_db_sqlite_connectionpool_h__

////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <nimble/database/sqlite.connection.h>
#include <nimble/core/thread.mutex.h>

////////////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace db{
        
    //! manages a set of database connection
    class ConnectionPool{
    private:
        
        std::string m_dbPath;
        int32_t m_maxConnections;
        bool m_isWritable;
        core::Mutex m_connectionsLock;
        
        typedef std::vector<Connection*> ConnectionList;
        ConnectionList m_connections;
        ConnectionList m_freeConnections;
        
    public:
        
        //! Constructor
        ConnectionPool(const char* dbPath, int32_t maxConnections, bool isWritable = true);
        //! Destructor
        ~ConnectionPool();

    public:
        
        //! returns the database path
        const char* getDbPath() const;
        //! returns the number of free connections
        uint32_t getNumFreeConnections();
        //! returns if this pool contains writable connections
        bool isWritableConnectionPool() const;
        
        //! acquires a connection from the pool
        Connection* acquire();
        //! releases a connection back into the pool
        void release(Connection *pConnection);
        
    private:
        
        //! owns this connection
        bool ownsConnection(Connection *pConnection);
    };
}
}

////////////////////////////////////////////////////////////////////////////////

#endif
