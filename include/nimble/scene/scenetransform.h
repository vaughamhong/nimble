//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_scene_scenetransform_h__
#define __nimble_scene_scenetransform_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/node.h>
#include <nimble/math/axisalignedbox.h>
#include <nimble/math/matrix.h>
#include <nimble/math/vector.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace scene{
        
		//! A scene transformation node
		class SceneTransform
        : public core::Node<SceneTransform>{
        private:
            
            math::AxisAlignedBox3f  m_localBounds;
            math::AxisAlignedBox3f  m_worldBounds;
            
            math::Matrix4x4f        m_localTransform;
            math::Matrix4x4f        m_invLocalTransform;
            math::Matrix4x4f        m_worldTransform;
            
            math::Vector3f          m_translation;
            math::Quaternion4f      m_rotation;
            math::Vector3f          m_scale;
            
        public:
            
            //! Constructor
            SceneTransform();
            //! Destructor
            virtual ~SceneTransform();
            
        public:
            
            //! sets local translation
            virtual void setTranslation(math::Vector3f const &translation);
            //! gets local translation
            virtual math::Vector3f const& getTranslation() const;
            //! sets local rotation
            virtual void setRotation(math::Quaternion4f const &rotation);
            //! gets local rotation
            virtual math::Quaternion4f const& getRotation() const;
            //! sets local scale
            virtual void setScale(math::Vector3f const &scale);
            //! gets local scale
            virtual math::Vector3f const& getScale();
            
        public:
            
            //! returns our transform
            virtual const math::Matrix4x4f& getLocalTransform() const;            
            //! returns our transform
            virtual const math::Matrix4x4f& getWorldTransform() const;
            
            //! returns our inverse transform
            virtual const math::Matrix4x4f getInvLocalTransform() const;
            //! returns our inverse transform
            virtual const math::Matrix4x4f getInvWorldTransform() const;
            
        public:
            
            //! returns the local bounds
            virtual math::AxisAlignedBox3f getLocalBounds() const;
            //! returns the world bounds
            virtual math::AxisAlignedBox3f getWorldBounds() const;
            //! sets the local bounds
            virtual void setLocalBounds(math::AxisAlignedBox3f const& bounds);
            
        public:
            
            //! Update local transform
            virtual void updateLocalTransform();
            //! Update world transform
            virtual void updateWorldTransform();
            //! Update world bounds
            virtual void updateWorldBounds();
		};        
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////