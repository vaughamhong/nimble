//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_scene_iscene_h__
#define __nimble_scene_iscene_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/core/node.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace scene{
        class SceneTransform;
        
		//! Abstract scene interface
		class IScene{
		public:
            
            //! Constructor
            IScene(){}
			//! Destructor
			virtual ~IScene(){}
            
        public:
            
            //! Returns the root transform
            virtual scene::SceneTransform* getRootSceneTransform() = 0;
            //! Returns the root transform
            virtual scene::SceneTransform const* getRootSceneTransform() const = 0;
        };
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////