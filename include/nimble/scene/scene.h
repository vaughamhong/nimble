//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_scene_scene_h__
#define __nimble_scene_scene_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <nimble/scene/iscene.h>
#include <nimble/scene/scenetransform.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace scene{
        class SceneTransform;
        
		//! scene interface
		class Scene
        : public scene::IScene
        , public scene::SceneTransform{
		public:
            
            //! Constructor
            Scene();
			//! Destructor
            virtual ~Scene();
            
        public:
            
            //! Returns the root transform
            virtual scene::SceneTransform* getRootSceneTransform();
            //! Returns the root transform
            virtual scene::SceneTransform const* getRootSceneTransform() const;
        };
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////