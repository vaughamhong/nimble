//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_scene_filtervisitor_h__
#define __nimble_scene_filtervisitor_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/core/functor.h>
#include <nimble/scene/scenetransform.h>
#include <vector>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace scene{
        class SceneTransform;
        
        //! Filter a scene hierarchy
		class FilterVisitor{
        public:
            
            typedef core::Functor<bool, TLIST_1(scene::SceneTransform*)> Func;
            
        private:
            
            scene::SceneTransform::ListType	&m_filteredList;
            Func							m_filter;
            
		public:
            
            //! Constructor
            FilterVisitor(scene::SceneTransform::ListType &filteredList, Func filter);
            //! Destructor
            virtual ~FilterVisitor();
            
        public:
            
            //! Executes on an object
            virtual void execute(scene::SceneTransform *pSceneTransform);
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////