//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_scene_flattenvisitor_h__
#define __nimble_scene_flattenvisitor_h__

//////////////////////////////////////////////////////////////////////////

#include <nimble/scene/scenetransform.h>
#include <vector>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace scene{
        class SceneTransform;
        
        //! Flattens a scene hierarchy
        class FlattenVisitor{
        public:
            
            enum eType{
                kBreadthFirst,
                kDepthFirst,
                kDefaultType = kBreadthFirst,
            };
            
        private:
            
            eType                                       m_type;
            scene::SceneTransform::ListType   &m_list;
            
		public:
            
            //! Constructor
            FlattenVisitor(scene::SceneTransform::ListType &list, eType type = kDefaultType);
            //! Destructor
            virtual ~FlattenVisitor();
            
        public:
            
            //! Executes on an object
            virtual void execute(scene::SceneTransform *pSceneTransform);
        };
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////