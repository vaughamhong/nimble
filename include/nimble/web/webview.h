//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_web_webview_h__
#define __nimble_web_webview_h__

//////////////////////////////////////////////////////////////////////////

#include <string>
#include <stdint.h>
#include <nimble/core/ilockable.h>

//////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
#include <Awesomium/WebCore.h>
#endif

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace renderdevice{
		class ITexture;
	};
	namespace web{

		//! A web view
		class WebView{
        private:
            
#if !defined(NIMBLE_TARGET_ANDROID)
            Awesomium::WebCore      *m_pWebCore;
            Awesomium::WebView      *m_pWebView;
#endif
			core::ILockable         *m_pLockTarget;

            std::string             m_path;
            size_t                  m_bufferWidth;
            size_t                  m_bufferHeight;
            size_t                  m_bytesPerPixel;
            
		public:

#if !defined(NIMBLE_TARGET_ANDROID)
            //! Cosntructor
            WebView(Awesomium::WebCore *pWebCore);
#else
            //! Cosntructor
            WebView();
#endif
            //! Destructor
			virtual ~WebView();
            
        public:
            
            //! Setup our internal state
			virtual void setup(renderdevice::ITexture *pRenderTarget);
            
        public:

            //! Loads a site
            virtual void load(const char *pRemotePath);
            //! Reloads a site
            virtual void reload();
            //! Stops loading a site
            virtual void stop();
            
		public:

            //! Gets the width of the buffer
			virtual size_t getBufferWidth() const;
			//! Gets the height of the buffer
			virtual size_t getBufferHeight() const;
            //! Gets our bytes per pixel
            virtual size_t getBytesPerPixel() const;
            //! Gets our lock taget
            virtual core::ILockable* getLockTarget() const;

        public:
            
#if !defined(NIMBLE_TARGET_ANDROID)
            //! Returns the internal web view
            virtual Awesomium::WebView* getWebView() const;
#endif
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////