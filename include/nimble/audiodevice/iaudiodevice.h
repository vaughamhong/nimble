//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_audiodevice_iaudiodevice_h__
#define __nimble_audiodevice_iaudiodevice_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace audiodevice{
        class IAudioBuffer;
        class IAudioPlayer;
        
		//! audio device
		class IAudioDevice{
		public:
            
			//! Destructor
			virtual ~IAudioDevice(){}
            
        public:
            
            //! creates a audio clip
            //! \return a audio clip
            virtual audiodevice::IAudioBuffer* createAudioBuffer(uint32_t frequency, uint32_t bitrate) = 0;
            //! creates a audio clip player
            //! \return a audio clip player
            virtual audiodevice::IAudioPlayer* createAudioPlayer() = 0;
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////
