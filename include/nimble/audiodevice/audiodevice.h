//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_audiodevice_audiobuffer_h__
#define __nimble_audiodevice_audiobuffer_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace audiodevice{
        class IAudioDevice;
        class IAudioBuffer;
        
		//! Creates a audio buffer resource
        //! \param frequency the frquency of the audio buffer
        //! \param bitrate the bitrate of the audio buffer
        //! \param pAudioDevice the AudioDevice to create this resource with
        //! \return a AudioBuffer resource
        audiodevice::IAudioBuffer* createAudioBuffer(uint32_t frequency,
                                                     uint32_t bitrate,
                                                     audiodevice::IAudioDevice *pAudioDevice = 0);
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////