//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_audiodevice_iaudioplayer_h__
#define __nimble_audiodevice_iaudioplayer_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace audiodevice{
        class IAudioBuffer;
        
		//! Plays an audio clip
        class IAudioPlayer{
        public:

			//! Destructor
			virtual ~IAudioPlayer(){}
            
        public:
            
            //! Binds to an audio clip
            virtual void bindAudioBuffer(audiodevice::IAudioBuffer *pAudioBuffer) = 0;
            //! Updates our audio clip player
            virtual void update(double elapsedTime) = 0;
            
        public:
            
            //! Plays the currently bound audio clip
            virtual void play() = 0;
            //! Stops the currently bound audio clip
            virtual void stop() = 0;
            //! Returns true if we have stopped
            virtual bool isPlaying() = 0;
            
        public:
            
            //! Returns the pitch
            virtual float getPitch() = 0;
            //! Returns the gain
            virtual float getGain() = 0;
            
            //! Modifies the pitch of the output
            virtual void setPitch(float pitch) = 0;
            //! Modifies the gain of the output
            virtual void setGain(float gain) = 0;
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif
