//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_audiodevice_iaudiobuffer_h__
#define __nimble_audiodevice_iaudiobuffer_h__

//////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <cstdlib>

//////////////////////////////////////////////////////////////////////////

namespace nimble{
	namespace audiodevice{
        class IAudioDevice;
                
		//! Abstract audio buffer
        class IAudioBuffer{
        public:

			//! Destructor
			virtual ~IAudioBuffer(){}

		public:

            //! Returns the frequency of this audio buffer
            virtual uint32_t getFrequency() const = 0;
            //! Returns the bitrate of this audio buffer
            virtual uint32_t getBitrate() const = 0;
            
            //! copy data
            //! \param data the data to copy
            virtual void copyData(char *pData, size_t size) = 0;
		};
	};
};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////