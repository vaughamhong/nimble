//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/message/messageservice.h>
#include <nimble/core/locator.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::message;

//////////////////////////////////////////////////////////////////////////

//! Constructor
MessageService::MessageService(){
}
//! Destructor
MessageService::~MessageService(){
}
//! Returns the default message service
MessageService* MessageService::get(){
    return core::locator_acquire<message::MessageService>();
}

//////////////////////////////////////////////////////////////////////////

//! Sends a message
//! \param pMessage the message to send
//! \param channel the channel to send the message
void MessageService::send(message::Message* pMessage, const char *channel){
    core::ScopedLock lock(&m_lock);
    message::MessagePublisher *pPublisher = getMessagePublisherByChannel(channel);
    pPublisher->send(pMessage);
}

//////////////////////////////////////////////////////////////////////////

//! removes message observers
//! \param pSubscriber the message subscriber
//! \param channel the channel to remove subscriber
void MessageService::removeSubscriber(message::MessageSubscriber *pSubscriber, const char *channel){
    core::ScopedLock lock(&m_lock);
    message::MessagePublisher *pPublisher = getMessagePublisherByChannel(channel);
    pPublisher->removeSubscriber(pSubscriber);
}
//! removes all message observers
//! \param channel the channel to remove subscriber
void MessageService::removeAllSubscribers(const char *channel){
    core::ScopedLock lock(&m_lock);
    message::MessagePublisher *pPublisher = getMessagePublisherByChannel(channel);
    pPublisher->removeAllSubscribers();
}

//////////////////////////////////////////////////////////////////////////

//! returns the publisher by channel
//! \param channel the channel reference to our publisher
//! \param createIfNotExist create the publisher if it does not exist
message::MessagePublisher* MessageService::getMessagePublisherByChannel(const char *channel, bool createIfNotExist){
    core::ScopedLock lock(&m_lock);
    
    ChannelToPublisherIndex::iterator it = m_channelToPublisherIndex.find(channel);
    if(it != m_channelToPublisherIndex.end()){
        return it->second;
    }else if(createIfNotExist){
        message::MessagePublisher *pNewPublisher = new /*( pool )*/ message::MessagePublisher();
        m_channelToPublisherIndex.insert(std::pair<std::string, message::MessagePublisher*>(channel, pNewPublisher));
        return pNewPublisher;
    }else{
        return 0;
    }
}

//////////////////////////////////////////////////////////////////////////