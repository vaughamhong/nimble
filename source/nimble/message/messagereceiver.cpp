//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/message/messagereceiver.h>
#include <nimble/message/message.h>
#include <nimble/core/thread.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::message;

//////////////////////////////////////////////////////////////////////////

//! Constructor
MessageReceiver::MessageReceiver()
:message::MessageSubscriber(){
}
//! Constructor
//! \param bufferSize the buffer size for our message queue
MessageReceiver::MessageReceiver(uint32_t bufferSize)
:message::MessageSubscriber(bufferSize){
}
//! Destructor
MessageReceiver::~MessageReceiver(){
}

//////////////////////////////////////////////////////////////////////////

//! Process and invoke callbacks (handlers) on received messages
void MessageReceiver::processMessages(){
    core::ScopedLock lock(&m_lock);

    while(!this->isEmpty()){
		Message* pMessage = this->getNextMessage();
        if(pMessage == 0){
            continue;
        }
        
        // try and see if there is a registered handler callback for this message
        MessageId messageId = pMessage->getId();
        MessageHandlerIndex::iterator handlerIt = m_messageHandlerIndex.find(messageId);
        if(handlerIt != m_messageHandlerIndex.end()){
            // use registered handler to respond to message
            handlerIt->second(pMessage);
        }else{
            // use default handler to respond to message
            onMessageReceived(pMessage);
        }
        delete pMessage;
	}
}
//! Callback when a message has been received
//! \param pMessage the message to handle
void MessageReceiver::onMessageReceived(message::Message* pMessage){
}

//////////////////////////////////////////////////////////////////////////