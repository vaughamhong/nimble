//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/message/messagesubscriber.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::message;

//////////////////////////////////////////////////////////////////////////

//! Constructor
MessageSubscriber::MessageSubscriber()
:m_pMessageQueue(new message::MessageQueue()){
}
//! Constructor
//! \param bufferSize the buffer size for our message queue
MessageSubscriber::MessageSubscriber(uint32_t bufferSize)
:m_pMessageQueue(new message::MessageQueue(bufferSize)){
}
//! Destructor
MessageSubscriber::~MessageSubscriber(){
    delete m_pMessageQueue; m_pMessageQueue = 0;
}

//////////////////////////////////////////////////////////////////////////

//! Returns the next message
message::Message* MessageSubscriber::getNextMessage(){
    core::ScopedLock lock(&m_lock);
    return m_pMessageQueue->pop();
}
//! Returns true if there are no more messages left
bool MessageSubscriber::isEmpty() const{
    core::ScopedLock lock(&m_lock);
    return m_pMessageQueue->isEmpty();
}

//////////////////////////////////////////////////////////////////////////

//! adds a message to this subscriber
//! \param pMessage the message to add
void MessageSubscriber::addMessage(const Message* pMessage){
    core::ScopedLock lock(&m_lock);
	m_pMessageQueue->push(pMessage);
}

//////////////////////////////////////////////////////////////////////////