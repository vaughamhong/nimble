//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/message/messagequeue.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::message;

//////////////////////////////////////////////////////////////////////////

//! Constructor
//! \param bufferSize the size of our internal buffer
MessageQueue::MessageQueue(unsigned int bufferSize)
:m_ringBuffer(bufferSize){
}
//! Destructor
MessageQueue::~MessageQueue(){
}

//////////////////////////////////////////////////////////////////////////

//! adds a message to the queue
//! \param pMessage the message to add
//! \return the message in the ring buffer
void MessageQueue::push(const Message* pMessage){
    core::ScopedLock lock(&m_lock);
    MessageId typeId = pMessage->getId();
    if(m_factory.builderExists(typeId)){
        m_ringBuffer << typeId;
        pMessage->encode(&m_ringBuffer);
    }
}
//! Returns an error code
//! \return the next message
Message* MessageQueue::pop(){
    core::ScopedLock lock(&m_lock);
    message::Message *pMessage = 0;
    while(pMessage == 0 && !m_ringBuffer.isEmpty()){
        MessageId typeId = 0;
        m_ringBuffer >> typeId;
        pMessage = m_factory.build(typeId);
        if(pMessage){
            pMessage->decode(&m_ringBuffer);
        }else{
            NIMBLE_LOG_ERROR("core", "Invalid (0) message was dispatched");
        }
    }
    return pMessage;
}
//! returns true when queue is empty
//! \return true when queue is empty
bool MessageQueue::isEmpty() const{
    core::ScopedLock lock(&m_lock);
    return m_ringBuffer.isEmpty();
}

//////////////////////////////////////////////////////////////////////////