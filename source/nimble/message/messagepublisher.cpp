//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/message/messagepublisher.h>
#include <nimble/message/messagesubscriber.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::message;

//////////////////////////////////////////////////////////////////////////

//! Constructor
MessagePublisher::MessagePublisher(){
}
//! Destructor
MessagePublisher::~MessagePublisher(){
}

//////////////////////////////////////////////////////////////////////////

//! Sends a message
//! \param pMessage the message to send
void MessagePublisher::send(message::Message* pMessage){
    if(pMessage != 0){
        core::ScopedLock lock(&m_lock);
        SubscriberList::iterator it;
        for(it = m_subscribers.begin(); it != m_subscribers.end(); it++){
            subscriber_t &subscriber = *it;
            if(subscriber.m_messageId == pMessage->getId()){
                subscriber.m_pSubscriber->addMessage(pMessage);
            }
        }
    }else{
        NIMBLE_LOG_WARNING("core", "Invalid 0 message being sent from MessageService");
    }
}

//////////////////////////////////////////////////////////////////////////

//! removes message observers
//! \param pSubscriber the message subscriber
void MessagePublisher::removeSubscriber(message::MessageSubscriber *pSubscriber){
    core::ScopedLock lock(&m_lock);
    
    SubscriberList::iterator it;
    for(it = m_subscribers.begin(); it != m_subscribers.end();){
        subscriber_t &subscriber = *it;
        if(subscriber.m_pSubscriber == pSubscriber){
            it = m_subscribers.erase(it);
        }else{
            it++;
        }
    }
}
//! removes all message observers
void MessagePublisher::removeAllSubscribers(){
    m_subscribers.clear();
}

//////////////////////////////////////////////////////////////////////////

//! adds a message subscriber
//! \param pSubscriber the message subscriber
//! \param messageId the message id we are observing on
void MessagePublisher::addSubscriber(message::MessageSubscriber *pSubscriber, message::MessageId messageId){
    if(!this->existsSubscriber(pSubscriber, messageId)){
        core::ScopedLock lock(&m_lock);
        
        subscriber_t subscriber;
        subscriber.m_pSubscriber = pSubscriber;
        subscriber.m_messageId = messageId;
        m_subscribers.push_back(subscriber);
    }else{
        NIMBLE_LOG_WARNING("core", "Duplicate subscription (%d, %x) already exists", messageId, pSubscriber);
    }
}
//! removes a message subscriber
//! \param pSubscriber the message subscriber
//! \param messageId the message id we are observing on
void MessagePublisher::removeSubscriber(message::MessageSubscriber *pSubscriber, message::MessageId messageId){
    core::ScopedLock lock(&m_lock);

    SubscriberList::iterator it;
    for(it = m_subscribers.begin(); it != m_subscribers.end(); it++){
        subscriber_t &subscriber = *it;
        if((subscriber.m_pSubscriber == pSubscriber) && (subscriber.m_messageId == messageId)){
            m_subscribers.erase(it);
            break;
        }
    }
}
//! exists an subscriber for a message
bool MessagePublisher::existsSubscriber(message::MessageSubscriber *pSubscriber, message::MessageId messageId){
    core::ScopedLock lock(&m_lock);
    
    SubscriberList::iterator it;
    for(it = m_subscribers.begin(); it != m_subscribers.end(); it++){
        subscriber_t &subscriber = *it;
        if((subscriber.m_pSubscriber == pSubscriber) && (subscriber.m_messageId == messageId)){
            return true;
        }
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////