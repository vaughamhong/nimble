//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/physics/worldsim.h>
#include <nimble/physics/rigidbody.h>
#include <btBulletDynamicsCommon.h>
#include <algorithm>
#include <assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::physics;

//////////////////////////////////////////////////////////////////////////

//! Constructor
WorldSim::WorldSim()
:m_pBroadphaseInterface(0)
,m_pCollisionConfiguration(0)
,m_pCollisionDispatcher(0)
,m_pSequentialImpulseConstraintSolver(0)
,m_pDiscreteDynamicsWorld(0){
    m_pBroadphaseInterface = new /*( physics dynamic )*/ btDbvtBroadphase();
    m_pCollisionConfiguration = new /*( physics dynamic )*/ btDefaultCollisionConfiguration();
    m_pCollisionDispatcher = new /*( physics dynamic )*/ btCollisionDispatcher(m_pCollisionConfiguration);
    m_pSequentialImpulseConstraintSolver = new /*( physics dynamic )*/ btSequentialImpulseConstraintSolver;
    m_pDiscreteDynamicsWorld = new /*( physics dynamic )*/ btDiscreteDynamicsWorld(m_pCollisionDispatcher,
                                                                                   m_pBroadphaseInterface,
                                                                                   m_pSequentialImpulseConstraintSolver,
                                                                                   m_pCollisionConfiguration);
    m_pDiscreteDynamicsWorld->setGravity(btVector3(0, -9.8f, 0));
}
//! Destructor
WorldSim::~WorldSim(){
    // destroy bullet
    delete m_pDiscreteDynamicsWorld; m_pDiscreteDynamicsWorld = 0;
    delete m_pSequentialImpulseConstraintSolver; m_pSequentialImpulseConstraintSolver = 0;
    delete m_pCollisionDispatcher; m_pCollisionDispatcher = 0;
    delete m_pCollisionConfiguration; m_pCollisionConfiguration = 0;
    delete m_pBroadphaseInterface; m_pBroadphaseInterface = 0;
}

//////////////////////////////////////////////////////////////////////////

//! Adds a rigid body
void WorldSim::addRigidBody(physics::rigidBody *pRigidBody){
    core::ScopedLock lock(&m_lock);
    m_pDiscreteDynamicsWorld->addRigidBody(pRigidBody->m_pRigidBody, pRigidBody->m_groupMask, pRigidBody->m_collisionMask);
    m_rigidBodies.push_back(pRigidBody);
}
//! Removes a rigid body
void WorldSim::removeRigidBody(physics::rigidBody *pRigidBody){
    core::ScopedLock lock(&m_lock);
    m_rigidBodies.erase(std::find(m_rigidBodies.begin(), m_rigidBodies.end(), pRigidBody));
    m_pDiscreteDynamicsWorld->removeRigidBody(pRigidBody->m_pRigidBody);
}

//////////////////////////////////////////////////////////////////////////