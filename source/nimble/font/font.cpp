//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/font/font.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <nimble/resource/resource.h>
#include <nimble/renderdevice/renderdevice.h>

//////////////////////////////////////////////////////////////////////////

static FT_Library g_freetype2Library;
static bool g_freetype2Initialized = false;

//////////////////////////////////////////////////////////////////////////

// note: freetype (inflate) requires these to be defined
int z_verbose = 0;
double z_error = 0.0;

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::text;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Font::Font(size_t size, uint32_t textureWidth, uint32_t textureHeight)
:m_filepath("")
,m_size(size)
,m_face(0)
,m_pTexture(0){
    // initialize freetype2
    if(!g_freetype2Initialized){
        FT_Error error = FT_Init_FreeType(&g_freetype2Library);
        NIMBLE_ASSERT_PRINT(error == 0, "Failed to load font from file - failed to initialize freetype2");
        g_freetype2Initialized = true;
    }
    
    // create our texture for stamping font glyphs
    renderdevice::eTextureFormat format = renderdevice::kTextureFormatR8G8B8A8;
    renderdevice::eTextureUsage usage = renderdevice::kTextureUsageDynamic;
    m_pTexture = renderdevice::createTexture(textureWidth, textureHeight, format, usage);
}
//! Constructor
Font::Font(const char *filepath, size_t size, uint32_t textureWidth, uint32_t textureHeight)
:m_filepath("")
,m_size(size)
,m_face(0)
,m_pTexture(0){
    // initialize freetype2
    if(!g_freetype2Initialized){
        FT_Error error = FT_Init_FreeType(&g_freetype2Library);
        NIMBLE_ASSERT_PRINT(error == 0, "Failed to load font from file - failed to initialize freetype2");
        g_freetype2Initialized = true;
    }
    
    // create our texture for stamping font glyphs
    renderdevice::eTextureFormat format = renderdevice::kTextureFormatR8G8B8A8;
    renderdevice::eTextureUsage usage = renderdevice::kTextureUsageDynamic;
    m_pTexture = renderdevice::createTexture(textureWidth, textureHeight, format, usage);
    
    // load our font
    loadFromFile(filepath);
}
//! Destructor
Font::~Font(){
    FT_Done_Face(m_face);
    delete m_pTexture;
}

//////////////////////////////////////////////////////////////////////////

//! Load from file
void Font::loadFromFile(const char *path){
    // load our font face
    FT_Error error = FT_New_Face(g_freetype2Library, path, 0, &m_face);
    if(error == FT_Err_Unknown_File_Format){
        NIMBLE_LOG_ERROR("text", "Failed to load font from file - unknown file format detected %s", path);
        return;
    }else if(error){
        NIMBLE_LOG_ERROR("text", "Failed to load font from file - freetype2 error %d", error);
        return;
    }
    
    // set our filepath of our successfully loaded font
    m_filepath = path;
    
    // set our default size
    setFontSize(m_size);
    
    // load default character set of glyphs
    loadGlyphs();
}

//////////////////////////////////////////////////////////////////////////

//! Sets font size
void Font::setFontSize(size_t size){
    m_size = size;
    FT_Error error = FT_Set_Char_Size(m_face, size * 64, size * 64, 0, 0);
    if(error){
        NIMBLE_LOG_ERROR("text", "Failed to change font size - freetype2 error %s", error);
    }
}
//! Returns the font size
size_t Font::getFontSize() const{
    return m_size;
}

//////////////////////////////////////////////////////////////////////////

//! Returns the family name
const char* Font::getFamilyName() const{
    return m_face->family_name;
}
//! Returns the style name
const char* Font::getStyleName() const{
    return m_face->style_name;
}

//////////////////////////////////////////////////////////////////////////

//! Loads glyphs
void Font::loadGlyphs(const char *characters){
    FT_Error error = 0;
    
    // set default character set if non provided
    if(characters == 0){
        characters = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
    }
    
    // clear our glyph cache
    m_glyphCache.clear();
    
    // get some information about our texture
    const uint32_t textureWidth = m_pTexture->getWidth();
    const uint32_t textureHeight = m_pTexture->getHeight();
    const uint32_t textureBytesPerPixel = m_pTexture->getBytesPerPixel();
    const uint32_t texturePitch = textureBytesPerPixel * textureWidth;
    core::Lockable *pLockable = dynamic_cast<core::Lockable*>(m_pTexture);
    
    // lock our texture to transfer glyph data
    char *pTextureBuffer = 0;
    pLockable->lock(core::kLockTypeWrite, &pTextureBuffer);
    
    // populate our texture atlas with character glyphs
    int numCharacters = strlen(characters);
    uint32_t topX = 0, topY = 0, lowestY = 0;
    for(int i = 0; i < numCharacters; i++){
        char character = characters[i];
        
        // load glyph image into the slot (erase previous one)
        error = FT_Load_Char(m_face, character, FT_LOAD_RENDER);
        if(error != 0){
            continue;
        }
        
        // retreive our bitmap to be drawn onto our target surface
        FT_Bitmap *pBitmap = &m_face->glyph->bitmap;
        if(pBitmap->buffer != 0){
            uint32_t width = pBitmap->width;
            uint32_t height = pBitmap->rows;
            
            // if we have run out of horizontal space go to the next row
            if(topX + width > textureWidth){
                topX = 0;
                topY = lowestY;
            }
            // if we have run out of vertical space, we are done
            if(topY + height > textureHeight){
                break;
            }
            
            // copy individual greyscale values
            for(uint32_t y = 0; y < height; y++){
                for(uint32_t x = 0; x < width; x++){
                    // find our greyscale color value
                    char color = pBitmap->buffer[y * width + x];
                    
                    // find our texture offset and transfer our glyph bitmap values
                    int32_t textureOffset = (topY + y) * texturePitch + (topX + x) * textureBytesPerPixel;
                    pTextureBuffer[textureOffset + 0] = color;
                    pTextureBuffer[textureOffset + 1] = color;
                    pTextureBuffer[textureOffset + 2] = color;
                    pTextureBuffer[textureOffset + 3] = (color == 0) ? 0 : 255;
                }
            }
            
            // store into our glyph cache
            text::glyphInfo_t info;
            info.x = topX;
            info.y = topY;
            info.width = width;
            info.height = height;
            m_glyphCache.insert(std::make_pair(character, info));
            
            // increment to the next horizontal position
            topX += width;
            
            // calculate our lowestY value, where the next row will start
            // if we have run out of horizontal space.
            if(topY + height > lowestY){
                lowestY = (topY + height);
            }
        }
    }
    // unlock texture after glyph transfer
    pLockable->unlock();
}
//! Returns our texture
renderdevice::ITexture* Font::getTexture() const{
    return m_pTexture;
}
//! Returns glyph location
text::glyphInfo_t Font::getGlyphInfo(char character) const{
    KeyToGlyphInfoIndex::const_iterator it = m_glyphCache.find(character);
    if(it != m_glyphCache.end()){
        return it->second;
    }
    return text::glyphInfo_t::zero();
}

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
FontLoader::FontLoader(){
    // initialize freetype2
    if(!g_freetype2Initialized){
        FT_Error error = FT_Init_FreeType(&g_freetype2Library);
        NIMBLE_ASSERT_PRINT(error == 0, "Failed to load font from file - failed to initialize freetype2");
        g_freetype2Initialized = true;
    }
}
//! Destructor
FontLoader::~FontLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* FontLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<text::Font>();
    text::Font *pObject = dynamic_cast<text::Font*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////