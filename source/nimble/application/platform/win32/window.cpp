//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/application/platform/win32/window.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::application;

//////////////////////////////////////////////////////////////////////////

//! default constructor
Window::Window(HWND hwnd, HDC hdc, HGLRC hrc)
:m_hwnd(hwnd)
,m_hdc(hdc)
,m_hrc(hrc){
	RECT windowRect;
	GetWindowRect(m_hwnd, &windowRect);
	m_size[0] = windowRect.right - windowRect.left;
	m_size[1] = windowRect.bottom - windowRect.top;
}
//! a destructor
Window::~Window(){
}

//////////////////////////////////////////////////////////////////////////

//! Gets our size
math::Size2i Window::getSize() const{
	return m_size;
}
//! Gets our aspect ratio
float Window::getAspectRatio() const{
	return (float)m_size[0]/m_size[1];
}
//! Returns our window handle
//! \return our window handle
HWND Window::getHWnd() const{
	return m_hwnd;
}
//! Returns our device context
HDC Window::getHDC() const{
	return m_hdc;
}
//! Returns our render handle
HGLRC Window::getHRC() const{
	return m_hrc;
}

//////////////////////////////////////////////////////////////////////////