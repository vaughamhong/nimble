//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/application/platform/osx/window.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::application;

//////////////////////////////////////////////////////////////////////////

//! default constructor
Window::Window(NSOpenGLView *pView, NSOpenGLContext *pGLContext)
:m_pView(pView)
,m_pGLContext(pGLContext){
    core::assert_warn(m_pView != 0, "Window created with Null pView");
    core::assert_warn(m_pGLContext != 0, "Window created with Null pGLContext");
    
    m_size[0] = (int32_t)m_pView.bounds.size.width;
	m_size[1] = (int32_t)m_pView.bounds.size.height;
}
//! a destructor
Window::~Window(){
    [m_pView resignFirstResponder];
}

//////////////////////////////////////////////////////////////////////////

//! Gets our size
math::Size2i Window::getSize() const{
    return m_size;
}
//! Gets our aspect ratio
float Window::getAspectRatio() const{
    return (float)m_size[0]/m_size[1];
}
//! Returns our NSOpenGLView
//! \return the NSOpenGLView for this window
NSOpenGLView* Window::getUIView() const{
    return m_pView;
}
//! Returns our NSOpenGLContext
//! \return the NSOpenGLContext for this window
NSOpenGLContext* Window::getGLContext() const{
    return m_pGLContext;
}
//! Returns our NSOpenGLPixelFormat
//! \return the NSOpenGLPixelFormat for this window
NSOpenGLPixelFormat* Window::getGLPixelFormat() const{
    return m_pView.pixelFormat;
}

//////////////////////////////////////////////////////////////////////////