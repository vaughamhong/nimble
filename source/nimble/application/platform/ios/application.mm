//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/application/platform/ios/application.h>
#include <nimble/renderer/platform/ios/rendersystem.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/entity/entitymanager.h>
#include <nimble/entity/systemsmanager.h>
#include <nimble/core/streamprovider.filesystem.h>
#include <nimble/core/locator.h>
#include <nimble/renderdevice-opengles_1_1/renderdevice.h>
#include <nimble/renderdevice-opengles_2_0/renderdevice.h>
#include <nimble/audiodevice-openal/audiodevice.h>
#include <nimble/input/touch.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::application::ios;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Application::Application(){
}
//! Destructor
Application::~Application(){
}

//////////////////////////////////////////////////////////////////////////

//! Initialize
void Application::initialize(const char *configFilename){
    application::Application::initialize(configFilename);
}
//! Initialize
void Application::destroy(){
    application::Application::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Called when system was created
void Application::systemWithNameWasCreated(const char *name, entity::ISystem *pSystem){
    application::Application::systemWithNameWasCreated(name, pSystem);
    if(strcmp(name, "RenderSystem") == 0){
        core::Locator<renderdevice::IRenderDevice>::provide(dynamic_cast<renderdevice::IRenderDevice*>(pSystem));
    }
}
//! Register file system paths
void Application::registerFileSystemPaths(core::StreamProvider *pFileSystem){
    application::Application::registerFileSystemPaths(pFileSystem);
    const char *resourcePath = [[[NSBundle mainBundle] resourcePath] UTF8String];
    const char *documentsPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] UTF8String];
    pFileSystem->addSearchPath(resourcePath);
    pFileSystem->addSearchPath(documentsPath);
}
//! Register systems
void Application::registerSystems(entity::SystemsManager *pSystemsManager){
    application::Application::registerSystems(pSystemsManager);
    pSystemsManager->registerSystem<renderer::ios::RenderSystem>("RenderSystem");
}

//////////////////////////////////////////////////////////////////////////

//! creates our render device
renderdevice::IRenderDevice* Application::createRenderDevice(){
    return new renderdevice::opengles_2_0::RenderDevice();
}
//! creates our audio device
audiodevice::IAudioDevice* Application::createAudioDevice(){
    return new audiodevice::openal::AudioDevice();
}
//! creates our touch input device
input::TouchDevice* Application::createTouchInputDevice(){
    return new input::TouchDevice();
}

//////////////////////////////////////////////////////////////////////////