//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/view/platform/ios/window.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::view::ios;

//////////////////////////////////////////////////////////////////////////

//! default constructor
Window::Window()
:m_pView(NULL)
,m_flags(0){
}
//! a destructor
Window::~Window(){
    [m_pView resignFirstResponder];
}

//////////////////////////////////////////////////////////////////////////

//! set our UIView
//! \param pView the UIView for this window
//! \param flags various additional UIView flags
void Window::setUIView(UIView *pView, core::Int32 flags){
    m_pView = pView;
    m_flags = flags;
    this->setSize(math::Size2f((core::Int32)m_pView.bounds.size.width * [UIScreen mainScreen].scale,
                               (core::Int32)m_pView.bounds.size.height * [UIScreen mainScreen].scale));
}

//////////////////////////////////////////////////////////////////////////

//! Returns our UIView
//! \return the UIView for this window
UIView* Window::getUIView() const{
    return m_pView;
}
//! Returns true if this window has a status bar
//! \return true if this window has a status bar
core::Bool Window::hasStatusBar() const{
    return m_flags & Window::kHasStatusBar;
}
//! Returns true if this window has a navigation bar
//! \return true if this window has a navigation bar
core::Bool Window::hasNavigationBar() const{
    return m_flags & Window::kHasNavigationBar;
}

//////////////////////////////////////////////////////////////////////////