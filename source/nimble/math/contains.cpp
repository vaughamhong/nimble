//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/math/contains.h>
#include <nimble/math/vector.h>
#include <nimble/math/vector.h>
#include <nimble/math/plane.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::math;

///////////////////////////////////////////////////////////////////////////////

//! returns true if aabb is contained inside the aab
bool nimble::math::contains(AxisAlignedBox3<> const& aab, AxisAlignedBox3<> const& sphere){
    // TODO: Implement
    assert(false);
	return false;
}

//! returns true if contained
bool nimble::math::contains(AxisAlignedBox3<> const& aab, Vector3<> const& point){
    bool b1 = aab.getMinimum()[0] <= point[0];
    bool b2 = aab.getMinimum()[1] <= point[1];
    bool b3 = aab.getMinimum()[2] <= point[2];
    bool b4 = aab.getMaximum()[0] >= point[0];
    bool b5 = aab.getMaximum()[1] >= point[1];
    bool b6 = aab.getMaximum()[2] >= point[2];
	return	b1 && b2 && b3 && b4 && b5 && b6;
}

//! returns true if ray is contained inside aabb
bool nimble::math::contains(AxisAlignedBox3<> const& aab, Ray3<> const& ray){
    // TODO: Implement
    assert(false);
	return false;
}

//! returns true if contained
bool nimble::math::contains(AxisAlignedBox3<> const& aab, Sphere3<> const& sphere){
    // TODO: Implement
    assert(false);
	return false;
}

///////////////////////////////////////////////////////////////////////////////

//! returns true if aabb is contained inside the sphere
bool nimble::math::contains(Sphere3<> const& sphere, AxisAlignedBox3<> const& aab){
    // TODO: Implement
    assert(false);
	return false;
}

//! returns true if contained within the sphere
bool nimble::math::contains(Sphere3<> const& sphere, Vector3<> const& point){
    // TODO: Implement
    assert(false);
	return	false;
}

//! returns true if ray is contained inside sphere
bool nimble::math::contains(Sphere3<> const& sphere, Ray3<> const& ray){
    // TODO: Implement
    assert(false);
	return false;
}

//! returns true if contained within the sphere
bool nimble::math::contains(Sphere3<> const& sphere, Sphere3<> const& sphere2){
    // TODO: Implement
    assert(false);
	return false;
}

///////////////////////////////////////////////////////////////////////////////