//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/math/intersects.h>
#include <nimble/math/vector.h>
#include <nimble/math/vector.h>
#include <nimble/math/plane.h>
#include <nimble/math/contains.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::math;

///////////////////////////////////////////////////////////////////////////////

//! returns true if the aab2 intersects the aab
bool nimble::math::intersects(AxisAlignedBox3<> const& aab, AxisAlignedBox3<> const& aab2){
    // TODO: Implement
    assert(false);
	return false;
}
//! returns true if the point intersects the aab
bool nimble::math::intersects(AxisAlignedBox3<> const& aab, Vector3<> const& point){
    // TODO: Implement
    assert(false);
	return false;
}

//! returns true if the ray intersects the box
bool nimble::math::intersects(AxisAlignedBox3<> const& aab, Ray3<> const& ray){
	Plane3<> p0 = aab.getFacePlane(0);
	Plane3<> p1 = aab.getFacePlane(1);
	Plane3<> p2 = aab.getFacePlane(2);
	Plane3<> p3 = aab.getFacePlane(3);
	Plane3<> p4 = aab.getFacePlane(4);
	Plane3<> p5 = aab.getFacePlane(5);

	Vector3<> intersectPoint;
	if(	intersects(p0, ray, intersectPoint) && contains(aab, intersectPoint)){
		return true;
	}
	if(	intersects(p1, ray, intersectPoint) && contains(aab, intersectPoint)){
		return true;
	}
	if(	intersects(p2, ray, intersectPoint) && contains(aab, intersectPoint)){
		return true;
	}
	if(	intersects(p3, ray, intersectPoint) && contains(aab, intersectPoint)){
		return true;
	}
	if(	intersects(p4, ray, intersectPoint) && contains(aab, intersectPoint)){
		return true;
	}
	if(	intersects(p5, ray, intersectPoint) && contains(aab, intersectPoint)){
		return true;
	}
	return false;
}
//! returns true if the sphere intersects the aab
bool nimble::math::intersects(AxisAlignedBox3<> const& aab, Sphere3<> const& sphere){
    // TODO: Implement
    assert(false);
	return false;
}
//! returns true if the triangle intersects the aab
bool nimble::math::intersects(AxisAlignedBox3<> const& aab, Vector3<> const& p1, Vector3<> const& p2, Vector3<> const& p3){
    // TODO: Implement
    assert(false);
    return false;
}

////////////////////////////////////////////////////////////////////////////////

//! returns true if the aab intersects the sphere
bool nimble::math::intersects(Sphere3<> const& sphere, AxisAlignedBox3<> const& aab){
    // TODO: Implement
    assert(false);
    return false;
}
//! returns true if the point intersects the sphere
bool nimble::math::intersects(Sphere3<> const& sphere, Vector3<> const& point){
    // TODO: Implement
    assert(false);
    return false;
}
//! returns true if the ray intersects the sphere
bool intersects(Sphere3<> const& sphere, Ray3<> const& ray){
    // TODO: Implement
    assert(false);
    return false;
}
//! returns true if the sphere2 intersects the sphere
bool nimble::math::intersects(Sphere3<> const& sphere, Sphere3<> const& sphere2){
    // TODO: Implement
    assert(false);
    return false;
}

////////////////////////////////////////////////////////////////////////////////

//! returns true if the aab intersects the ray
bool nimble::math::intersects(Ray3<> const& ray, AxisAlignedBox3<> const& aab){
    // TODO: Implement
    assert(false);
    return false;
}
//! returns true if the point intersects the ray
bool nimble::math::intersects(Ray3<> const& ray, Vector3<> const& point){
    // TODO: Implement
    assert(false);
    return false;
}
//! returns true if the ray2 intersects the ray
bool nimble::math::intersects(Ray3<> const& ray, Ray3<> const& ray2){
    // TODO: Implement
    assert(false);
    return false;
}
//! returns true if the sphere intersects the ray
bool nimble::math::intersects(Ray3<> const& ray, Sphere3<> const& sphere){
    // TODO: Implement
    assert(false);
    return false;
}

////////////////////////////////////////////////////////////////////////////////

//! returns true if the aab intersects the plane
bool nimble::math::intersects(Plane3<> const& plane, AxisAlignedBox3<> const& aab){
    // TODO: Implement
    assert(false);
    return false;
}
//! returns true if the point intersects the plane
bool nimble::math::intersects(Plane3<> const& plane, Vector3<> const& point){
    // TODO: Implement
    assert(false);
    return false;
}
//! returns true if the ray intersects the plane
bool nimble::math::intersects(Plane3<> const& plane, Ray3<> const& ray){
	Vector3<> intersectPoint;
	return intersects(plane, ray, intersectPoint);
}

//! returns true if the ray intersects the plane
bool nimble::math::intersects(Plane3<> const& plane, Ray3<> const& ray, Vector3<>& intersectPoint){
	float t = 0.0f;
	float D = plane.getDistance();
	Vector3<> N = plane.getNormal();
	Vector3<> V = ray.getDirection();
	Vector3<> Q = ray.getPosition();

	float NdotV = N.dot(V);
	float NdotQ = N.dot(Q);
	float NdotQplusD = NdotQ + D;

	if(NdotV != 0.0f){
		t = -1.0f * NdotQplusD / NdotV;
		if(t >= 0.0f){
			// ray intersects plane
			intersectPoint = ray.getPosition();
			intersectPoint += (V * t);
			return true;
		}else{
			// ray looking away from plane
			intersectPoint = Vector3<>::zero();
			return false;
		}
	}else if(NdotQplusD == 0.0f){
		// ray parallel to plane, but on the plane
		intersectPoint = Vector3<>::zero();
		return true;
	}else{
		// ray parallel to plane
		intersectPoint = Vector3<>::zero();
		return false;
	}
}

//! returns true if the sphere intersects the plane
bool nimble::math::intersects(Plane3<> const& plane, Sphere3<> const& sphere){
    // TODO: Implement
    assert(false);
    return false;
}

////////////////////////////////////////////////////////////////////////////////

//! returns true if the ray intersects the triangle
bool nimble::math::intersects(Vector3<> const& p1, Vector3<> const& p2, Vector3<> const& p3, Ray3<> const& ray){
	Vector3<> intersectPoint;
	return intersects(p1, p2, p3, ray, intersectPoint);
}
//! returns true if the ray intersects the triangle
bool nimble::math::intersects(Vector3<> const& p1, Vector3<> const& p2, Vector3<> const& p3, Ray3<> const& ray, Vector3<>& intersectPoint){
	// find our plane normal
	Vector3<> v1 = p1 - p2;
	Vector3<> v2 = p3 - p2;
	Vector3<> normal = v1 ^ v2;
	normal.normalize();

	// find our distance
	Vector3<> pointVector = p1;
	float D = -1.0f * normal.dot(pointVector);

	// calculate our plane
	Plane3<> plane(normal, D);

	// check for intersection
	if(nimble::math::intersects(plane, ray, intersectPoint)){
		Vector3<> edge1 = p1 - p2;
		Vector3<> edge2 = p2 - p3;
		Vector3<> edge3 = p3 - p1;

		Vector3<> edgeNormal1 = edge1 ^ normal;
		Vector3<> edgeNormal2 = edge2 ^ normal;
		Vector3<> edgeNormal3 = edge3 ^ normal;

		Vector3<> pointVector1 = intersectPoint - p1;
		Vector3<> pointVector2 = intersectPoint - p2;
		Vector3<> pointVector3 = intersectPoint - p3;

		float dot1 = pointVector1.dot(edgeNormal1);
		float dot2 = pointVector2.dot(edgeNormal2);
		float dot3 = pointVector3.dot(edgeNormal3);

		if(dot1 < 0.0f && dot2 < 0.0f && dot3 < 0.0f){
			return true;
		}
	}

	intersectPoint = Vector3<>::zero();
	return false;
}

//! returns true if the ray intersects the point
bool nimble::math::intersects(Vector3<> const& p1, Ray3<> const& ray, float elipsonDistance){
	Vector3<> rayPosition = ray.getPosition();
	Vector3<> rayVector = ray.getDirection();
	rayVector.normalize();

	// find the vector from our ray position to our point
	Vector3<> pointVector = p1 - rayPosition;

	// find the distance from the point to the ray
	float pointDistanceFromRayPosition = rayVector.dot(pointVector);

	// use the distance to find point on ray closest to point
	Vector3<> closestPointOnRay = rayPosition + rayVector * pointDistanceFromRayPosition;

	// create a vector from the closest point to the point
	Vector3<> distanceToRayVector = p1 - closestPointOnRay;
	
	// if the distance is less than elipson
	return distanceToRayVector.length() <= elipsonDistance;
}

///////////////////////////////////////////////////////////////////////////////