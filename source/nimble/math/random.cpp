//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/math/random.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::math;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
Random::Random(uint64_t seed){
    m_seed = seed;
    m_a = 48271;
    m_m = 2147483647;
    m_q = m_m / m_a;
    m_r = m_m % m_a;
    m_oneOverM = 1.0 / m_m;
}
//! Destructor
Random::~Random(){
}

///////////////////////////////////////////////////////////////////////////////

//! returns a random number
uint64_t Random::rand(){
    uint64_t a = m_seed;
    a = (a+0x7ed55d15) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3);
    a = (a^0xb55a4f09) ^ (a>>16);
    m_seed = a;
    return m_seed % 65536;
}

///////////////////////////////////////////////////////////////////////////////