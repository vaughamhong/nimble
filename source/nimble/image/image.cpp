//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/image/image.h>
#include <nimble/core/stream.buffer.h>
#include <nimble/core/filesystem.h>
#include <nimble/core/scopedprofiler.h>
#include <nimble/image/color.h>
#include <nimble/image/colorconvert.h>
#include "freeImage.h"

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::image;

//////////////////////////////////////////////////////////////////////////

static void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message){
    if(fif != FIF_UNKNOWN){
        NIMBLE_LOG_ERROR("image", "Failed to load image - detected unknown format %s\n", FreeImage_GetFormatFromFIF(fif));
    }
}
//! returns the internal image format
image::eImageFormat freeimageFormatToInternalFormat(FREE_IMAGE_TYPE type, FREE_IMAGE_COLOR_TYPE colorFormat, unsigned bpp){
    if(type == FIT_RGBAF){
        return image::kImageFormatRGBAF;
    }else if(type == FIT_BITMAP){
        if(bpp == 24){
#if FREEIMAGE_COLORORDER == FREEIMAGE_COLORORDER_BGR
            if(colorFormat == FIC_RGB){
                return image::kImageFormatB8G8R8;
            }
#else
            if(colorFormat == FIC_RGB){
                return image::kImageFormatR8G8B8;
            }
#endif
        }else if(bpp == 32){
#if FREEIMAGE_COLORORDER == FREEIMAGE_COLORORDER_BGR
            if(colorFormat == FIC_RGB){
                return image::kImageFormatB8G8R8A8;
            }else if(colorFormat == FIC_RGBALPHA){
                return image::kImageFormatB8G8R8A8;
            }
#else
            if(colorFormat == FIC_RGB){
                return image::kImageFormatR8G8B8A8;
            }else if(colorFormat == FIC_RGBALPHA){
                return image::kImageFormatR8G8B8A8;
            }
#endif
        }
    }
    return image::kImageFormatUnknown;
}

//////////////////////////////////////////////////////////////////////////

#define IMAGE_TUPLE(ENUM, PIXEL_SIZE) PIXEL_SIZE,
static uint32_t gBytesPerPixel[] ={
    IMAGE_TUPLESET
};
#undef IMAGE_TUPLE

//////////////////////////////////////////////////////////////////////////

//! Constructor
Image::Image()
:m_width(0)
,m_height(0)
,m_format(){
    m_buffer.resize(m_width * m_height * getBytesPerPixel());
}
//! Constructor
Image::Image(uint32_t width, uint32_t height, image::eImageFormat format)
:m_width(width)
,m_height(height)
,m_format(format){
    m_buffer.resize(m_width * m_height * getBytesPerPixel());
}
//! Constructor
Image::Image(const char *path){
    this->loadFromFile(path);
}
//! Construtor
Image::Image(Image const &image){
    this->operator=(image);
}
//! Destructor
Image::~Image(){
}

//////////////////////////////////////////////////////////////////////////

//! Load from file
void Image::loadFromFile(const char *path){
    static bool freeimageInitialized = false;
    if(freeimageInitialized == false){
        FreeImage_Initialise();
        freeimageInitialized = true;
    }
    
    // load our (arbitrary) image
    FreeImage_SetOutputMessage(FreeImageErrorHandler);
    FREE_IMAGE_FORMAT srcFormat = FreeImage_GetFileType(path, 0);
    FIBITMAP *image = FreeImage_Load(srcFormat, path);
    FIBITMAP *b8g8r8a8Image = 0;
    
    // failed to load image
    if(image == 0){
        NIMBLE_LOG_WARNING("image", "Failed to load image - %s", path);
        return;
    }
    
    // get information on our image format
    FREE_IMAGE_TYPE type = FreeImage_GetImageType(image);
    FREE_IMAGE_COLOR_TYPE colorFormat = FreeImage_GetColorType(image);
    unsigned imageBpp = FreeImage_GetBPP(image);
    int imageWidth = FreeImage_GetWidth(image);
    int imageHeight = FreeImage_GetHeight(image);
    
    // calculate our internal image format and get a pointer to our data
    image::eImageFormat imageFormat = freeimageFormatToInternalFormat(type, colorFormat, imageBpp);
    char *pData = 0;
    if(imageFormat != image::kImageFormatUnknown){
        // retreive our bits
        pData = (char*)FreeImage_GetBits(image);
    }else{
        // the image format is unknown, convert our image to R8G8B8A8
#if FREEIMAGE_COLORORDER == FREEIMAGE_COLORORDER_BGR
        imageFormat = image::kImageFormatB8G8R8A8;
#else
        imageFormat = image::kImageFormatR8G8B8A8;
#endif
        b8g8r8a8Image = FreeImage_ConvertTo32Bits(image);
        pData = (char*)FreeImage_GetBits(b8g8r8a8Image);
    }
    
    // copy image data
    m_width = imageWidth;
    m_height = imageHeight;
    m_format = imageFormat;
    m_buffer.resize(m_width * m_height * getBytesPerPixel());
    this->copy(pData, imageWidth * imageHeight * gBytesPerPixel[imageFormat]);
    
    // clean up
    if(b8g8r8a8Image){
        FreeImage_Unload(b8g8r8a8Image);
    }
    if(image){
        FreeImage_Unload(image);
    }
}

//////////////////////////////////////////////////////////////////////////

//! returns true if image is empty
bool Image::isEmpty(){
    return m_buffer.getSize() != 0;
}

//////////////////////////////////////////////////////////////////////////

//! gets the format of this image
image::eImageFormat Image::getFormat() const{
    return m_format;
}
//! gets the width of the image
uint32_t Image::getWidth() const{
    return m_width;
}
//! gets the height of the image
uint32_t Image::getHeight() const{
    return m_height;
}

//////////////////////////////////////////////////////////////////////////

//!	gets the size in bytes for a single pixel
uint32_t Image::getBytesPerPixel() const{
    if(m_format != image::kImageFormatUnknown){
        NIMBLE_ASSERT(0 <= m_format && m_format < image::kMaxImageFormats);
        return gBytesPerPixel[m_format];
    }else{
        return 0;
    }
}
//! gets the size of our image in bytes
uint32_t Image::getSize() const{
    return m_width * m_height * getBytesPerPixel();
}
//! gets the image buffer
char* Image::getBuffer() const{
    return m_buffer.getPointer();
}

//////////////////////////////////////////////////////////////////////////

//! Copies an image
void Image::copy(const char *pData, size_t size){
    size_t copySize = size > getSize() ? getSize() : size;
    memcpy(m_buffer.getPointer(), pData, copySize);
}
//! Copies an image
void Image::operator=(image::Image const &image){
    // make sure our formats are the same
    if(image.getWidth() != this->getWidth() ||
       image.getHeight() != this->getHeight() ||
       image.getFormat() != this->getFormat()){
        m_width = image.getWidth();
        m_height = image.getHeight();
        m_format = image.getFormat();
    }
    copy(image.getBuffer(), image.getSize());
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
ImageLoader::ImageLoader(){
}
//! Destructor
ImageLoader::~ImageLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* ImageLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<image::Image>();
    image::Image *pObject = dynamic_cast<image::Image*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////