//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/web/webview.h>
#include <nimble/renderdevice/renderdevice.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <algorithm>

#if !defined(NIMBLE_TARGET_ANDROID)
#include <Awesomium/STLHelpers.h>
#endif

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::web;

//////////////////////////////////////////////////////////////////////////

//! Cosntructor
#if !defined(NIMBLE_TARGET_ANDROID)
WebView::WebView(Awesomium::WebCore *pWebCore)
:m_pWebCore(pWebCore)
,m_pWebView(0)
,m_pLockTarget(0)
#else
WebView::WebView()
:m_pLockTarget(0)
#endif
,m_path("")
,m_bufferWidth(0)
,m_bufferHeight(0)
,m_bytesPerPixel(0){
}
//! Destructor
WebView::~WebView(){
#if !defined(NIMBLE_TARGET_ANDROID)
    if(m_pWebView != 0){
        m_pWebView->Stop();
        m_pWebView->Destroy();
        m_pWebView = 0;
    }
#endif
}

//////////////////////////////////////////////////////////////////////////

//! Setup our internal state
void WebView::setup(renderdevice::ITexture *pRenderTarget){
#if !defined(NIMBLE_TARGET_ANDROID)
    // make sure we have a valid lockable
    if(!pRenderTarget || !dynamic_cast<core::ILockable*>(pRenderTarget)){
        NIMBLE_LOG_ERROR("MediaPlayer", "Failed to setup video player - invalid input target");
        return;
    }
    // make sure we have valid width / heights
    if(pRenderTarget->getWidth() <= 0){
        NIMBLE_LOG_ERROR("MediaPlayer", "Failed to setup video player - invalid buffer width");
        return;
    }
    if(pRenderTarget->getHeight() <= 0){
        NIMBLE_LOG_ERROR("MediaPlayer", "Failed to setup video player - invalid buffer height");
        return;
    }
    // we only accept BGRA8 types
    if(pRenderTarget->getFormat() != renderdevice::kTextureFormatB8G8R8A8){
        NIMBLE_LOG_ERROR("MediaPlayer", "Failed to setup video player - invalid format, BGRA8 expected");
        return;
    }

    // set internal members
    m_bufferWidth = pRenderTarget->getWidth();
    m_bufferHeight = pRenderTarget->getHeight();
    m_bytesPerPixel = pRenderTarget->getBytesPerPixel();
    m_pLockTarget = dynamic_cast<core::ILockable*>(pRenderTarget);
    
    // create web view
    m_pWebView = m_pWebCore->CreateWebView(m_bufferWidth, m_bufferHeight);
#endif
}

//////////////////////////////////////////////////////////////////////////

//! Loads a site
void WebView::load(const char *pRemotePath){
#if !defined(NIMBLE_TARGET_ANDROID)
    if(m_pWebView->IsLoading() == false){
        m_path = pRemotePath;
        m_pWebView->LoadURL(Awesomium::WebURL(Awesomium::WSLit(m_path.c_str())));
    }
#endif
}
//! Reloads a site
void WebView::reload(){
#if !defined(NIMBLE_TARGET_ANDROID)
    if(m_pWebView->IsLoading()){
        m_pWebView->Reload(false);
    }else{
        m_pWebView->LoadURL(Awesomium::WebURL(Awesomium::WSLit(m_path.c_str())));
    }
#endif
}
//! Stops loading a site
void WebView::stop(){
#if !defined(NIMBLE_TARGET_ANDROID)
    m_pWebView->Stop();
#endif
}

//////////////////////////////////////////////////////////////////////////

//! gets the width of the buffer
size_t WebView::getBufferWidth() const{
    return m_bufferWidth;
}
//! gets the height of the buffer
size_t WebView::getBufferHeight() const{
    return m_bufferHeight;
}
//! Gets our bytes per pixel
size_t WebView::getBytesPerPixel() const{
    return m_bytesPerPixel;
}
//! Gets our lock taget
core::ILockable* WebView::getLockTarget() const{
    return m_pLockTarget;
}

//////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
//! Returns the internal web view
Awesomium::WebView* WebView::getWebView() const{
    return m_pWebView;
}
#endif

//////////////////////////////////////////////////////////////////////////