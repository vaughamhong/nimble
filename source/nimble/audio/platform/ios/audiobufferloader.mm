//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/audio/audiobufferloader.h>
#include <nimble/audiodevice/audiodevice.h>
#include <nimble/audiodevice/iaudiobuffer.h>
#include <nimble/core/streamprovider.h>
#include <nimble/core/lockable.h>

//////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::audio;

//////////////////////////////////////////////////////////////////////////

//! Constructor
AudioBufferLoader::AudioBufferLoader(){
}
//! Destructor
AudioBufferLoader::~AudioBufferLoader(){
}

//////////////////////////////////////////////////////////////////////////

//! loads a resource
//! \param filePath the name of the file we want to load
//! \param pStreamProvider the data provider to use
resource::Resource* AudioBufferLoader::load(const char* filePath, core::PropertyList &propertyList, core::StreamProvider* pStreamProvider){
    std::string fullPath = pStreamProvider->findFullPath(filePath);
    OSStatus result = 0;
    UInt32 propertySize = sizeof(UInt64);
    
    // open our file
    AudioFileID audioFileId;
    NSURL *audioFileUrl = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%s", fullPath.c_str()]];
	result = AudioFileOpenURL((CFURLRef)audioFileUrl, kAudioFileReadPermission, 0, &audioFileId);
    if(result == 0){
        // find our file size
        UInt32 dataSize = 0;
        result = AudioFileGetProperty(audioFileId, kAudioFilePropertyAudioDataByteCount, &propertySize, &dataSize);
        if(result == 0){
            // allocate buffer
            char *pBuffer = (char*)malloc(dataSize);
            result = AudioFileReadBytes(audioFileId, false, 0, &dataSize, (void*)pBuffer);
            if(result == 0){
                // create audio clip and and copy audio data
                if(audiodevice::IAudioBuffer *pAudioBuffer = audiodevice::createAudioBuffer(44100, 16)){
                    pAudioBuffer->copyData(pBuffer, dataSize);
                    
                    resource::Resource* pResource = dynamic_cast<resource::Resource*>(pAudioBuffer);
                    core::assert_error(pResource != NULL);
                    return pResource;
                }else{
                    core::logError("audio", "Failed to create audio buffer object");
                }
            }else{
                core::logError("audio", "Failed to read buffer data for file: %s", filePath);
            }
            free(pBuffer);
        }else{
            core::logError("audio", "Failed to find buffer size for file: %s", filePath);
        }
        AudioFileClose(audioFileId);
    }else{
        core::logError("audio", "Failed to open file: %s", filePath);
    }
    return NULL;
}

//////////////////////////////////////////////////////////////////////////