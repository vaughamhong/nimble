//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/audio/platform/osx/audiobufferloader.h>
#include <nimble/audiodevice/audiodevice.h>
#include <nimble/audiodevice/iaudiobuffer.h>
#include <nimble/resource/resource.h>
#include <nimble/core/lockable.h>
#include <nimble/core/filesystem.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::audio;

//////////////////////////////////////////////////////////////////////////

//! Constructor
AudioBufferLoader::AudioBufferLoader(){
}
//! Destructor
AudioBufferLoader::~AudioBufferLoader(){
}

//////////////////////////////////////////////////////////////////////////

//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* AudioBufferLoader::loadResource(const char* path){
    OSStatus result = 0;
    AudioFileID audioFileId;
    
    // open our file
    NSURL *audioFileUrl = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%s", path]];
	result = AudioFileOpenURL((CFURLRef)audioFileUrl, kAudioFileReadPermission, 0, &audioFileId);
    if(result != 0){
        NIMBLE_LOG_ERROR("audio", "Failed to open file: %s", path);
        return 0;
    }
    
    // find our property size
    UInt32 propertySize, isWritable;
    result = AudioFileGetPropertyInfo(audioFileId, kAudioFilePropertyAudioDataByteCount, &propertySize, &isWritable);
    if(result != 0){
        NIMBLE_LOG_ERROR("audio", "Failed to find buffer size property for file: %s", path);
        AudioFileClose(audioFileId);
        return 0;
    }
    
    // find our file size
    UInt64 dataByteCount = 0;
    NIMBLE_ASSERT(sizeof(dataByteCount) == propertySize);
    result = AudioFileGetProperty(audioFileId, kAudioFilePropertyAudioDataByteCount, &propertySize, &dataByteCount);
    if(result != 0){
        NIMBLE_LOG_ERROR("audio", "Failed to find buffer size for file: %s", path);
        AudioFileClose(audioFileId);
        return 0;
    }
    
    // allocate buffer
    UInt32 bufferSize = dataByteCount;
    char *pBuffer = (char*)malloc(bufferSize);
    result = AudioFileReadBytes(audioFileId, false, 0, &bufferSize, (void*)pBuffer);
    if(result != 0){
        NIMBLE_LOG_ERROR("audio", "Failed to read buffer data for file: %s", path);
        free(pBuffer);
        AudioFileClose(audioFileId);
        return 0;
    }
    
    // create audio clip and and copy audio data
    audiodevice::IAudioBuffer *pAudioBuffer = audiodevice::createAudioBuffer(44100, 16);
    if(pAudioBuffer == 0){
        NIMBLE_LOG_ERROR("audio", "Failed to create audio buffer object");
        free(pBuffer);
        AudioFileClose(audioFileId);
        return 0;
    }
    
    pAudioBuffer->copyData(pBuffer, bufferSize);
    resource::IResource* pResource = dynamic_cast<resource::IResource*>(pAudioBuffer);
    NIMBLE_ASSERT(pResource != 0);
    free(pBuffer);
    AudioFileClose(audioFileId);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////