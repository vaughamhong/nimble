//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/audio/audiobufferloader.h>
#include <nimble/audiodevice/iaudiodevice.h>
#include <nimble/core/locator.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::audio;

//////////////////////////////////////////////////////////////////////////

//! Constructor
AudioBufferLoader::AudioBufferLoader(){
}
//! Destructor
AudioBufferLoader::~AudioBufferLoader(){
}

//////////////////////////////////////////////////////////////////////////

//! loads a resource
//! \param filePath the name of the file we want to load
//! \param pStreamProvider the data provider to use
resource::IResource* AudioBufferLoader::load(const char *path){
#if 0
    std::string fullPath = pStreamProvider->findFullPath(filePath);
    OSStatus result = 0;
    UInt32 propertySize = sizeof(UInt64);
    
    // open our file
    AudioFileID audioFileId;
    NSURL *audioFileUrl = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%s", fullPath.c_str()]];
	result = AudioFileOpenURL((CFURLRef)audioFileUrl, kAudioFileReadPermission, 0, &audioFileId);
    if(result != 0){
        NSLog(@"cannot open file: %@", [NSString stringWithFormat:@"%s", filePath]);
    }
    
    // find our file size
    UInt32 dataSize = 0;
	result = AudioFileGetProperty(audioFileId, kAudioFilePropertyAudioDataByteCount, &propertySize, &dataSize);
	if(result != 0){NSLog(@"cannot find file size");}
        
    // allocate buffer
    char *pBuffer = (char*)malloc(dataSize);
    result = AudioFileReadBytes(audioFileId, false, 0, &dataSize, (void*)pBuffer);
    if(result != 0){
        NSLog(@"failed");
    }
    
    // create audio clip
    resource::ResourcePtr AudioBufferAssetPtr = audio::AudioBuffer::create(44100, 16);
    core::assert_error(AudioBufferAssetPtr.isValid());
    
    // copy audio data
    audio::AudioBuffer *pAudioBuffer = 0;
    if(AudioBufferAssetPtr->queryInterface(&pAudioBuffer) == 0){
        pAudioBuffer->copyData(pBuffer, dataSize);
    }
    
    // clean up
    free(pBuffer);
    AudioFileClose(audioFileId);
    
    // return asset
    return AudioBufferAssetPtr;
#endif
	return NULL;
}

//////////////////////////////////////////////////////////////////////////