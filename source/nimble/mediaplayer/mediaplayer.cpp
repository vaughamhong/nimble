//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/mediaplayer/mediaplayer.h>
#include <nimble/renderdevice/renderdevice.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <nimble/core/runloop.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

#if defined(NIMBLE_TARGET_WIN32)
    #include <nimble/mediaplayer/platform/win32/resolvetarget.h>
#elif defined(NIMBLE_TARGET_OSX)
    #include <nimble/mediaplayer/platform/osx/resolvetarget.h>
#endif

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::media;

//////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
// replaces all strings that match 'from' to 'to'
void _replaceAll(std::string& str, const std::string& from, const std::string& to){
    if(from.empty()){
        return;
    }
    size_t startPosition = 0;
    while((startPosition = str.find(from, startPosition)) != std::string::npos){
        str.replace(startPosition, from.length(), to);
        startPosition += to.length();
    }
}

// logs error messages from the media component
void libvlc_log(void *data, int level, const libvlc_log_t *ctx, const char *fmt, va_list args){
    char buffer[1024];
	vsnprintf(buffer, 1024, fmt, args);
	buffer[1024 - 1] = 0;
	NIMBLE_LOG_INFO("media", "%s", buffer);
}
#endif

//////////////////////////////////////////////////////////////////////////

//! Cosntructor
MediaPlayer::MediaPlayer()
#if !defined(NIMBLE_TARGET_ANDROID)
:m_pVLCHandle(0)
,m_pMediaPlayerHandle(0)
,m_pLockTarget(0)
#else
:m_pLockTarget(0)
#endif
,m_path("")
,m_bufferWidth(0)
,m_bufferHeight(0)
,m_bytesPerPixel(0){
}
//! Destructor
MediaPlayer::~MediaPlayer(){
    destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Initialize
void MediaPlayer::initialize(const char *pPath, renderdevice::ITexture *pRenderTarget, size_t argc, const char *argv[]){
#if !defined(NIMBLE_TARGET_ANDROID)
	// set internal members
	m_path = pPath;
    if(pRenderTarget != 0){
        m_bufferWidth = pRenderTarget->getWidth();
        m_bufferHeight = pRenderTarget->getHeight();
        m_bytesPerPixel = pRenderTarget->getBytesPerPixel();
        m_pLockTarget = dynamic_cast<core::ILockable*>(pRenderTarget);
        m_buffer.resize(m_bufferWidth * m_bufferHeight * m_bytesPerPixel);
    }else{
        m_bufferWidth = 0;
        m_bufferHeight = 0;
        m_bytesPerPixel = 0;
        m_pLockTarget = 0;
    }
    
    // start a libvlc instance with parameters
	m_pVLCHandle = libvlc_new(argc, argv);
	#if defined(WIN32)
		libvlc_log_set(m_pVLCHandle, libvlc_log, (void*)this);
	#endif
    if(m_pVLCHandle == 0){
        NIMBLE_LOG_ERROR("media", "Failed to initialize media player - could not create VLC handle");
        return;
    }
    
    // create a media
	libvlc_media_t *pMediaHandle = 0;
    std::string lpath = m_path;
    std::transform(lpath.begin(), lpath.end(), lpath.begin(), ::tolower);
    bool isRemote = (lpath.substr(0, strlen("http://")) == "http://") || (lpath.substr(0, strlen("https://")) == "https://");
    if(isRemote){
		pMediaHandle = libvlc_media_new_location(m_pVLCHandle, m_path.c_str());
    }else{
#if defined(NIMBLE_TARGET_WIN32)
		std::string pathStr = pPath;
        std::string from = "/";
        std::string to = "\\";
        _replaceAll(pathStr, from, to);
		pMediaHandle = libvlc_media_new_path(m_pVLCHandle, pathStr.c_str());
#else
		pMediaHandle = libvlc_media_new_path(m_pVLCHandle, m_path.c_str());
#endif
    }
	if (pMediaHandle == 0){
        const char *error = libvlc_errmsg();
        NIMBLE_LOG_ERROR("media", "Failed to initialize media - %s", error);
        return;
    }

    // create a media player to play the media
	m_pMediaPlayerHandle = libvlc_media_player_new_from_media(pMediaHandle);
	if (m_pMediaPlayerHandle == 0){
		const char *error = libvlc_errmsg();
		NIMBLE_LOG_INFO("media", "Failed to initialize media player - %s", error);
		return;
	}

	// setup our event manager to detect for media player state changes
	libvlc_event_manager_t *pMediaPlayerEventMgr = libvlc_media_player_event_manager(m_pMediaPlayerHandle);
	libvlc_event_attach(pMediaPlayerEventMgr, libvlc_MediaPlayerPlaying, MediaPlayer::mediaPlayerEventCallback, this);
	libvlc_event_attach(pMediaPlayerEventMgr, libvlc_MediaPlayerPaused, MediaPlayer::mediaPlayerEventCallback, this);
	libvlc_event_attach(pMediaPlayerEventMgr, libvlc_MediaPlayerStopped, MediaPlayer::mediaPlayerEventCallback, this);

	// setup our event manager to detect for media sub items
	// this is the mechanism we use to detect and play Youtube videos (media embedded in a container site)
	// playing the (Youtube) media will parse the url (through a list of lua scripts) to generate valid url subitems.
	libvlc_event_manager_t *pMediaEventMgr = libvlc_media_event_manager(pMediaHandle);
	libvlc_event_attach(pMediaEventMgr, libvlc_MediaSubItemAdded, MediaPlayer::mediaEventCallback, this);

    // the media player has ownership of the media which means we can release it here
	libvlc_media_release(pMediaHandle);
    
    // set up our decoder callbacks for unlocking / locking our final output buffer
    // note: RV32 is in BGRA format
    if(pRenderTarget != 0){
        libvlc_video_set_callbacks(m_pMediaPlayerHandle, lockTargetPixels, unlockTargetPixels, NULL, (void*)this);
        libvlc_video_set_format(m_pMediaPlayerHandle, "RV32", m_bufferWidth, m_bufferHeight, m_bufferWidth * m_bytesPerPixel);
    }
#endif
}
//! Destroy
void MediaPlayer::destroy(){
#if !defined(NIMBLE_TARGET_ANDROID)
    if(m_pMediaPlayerHandle != 0){
		// clean up media event handlers
		libvlc_media_t *pCurrentMedia = libvlc_media_player_get_media(m_pMediaPlayerHandle);
		if(pCurrentMedia != 0){
			libvlc_event_manager_t *pMediaEventMgr = libvlc_media_event_manager(pCurrentMedia);
			libvlc_event_detach(pMediaEventMgr, libvlc_MediaSubItemAdded, MediaPlayer::mediaEventCallback, this);
		}

		// clean up media player event handlers
		libvlc_event_manager_t *pMediaPlayerEventMgr = libvlc_media_player_event_manager(m_pMediaPlayerHandle);
		libvlc_event_detach(pMediaPlayerEventMgr, libvlc_MediaPlayerPlaying, MediaPlayer::mediaPlayerEventCallback, this);
		libvlc_event_detach(pMediaPlayerEventMgr, libvlc_MediaPlayerPaused, MediaPlayer::mediaPlayerEventCallback, this);
		libvlc_event_detach(pMediaPlayerEventMgr, libvlc_MediaPlayerStopped, MediaPlayer::mediaPlayerEventCallback, this);

		// clean up media player
        libvlc_media_player_release(m_pMediaPlayerHandle);
        m_pMediaPlayerHandle = 0;
    }
    if(m_pVLCHandle != 0){
        libvlc_release(m_pVLCHandle);
        m_pVLCHandle = 0;
    }
#endif
}

//////////////////////////////////////////////////////////////////////////

//! Play video
void MediaPlayer::play(){
#if !defined(NIMBLE_TARGET_ANDROID)
	if(m_pMediaPlayerHandle){
		libvlc_media_player_play(m_pMediaPlayerHandle);
	}
#endif
}
//! Stop video
void MediaPlayer::pause(){
#if !defined(NIMBLE_TARGET_ANDROID)
	if(m_pMediaPlayerHandle){
		libvlc_media_player_pause(m_pMediaPlayerHandle);
	}
#endif
}
//! Stop video
void MediaPlayer::stop(){
#if !defined(NIMBLE_TARGET_ANDROID)
	if(m_pMediaPlayerHandle){
		libvlc_media_player_stop(m_pMediaPlayerHandle);
	}
#endif
}
//! Restart video
void MediaPlayer::restart(){
#if !defined(NIMBLE_TARGET_ANDROID)
	if(m_pMediaPlayerHandle){
        stop();
        play();
	}
#endif
}
//! Returns true if valid
bool MediaPlayer::isValid() const{
#if !defined(NIMBLE_TARGET_ANDROID)
	return (m_pMediaPlayerHandle != 0);
#else
    return true;
#endif
}
//! Returns true if playing
bool MediaPlayer::isPlaying() const{
#if !defined(NIMBLE_TARGET_ANDROID)
	if(m_pMediaPlayerHandle){
		return (libvlc_media_player_is_playing(m_pMediaPlayerHandle) == 1);
	}else{
		return false;
	}
#else
    return false;
#endif
}

//////////////////////////////////////////////////////////////////////////

//! Returns our path
const char* MediaPlayer::getPath() const{
    return m_path.c_str();
}
//! Returns our lock target
core::ILockable* MediaPlayer::getLockTarget() const{
    return m_pLockTarget;
}

//////////////////////////////////////////////////////////////////////////

//! Gets our buffer
char* MediaPlayer::getBuffer() const{
    return m_buffer.getPointer();
}
//! gets the width of the buffer
size_t MediaPlayer::getBufferWidth() const{
    return m_bufferWidth;
}
//! gets the height of the buffer
size_t MediaPlayer::getBufferHeight() const{
    return m_bufferHeight;
}
//! Gets our bytes per pixel
size_t MediaPlayer::getBytesPerPixel() const{
    return m_bytesPerPixel;
}

//////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
//! A media sub item was added
void MediaPlayer::mediaSubItemWasAdded(){
	// make sure we execute on main thread
	core::IRunLoop *pRunLoop = core::getCurrentRunLoop();
	if (pRunLoop != core::getMainRunLoop()){
		core::Functor<void> mediaSubItemWasAddedFunc(this, &MediaPlayer::mediaSubItemWasAdded);
		return core::getMainRunLoop()->runAsync(mediaSubItemWasAddedFunc);
	}else{
		// find the first sub media for us to play
		libvlc_media_t *pCurrentMedia = libvlc_media_player_get_media(m_pMediaPlayerHandle);
		if(pCurrentMedia == 0){
			NIMBLE_LOG_WARNING("media", "Failed to play sub item - current media is null");
			return;
		}
		libvlc_media_list_t *pMediaList = libvlc_media_subitems(pCurrentMedia);
		if(pMediaList == 0){
			NIMBLE_LOG_WARNING("media", "Failed to play sub item - media list is null");
			return;
		}
		int count = libvlc_media_list_count(pMediaList);
		libvlc_media_t *pNewMedia = libvlc_media_list_item_at_index(pMediaList, 0);
		if(pNewMedia == 0){
			libvlc_media_list_release(pMediaList);
			NIMBLE_LOG_WARNING("media", "Failed to play sub item - no sub item media found in list");
			return;
		}

		NIMBLE_LOG_INFO("media", "Media title - %s", libvlc_media_get_meta(pCurrentMedia, libvlc_meta_Title));
		NIMBLE_LOG_INFO("media", "Media artist - %s", libvlc_media_get_meta(pCurrentMedia, libvlc_meta_Artist));
		NIMBLE_LOG_INFO("media", "Media url - %s", libvlc_media_get_meta(pCurrentMedia, libvlc_meta_URL));
		NIMBLE_LOG_INFO("media", "Media encoding - %s", libvlc_media_get_meta(pCurrentMedia, libvlc_meta_EncodedBy));

		// unregister previous media event handler
		libvlc_event_manager_t *pCurrentMediaEventMgr = libvlc_media_event_manager(pCurrentMedia);
		libvlc_event_detach(pCurrentMediaEventMgr, libvlc_MediaSubItemAdded, MediaPlayer::mediaEventCallback, this);

		// setup our event manager to detect for media sub items
		// this is the mechanism we use to detect and play Youtube videos (media embedded in a container site)
		// playing the (Youtube) media will parse the url (through a list of lua scripts) to generate valid url subitems.
		libvlc_event_manager_t *pNewMediaEventMgr = libvlc_media_event_manager(pNewMedia);
		libvlc_event_attach(pNewMediaEventMgr, libvlc_MediaSubItemAdded, MediaPlayer::mediaEventCallback, this);

		// start play the new media
		libvlc_media_player_stop(m_pMediaPlayerHandle);
		libvlc_media_player_set_media(m_pMediaPlayerHandle, pNewMedia);
		libvlc_media_player_play(m_pMediaPlayerHandle);

		// clean up media list
		libvlc_media_list_release(pMediaList);
	}
}

//! Called when we receive an event regarding the media we are playing
void MediaPlayer::mediaEventCallback(const struct libvlc_event_t *event, void *data){
	if (event->type == libvlc_MediaSubItemAdded){
		MediaPlayer *pMediaPlayer = (MediaPlayer*)data;
		pMediaPlayer->mediaSubItemWasAdded();
	}
}
//! Called when we receive an event regarding the media player
void MediaPlayer::mediaPlayerEventCallback(const struct libvlc_event_t *event, void *data){
	if (event->type == libvlc_MediaPlayerPlaying){
		NIMBLE_LOG_INFO("media", "Media player has started playing");
	}else if (event->type == libvlc_MediaPlayerPaused){
		NIMBLE_LOG_INFO("media", "Media player has paused");
	}else if (event->type == libvlc_MediaPlayerStopped){
		NIMBLE_LOG_INFO("media", "Media player has stopped playing");
	}
}
#endif

//////////////////////////////////////////////////////////////////////////