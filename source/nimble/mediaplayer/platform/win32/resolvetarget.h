//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#ifndef __nimble_video_osx_resolvetarget_h__
#define __nimble_video_osx_resolvetarget_h__

//////////////////////////////////////////////////////////////////////////

namespace nimble{
namespace media{
    
    //! Locks pixels to write to
    void* lockTargetPixels(void *data, void **p_pixels);
    //! Unlocks pixels that have been written to
    void unlockTargetPixels(void *data, void *id, void * const *p_pixels);
};};

//////////////////////////////////////////////////////////////////////////

#endif

//////////////////////////////////////////////////////////////////////////