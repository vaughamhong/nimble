//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/mediaplayer/platform/osx/resolvetarget.h>
#include <nimble/mediaplayer/mediaplayer.h>
#include <nimble/renderdevice/itexture.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/locator.h>
//#include <nimble/application/window.h>
#include <gl/gl.h>

extern HGLRC g_videoContext;

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::media;

//////////////////////////////////////////////////////////////////////////

//! Locks pixels to write to
void* nimble::media::lockTargetPixels(void *data, void **p_pixels){
	media::MediaPlayer *pPlayer = (media::MediaPlayer*)data;
    (*p_pixels) = pPlayer->getBuffer();
    return NULL;
}
//! Unlocks pixels that have been written to
void nimble::media::unlockTargetPixels(void *data, void *id, void * const *p_pixels){
//    // this job may want to create render assets which means
//    // we need to create a shared context so that assets created on this thread (job)
//    // is visible to the render thread context. We create our render context as a thread local
//    // so there is only 1 shared render context per thread.
//
//#if defined(NIMBLE_TARGET_WIN32)
//	application::IWindow *pWindowInterface = core::Locator<application::IWindow>::acquire();
//	application::Window *pWindow = dynamic_cast<application::Window*>(pWindowInterface);
//	HDC hdc = GetDC(pWindow->getHWnd());
//	HGLRC hrc = pWindow->getHRC();
//	wglMakeCurrent(hdc, g_videoContext);
//#else
//    static core::ThreadLocalStorage<NSOpenGLContext> sharedGLContext;
//    if(!sharedGLContext){
//        application::IWindow *pWindowInterface = core::Locator<application::IWindow>::acquire();
//        application::Window *pWindow = dynamic_cast<application::Window*>(pWindowInterface);
//        NSOpenGLContext *context = pWindow->getGLContext();
//        NSOpenGLPixelFormat *pixelFormat = pWindow->getGLPixelFormat();
//        sharedGLContext = [[NSOpenGLContext alloc] initWithFormat:pixelFormat
//                                                     shareContext:context];
//    }
//    NSOpenGLContext *context = sharedGLContext.get();
//    [context makeCurrentContext];
//#endif
//    
//    // resolve our decoded buffer into our texture
//	if(video::VideoPlayer *pVideoPlayer = (video::VideoPlayer*)data){
//		pVideoPlayer->flipLockTarget();
//		//renderdevice::ITexture *pTexture = dynamic_cast<renderdevice::ITexture*>(pVideoPlayer->getLockTarget());
//		//if (pTexture != 0){
//		//	pTexture->copy(pVideoPlayer->getBuffer());
//		//}
//		core::ILockable *pLockable = pVideoPlayer->getLockTarget();
//		char *pDestBuffer = 0;
//		if (pLockable && pLockable->lock(core::kLockTypeReadWrite, &pDestBuffer)){
//			memcpy(pDestBuffer,
//				pVideoPlayer->getBuffer(),
//				pVideoPlayer->getBufferWidth() * pVideoPlayer->getBufferHeight() * pVideoPlayer->getBytesPerPixel());
//			pLockable->unlock();
//		}
//
//  //      core::ILockable *pLockable = pPlayer->getLockTarget();
//  //      char *pDestBuffer = 0;
//		//if(pLockable && pLockable->lock(core::kLockTypeWrite, &pDestBuffer)){
//  //          memcpy(pDestBuffer,
//  //                 pPlayer->getBuffer(),
//  //                 pPlayer->getBufferWidth() * pPlayer->getBufferHeight() * pPlayer->getBytesPerPixel());
//  //          pLockable->unlock();
//  //      }
//    }
//
//	// we may have created render assets on this thread
//    // we make sure the driver has caught up before we move on or
//    // we may cause data corruption (eg. half transfered textures)
//    //glFinish();
//            
//#if defined(NIMBLE_TARGET_WIN32)
//    wglMakeCurrent(0, 0);
//#endif
}

//////////////////////////////////////////////////////////////////////////