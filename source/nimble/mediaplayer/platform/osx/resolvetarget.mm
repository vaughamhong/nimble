//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/mediaplayer/platform/osx/resolvetarget.h>
#include <nimble/mediaplayer/mediaplayer.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/locator.h>
#import <OpenGL/gl.h>
#import <AppKit/AppKit.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::media;

//////////////////////////////////////////////////////////////////////////

//! Locks pixels to write to
void* nimble::media::lockTargetPixels(void *data, void **p_pixels){
    // access the vide player's scratch buffer
    media::MediaPlayer *pPlayer = (media::MediaPlayer*)data;
    (*p_pixels) = pPlayer->getBuffer();
    return NULL;
}
//! Unlocks pixels that have been written to
void nimble::media::unlockTargetPixels(void *data, void *id, void * const *p_pixels){
//    // this job may want to create render assets which means
//    // we need to create a shared context so that assets created on this thread (job)
//    // is visible to the render thread context. We create our render context as a thread local
//    // so there is only 1 shared render context per thread.
//    static core::ThreadLocalStorage<NSOpenGLContext> sharedGLContext;
//    if(!sharedGLContext){
//        application::IWindow *pWindowInterface = core::Locator<application::IWindow>::acquire();
//        application::Window *pWindow = dynamic_cast<application::Window*>(pWindowInterface);
//        NSOpenGLContext *context = pWindow->getGLContext();
//        NSOpenGLPixelFormat *pixelFormat = pWindow->getGLPixelFormat();
//        sharedGLContext = [[NSOpenGLContext alloc] initWithFormat:pixelFormat
//                                                     shareContext:context];
//    }
//    NSOpenGLContext *context = sharedGLContext.get();
//    [context makeCurrentContext];
//    
//    // resolve our decoded buffer into our texture
//    if(media::MediaPlayer *pPlayer = (media::MediaPlayer*)data){
//        core::ILockable *pLockable = pPlayer->getLockTarget();
//        char *pDestBuffer = 0;
//        if(pLockable->lock(core::kLockTypeReadWrite, &pDestBuffer)){
//            memcpy(pDestBuffer,
//                   pPlayer->getBuffer(),
//                   pPlayer->getBufferWidth() * pPlayer->getBufferHeight() * pPlayer->getBytesPerPixel());
//            pLockable->unlock();
//        }
//    }
//    
//    // we may have created render assets on this thread
//    // we make sure the driver has caught up before we move on or
//    // we may cause data corruption (eg. half transfered textures)
//    glFinish();
}

//////////////////////////////////////////////////////////////////////////