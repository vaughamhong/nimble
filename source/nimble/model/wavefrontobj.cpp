//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/model/wavefrontobj.h>
#include <nimble/core/tokenize.h>
#include <nimble/core/logger.h>
#include <nimble/core/filesystem.h>
#include <nimble/resource/resource.h>
#include <fstream>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::model;

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
WavefrontOBJ::WavefrontOBJ(){
}
//! Constructor
WavefrontOBJ::WavefrontOBJ(const char *path){
    loadFromFile(path);
}
//! Destructor
WavefrontOBJ::~WavefrontOBJ(){
}
//! Load from file
void WavefrontOBJ::loadFromFile(const char *path){
    std::fstream fileStream(path, std::fstream::in | std::fstream::out | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("core", "Failed to open file %s - %s", path, strerror(errno));
        return;
    }

    std::vector<tinyobj::material_t> materials;
    std::string basePath = core::Path(path).getDirectory() + "/";
    tinyobj::parse_obj(m_data, fileStream);
}
//! Find face lists that belongs to a group
WavefrontOBJ::mesh_t* WavefrontOBJ::findFaceListsByGroup(const char *group){
    for(std::vector<tinyobj::mesh_t>::iterator it = m_data.meshes.begin(); it != m_data.meshes.end(); it++){
        if(it->name == std::string(group)){
            return &(*it);
        }
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
WavefrontOBJLoader::WavefrontOBJLoader(){
}
//! Destructor
WavefrontOBJLoader::~WavefrontOBJLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* WavefrontOBJLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<model::WavefrontOBJ>();
    model::WavefrontOBJ *pObject = dynamic_cast<model::WavefrontOBJ*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////