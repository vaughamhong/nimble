//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/model/quake3bsp.h>
#include <nimble/core/stream.buffer.h>
#include <nimble/core/filesystem.h>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::model;

///////////////////////////////////////////////////////////////////////////////////////////////

void readHeader(std::fstream &stream, Quake3BSP& bsp);
void readTexture(std::fstream &stream, Quake3BSP& bsp);
void readEntity(std::fstream &stream, Quake3BSP& bsp);
void readPlane(std::fstream &stream, Quake3BSP& bsp);
void readNode(std::fstream &stream, Quake3BSP& bsp);
void readLeaf(std::fstream &stream, Quake3BSP& bsp);
void readLeafFace(std::fstream &stream, Quake3BSP& bsp);
void readLeafBrush(std::fstream &stream, Quake3BSP& bsp);
void readModel(std::fstream &stream, Quake3BSP& bsp);
void readBrush(std::fstream &stream, Quake3BSP& bsp);
void readBrushSide(std::fstream &stream, Quake3BSP& bsp);
void readVertex(std::fstream &stream, Quake3BSP& bsp);
void readMeshVert(std::fstream &stream, Quake3BSP& bsp);
void readEffect(std::fstream &stream, Quake3BSP& bsp);
void readFace(std::fstream &stream, Quake3BSP& bsp);
void readLightMap(std::fstream &stream, Quake3BSP& bsp);
void readLightVol(std::fstream &stream, Quake3BSP& bsp);
void readVisData(std::fstream &stream, Quake3BSP& bsp);
void debugInformations(Quake3BSP& bsp);

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
Quake3BSP::Quake3BSP(){
}
//! Constructor
Quake3BSP::Quake3BSP(const char *path){
    loadFromFile(path);
}
//! Destructor
Quake3BSP::~Quake3BSP(){
}

///////////////////////////////////////////////////////////////////////////////////////////////

//! load from file
void Quake3BSP::loadFromFile(const char* path){
    std::fstream fileStream(path, std::fstream::in | std::fstream::out | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("system", "Failed to open file %s - %s", path, strerror(errno));
        return;
    }
    
    readHeader(fileStream, *this);
	readTexture(fileStream, *this);
    readEntity(fileStream, *this);
	readPlane(fileStream, *this);
	readNode(fileStream, *this);
	readLeaf(fileStream, *this);
	readLeafFace(fileStream, *this);
	readLeafBrush(fileStream, *this);
	readModel(fileStream, *this);
	readBrush(fileStream, *this);
	readBrushSide(fileStream, *this);
	readVertex(fileStream, *this);
	readMeshVert(fileStream, *this);
	readEffect(fileStream, *this);
	readFace(fileStream, *this);
	readLightMap(fileStream, *this);
	readLightVol(fileStream, *this);
	readVisData(fileStream, *this);
}

//////////////////////////////////////////////////////////////////////////

/**
 * Read the header of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readHeader(std::fstream &stream, Quake3BSP& bsp){
    stream.read((char*)&bsp.mHeader, sizeof(Quake3BSP::THeader));
}

/**
 * Read the texture lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readTexture(std::fstream &stream, Quake3BSP& bsp){
	int	lNbTextures = bsp.mHeader.mLumpes[Quake3BSP::cTextureLump].mLength / sizeof(Quake3BSP::TTexture);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cTextureLump].mOffset, std::ios::beg);
	for(int lTextureCounter = 0; lTextureCounter < lNbTextures; ++lTextureCounter){
		Quake3BSP::TTexture lTexture;
        stream.read((char*)&lTexture, sizeof(Quake3BSP::TTexture));
		bsp.mTextures.push_back(lTexture);
	}
}

/**
 * Read the entity lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readEntity(std::fstream &stream, Quake3BSP& bsp){
	// Set the entity size.
	bsp.mEntity.mSize = bsp.mHeader.mLumpes[Quake3BSP::cEntityLump].mLength;
	
	// Allocate the entity buffer.
	bsp.mEntity.mBuffer =  new char[bsp.mEntity.mSize];
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cEntityLump].mOffset, std::ios::beg);
	
	// Read the buffer.
    stream.read((char*)bsp.mEntity.mBuffer, bsp.mEntity.mSize * sizeof(char));
};

/**
 * Read the plane lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readPlane(std::fstream &stream, Quake3BSP& bsp){
	int	lNbPlanes = bsp.mHeader.mLumpes[Quake3BSP::cPlaneLump].mLength / sizeof(Quake3BSP::TPlane);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cPlaneLump].mOffset, std::ios::beg);
	for(int lPlaneCounter = 0; lPlaneCounter < lNbPlanes; ++lPlaneCounter){
		Quake3BSP::TPlane lPlane;
        stream.read((char*)&lPlane, sizeof(Quake3BSP::TPlane));
		bsp.mPlanes.push_back(lPlane);
	}
}

/**
 * Read the node lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readNode(std::fstream &stream, Quake3BSP& bsp){
	int	lNbNodes = bsp.mHeader.mLumpes[Quake3BSP::cNodeLump].mLength / sizeof(Quake3BSP::TNode);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cNodeLump].mOffset, std::ios::beg);
	for(int lNodeCounter = 0; lNodeCounter < lNbNodes; ++lNodeCounter){
		Quake3BSP::TNode lNode;
		stream.read((char*)&lNode, sizeof(Quake3BSP::TNode));
		bsp.mNodes.push_back(lNode);
	}
}

/**
 * Read the leaf lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readLeaf(std::fstream &stream, Quake3BSP& bsp){
	int	lNbLeaves = bsp.mHeader.mLumpes[Quake3BSP::cLeafLump].mLength / sizeof(Quake3BSP::TLeaf);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cLeafLump].mOffset, std::ios::beg);
	for(int lLeafCounter = 0; lLeafCounter < lNbLeaves; ++lLeafCounter){
		Quake3BSP::TLeaf lLeaf;
		stream.read((char*)&lLeaf, sizeof(Quake3BSP::TLeaf));
		bsp.mLeaves.push_back(lLeaf);
	}
}

/**
 * Read the leafface lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readLeafFace(std::fstream &stream, Quake3BSP& bsp){
	int	lNbLeafFaces = bsp.mHeader.mLumpes[Quake3BSP::cLeafFaceLump].mLength / sizeof(Quake3BSP::TLeafFace);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cLeafFaceLump].mOffset, std::ios::beg);
	for(int lLeafFaceCounter = 0; lLeafFaceCounter < lNbLeafFaces; ++lLeafFaceCounter){
        Quake3BSP::TLeafFace lLeafFace;
		stream.read((char*)&lLeafFace, sizeof(Quake3BSP::TLeafFace));
		bsp.mLeafFaces.push_back(lLeafFace);
	}
}

/**
 * Read the leafbrush lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readLeafBrush(std::fstream &stream, Quake3BSP& bsp){
	int	lNbLeafBrushes = bsp.mHeader.mLumpes[Quake3BSP::cLeafBrushLump].mLength / sizeof(Quake3BSP::TLeafBrush);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cLeafBrushLump].mOffset, std::ios::beg);
	for(int lLeafBrusheCounter = 0; lLeafBrusheCounter < lNbLeafBrushes; ++lLeafBrusheCounter){
		Quake3BSP::TLeafBrush lLeafBrush;
		stream.read((char*)&lLeafBrush, sizeof(Quake3BSP::TLeafBrush));
		bsp.mLeafBrushes.push_back(lLeafBrush);
	}
}

/**
 * Read the model lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readModel(std::fstream &stream, Quake3BSP& bsp){
	int	lNbModels = bsp.mHeader.mLumpes[Quake3BSP::cModelLump].mLength / sizeof(Quake3BSP::TModel);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cModelLump].mOffset, std::ios::beg);
	for(int lModelCounter = 0; lModelCounter < lNbModels; ++lModelCounter){
		Quake3BSP::TModel lModel;
		stream.read((char*)&lModel, sizeof(Quake3BSP::TModel));
		bsp.mModels.push_back(lModel);
	}
}

/**
 * Read the brush lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readBrush(std::fstream &stream, Quake3BSP& bsp){
	int	lNbBrushes = bsp.mHeader.mLumpes[Quake3BSP::cBrushLump].mLength / sizeof(Quake3BSP::TBrush);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cBrushLump].mOffset, std::ios::beg);
	for(int lBrusheCounter = 0; lBrusheCounter < lNbBrushes; ++lBrusheCounter){
		Quake3BSP::TBrush lBrush;
		stream.read((char*)&lBrush, sizeof(Quake3BSP::TBrush));
		bsp.mBrushes.push_back(lBrush);
	}
}

/**
 * Read the brush side lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readBrushSide(std::fstream &stream, Quake3BSP& bsp){
	int	lNbBrushSides = bsp.mHeader.mLumpes[Quake3BSP::cBrushSideLump].mLength / sizeof(Quake3BSP::TBrushSide);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cBrushSideLump].mOffset, std::ios::beg);
	for(int lBrushSideCounter = 0; lBrushSideCounter < lNbBrushSides; ++lBrushSideCounter){
		Quake3BSP::TBrushSide lBrushSide;
		stream.read((char*)&lBrushSide, sizeof(Quake3BSP::TBrushSide));
		bsp.mBrushSides.push_back(lBrushSide);
	}
}

/**
 * Read the vertex lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readVertex(std::fstream &stream, Quake3BSP& bsp){
	int	lNbVertices = bsp.mHeader.mLumpes[Quake3BSP::cVertexLump].mLength / sizeof(Quake3BSP::TVertex);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cVertexLump].mOffset, std::ios::beg);
	for(int lVerticeCounter = 0; lVerticeCounter < lNbVertices; ++lVerticeCounter){
		Quake3BSP::TVertex lVertex;
		stream.read((char*)&lVertex, sizeof(Quake3BSP::TVertex));
		bsp.mVertices.push_back(lVertex);
	}
}

/**
 * Read the meshvert lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readMeshVert(std::fstream &stream, Quake3BSP& bsp){
	int	lNbMeshVertices = bsp.mHeader.mLumpes[Quake3BSP::cMeshVertLump].mLength / sizeof(Quake3BSP::TMeshVert);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cMeshVertLump].mOffset, std::ios::beg);
	for(int lVerticeCounter = 0; lVerticeCounter < lNbMeshVertices; ++lVerticeCounter){
		Quake3BSP::TMeshVert lMeshVertex;
        stream.read((char*)&lMeshVertex, sizeof(Quake3BSP::TMeshVert));
		bsp.mMeshVertices.push_back(lMeshVertex);
	}
}

/**
 * Read the effect lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readEffect(std::fstream &stream, Quake3BSP& bsp){
	int	lNbEffects = bsp.mHeader.mLumpes[Quake3BSP::cEffectLump].mLength / sizeof(Quake3BSP::TEffect);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cEffectLump].mOffset, std::ios::beg);
	for(int lEffectCounter = 0; lEffectCounter < lNbEffects; ++lEffectCounter){
		Quake3BSP::TEffect lEffect;
        stream.read((char*)&lEffect, sizeof(Quake3BSP::TEffect));
		bsp.mEffects.push_back(lEffect);
	}
}

/**
 * Read the face lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readFace(std::fstream &stream, Quake3BSP& bsp){
	int	lNbFaces = bsp.mHeader.mLumpes[Quake3BSP::cFaceLump].mLength / sizeof(Quake3BSP::TFace);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cFaceLump].mOffset, std::ios::beg);
	for(int lFaceCounter = 0; lFaceCounter < lNbFaces; ++lFaceCounter){
		Quake3BSP::TFace lFace;
        stream.read((char*)&lFace, sizeof(Quake3BSP::TFace));
		bsp.mFaces.push_back(lFace);
	}
}

/**
 * Read the effect lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readLightMap(std::fstream &stream, Quake3BSP& bsp){
	int	lNbLightMaps = bsp.mHeader.mLumpes[Quake3BSP::cLightMapLump].mLength / sizeof(Quake3BSP::TLightMap);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cLightMapLump].mOffset, std::ios::beg);
	for(int lLightMapCounter = 0; lLightMapCounter < lNbLightMaps; ++lLightMapCounter){
		Quake3BSP::TLightMap lLightMap;
        stream.read((char*)&lLightMap, sizeof(Quake3BSP::TLightMap));
		bsp.mLightMaps.push_back(lLightMap);
	}
}

/**
 * Read the effect lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readLightVol(std::fstream &stream, Quake3BSP& bsp){
	int	lNbLightVols = bsp.mHeader.mLumpes[Quake3BSP::cLightVolLump].mLength / sizeof(Quake3BSP::TLightVol);
    
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cLightVolLump].mOffset, std::ios::beg);
	for(int lLightVolCounter = 0; lLightVolCounter < lNbLightVols; ++lLightVolCounter){
		Quake3BSP::TLightVol lLightVol;
        stream.read((char*)&lLightVol, sizeof(Quake3BSP::TLightVol));
		bsp.mLightVols.push_back(lLightVol);
	}
}

/**
 * Read the effect lump of the Q3 map.
 *
 * @param pFile	The stream on the Q3 file data.
 * @param pMap	The map structure to fill.
 */
void readVisData(std::fstream &stream, Quake3BSP& bsp){
	// Go to the start of the chunk.
    stream.seekg(bsp.mHeader.mLumpes[Quake3BSP::cVisDataLump].mOffset, std::ios::beg);
    
    stream.read((char*)&bsp.mVisData.mNbClusters, sizeof(int));
    stream.read((char*)&bsp.mVisData.mBytesPerCluster, sizeof(int));
    
	// Allocate the buffer.
	int lBufferSize = bsp.mVisData.mNbClusters * bsp.mVisData.mBytesPerCluster;
	bsp.mVisData.mBuffer = new /*( ??? )*/ char[lBufferSize];
    stream.read((char*)bsp.mVisData.mBuffer, lBufferSize * sizeof(char));
}

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
Quake3BSPLoader::Quake3BSPLoader(){
}
//! Destructor
Quake3BSPLoader::~Quake3BSPLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* Quake3BSPLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<model::Quake3BSP>();
    model::Quake3BSP *pObject = dynamic_cast<model::Quake3BSP*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////

//void debugInformations(Quake3BSP& bsp){
//	printf("********* Header *********\n");
//	printf("Magic Number : %s\n", bsp.mHeader.mMagicNumber);
//	printf("Version : %d\n", bsp.mHeader.mVersion);
//	for(int lLumpCounter = 0; lLumpCounter < 17; ++lLumpCounter){
//		printf("Lump %d\n", lLumpCounter);
//		printf("\tOffset : %d\n", bsp.mHeader.mLumpes[lLumpCounter].mOffset);
//		printf("\tLength : %d\n", bsp.mHeader.mLumpes[lLumpCounter].mLength);
//	}
//	printf("\n");
//	
//	printf("********* Entity Lump *********\n");
//	printf("Size : %d\n", bsp.mEntity.mSize);
//	if(bsp.mEntity.mSize != 0){
//		printf("Buffer : %s\n", bsp.mEntity.mBuffer);
//	}
//	printf("\n");
//    
//	printf("********* Texture Lump *********\n");
//	for(int lTextureCounter = 0; lTextureCounter <bsp.mTextures.size(); ++lTextureCounter){
//		printf("Texture %d\n", lTextureCounter);
//		printf("\tName : %s\n", bsp.mTextures[lTextureCounter].mName);
//		printf("\tFlags : %d\n", bsp.mTextures[lTextureCounter].mFlags);
//		printf("\tContents : %d\n", bsp.mTextures[lTextureCounter].mContents);
//	}
//	printf("\n");
//    
//	printf("********* Plane Lump *********\n");
//	for(int lPlaneCounter = 0; lPlaneCounter <bsp.mPlanes.size(); ++lPlaneCounter){
//		printf("Plane %d\n", lPlaneCounter);
//		printf("\tNormal : %f %f %f\n", bsp.mPlanes[lPlaneCounter].mNormal[0], bsp.mPlanes[lPlaneCounter].mNormal[1], bsp.mPlanes[lPlaneCounter].mNormal[2]);
//		printf("\tDistance : %f\n", bsp.mPlanes[lPlaneCounter].mDistance);
//	}
//	printf("\n");
//    
//	printf("********* Node Lump *********\n");
//	for(int lNodeCounter = 0; lNodeCounter <bsp.mNodes.size(); ++lNodeCounter){
//		printf("Node %d\n", lNodeCounter);
//		printf("\tPlane index : %d\n", bsp.mNodes[lNodeCounter].mPlane);
//		printf("\tChildren index : %d %d\n", bsp.mNodes[lNodeCounter].mChildren[0], bsp.mNodes[lNodeCounter].mChildren[1]);
//		printf("\tMin Bounding Box : %d %d %d\n", bsp.mNodes[lNodeCounter].mMins[0], bsp.mNodes[lNodeCounter].mMins[1], bsp.mNodes[lNodeCounter].mMins[2]);
//		printf("\tMax Bounding Box : %d %d %d\n", bsp.mNodes[lNodeCounter].mMaxs[0], bsp.mNodes[lNodeCounter].mMaxs[1], bsp.mNodes[lNodeCounter].mMaxs[2]);
//	}
//	printf("\n");
//    
//	printf("********* Leaf Lump *********\n");
//	for(int lLeafCounter = 0; lLeafCounter <bsp.mLeaves.size(); ++lLeafCounter){
//		printf("Leaf %d\n", lLeafCounter);
//		printf("\tCluster %d\n", bsp.mLeaves[lLeafCounter].mCluster);
//		printf("\tMin Bounding Box : %d %d %d\n", bsp.mLeaves[lLeafCounter].mMins[0], bsp.mLeaves[lLeafCounter].mMins[1], bsp.mLeaves[lLeafCounter].mMins[2]);
//		printf("\tMax Bounding Box : %d %d %d\n", bsp.mLeaves[lLeafCounter].mMaxs[0], bsp.mLeaves[lLeafCounter].mMaxs[1], bsp.mLeaves[lLeafCounter].mMaxs[2]);
//		printf("\tLeafFace %d\n", bsp.mLeaves[lLeafCounter].mLeafFace);
//		printf("\tNb LeafFace %d\n", bsp.mLeaves[lLeafCounter].mNbLeafFaces);
//		printf("\tLeafBrush %d\n", bsp.mLeaves[lLeafCounter].mLeafBrush);
//		printf("\tNb LeafBrushes %d\n", bsp.mLeaves[lLeafCounter].mNbLeafBrushes);
//	}
//	printf("\n");
//    
//	printf("********* LeafFace Lump *********\n");
//	for(int lLeafFaceCounter = 0; lLeafFaceCounter <bsp.mLeafFaces.size(); ++lLeafFaceCounter){
//		printf("LeafFace %d\n", lLeafFaceCounter);
//		printf("\tFaceIndex %d\n", bsp.mLeafFaces[lLeafFaceCounter].mFaceIndex);
//	}
//	printf("\n");
//    
//	printf("********* LeafBrush Lump *********\n");
//	for(int lLeafBrushCounter = 0; lLeafBrushCounter <bsp.mLeafBrushes.size(); ++lLeafBrushCounter){
//		printf("LeafBrush %d\n", lLeafBrushCounter);
//		printf("\tBrushIndex %d\n", bsp.mLeafBrushes[lLeafBrushCounter].mBrushIndex);
//	}
//	printf("\n");
//    
//	printf("********* Model Lump *********\n");
//	for(int lModelCounter = 0; lModelCounter <bsp.mModels.size(); ++lModelCounter)
//	{
//		printf("Model %d\n", lModelCounter);
//		printf("\tMin Bounding Box : %d %d %d\n", bsp.mModels[lModelCounter].mMins[0], bsp.mModels[lModelCounter].mMins[1], bsp.mModels[lModelCounter].mMins[2]);
//		printf("\tMax Bounding Box : %d %d %d\n", bsp.mModels[lModelCounter].mMaxs[0], bsp.mModels[lModelCounter].mMaxs[1], bsp.mModels[lModelCounter].mMaxs[2]);
//		printf("\tFace %d\n", bsp.mModels[lModelCounter].mFace);
//		printf("\tNbFaces %d\n", bsp.mModels[lModelCounter].mNbFaces);
//		printf("\tBrush %d\n", bsp.mModels[lModelCounter].mBrush);
//		printf("\tNbBrushes %d\n", bsp.mModels[lModelCounter].mNBrushes);
//	}
//	printf("\n");
//    
//	printf("********* Brush Lump *********\n");
//	for(int lBrushCounter = 0; lBrushCounter <bsp.mBrushes.size(); ++lBrushCounter)
//	{
//		printf("Brush %d\n", lBrushCounter);
//		printf("\tBrushSide %d\n", bsp.mBrushes[lBrushCounter].mBrushSide);
//		printf("\tNbBrushSides %d\n", bsp.mBrushes[lBrushCounter].mNbBrushSides);
//		printf("\tTextureIndex %d\n", bsp.mBrushes[lBrushCounter].mTextureIndex);
//	}
//	printf("\n");
//    
//	printf("********* BrushSide Lump *********\n");
//	for(int lBrushSideCounter = 0; lBrushSideCounter <bsp.mBrushSides.size(); ++lBrushSideCounter){
//		printf("BrushSide %d\n", lBrushSideCounter);
//		printf("\tPlaneIndex %d\n", bsp.mBrushSides[lBrushSideCounter].mPlaneIndex);
//		printf("\tTextureIndex %d\n", bsp.mBrushSides[lBrushSideCounter].mTextureIndex);
//	}
//	printf("\n");
//    
//	printf("********* Vertex Lump *********\n");
//	for(int lVertexCounter = 0; lVertexCounter <bsp.mVertices.size(); ++lVertexCounter){
//		printf("Vertex %d\n", lVertexCounter);
//		printf("\tPosition : %f %f %f\n", bsp.mVertices[lVertexCounter].mPosition[0], bsp.mVertices[lVertexCounter].mPosition[1], bsp.mVertices[lVertexCounter].mPosition[2]);
//		printf("\tTexCoord0 : %f %f\n", bsp.mVertices[lVertexCounter].mTexCoord[0], bsp.mVertices[lVertexCounter].mTexCoord[1]);
//		printf("\tTexCoord0 : %f %f\n", bsp.mVertices[lVertexCounter].mTexCoord[2], bsp.mVertices[lVertexCounter].mTexCoord[3]);
//		printf("\tNormal : %f %f %f\n", bsp.mVertices[lVertexCounter].mNormal[0], bsp.mVertices[lVertexCounter].mNormal[1], bsp.mVertices[lVertexCounter].mNormal[2]);
//		printf("\tColor : %d %d %d %d\n", bsp.mVertices[lVertexCounter].mColor[0], bsp.mVertices[lVertexCounter].mColor[1], bsp.mVertices[lVertexCounter].mColor[2], bsp.mVertices[lVertexCounter].mColor[3]);
//	}
//	printf("\n");
//    
//	printf("********* MeshVert Lump *********\n");
//	for(int lMeshVertCounter = 0; lMeshVertCounter <bsp.mMeshVertices.size(); ++lMeshVertCounter){
//		printf("MeshVert %d\n", lMeshVertCounter);
//		printf("\tVertex Index : %d\n", bsp.mMeshVertices[lMeshVertCounter].mMeshVert);
//	}
//	printf("\n");
//    
//	printf("********* Effect Lump *********\n");
//	for(int lEffectCounter = 0; lEffectCounter <bsp.mEffects.size(); ++lEffectCounter){
//		printf("Effect %d\n", lEffectCounter);
//		printf("\tName : %s\n", bsp.mEffects[lEffectCounter].mName);
//		printf("\tBrush : %d\n", bsp.mEffects[lEffectCounter].mBrush);
//		printf("\tUnknown : %d\n", bsp.mEffects[lEffectCounter].mUnknown);
//	}
//	printf("\n");
//    
//	printf("********* Face Lump *********\n");
//	for(int lFaceCounter = 0; lFaceCounter <bsp.mFaces.size(); ++lFaceCounter){
//		printf("Face %d\n", lFaceCounter);
//		printf("\tTextureIndex : %d\n", bsp.mFaces[lFaceCounter].mTextureIndex);
//		printf("\tEffectIndex : %d\n", bsp.mFaces[lFaceCounter].mEffectIndex);
//		printf("\tType : %d\n", bsp.mFaces[lFaceCounter].mType);
//		printf("\tVertex : %d\n", bsp.mFaces[lFaceCounter].mVertex);
//		printf("\tNbVertices : %d\n", bsp.mFaces[lFaceCounter].mNbVertices);
//		printf("\tMeshVertex : %d\n", bsp.mFaces[lFaceCounter].mMeshVertex);
//		printf("\tNbMeshVertices : %d\n", bsp.mFaces[lFaceCounter].mNbMeshVertices);
//		printf("\tLightMapIndex : %d\n", bsp.mFaces[lFaceCounter].mLightmapIndex);
//		printf("\tLightMapCorner : %d %d\n", bsp.mFaces[lFaceCounter].mLightmapCorner[0], bsp.mFaces[lFaceCounter].mLightmapCorner[1]);
//		printf("\tLightmapSize : %d %d\n", bsp.mFaces[lFaceCounter].mLightmapSize[0], bsp.mFaces[lFaceCounter].mLightmapSize[1]);
//		printf("\tLightmapOrigin : %f %f %f\n", bsp.mFaces[lFaceCounter].mLightmapOrigin[0], bsp.mFaces[lFaceCounter].mLightmapOrigin[1], bsp.mFaces[lFaceCounter].mLightmapOrigin[2]);
//		printf("\tLightmapVecs S : %f %f %f\n", bsp.mFaces[lFaceCounter].mLightmapVecs[0][0], bsp.mFaces[lFaceCounter].mLightmapVecs[0][1], bsp.mFaces[lFaceCounter].mLightmapVecs[0][2]);
//		printf("\tLightmapVecs T : %f %f %f\n", bsp.mFaces[lFaceCounter].mLightmapVecs[1][0], bsp.mFaces[lFaceCounter].mLightmapVecs[1][1], bsp.mFaces[lFaceCounter].mLightmapVecs[1][2]);
//		printf("\tNormal : %f %f %f\n", bsp.mFaces[lFaceCounter].mNormal[0], bsp.mFaces[lFaceCounter].mNormal[1], bsp.mFaces[lFaceCounter].mNormal[2]);
//		printf("\tPatchSize : %d %d\n", bsp.mFaces[lFaceCounter].mPatchSize[0], bsp.mFaces[lFaceCounter].mPatchSize[1]);
//	}
//	printf("\n");
//    
//	printf("********* LightMap Lump *********\n");
//	printf("NbLightMaps %d\n", bsp.mLightMaps.size());
//	printf("\n");
//    
//	printf("********* LightVol Lump *********\n");
//	for(int lLightVolCounter = 0; lLightVolCounter <bsp.mLightVols.size(); ++lLightVolCounter){
//		printf("LightVol %d\n", lLightVolCounter);
//		printf("\tAmbient : %d %d %d\n", bsp.mLightVols[lLightVolCounter].mAmbient[0], bsp.mLightVols[lLightVolCounter].mAmbient[1], bsp.mLightVols[lLightVolCounter].mAmbient[2]);
//		printf("\tDirectional : %d %d %d\n", bsp.mLightVols[lLightVolCounter].mDirectional[0], bsp.mLightVols[lLightVolCounter].mDirectional[1], bsp.mLightVols[lLightVolCounter].mDirectional[2]);
//		printf("\tDir : %d %d\n", bsp.mLightVols[lLightVolCounter].mDir[0], bsp.mLightVols[lLightVolCounter].mDir[1]);
//	}
//	printf("\n");
//    
//	printf("********* VisData Lump *********\n");
//	printf("\tNbCluster %d\n", bsp.mVisData.mNbClusters);
//	printf("\tBytePerCluster %d\n", bsp.mVisData.mBytesPerCluster);
//	printf("\n");
//}

//////////////////////////////////////////////////////////////////////////