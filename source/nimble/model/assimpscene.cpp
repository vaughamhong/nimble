//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/model/assimpscene.h>

#if 0

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

///////////////////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::model;

//////////////////////////////////////////////////////////////////////////

//! Constructor
AssImpScene::AssImpScene()
:m_scene(0){
}
//! Constructor
AssImpScene::AssImpScene(const char *path){
    loadFromFile(path);
}
//! Destructor
AssImpScene::~AssImpScene(){
    if(m_scene){
        aiReleaseImport(m_scene);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Load from file
void AssImpScene::loadFromFile(const char *path){
    m_scene = aiImportFile(path, aiProcessPreset_TargetRealtime_MaxQuality);
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
AssImpSceneLoader::AssImpSceneLoader(){
}
//! Destructor
AssImpSceneLoader::~AssImpSceneLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* AssImpSceneLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<model::AssImpScene>();
    model::AssImpScene *pObject = dynamic_cast<model::AssImpScene*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

#endif

//////////////////////////////////////////////////////////////////////////