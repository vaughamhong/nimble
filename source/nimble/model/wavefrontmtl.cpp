//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/model/wavefrontmtl.h>
#include <nimble/core/tokenize.h>
#include <nimble/core/logger.h>
#include <nimble/core/filesystem.h>
#include <nimble/resource/resource.h>
#include <fstream>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::model;

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
WavefrontMTL::WavefrontMTL(){
}
//! Constructor
WavefrontMTL::WavefrontMTL(const char *path){
    loadFromFile(path);
}
//! Destructor
WavefrontMTL::~WavefrontMTL(){
}
//! Load from file
void WavefrontMTL::loadFromFile(const char *path){
    std::fstream fileStream(path, std::fstream::in | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("core", "Failed to open file %s - %s", path, strerror(errno));
        return;
    }
    
    std::vector<tinyobj::material_t> materials;
    std::string basePath = core::Path(path).getDirectory() + "/";
    tinyobj::parse_mtl(m_data, fileStream);
}
//! Finds material by name
WavefrontMTL::material_t* WavefrontMTL::findMaterialByName(const char *name){
    for(std::vector<tinyobj::material_t>::iterator it = m_data.materials.begin(); it != m_data.materials.end(); it++){
        if(it->name == std::string(name)){
            return &(*it);
        }
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
WavefrontMTLLoader::WavefrontMTLLoader(){
}
//! Destructor
WavefrontMTLLoader::~WavefrontMTLLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* WavefrontMTLLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<model::WavefrontMTL>();
    model::WavefrontMTL *pObject = dynamic_cast<model::WavefrontMTL*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////