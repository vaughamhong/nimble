//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/model/chumbalumsoftms3d.h>
#include <nimble/core/stream.buffer.h>
#include <nimble/core/filesystem.h>
#include <errno.h>

///////////////////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::model;

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
ChumbalumsoftMS3D::ChumbalumsoftMS3D(){
}
//! Constructor
ChumbalumsoftMS3D::ChumbalumsoftMS3D(const char *path){
    loadFromFile(path);
}
//! Destructor
ChumbalumsoftMS3D::~ChumbalumsoftMS3D(){
    short wIndex;
    
    // Free Vertex Data
    if(paVertices){
        delete [] paVertices;
        paVertices = 0;
    }
    // Free Triangle Data
    if(paTriangles){
        delete [] paTriangles;
        paTriangles = 0;
    }
    // Free Triangle indices
    for(wIndex = 0; wIndex < wNumGroups; wIndex += 1){
        if(paGroups[wIndex].wpaTriangleIndices){
            delete [] paGroups[wIndex].wpaTriangleIndices;
            paGroups[wIndex].wpaTriangleIndices = 0;
        }
    }
    // Free Group Data
    if(paGroups){
        delete [] paGroups;
        paGroups = 0;
    }
    // Free Material Data
    if(paMaterials){
        delete [] paMaterials;
        paMaterials = 0;
    }
    // Free Joint keyframes
    for(wIndex = 0; wIndex < wNumJoints; wIndex += 1){
        // for all rotation keyframes
        if(paJoints[wIndex].paKeyFrameRotations){
            delete [] paJoints[wIndex].paKeyFrameRotations;
            paJoints[wIndex].paKeyFrameRotations = 0;
        }
        // for all translation keyframes
        if(paJoints[wIndex].paKeyFrameTranslations){
            delete [] paJoints[wIndex].paKeyFrameTranslations;
            paJoints[wIndex].paKeyFrameTranslations = 0;
        }
    }
    // Free joint information
    if(paJoints){
        delete [] paJoints;
        paJoints = 0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////

//! Load from file
void ChumbalumsoftMS3D::loadFromFile(const char *path){
    std::fstream fileStream(path, std::fstream::in | std::fstream::out | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("core", "Failed to open file %s - %s", path, strerror(errno));
        return;
    }

    short wIndex;
    short wFrameIndex;
    
    // read and check ms3d header
    fileStream.read((char*)&this->header.cID[0], sizeof(this->header.cID));
    fileStream.read((char*)&this->header.iVersion, sizeof(this->header.iVersion));
    if(strncmp(this->header.cID, "MS3D000000", 10) != 0 || this->header.iVersion != 4){
        NIMBLE_LOG_ERROR("core", "Failed to load file %s - invalid header", path);
        return;
    }
    
    // read in Vertex Data
    fileStream.read((char*)&this->wNumVertices, sizeof(short));
    this->paVertices = new /*( external dynamic )*/ model::ChumbalumsoftMS3D::ms3dVertex[this->wNumVertices];
    for(wIndex = 0; wIndex < this->wNumVertices; wIndex += 1){
        fileStream.read((char*)&this->paVertices[wIndex].ucFlags, sizeof(unsigned char));
        fileStream.read((char*)&this->paVertices[wIndex].point, sizeof(nimble::math::Vector3f));
        fileStream.read((char*)&this->paVertices[wIndex].cBoneID, sizeof(char));
        fileStream.read((char*)&this->paVertices[wIndex].ucRefCount, sizeof(char));
    }
    
    //Read in Triangle Data
    fileStream.read((char*)&this->wNumTriangles, sizeof(short));
    this->paTriangles = new /*( external dynamic )*/ model::ChumbalumsoftMS3D::ms3dTriangle[this->wNumTriangles];
    for(wIndex = 0; wIndex < this->wNumTriangles; wIndex += 1){
        fileStream.read((char*)&this->paTriangles[wIndex].wFlags, sizeof(short));
        fileStream.read((char*) this->paTriangles[wIndex].waVertexIndices, sizeof(short) * 3);
        fileStream.read((char*)&this->paTriangles[wIndex].vertexNormals[0], sizeof(float) * 3);
        fileStream.read((char*)&this->paTriangles[wIndex].vertexNormals[1], sizeof(float) * 3);
        fileStream.read((char*)&this->paTriangles[wIndex].vertexNormals[2], sizeof(float) * 3);
        fileStream.read((char*) this->paTriangles[wIndex].faU, sizeof(float) * 3);
        fileStream.read((char*) this->paTriangles[wIndex].faV, sizeof(float) * 3);
        fileStream.read((char*)&this->paTriangles[wIndex].bySmoothingGroup, sizeof(char));
        fileStream.read((char*)&this->paTriangles[wIndex].byGroupIndex, sizeof(char));
    }
    
    //Read in Group Data
    fileStream.read((char*)&this->wNumGroups, sizeof(short));
    this->paGroups = new /*( external dynamic )*/ model::ChumbalumsoftMS3D::ms3dGroup[this->wNumGroups];
    for(wIndex = 0; wIndex < this->wNumGroups; wIndex += 1){
        fileStream.read((char*)&this->paGroups[wIndex].byFlags, sizeof(char));
        fileStream.read((char*) this->paGroups[wIndex].caName, sizeof(char) * 32);
        fileStream.read((char*)&this->paGroups[wIndex].wNumIndices, sizeof(short));
        
        this->paGroups[wIndex].wpaTriangleIndices = new /*( external dynamic )*/ short[this->paGroups[wIndex].wNumIndices];
        fileStream.read((char*) this->paGroups[wIndex].wpaTriangleIndices, sizeof(short) * this->paGroups[wIndex].wNumIndices);
        fileStream.read((char*)&this->paGroups[wIndex].cMaterialIndex, sizeof(char));
    }
    
    //Read in Material Data
    fileStream.read((char*)&this->wNumMaterials, sizeof(short));
    this->paMaterials = new /*( external dynamic )*/ model::ChumbalumsoftMS3D::ms3dMaterial[this->wNumMaterials];
    for(wIndex = 0; wIndex < this->wNumMaterials; wIndex += 1){
        fileStream.read((char*) this->paMaterials[wIndex].cName, sizeof(char) * 32);
        fileStream.read((char*) this->paMaterials[wIndex].fAmbient, sizeof(float) * 4);
        fileStream.read((char*) this->paMaterials[wIndex].fDiffuse, sizeof(float) * 4);
        fileStream.read((char*) this->paMaterials[wIndex].fSpecular, sizeof(float) * 4);
        fileStream.read((char*) this->paMaterials[wIndex].fEmissive, sizeof(float) * 4);
        fileStream.read((char*)&this->paMaterials[wIndex].fShininess, sizeof(float));
        fileStream.read((char*)&this->paMaterials[wIndex].fTransparency, sizeof(float));
        fileStream.read((char*)&this->paMaterials[wIndex].cMode, sizeof(char));
        fileStream.read((char*) this->paMaterials[wIndex].caTexture, sizeof(char) * 128);
        fileStream.read((char*) this->paMaterials[wIndex].caAlphamap, sizeof(char) * 128);
    }
    
    //Read in key frame data
    fileStream.read((char*)&this->fAnimationFPS, sizeof(float));
    fileStream.read((char*)&this->fCurrentTime, sizeof(float));
    fileStream.read((char*)&this->iTotalFrames, sizeof(int));
    
    //read joint information
    fileStream.read((char*)&this->wNumJoints, sizeof(short));
    this->paJoints = new /*( external dynamic )*/ model::ChumbalumsoftMS3D::ms3dJoint[this->wNumJoints];
    for(wIndex = 0; wIndex < this->wNumJoints; wIndex += 1){
        fileStream.read((char*)&this->paJoints[wIndex].byFlags, sizeof(char));
        fileStream.read((char*) this->paJoints[wIndex].caName, sizeof(char) * 32);
        fileStream.read((char*) this->paJoints[wIndex].caParentName, sizeof(char) * 32);
        fileStream.read((char*) this->paJoints[wIndex].fRotation, sizeof(float) * 3);
        fileStream.read((char*) this->paJoints[wIndex].fPosition, sizeof(float) * 3);
        fileStream.read((char*)&this->paJoints[wIndex].wNumKeyFrameRot, sizeof(short));
        fileStream.read((char*)&this->paJoints[wIndex].wNumKeyFrameTran, sizeof(short));
        
        // for all rotation keyframes
        model::ChumbalumsoftMS3D::ms3dKeyFrameRotation* tempFrameRot = new /*( external dynamic )*/ model::ChumbalumsoftMS3D::ms3dKeyFrameRotation[this->paJoints[wIndex].wNumKeyFrameRot];
        for(wFrameIndex = 0; wFrameIndex < this->paJoints[wIndex].wNumKeyFrameRot; wFrameIndex += 1){
            fileStream.read((char*)&tempFrameRot[wFrameIndex].fTime, sizeof(float));
            fileStream.read((char*)&tempFrameRot[wFrameIndex].fPosition, sizeof(float) * 3);
            tempFrameRot[wFrameIndex].fTime *= this->fAnimationFPS;
            tempFrameRot[wFrameIndex].fTime -= 1.0f;
        }
        this->paJoints[wIndex].paKeyFrameRotations = tempFrameRot;
        
        // for all translation keyframes
        model::ChumbalumsoftMS3D::ms3dKeyFramePosition* tempFramePos = new /*( external dynamic )*/ model::ChumbalumsoftMS3D::ms3dKeyFramePosition[this->paJoints[wIndex].wNumKeyFrameTran];
        for(wFrameIndex = 0; wFrameIndex < this->paJoints[wIndex].wNumKeyFrameTran; wFrameIndex += 1){
            fileStream.read((char*)&tempFramePos[wFrameIndex].fTime, sizeof(float));
            fileStream.read((char*) tempFramePos[wFrameIndex].fPosition, sizeof(float) * 3);
            tempFramePos[wFrameIndex].fTime *= this->fAnimationFPS;
            tempFramePos[wFrameIndex].fTime -= 1.0f;
        }
        this->paJoints[wIndex].paKeyFrameTranslations = tempFramePos;
    }
}

//////////////////////////////////////////////////////////////////////////

//! returns the group by name
model::ChumbalumsoftMS3D::ms3dGroup* ChumbalumsoftMS3D::getGroupByName(const char *name){
    for(int32_t i = 0; i < this->wNumGroups; i++){
        model::ChumbalumsoftMS3D::ms3dGroup &group = this->paGroups[i];
        if(strcmp(group.caName, name) == 0){
            return &group;
        }
    }
    NIMBLE_LOG_ERROR("model", "Failed to find group named %s for ChumbalumsoftMS3D", name);
    return 0;
}
//! returns the material by name
model::ChumbalumsoftMS3D::ms3dMaterial* ChumbalumsoftMS3D::getMaterialByName(const char *name){
    for(int32_t i = 0; i < this->wNumMaterials; i++){
        model::ChumbalumsoftMS3D::ms3dMaterial &material = this->paMaterials[i];
        if(strcmp(material.cName, name) == 0){
            return &material;
        }
    }
    NIMBLE_LOG_ERROR("model", "Failed to find material named %s for ChumbalumsoftMS3D", name);
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
ChumbalumsoftMS3DLoader::ChumbalumsoftMS3DLoader(){
}
//! Destructor
ChumbalumsoftMS3DLoader::~ChumbalumsoftMS3DLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* ChumbalumsoftMS3DLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<model::ChumbalumsoftMS3D>();
    model::ChumbalumsoftMS3D *pObject = dynamic_cast<model::ChumbalumsoftMS3D*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////