//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/model/tuscanyxml.h>
#include <nimble/resource/resource.h>
#include <nimble/core/filesystem.h>
#include <iterator>
#include <assert.h>
#include <fstream>
#include <errno.h>
#include "tinyxml2.h"

///////////////////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::model;

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
TuscanyXML::TuscanyXML(){
}
//! Constructor
TuscanyXML::TuscanyXML(const char *path){
    loadFromFile(path);
}
//! Destructor
TuscanyXML::~TuscanyXML(){
}

///////////////////////////////////////////////////////////////////////////////////////////////

//! load from file
void TuscanyXML::loadFromFile(const char* path){
    std::fstream fileStream(path, std::fstream::in | std::fstream::out | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("system", "Failed to open file %s - %s", path, strerror(errno));
        return;
    }
    
    // load our file into memory
    long fileSize = core::fileSize(path);
    size_t bufferSize = fileSize + 1;
    char *pBuffer = new /*( volatile )*/ char[bufferSize];
    fileStream.seekg(0, std::ios::beg);
    fileStream.read(pBuffer, fileSize);
    pBuffer[fileSize] = 0;
    fileStream.close();    
    tinyxml2::XMLDocument doc;
    doc.Parse(pBuffer);
    
    tinyxml2::XMLElement *pSceneNode = doc.FirstChildElement("scene");
    if(!pSceneNode){
        NIMBLE_LOG_ERROR("system", "Failed to parse Tuscany XML file - scene root node not found", path);
        return;
    }
    
    // load textures
    tinyxml2::XMLElement *pTexturesNode = pSceneNode->FirstChildElement("textures");
    if(pTexturesNode){
        for(tinyxml2::XMLElement const *pTextureNode = pTexturesNode->FirstChildElement("texture"); pTextureNode; pTextureNode = pTextureNode->NextSiblingElement()){
            std::string filename = pTextureNode->FindAttribute("fileName")->Value();
            assert(filename.size() < 32);
            
            TuscanyXML::TTexture texture;
            memset(texture.mFilename, 0, 32);
            memcpy(texture.mFilename, filename.c_str(), filename.size() <= 32 ? filename.size() : 32);
            this->mTextures.push_back(texture);
        }
    }
    
    // load models
    tinyxml2::XMLElement *pModelsNode = pSceneNode->FirstChildElement("models");
    if(pModelsNode){
        for(tinyxml2::XMLElement const *pModelNode = pModelsNode->FirstChildElement("model"); pModelNode; pModelNode = pModelNode->NextSiblingElement()){
            if(0 != atoi(pModelNode->FindAttribute("isCollisionModel")->Value())){
                continue;
            }
            
            tinyxml2::XMLElement const *pVerticesNode = pModelNode->FirstChildElement("vertices");
            tinyxml2::XMLElement const *pNormalsNode = pModelNode->FirstChildElement("normals");
            tinyxml2::XMLElement const *pMaterialDiffuseNode = pModelNode->FirstChildElement("material");
            tinyxml2::XMLElement const *pTexCoordDiffuseNode = pMaterialDiffuseNode->FirstChildElement("texture");
            tinyxml2::XMLElement const *pMaterialLightmapNode = pMaterialDiffuseNode->NextSiblingElement();
            tinyxml2::XMLElement const *pTexCoordLightmapNode = pMaterialLightmapNode->FirstChildElement("texture");
            tinyxml2::XMLElement const *pIndicesNode = pModelNode->FirstChildElement("indices");

            int textureIndex = atoi(pTexCoordDiffuseNode->FindAttribute("index")->Value());
            int lightmapIndex = atoi(pTexCoordLightmapNode->FindAttribute("index")->Value());

            // use stream iterators to copy the stream to the vector as whitespace separated strings
            std::stringstream verticesStringBuffer(pVerticesNode->GetText());
            std::istream_iterator<float> verticesStringIt(verticesStringBuffer);
            std::vector<float> vertices(verticesStringIt, std::istream_iterator<float>());
            std::stringstream normalsStringBuffer(pNormalsNode->GetText());
            std::istream_iterator<float> normalsStringIt(normalsStringBuffer);
            std::vector<float> normals(normalsStringIt, std::istream_iterator<float>());
            std::stringstream texCoordDiffuseStringBuffer(pTexCoordDiffuseNode->GetText() ? pTexCoordDiffuseNode->GetText() : "");
            std::istream_iterator<float> texCoordDiffuseStringIt(texCoordDiffuseStringBuffer);
            std::vector<float> texCoordDiffuse(texCoordDiffuseStringIt, std::istream_iterator<float>());
            std::stringstream texCoordLightmapStringBuffer(pTexCoordLightmapNode->GetText() ? pTexCoordLightmapNode->GetText() : "");
            std::istream_iterator<float> texCoordLightmapStringIt(texCoordLightmapStringBuffer);
            std::vector<float> texCoordLightmap(texCoordLightmapStringIt, std::istream_iterator<float>());
            std::stringstream indicesStringBuffer(pIndicesNode->GetText());
            std::istream_iterator<int> indicesStringIt(indicesStringBuffer);
            std::vector<int> indices(indicesStringIt, std::istream_iterator<int>());
            
            assert(vertices.size() % 3 == 0);
            assert(normals.size() / 3 == vertices.size() / 3);
            assert(indices.size() % 3 == 0);
            
            int startVertex = (int)this->mVertices.size();
            int startIndex = (int)this->mIndices.size();
            
            // process vertices
            int numVertices = (int)vertices.size() / 3;
            for(int i = 0; i < numVertices; i++){
                TuscanyXML::TVertex vertex;
                vertex.mPosition[0] = vertices[i * 3 + 0];
                vertex.mPosition[1] = vertices[i * 3 + 1];
                vertex.mPosition[2] = vertices[i * 3 + 2];
                vertex.mNormal[0] = normals[i * 3 + 0];
                vertex.mNormal[1] = normals[i * 3 + 1];
                vertex.mNormal[2] = normals[i * 3 + 2];
                if(texCoordDiffuse.size() > 0){
                    vertex.mTexCoord[0][0] = texCoordDiffuse[i * 2 + 0];
                    vertex.mTexCoord[0][1] = (1.0f - texCoordDiffuse[i * 2 + 1]);
                }else{
                    vertex.mTexCoord[0][0] = 0.0f;
                    vertex.mTexCoord[0][1] = 0.0f;
                }
                if(texCoordLightmap.size() > 0){
                    vertex.mTexCoord[1][0] = texCoordLightmap[i * 2 + 0];
                    vertex.mTexCoord[1][1] = (1.0f - texCoordLightmap[i * 2 + 1]);
                }else{
                    vertex.mTexCoord[1][0] = 0.0f;
                    vertex.mTexCoord[1][1] = 0.0f;
                }
                this->mVertices.push_back(vertex);
            }
            
            // process indices
            int numIndices = (int)indices.size();
            for(int i = 0; i < numIndices; i++){
                TuscanyXML::TIndex index;
                index.mIndex = indices[i];
                this->mIndices.push_back(index);
            }
            
            // models
            TuscanyXML::TModel model;
            model.mNumIndices = numIndices;
            model.startIndex = startIndex;
            model.mNumVertices = numVertices;
            model.startVertex = startVertex;
            model.textureIndex = textureIndex;
            model.lightmapIndex = lightmapIndex;
            std::string name = pModelNode->FindAttribute("name")->Value();
            memset(model.mName, 0, 32);
            memcpy(model.mName, name.c_str(), name.size() <= 32 ? name.size() : 31);
            this->mModels.push_back(model);
        }
    }
    
    // generate collision data
    tinyxml2::XMLElement *pGroundCollisionModelsNode = pSceneNode->FirstChildElement("groundCollisionModels");
    if(pGroundCollisionModelsNode){
        for(tinyxml2::XMLElement const *pGroundCollisionModelNode = pGroundCollisionModelsNode->FirstChildElement("collisionModel"); pGroundCollisionModelNode; pGroundCollisionModelNode = pGroundCollisionModelNode->NextSiblingElement()){
            int planeStartIndex = (int)this->mPlanes.size();
            for(tinyxml2::XMLElement const *pPlaneNode = pGroundCollisionModelNode->FirstChildElement("plane"); pPlaneNode; pPlaneNode = pPlaneNode->NextSiblingElement()){
                TuscanyXML::TPlane plane;
                plane.mNormal[0] = (float)atof(pPlaneNode->FindAttribute("nx")->Value());
                plane.mNormal[1] = (float)atof(pPlaneNode->FindAttribute("ny")->Value());
                plane.mNormal[2] = (float)atof(pPlaneNode->FindAttribute("nz")->Value());
                plane.mDistance = (float)atof(pPlaneNode->FindAttribute("d")->Value());
                this->mPlanes.push_back(plane);
            }
            if(planeStartIndex != (int)this->mPlanes.size()){
                TuscanyXML::TCollisionModel collisionModel;
                collisionModel.mPlaneIndex = planeStartIndex;
                collisionModel.mNumPlanes = (int)(this->mPlanes.size() - planeStartIndex);
                this->mCollisionModels.push_back(collisionModel);
            }
        }
    }
    tinyxml2::XMLElement *pCollisionModelsNode = pSceneNode->FirstChildElement("collisionModels");
    if(pCollisionModelsNode){
        for(tinyxml2::XMLElement const *pCollisionModelNode = pCollisionModelsNode->FirstChildElement("collisionModel"); pCollisionModelNode;
            pCollisionModelNode = pCollisionModelNode->NextSiblingElement()){
            int planeStartIndex = (int)this->mPlanes.size();
            for(tinyxml2::XMLElement const *pPlaneNode = pCollisionModelNode->FirstChildElement("plane"); pPlaneNode; pPlaneNode = pPlaneNode->NextSiblingElement()){
                TuscanyXML::TPlane plane;
                plane.mNormal[0] = (float)atof(pPlaneNode->FindAttribute("nx")->Value());
                plane.mNormal[1] = (float)atof(pPlaneNode->FindAttribute("ny")->Value());
                plane.mNormal[2] = (float)atof(pPlaneNode->FindAttribute("nz")->Value());
                plane.mDistance = (float)atof(pPlaneNode->FindAttribute("d")->Value());
                this->mPlanes.push_back(plane);
            }
            if(planeStartIndex != (int)this->mPlanes.size()){
                TuscanyXML::TCollisionModel collisionModel;
                collisionModel.mPlaneIndex = planeStartIndex;
                collisionModel.mNumPlanes = (int)(this->mPlanes.size() - planeStartIndex);
                this->mCollisionModels.push_back(collisionModel);
            }
        }
    }
    
    // clean up
    delete [] pBuffer;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
TuscanyXMLLoader::TuscanyXMLLoader(){
}
//! Destructor
TuscanyXMLLoader::~TuscanyXMLLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* TuscanyXMLLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<model::TuscanyXML>();
    model::TuscanyXML *pObject = dynamic_cast<model::TuscanyXML*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////