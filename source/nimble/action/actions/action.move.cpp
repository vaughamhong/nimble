//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actions/action.move.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
MoveAction::MoveAction(math::Vector3f destination, float duration, math::eCurve curve)
:action::AnimationAction("MoveAction", duration, curve)
,m_elapsedTime(0.0f)
,m_startPosition()
,m_targetPosition(destination){
}
//! Constructor
MoveAction::~MoveAction(){
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void MoveAction::initialize(action::ActionQueue *pActionQueue){
    action::Action::initialize(pActionQueue);
}
//! Destroys this action
void MoveAction::destroy(){
    action::Action::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void MoveAction::start(){
    action::AnimationAction::start();

    // get our scene node
    scene::SceneTransform *pTransform = getSceneTransform();
    if(pTransform){
        // grab our current position before we start running
        m_startPosition = pTransform->getTranslation();
    }else{
        NIMBLE_LOG_ERROR("action", "Failed to query and setup transform node");
    }
    
    // start from the beginning
    m_elapsedTime = 0.0f;
}
//! Stops this action
void MoveAction::stop(){
    action::AnimationAction::stop();
}

//////////////////////////////////////////////////////////////////////////

//! returns true if this action has completed
bool MoveAction::isFinished(){
    scene::SceneTransform *pTransform = getSceneTransform();
    return pTransform->getTranslation() == m_targetPosition;
}
//! Updates this controller
void MoveAction::update(double elapsedTime){
    if(!this->isFinished()){
        m_elapsedTime += elapsedTime;
        
        // time interval
        float duration = getDuration();
        float t = 1.0f;
        if(duration > 0.0f){
            t = math::min((float)(m_elapsedTime / duration), 1.0f);
        }
        
        // animate
        math::eCurve curve = getCurve();
        math::Vector3f p0 = m_startPosition;
        math::Vector3f p1 = m_targetPosition;
        math::Vector3f p2 = math::interpolate(t, p0, p1, curve);

        // set our new position
        scene::SceneTransform *pTransform = getSceneTransform();
        pTransform->setTranslation(p2);
    }
}

//////////////////////////////////////////////////////////////////////////