//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actions/action.repeat.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
RepeatAction::RepeatAction(action::Action *pAction, int32_t count)
:action::Action("RepeatAction")
,m_count(count)
,m_currentCount(count)
,m_action(pAction){
}
//! Constructor
RepeatAction::RepeatAction(action::ActionPtr action, int32_t count)
:action::Action("RepeatAction")
,m_count(count)
,m_currentCount(count)
,m_action(action){
}
//! Constructor
RepeatAction::~RepeatAction(){
    m_action = action::ActionPtr();
    m_count = 0;
    m_currentCount = 0;
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void RepeatAction::initialize(action::ActionQueue *pActionQueue){
    action::Action::initialize(pActionQueue);
    m_action->initialize(pActionQueue);
}
//! Destroys this action
void RepeatAction::destroy(){
    m_action->destroy();
    action::Action::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void RepeatAction::start(){
    action::Action::start();
    m_currentCount = m_count;
}
//! Stops this action
void RepeatAction::stop(){
    action::Action::stop();
}

//////////////////////////////////////////////////////////////////////////

//! returns true if this action has completed
bool RepeatAction::isFinished(){
    return m_currentCount == 0;
}
//! Updates this controller
void RepeatAction::update(double elapsedTime){
    if(!this->isFinished()){
        // initialize if the front action has not
        if(!m_action->hasStarted()){
            m_action->start();
        }
        
        // run our action update
        m_action->update(elapsedTime);
        
        // clean up if we are finished
        if(m_action->isFinished()){
            m_action->stop();
            m_currentCount -= 1;
        }
    }
}

//////////////////////////////////////////////////////////////////////////