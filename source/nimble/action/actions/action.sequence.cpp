//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actions/action.sequence.h>
#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
SequenceAction::SequenceAction(action::Action *pAction, ...)
:action::Action("SequenceAction"){
    va_list ap;
    va_start(ap, pAction);
    m_actions.push_back(pAction);
    while(action::Action *pNextAction = va_arg(ap, action::Action*)){
        m_actions.push_back(pNextAction);
    }
    va_end(ap);
}
//! Constructor
SequenceAction::SequenceAction(action::ActionPtr action, ...)
:action::Action("SequenceAction"){
    va_list ap;
    va_start(ap, action);
    m_actions.push_back(action);
    while(action::Action *pNextAction = va_arg(ap, action::Action*)){
        m_actions.push_back(pNextAction);
    }
    va_end(ap);
}
//! Constructor
SequenceAction::~SequenceAction(){
    m_actions.clear();
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void SequenceAction::initialize(action::ActionQueue *pActionQueue){
    action::Action::initialize(pActionQueue);
    for(SequenceAction::ActionList::iterator it = m_actions.begin(); it != m_actions.end(); it++){
        (*it)->initialize(pActionQueue);
    }
}
//! Destroys this action
void SequenceAction::destroy(){
    for(SequenceAction::ActionList::iterator it = m_actions.begin(); it != m_actions.end(); it++){
        (*it)->destroy();
    }
    action::Action::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void SequenceAction::start(){
    action::Action::start();
    m_sequence = m_actions;
}
//! Stops this action
void SequenceAction::stop(){
    action::Action::stop();
}

//////////////////////////////////////////////////////////////////////////

//! returns true if this action has completed
bool SequenceAction::isFinished(){
    return m_sequence.size() == 0;
}
//! Updates this controller
void SequenceAction::update(double elapsedTime){
    if(!this->isFinished()){
        action::ActionPtr action = m_sequence.front();
        
        // initialize if the front action has not
        if(!action->hasStarted()){
            action->start();
        }
        
        // run our action update
        action->update(elapsedTime);
        
        // clean up if we are finished
        if(action->isFinished()){
            action->stop();
            m_sequence.erase(m_sequence.begin());
        }
    }
}

//////////////////////////////////////////////////////////////////////////