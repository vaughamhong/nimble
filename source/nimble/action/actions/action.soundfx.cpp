//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actions/action.soundfx.h>
#include <nimble/audiodevice/iaudiobuffer.h>
#include <nimble/audiodevice/iaudioplayer.h>
#include <nimble/audiodevice/iaudiodevice.h>
#include <nimble/core/locator.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
SoundFxAction::SoundFxAction(audiodevice::IAudioBuffer *pAudioBuffer, audiodevice::IAudioDevice *pAudioDevice, float pitch, float gain)
:action::Action("SoundFxAction")
,m_pAudioDevice(pAudioDevice)
,m_pAudioBuffer(pAudioBuffer)
,m_pAudioPlayer(0)
,m_pitch(pitch)
,m_gain(gain){
}
//! Constructor
SoundFxAction::~SoundFxAction(){
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void SoundFxAction::initialize(action::ActionQueue *pActionQueue){
    action::Action::initialize(pActionQueue);
    if(m_pAudioBuffer){
        if(!m_pAudioDevice){
            m_pAudioDevice = core::locator_acquire<audiodevice::IAudioDevice>();
        }
        if(m_pAudioDevice){
            m_pAudioPlayer = m_pAudioDevice->createAudioPlayer();
        }else{
            NIMBLE_LOG_ERROR("action", "Failed to locate and aquire audio device");
        }
        m_pAudioPlayer->setPitch(m_pitch);
        m_pAudioPlayer->setGain(m_gain);
        m_pAudioPlayer->bindAudioBuffer(m_pAudioBuffer);
    }
}
//! Destroys this action
void SoundFxAction::destroy(){
    if(m_pAudioPlayer){
        delete m_pAudioPlayer;
        m_pAudioPlayer = 0;
    }
    m_pAudioDevice = 0;
    action::Action::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void SoundFxAction::start(){
    action::Action::start();
    if(m_pAudioPlayer){
        m_pAudioPlayer->play();
    }
}
//! Stops this action
void SoundFxAction::stop(){
    action::Action::stop();
    if(m_pAudioPlayer){
        m_pAudioPlayer->stop();
    }
}

//////////////////////////////////////////////////////////////////////////

//! returns true if this action has completed
bool SoundFxAction::isFinished(){
    if(m_pAudioPlayer){
        return !m_pAudioPlayer->isPlaying();
    }
    return true;
}
//! Updates this controller
void SoundFxAction::update(double elapsedTime){
    if(m_pAudioPlayer){
        m_pAudioPlayer->update(elapsedTime);
    }
}

//////////////////////////////////////////////////////////////////////////