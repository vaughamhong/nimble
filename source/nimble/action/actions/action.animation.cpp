//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actions/action.animation.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
AnimationAction::AnimationAction(const char *name, float duration, math::eCurve curve)
:action::Action(name)
,m_pSceneTransform(0)
,m_duration(duration)
,m_curve(curve){
}
//! Constructor
AnimationAction::~AnimationAction(){
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void AnimationAction::initialize(action::ActionQueue *pActionQueue){
    action::Action::initialize(pActionQueue);
}
//! Destroys this action
void AnimationAction::destroy(){
    action::Action::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void AnimationAction::start(){
    action::Action::start();
}
//! Stops this action
void AnimationAction::stop(){
    action::Action::stop();
}

//////////////////////////////////////////////////////////////////////////

//! sets our scene transform
void AnimationAction::setSceneTransform(scene::SceneTransform *pTransform){
    m_pSceneTransform = pTransform;
}
//! gets our scene transform
scene::SceneTransform* AnimationAction::getSceneTransform() const{
    return m_pSceneTransform;
}

//////////////////////////////////////////////////////////////////////////

//! returns the curve type
math::eCurve AnimationAction::getCurve(){
    return m_curve;
}
//! returns the duration
float AnimationAction::getDuration(){
    return m_duration;
}

//////////////////////////////////////////////////////////////////////////