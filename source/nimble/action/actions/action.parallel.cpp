//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actions/action.parallel.h>
#include <nimble/action/actionqueue.h>
#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ParallelAction::ParallelAction(action::Action *pAction, ...)
:action::Action("ParallelAction"){
    va_list ap;
    va_start(ap, pAction);
    m_actions.push_back(pAction);
    while(action::Action *pNextAction = va_arg(ap, action::Action*)){
        m_actions.push_back(pNextAction);
    }
    va_end(ap);
}
//! Constructor
ParallelAction::ParallelAction(action::ActionPtr action, ...)
:action::Action("ParallelAction"){
    va_list ap;
    va_start(ap, action);
    m_actions.push_back(action);
    while(action::Action *pNextAction = va_arg(ap, action::Action*)){
        m_actions.push_back(pNextAction);
    }
    va_end(ap);
}
//! Constructor
ParallelAction::~ParallelAction(){
    m_actions.clear();
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void ParallelAction::initialize(action::ActionQueue *pActionQueue){
    action::Action::initialize(pActionQueue);
    for(ParallelAction::ActionList::iterator it = m_actions.begin(); it != m_actions.end(); it++){
        (*it)->initialize(pActionQueue);
    }
}
//! Destroys this action
void ParallelAction::destroy(){
    for(ParallelAction::ActionList::iterator it = m_actions.begin(); it != m_actions.end(); it++){
        (*it)->destroy();
    }
    action::Action::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void ParallelAction::start(){
    action::Action::start();
}
//! Stops this action
void ParallelAction::stop(){
    action::Action::stop();
}

//////////////////////////////////////////////////////////////////////////

//! returns true if this action has completed
bool ParallelAction::isFinished(){
    for(ParallelAction::ActionList::iterator it = m_actions.begin(); it != m_actions.end(); it++){
        if(!(*it)->isFinished()){
            return false;
        }
    }
    return true;
}
//! Updates this controller
void ParallelAction::update(double elapsedTime){
    if(!this->isFinished()){
        for(ParallelAction::ActionList::iterator it = m_actions.begin(); it != m_actions.end(); it++){
            (*it)->update(elapsedTime);
        }
    }
}

//////////////////////////////////////////////////////////////////////////