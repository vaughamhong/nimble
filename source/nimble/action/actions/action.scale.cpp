//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actions/action.scale.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ScaleAction::ScaleAction(math::Vector3f magnitude, float duration, math::eCurve curve)
:action::AnimationAction("ScaleAction", duration, curve)
,m_elapsedTime(0.0f)
,m_startMagnitude()
,m_targetMagnitude(magnitude){
}
//! Constructor
ScaleAction::~ScaleAction(){
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void ScaleAction::initialize(action::ActionQueue *pActionQueue){
    action::Action::initialize(pActionQueue);
}
//! Destroys this action
void ScaleAction::destroy(){
    action::Action::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void ScaleAction::start(){
    action::AnimationAction::start();

    // get our scene node
    scene::SceneTransform *pTransform = getSceneTransform();
    if(pTransform != 0){
        // grab our current position before we start running
        m_startMagnitude = pTransform->getScale();
    }else{
        NIMBLE_LOG_ERROR("action", "Failed to query and setup transform node");
    }
    // start from the beginning
    m_elapsedTime = 0.0f;
}
//! Stops this action
void ScaleAction::stop(){
    action::AnimationAction::stop();
}

//////////////////////////////////////////////////////////////////////////

//! returns true if this action has completed
bool ScaleAction::isFinished(){
    scene::SceneTransform *pTransform = getSceneTransform();
    return pTransform->getScale() == m_targetMagnitude;
}
//! Updates this controller
void ScaleAction::update(double elapsedTime){
    if(!this->isFinished()){
        m_elapsedTime += elapsedTime;
        
        // time interval
        float duration = getDuration();
        float t = 1.0f;
        if(duration > 0.0f){
            t = math::min((float)(m_elapsedTime / duration), 1.0f);
        }
        
        // animate
        math::eCurve curve = getCurve();
        math::Vector3f p0 = m_startMagnitude;
        math::Vector3f p1 = m_targetMagnitude;
        math::Vector3f p2 = math::interpolate(t, p0, p1, curve);
        
        // set our new scale
        scene::SceneTransform *pTransform = getSceneTransform();
        pTransform->setScale(p2);
    }
}

//////////////////////////////////////////////////////////////////////////