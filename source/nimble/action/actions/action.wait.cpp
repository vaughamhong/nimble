//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actions/action.wait.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
WaitAction::WaitAction(float duration)
:action::Action("WaitAction")
,m_waitDuration(duration){
}
//! Constructor
WaitAction::~WaitAction(){
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void WaitAction::initialize(action::ActionQueue *pActionQueue){
    action::Action::initialize(pActionQueue);
}
//! Destroys this action
void WaitAction::destroy(){
    action::Action::destroy();
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void WaitAction::start(){
    action::Action::start();
    m_currentWaitDuration = 0.0;
}
//! Stops this action
void WaitAction::stop(){
    action::Action::stop();
}

//////////////////////////////////////////////////////////////////////////

//! returns true if this action has completed
bool WaitAction::isFinished(){
    return m_currentWaitDuration >= m_waitDuration;
}
//! Updates this controller
void WaitAction::update(double elapsedTime){
    if(!this->isFinished()){
        m_currentWaitDuration += elapsedTime;
    }
}

//////////////////////////////////////////////////////////////////////////