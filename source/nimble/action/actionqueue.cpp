//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/actionqueue.h>
#include <nimble/action/action.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ActionQueue::ActionQueue(){
}
//! Destructor
ActionQueue::~ActionQueue(){
}

//////////////////////////////////////////////////////////////////////////

//! Enqueue an action
void ActionQueue::enqueue(action::ActionPtr action){
    action->initialize(this);
    m_actionQueue.push(action);
}
//! Dequeue an action
void ActionQueue::dequeue(){
    action::ActionPtr action = m_actionQueue.back();
    m_actionQueue.pop();
    action->destroy();
}
//! Returns the size of the action list
uint32_t ActionQueue::getQueueSize() const{
    return m_actionQueue.size();
}
//! Returns the front action
action::ActionPtr ActionQueue::getFrontAction(){
    return m_actionQueue.front();
}
//! clears queue
void ActionQueue::clear(){
    while(m_actionQueue.size()){
        m_actionQueue.pop();
    }
}

//////////////////////////////////////////////////////////////////////////