//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/action/action.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::action;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Action::Action(const char *name)
:m_name(name)
,m_hasStarted(false)
,m_pActionQueue(0){
}
//! Destructor
Action::~Action(){
}

//////////////////////////////////////////////////////////////////////////

//! Initializes this action
void Action::initialize(action::ActionQueue *pActionQueue){
    m_pActionQueue = pActionQueue;
}
//! Destroys this action
void Action::destroy(){
    m_pActionQueue = 0;
}

//////////////////////////////////////////////////////////////////////////

//! Starts this action
void Action::start(){
    m_hasStarted = true;
}
//! Stops this action
void Action::stop(){
    m_hasStarted = false;
}

//////////////////////////////////////////////////////////////////////////

//! returns if we have started
bool Action::hasStarted(){
    return m_hasStarted;
}
//! gets the message queue
action::ActionQueue* Action::getActionQueue(){
    return m_pActionQueue;
}

//////////////////////////////////////////////////////////////////////////