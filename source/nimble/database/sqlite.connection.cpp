//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/database/sqlite.connection.h>
#include <nimble/core/error.h>

////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::db;

////////////////////////////////////////////////////////////////////////////////

#define DBERRORS_TUPLE(VAR, CODE, CATEGORY, STRING) nimble::core::Error nimble::db::VAR(CODE, CATEGORY, STRING);
    DBERRORS_TUPLESET
#undef DBERRORS_TUPLE

////////////////////////////////////////////////////////////////////////////////

//! static creation method
core::Error Connection::create(Connection **ppConnection, const char *dbPath, bool isWritable, int32_t busyMaxRetries, int32_t busyWaitMs){
    try{
        *ppConnection = new /*( external dynamic )*/ Connection(dbPath, isWritable, busyMaxRetries, busyWaitMs);
        return core::kSuccess;
    }catch(core::Error e){
        return e;
    }
}

////////////////////////////////////////////////////////////////////////////////

//! Constructor
Connection::Connection(const char *dbPath, bool isWritable, int32_t busyMaxRetries, int32_t busyWaitMs)
:m_pHandle(0)
,m_dbPath(dbPath)
,m_isWritable(isWritable)
,m_inTransaction(false)
,m_busyMaxRetries(busyMaxRetries)
,m_busyWaitMs(busyWaitMs){
    int flags = (m_isWritable ? SQLITE_OPEN_READWRITE : SQLITE_OPEN_READONLY);
    int returnCode = sqlite3_open_v2(dbPath, &m_pHandle, flags, 0);
    if(returnCode == SQLITE_OK){
        sqlite3_busy_handler(m_pHandle, sqliteBusyHandler, this);
    }else{
        throw core::kFailure;
    }
}
//! Destructor
Connection::~Connection(){
    sqlite3_close(m_pHandle);
}

////////////////////////////////////////////////////////////////////////////////

//! returns the busy max retries
int32_t Connection::getBusyMaxRetries() const{
    return m_busyMaxRetries;
}
//! returns the busy wait ms
int32_t Connection::getBusyWaitMs() const{
    return m_busyWaitMs;
}
//! returns true if this is a write connection
bool Connection::isWritable() const{
    return m_isWritable;
}

////////////////////////////////////////////////////////////////////////////////

//! validates connection
bool Connection::isConnectionValid(){
    return (m_pHandle != 0);
}

////////////////////////////////////////////////////////////////////////////////

//! called when trying to access locked table
int Connection::sqliteBusyHandler(void *data, int retry){
    Connection *pConnection = (Connection*)data;
    if(retry < pConnection->getBusyMaxRetries()){
        // sleep before trying again
		sqlite3_sleep(pConnection->getBusyWaitMs());
        // non-zero return lets caller retry again
		return 1;
	}
    // zero return lets caller return SQLITE_BUSY
	return 0;
}

////////////////////////////////////////////////////////////////////////////////

//! runs a database script on the current database
core::Error Connection::runScript(const char *scriptPath){
    return core::kNotImplemented;
}

////////////////////////////////////////////////////////////////////////////////