//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/database/sqlite.connection.h>
#include <nimble/core/error.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>

////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::db;

////////////////////////////////////////////////////////////////////////////////

//! starts a transaction
core::Error Connection::beginTransaction(){
    if(m_inTransaction){
        NIMBLE_LOG_WARNING("database", "Connection trying to begin transaction when already in a transaction");
        return core::Error(SQLITE_MISUSE, "db", sqlite3_errmsg(m_pHandle));
    }else{
        if(!isConnectionValid()){
            return kInvalidDBConnection;
        }
        int returnCode = sqlite3_exec(m_pHandle, "BEGIN", 0, 0, 0);
        if(returnCode == SQLITE_OK){
            m_inTransaction = true;
            return core::kSuccess;
        }else{
            NIMBLE_LOG_ERROR("database", sqlite3_errmsg(m_pHandle));
            return db::kBeginTransactionFail;
        }
    }
}
//! commits a transaction
core::Error Connection::commitTransaction(){
    if(!m_inTransaction){
        NIMBLE_LOG_WARNING("database", "Connection trying to commit when not in a transaction");
        return core::Error(SQLITE_MISUSE, "db", sqlite3_errmsg(m_pHandle));
    }else{
        if(m_pHandle == 0){
            return core::Error(SQLITE_MISUSE, "db", sqlite3_errmsg(m_pHandle));
        }
        int returnCode = sqlite3_exec(m_pHandle, "COMMIT", 0, 0, 0);
        if(returnCode == SQLITE_OK){
            m_inTransaction = false;
            return core::kSuccess;
        }else{
            NIMBLE_LOG_ERROR("database", sqlite3_errmsg(m_pHandle));
            return db::kCommitTransactionFail;
        }
    }
}
//! rolls back a transaction
core::Error Connection::rollbackTransaction(){
    if(!m_inTransaction){
        NIMBLE_LOG_WARNING("database", "Connection trying to rollback when not in a transaction");
        return core::Error(SQLITE_MISUSE, "db", sqlite3_errmsg(m_pHandle));
    }else{
        if(m_pHandle == 0){
            return core::Error(SQLITE_MISUSE, "db", sqlite3_errmsg(m_pHandle));
        }
        int returnCode = sqlite3_exec(m_pHandle, "ROLLBACK", 0, 0, 0);
        if(returnCode == SQLITE_OK){
            m_inTransaction = false;
            return core::kSuccess;
        }else{
            NIMBLE_LOG_ERROR("database", sqlite3_errmsg(m_pHandle));
            return db::kRollbackTransactionFail;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////