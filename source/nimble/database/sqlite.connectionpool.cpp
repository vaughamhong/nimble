//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/database/sqlite.connectionpool.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <algorithm>

////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::db;

////////////////////////////////////////////////////////////////////////////////

//! Constructor
ConnectionPool::ConnectionPool(const char* dbPath, int32_t maxConnections, bool isWritable)
:m_dbPath(dbPath)
,m_maxConnections(maxConnections)
,m_isWritable(isWritable){
    NIMBLE_ASSERT_PRINT(m_maxConnections > 0, "Invalid max connection pools parameter");
    
    // create connections
    for(int32_t i = 0; i < m_maxConnections; i++){
        Connection *pNewConnection = 0;
        core::Error error = Connection::create(&pNewConnection, dbPath, m_isWritable);
        NIMBLE_ASSERT_PRINT(error == core::kSuccess, error.getString());
        m_freeConnections.push_back(pNewConnection);
        m_connections.push_back(pNewConnection);
    }
}
//! destructtor
ConnectionPool::~ConnectionPool(){
    // check if our connections have all been released
    if(m_connections.size() != m_freeConnections.size()){
        NIMBLE_LOG_WARNING("database", "Not all connections have been released while attempting to destroy connection pool");
    }
    // clean up connections
    for(int32_t i = 0; i < m_maxConnections; i++){
        Connection *pConnection = m_connections.back();
        delete pConnection;
        m_connections.pop_back();
    }
    m_connections.clear();
    m_freeConnections.clear();
}

////////////////////////////////////////////////////////////////////////////////

//! returns the database path
const char* ConnectionPool::getDbPath() const{
    return m_dbPath.c_str();
}
//! returns the number of free connections
uint32_t ConnectionPool::getNumFreeConnections(){
    core::ScopedLock lock(&m_connectionsLock);
    return m_connections.size();
}
//! returns if this pool contains writable connections
bool ConnectionPool::isWritableConnectionPool() const{
    return m_isWritable;
}

////////////////////////////////////////////////////////////////////////////////

//! acquires a connection from the pool
Connection* ConnectionPool::acquire(){
    Connection *pConnection = 0;
    core::ScopedLock lock(&m_connectionsLock);
    if(m_freeConnections.size() > 0){
        pConnection = m_freeConnections.back();
        m_freeConnections.pop_back();
    }
    return pConnection;
}
//! releases a connection back into the pool
void ConnectionPool::release(Connection *pConnection){
    if(ownsConnection(pConnection)){
        core::ScopedLock lock(&m_connectionsLock);
        if(std::find(m_freeConnections.begin(), m_freeConnections.end(), pConnection) == m_freeConnections.end()){
            m_freeConnections.push_back(pConnection);
        }else{
            NIMBLE_LOG_WARNING("database", "Released connection was already free");
        }
    }else{
        NIMBLE_LOG_WARNING("database", "This connection pool does not own the connection being released");
    }
}

////////////////////////////////////////////////////////////////////////////////

//! owns this connection
bool ConnectionPool::ownsConnection(Connection *pConnection){
    ConnectionList::iterator it = std::find(m_connections.begin(), m_connections.end(), pConnection);
    return (it != m_connections.end());
}

////////////////////////////////////////////////////////////////////////////////