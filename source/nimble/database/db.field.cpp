//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/database/db.field.h>
#include <cstdlib>

////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::db;

////////////////////////////////////////////////////////////////////////////////

//! Constructor
Field::Field()
:m_type(Field::kTypeNull)
,m_data(""){
}
//! Constructor
Field::Field(eType type, std::string const &data)
:m_type(type)
,m_data(data){
}
//! Constructor
Field::Field(const Field &field)
:m_type(field.m_type)
,m_data(field.m_data){
}
//! Constructor
Field::Field(const int32_t &value)
:m_type(Field::kTypeInteger)
,m_data(core::lexical_cast(value)){
}
//! Constructor
Field::Field(const double &value)
:m_type(Field::kTypeReal)
,m_data(core::lexical_cast(value)){
}
//! Constructor
Field::Field(const char *value, bool isText)
:m_type(isText ? Field::kTypeText : Field::kTypeBlob)
,m_data(core::lexical_cast(value)){
}
//! Destructor
Field::~Field(){
}

////////////////////////////////////////////////////////////////////////////////

//! returns the field type
db::Field::eType Field::getType() const{
    return m_type;
}
//! returns true if null
bool Field::isNull() const{
    return getType() == Field::kTypeNull;
}
//! returns our integer value
int32_t Field::getInteger() const{
    core::check_print(getType() == Field::kTypeInteger, __LINE__, __FILE__, "Returning invalid integer field value");
    return atoi(m_data.c_str());
}
//! returns our real value
double Field::getReal() const{
    core::check_print(getType() == Field::kTypeReal, __LINE__, __FILE__, "Returning invalid real field value");
    return atof(m_data.c_str());
}
//! returns our text value
const char* Field::getText() const{
    core::check_print(getType() == Field::kTypeText, __LINE__, __FILE__, "Returning invalid text field value");
    return m_data.c_str();
}
//! returns our blob value
const char* Field::getBlob() const{
    core::check_print(getType() == Field::kTypeBlob, __LINE__, __FILE__, "Returning invalid blob field value");
    return m_data.c_str();
}

////////////////////////////////////////////////////////////////////////////////