//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/database/sqlite.connection.h>
#include <nimble/core/error.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>

////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::db;

////////////////////////////////////////////////////////////////////////////////

//! finds a record given a query
core::Error Connection::findRecord(Record &record, const char *table, PrimaryKey pk){
    Records records;
    record.clear();
    core::Error error = this->match(records, table, "pk", pk, 0);
    if(error == core::kSuccess){
        if(records.size() == 1){
            record = records[0];
        }else if(records.size() > 1){
            return db::kMultipleRecordsFound;
        }
    }
    return error;
}
//! returns a record that matches a set of conditions
core::Error Connection::match(Records &records, const char *table, ...){
    // construct our query
    va_list args;
    std::ostringstream qStream;
    qStream << "SELECT * FROM " << table;
    va_start(args, table);
    const char *key = va_arg(args, const char*);
    const char *value= va_arg(args, const char*);
    if(key != 0 && value != 0){
        qStream << " WHERE ";
    }
    while(key != 0 && value != 0){
        qStream << key << "=\"" << value << "\"";
        key = va_arg(args, const char*);
        if(key == 0){break;}
        value = va_arg(args, const char*);
        if(value == 0){break;}
        qStream << " AND ";
    };
    va_end(args);
    qStream << ";";
    std::string query = qStream.str();
    
    // execute our query
    int returnCode;
    sqlite3_stmt *stmt = 0;
    records.clear();
    if(!isConnectionValid()){
        return kInvalidDBConnection;
    }else{
        returnCode = sqlite3_prepare_v2(m_pHandle, query.c_str(), -1, &stmt, 0);
        if(returnCode == SQLITE_OK){
            while(sqlite3_step(stmt) == SQLITE_ROW){
                Record record;
                int columns = sqlite3_column_count(stmt);
                for(int i = 0; i < columns; i++){
                    const char *columnName = sqlite3_column_name(stmt, i);
                    int columnType = sqlite3_column_type(stmt, i);
                    if(columnType == SQLITE_INTEGER){
                        record[columnName] = sqlite3_column_int(stmt, i);
                    }else if(columnType == SQLITE_FLOAT){
                        record[columnName] = sqlite3_column_double(stmt, i);
                    }else if(columnType == SQLITE_TEXT){
                        const char *pText = (const char *)sqlite3_column_text(stmt, i);
                        record[columnName] = db::Field(pText, true);
                    }else if(columnType == SQLITE_BLOB){
                        NIMBLE_ASSERT(false);
                        //int numBytes = sqlite3_column_bytes(stmt, i);
                        //const char *pBytes = (const char *)sqlite3_column_blob(stmt, i);
                        //std::string buffer = std::string(pBytes, numBytes);
                        //record[columnName] = db::Field(buffer.c_str(), false);
                    }else if(columnType == SQLITE_NULL){
                        record[columnName] = db::kNull;
                    }
                }
                records.push_back(record);
            }
            returnCode = sqlite3_finalize(stmt);
        }else{
            NIMBLE_LOG_ERROR("database", "Prepare statement failed with error code %d for query %s", returnCode, query.c_str());
        }
    }
    if(returnCode == SQLITE_OK){
        return core::kSuccess;
    }else{
        return core::Error(returnCode, "db", sqlite3_errmsg(m_pHandle));
    }
}
//! inserts a record
core::Error Connection::insert(PrimaryKey &pk, const char *table, ...){
    // construct our query
    std::ostringstream qKeysStream, qValuesStream;
    qKeysStream << "INSERT INTO " << table << "(";
    qValuesStream << "VALUES(";
    va_list args;
    va_start(args, table);
    const char *key = va_arg(args, const char*);
    const char *value= va_arg(args, const char*);
    while(key != 0 && value != 0){
        qKeysStream << key;
        if(strcmp(value, "0") == 0){
            qValuesStream << "0";
        }else{
            qValuesStream << "\"" << value << "\"";
        }
        key = va_arg(args, const char*);
        if(key == 0){break;}
        value = va_arg(args, const char*);
        if(value == 0){break;}
        qKeysStream << ", ";
        qValuesStream << ", ";
    };
    va_end(args);
    qKeysStream << ") " << qValuesStream.str().c_str() << ");";
    std::string query = qKeysStream.str();
    
    // execute our query
    int returnCode;
    sqlite3_stmt *stmt = 0;
    sqlite_int64 insertedId  = -1;
    if(!isConnectionValid()){
        return kInvalidDBConnection;
    }else{
        returnCode = sqlite3_prepare_v2(m_pHandle, query.c_str(), -1, &stmt, 0);
        if(returnCode == SQLITE_OK){
            returnCode = sqlite3_step(stmt);
            if(returnCode == SQLITE_DONE){
                insertedId = sqlite3_last_insert_rowid(m_pHandle);
            }else{
                NIMBLE_LOG_ERROR("database", "Statement failed with error code %d for query %s", returnCode, query.c_str());
            }
            returnCode = sqlite3_finalize(stmt);
        }else{
            NIMBLE_LOG_ERROR("database", "Prepare statement failed with error code %d for query %s", returnCode, query.c_str());
        }
    }
    if(returnCode == SQLITE_OK){
        pk = insertedId;
        return core::kSuccess;
    }else{
        pk = -1;
        return core::Error(returnCode, "db", sqlite3_errmsg(m_pHandle));
    }
}
//! removes a record
core::Error Connection::remove(const char *table, PrimaryKey pk){
    // construct our delete query
    std::ostringstream qStream;
    qStream << "DELETE FROM " << table;
    qStream << " WHERE pk = " << core::lexical_cast(pk).c_str();
    std::string query = qStream.str();
    
    // execute our query
    int returnCode = 0;
    sqlite3_stmt *stmt = 0;
    if(!isConnectionValid()){
        return kInvalidDBConnection;
    }else{
        returnCode = sqlite3_exec(m_pHandle, "PRAGMA foreign_keys = ON;", 0, 0, 0);
        if(returnCode == SQLITE_OK){
            returnCode = sqlite3_prepare_v2(m_pHandle, query.c_str(), -1, &stmt, 0);
            if(returnCode == SQLITE_OK){
                sqlite3_step(stmt);
            }
        }
        returnCode = sqlite3_finalize(stmt);
    }
    if(returnCode == SQLITE_OK){
        return core::kSuccess;
    }else{
        return core::Error(returnCode, "db", sqlite3_errmsg(m_pHandle));
    }
}
//! Updates a record
core::Error Connection::update(const char *table, PrimaryKey pk, ...){
    // construct our query
    std::ostringstream qStream;
    qStream << "UPDATE " << table << " SET ";
    va_list args;
    va_start(args, pk);
    const char *key = va_arg(args, const char*);
    const char *value= va_arg(args, const char*);
    while(key != 0 && value != 0){
        qStream << key << "=\"" << value << "\"";
        key = va_arg(args, const char*);
        if(key == 0){break;}
        value = va_arg(args, const char*);
        if(value == 0){break;}
        qStream << ", ";
    };
    va_end(args);
    qStream << "WHERE pk=\"" << pk << "\";";
    std::string query = qStream.str();
    
    // execute our query
    int returnCode;
    sqlite3_stmt *stmt = 0;
    if(isConnectionValid()){
        return kInvalidDBConnection;
    }else{    
        returnCode = sqlite3_prepare_v2(m_pHandle, query.c_str(), -1, &stmt, 0);
        if(returnCode == SQLITE_OK){
            while(sqlite3_step(stmt) == SQLITE_ROW){
            }
            returnCode = sqlite3_finalize(stmt);
        }else{
            NIMBLE_LOG_ERROR("database", "Prepare statement failed with error code %d for query %s", returnCode, query.c_str());
        }
    }
    if(returnCode == SQLITE_OK){
        return core::kSuccess;
    }else{
        return core::Error(returnCode, "db", sqlite3_errmsg(m_pHandle));
    }
}
//! inserts or updates a record
core::Error Connection::insertOrUpdate(bool &inserted, PrimaryKey &pk, const char *table, ...){
    // construct our UPSERT (update / insert) transaction
    std::ostringstream qUpdateStream, qInsertStream;
    std::string updateQuery, insertQuery;
    const char *key;
    const char *value;
    va_list args;
    
    // construct our update query string
    qUpdateStream << "UPDATE " << table << " SET ";
    va_start(args, table);
    key = va_arg(args, const char*);
    value= va_arg(args, const char*);
    while(key != 0 && value != 0){
        if(strcmp(value, "0") == 0){
            qUpdateStream << key << "=0 ";
        } else {
            qUpdateStream << key << "=\"" << value << "\" ";
        }
        key = va_arg(args, const char*);
        if(key == 0){break;}
        value = va_arg(args, const char*);
        if(value == 0){break;}
        qUpdateStream << ", ";
    };
    va_end(args);
    qUpdateStream << "WHERE pk=\"" << pk << "\";";
    updateQuery = qUpdateStream.str();
    
    // construct our insert query string
    qUpdateStream.str(""); qUpdateStream.clear();
    qInsertStream.str(""); qInsertStream.clear();
    qUpdateStream << "INSERT INTO " << table << "(";
    qInsertStream << "VALUES(";
    va_start(args, table);
    key = va_arg(args, const char*);
    value= va_arg(args, const char*);
    while(key != 0 && value != 0){
        qUpdateStream << key;
        if(strcmp(value, "0") == 0){
            qInsertStream << "0";
        }else{
            qInsertStream << "\"" << value << "\"";
        }
        
        key = va_arg(args, const char*);
        if(key == 0){break;}
        value = va_arg(args, const char*);
        if(value == 0){break;}
        qUpdateStream << ", ";
        qInsertStream << ", ";
    };
    va_end(args);
    qUpdateStream << ") " << qInsertStream.str().c_str() << ");";
    insertQuery = qUpdateStream.str();
    
    // execute our query
    int returnCode = SQLITE_OK;
    sqlite3_stmt *stmt = 0;
    sqlite_int64 insertedId  = -1;
    if(!isConnectionValid()) {
        return kInvalidDBConnection;
    }else{
        if (!m_inTransaction) {
            //don't start a new transaction if we are already executing in one
            sqlite3_exec(m_pHandle, "BEGIN", 0, 0, 0);
        }
        if(pk > 0){
            returnCode = sqlite3_prepare_v2(m_pHandle, updateQuery.c_str(), -1, &stmt, 0);
            if(returnCode == SQLITE_OK){
                returnCode = sqlite3_step(stmt);
                if(returnCode != SQLITE_DONE){
                    NIMBLE_LOG_ERROR("database", "Statement failed with error code %d for query %s", returnCode, updateQuery.c_str());
                }
                returnCode = sqlite3_finalize(stmt);
            }else{
                NIMBLE_LOG_ERROR("database", "Prepare statement failed with error code %d for query %s", returnCode, updateQuery.c_str());
            }
        }
        if(pk <= 0 || sqlite3_changes(m_pHandle) == 0){
            returnCode = sqlite3_prepare_v2(m_pHandle, insertQuery.c_str(), -1, &stmt, 0);
            if(returnCode == SQLITE_OK){
                returnCode = sqlite3_step(stmt);
                if(returnCode != SQLITE_DONE){
                    NIMBLE_LOG_ERROR("database", "Statement failed with error code %d for query %s", returnCode, insertQuery.c_str());
                }else{
                    insertedId = sqlite3_last_insert_rowid(m_pHandle);
                }
                returnCode = sqlite3_finalize(stmt);
            }else{
                NIMBLE_LOG_ERROR("database", "Prepare statement failed with error code %d for query %s", returnCode, insertQuery.c_str());
            }
        }
        if (!m_inTransaction) {
            sqlite3_exec(m_pHandle, "COMMIT", 0, 0, 0);
        }
    }
    // update our pk if we inserted
    if(insertedId > 0){
        inserted = true;
        pk = insertedId;
    }else{
        inserted = false;
    }
    // return code
    if(returnCode == SQLITE_OK){
        return core::kSuccess;
    }else{
        return core::Error(returnCode, "db", sqlite3_errmsg(m_pHandle));
    }
}

////////////////////////////////////////////////////////////////////////////////