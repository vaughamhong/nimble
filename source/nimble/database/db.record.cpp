//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/database/db.record.h>

////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::db;

////////////////////////////////////////////////////////////////////////////////

db::Field Record::m_nullField;

////////////////////////////////////////////////////////////////////////////////

//! Constructor
Record::Record(){
}
//! Constructor
Record::Record(const Record &record)
:m_rowData(record.m_rowData){
}
//! Destructor
Record::~Record(){
}

////////////////////////////////////////////////////////////////////////////////

//! clears this record
void Record::clear(){
    m_rowData.clear();
}

////////////////////////////////////////////////////////////////////////////////

//! returns the starting iterator
Record::RowData::iterator Record::begin(){
    return m_rowData.begin();
}
//! returns the end iterator
Record::RowData::iterator Record::end(){
    return m_rowData.end();
}

////////////////////////////////////////////////////////////////////////////////

//! returns the number of columns
uint32_t Record::getNumColumns() const{
    return m_rowData.size();
}
//! returns a field from a column name
db::Field& Record::operator[](std::string columnName){
    RowData::iterator it = m_rowData.find(columnName);
    if(it != m_rowData.end()){
        return it->second;
    }else{
        return m_nullField;
    }
}

////////////////////////////////////////////////////////////////////////////////