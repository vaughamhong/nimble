//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/audiodevice/audiodevice.h>
#include <nimble/audiodevice/iaudiodevice.h>
#include <nimble/core/locator.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::audiodevice;

//////////////////////////////////////////////////////////////////////////

//! Creates a audio buffer resource
//! \param frequency the frquency of the audio buffer
//! \param bitrate the bitrate of the audio buffer
//! \param resourceName the name of the resource
//! \param pAudioDevice the AudioDevice to create this resource with
//! \param pResourceManager the ResourceManager to manage this resource
//! \return a AudioBuffer resource
audiodevice::IAudioBuffer* nimble::audiodevice::createAudioBuffer(uint32_t frequency, uint32_t bitrate, audiodevice::IAudioDevice *pAudioDevice){
    if(pAudioDevice == 0){
        pAudioDevice = core::locator_acquire<audiodevice::IAudioDevice>();
    }
    if(pAudioDevice != 0){
        audiodevice::IAudioBuffer* pAudioBuffer = pAudioDevice->createAudioBuffer(frequency, bitrate);
        NIMBLE_ASSERT_PRINT(pAudioBuffer != 0, "Failed to create AudioBuffer");
        return pAudioBuffer;
    }
    NIMBLE_ASSERT_PRINT(pAudioDevice != 0, "Failed to create AudioBuffer - AudioDevice not found");
    return 0;
}

//////////////////////////////////////////////////////////////////////////