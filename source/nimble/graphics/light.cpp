//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/graphics/light.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::graphics;

//////////////////////////////////////////////////////////////////////////

//! Constructor
light_t::light_t()
:diffuse(math::Vector3f::zero())
,ambient(math::Vector3f::zero())
,specular(math::Vector3f::zero())
,emissive(math::Vector3f::zero()){
}
//! Destructor
light_t::~light_t(){
}
//! Returns the metatype data
core::TypeMetaData* light_t::getTypeMetaData(){
    // remains in memory for the lifetime of the application, however with local scope
    static nimble::core::TypeMetaData *pTypeMetaData = 0;
    static nimble::core::TypeMetaData typeMetaData(0, core::getTypeId<light_t>(), "light_t", sizeof(light_t));
    
    // setup our type metadata if it has not been done so already
    if(pTypeMetaData == 0){
        pTypeMetaData = &typeMetaData;
        nimble::core::FieldMetaData fields[] = {
            nimble::core::FieldMetaData("diffuse", &light_t::diffuse),
            nimble::core::FieldMetaData("ambient", &light_t::ambient),
            nimble::core::FieldMetaData("specular", &light_t::specular),
            nimble::core::FieldMetaData("emissive", &light_t::emissive),
        };
        pTypeMetaData->fields(fields);
    }
    return pTypeMetaData;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
Light::Light(){
}
//! Destructor
Light::~Light(){
}

//////////////////////////////////////////////////////////////////////////

//! Returns the diffuse color
math::Vector3f& Light::diffuseColor(){
    return *graphics::ILight::getParam<math::Vector3f>("diffuse");
}
//! Returns the ambient color
math::Vector3f& Light::ambientColor(){
    return *graphics::ILight::getParam<math::Vector3f>("ambient");
}
//! Returns the specular color
math::Vector3f& Light::specularColor(){
    return *graphics::ILight::getParam<math::Vector3f>("specular");
}
//! Returns the emissive color
math::Vector3f& Light::emissiveColor(){
    return *graphics::ILight::getParam<math::Vector3f>("emissve");
}

//////////////////////////////////////////////////////////////////////////