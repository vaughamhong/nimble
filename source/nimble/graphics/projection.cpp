//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/graphics/projection.h>
#include <nimble/math/mathops.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::graphics;

//////////////////////////////////////////////////////////////////////////

//! calculate projection matrix
math::Matrix4x4f nimble::graphics::calculatePerspectiveProjMatrix(size_t width, size_t height, float fov, float nearPlane, float farPlane){
    // fovRad		= field of view (in radians)
    // f			= far plane
    // n			= near plane
    // aspect		= aspect ratio (screen width/height)
    float aspect = (float)width / height;
    float fovRad = math::degToRad(fov);
    float fovRadDiv2 = fovRad / 2.0f;
    float tanFovRadDiv2 = math::tanf(fovRadDiv2);
    
    // note: this matrix applies the projection matrix and maps coordinates into
    // a unit cube of [-1, 1] (expected by OpenGL)
    math::Matrix4x4<> projectionMatrix = math::Matrix4x4<>::identity();
    projectionMatrix.set(0, 0, 1 / (tanFovRadDiv2 * aspect));
    projectionMatrix.set(1, 1, 1 / tanFovRadDiv2);
    projectionMatrix.set(2, 2, -1 * (farPlane + nearPlane) / (farPlane - nearPlane));
    projectionMatrix.set(2, 3, -1 * (2 * farPlane * nearPlane) / (farPlane - nearPlane));
    projectionMatrix.set(3, 2, -1);
    projectionMatrix.set(3, 3, 0);
    return projectionMatrix;
}
//! calculate projection matrix
math::Matrix4x4f nimble::graphics::calculateOrthographicProjMatrix(size_t width, size_t height, float fov, float nearPlane, float farPlane){
    float left = -(float)width/2;
    float right = (float)width/2;
    float bottom = -(float)height/2;
    float top = (float)height/2;
    
    math::Matrix4x4<> projectionMatrix = math::Matrix4x4<>::identity();
    projectionMatrix.set(0, 0, 2 / (right - left));
    projectionMatrix.set(1, 1, 2 / (top - bottom));
    projectionMatrix.set(2, 2, -1 / (farPlane - nearPlane));
    projectionMatrix.set(3, 0, -(right + left) / (right - left));
    projectionMatrix.set(3, 1, -(top + bottom) / (top - bottom));
    projectionMatrix.set(3, 2, 0);
    projectionMatrix.set(3, 3, 1);
    return projectionMatrix;
}

////! calculate inverse projection matrix
//math::Matrix4x4f nimble::graphics::calculateInverseProjectionMatrix(graphics::ICamera const *pCamera, graphics::Viewport const *pViewport){
//	math::Matrix4x4<> projectionMatrix = calculateProjectionMatrix(pCamera, pViewport);
//	math::Matrix4x4<> inverseProjectionMatrix = math::Matrix4x4<>::identity();
//
//	// A typical projection matrix looks like this
//	//	[	a,		0,		0,	0]
//	//	[	0,		b,		0,	0]
//	//	[	0,		0,		c,	d]
//	//	[	0,      0,		e,	0]
//	float a = projectionMatrix.get(0, 0);
//	float b = projectionMatrix.get(1, 1);
//	float c = projectionMatrix.get(2, 2);
//	float d = projectionMatrix.get(2, 3);
//	float e = projectionMatrix.get(3, 2);
//
//	// The inverse projection matrix looks like this
//	//	[ 1/a,		0,		0,		0]
//	//	[	0,	  1/b,		0,		0]
//	//	[	0,		0,		0,	  1/e]
//	//	[	0,      0,	  1/d,	-c/de]
//	inverseProjectionMatrix.set(0, 0, 1.0f / a);
//	inverseProjectionMatrix.set(1, 1, 1.0f / b);
//	inverseProjectionMatrix.set(2, 2, 0.0f);
//	inverseProjectionMatrix.set(2, 3, 1.0f / e);
//	inverseProjectionMatrix.set(3, 2, 1.0f / d);
//	inverseProjectionMatrix.set(3, 3, (-1.0f * c) / (d * e));
//
//	return inverseProjectionMatrix;
//}

//////////////////////////////////////////////////////////////////////////