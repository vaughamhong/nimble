//
//  VRBrowser
//
//  Created by Vaugham Hong on 2013-6-29.
//  Copyright (c) 2012 CookieMob. All rights reserved.
//

#include <nimble/graphics/camera.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::graphics;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Camera::Camera()
:m_position(math::Vector3f::zero())
,m_orientation(math::Quaternion4f::identity())
,m_fieldOfView(0), m_nearPlane(0.1f), m_farPlane(10.0f)
,m_projectionType(kProjectionPerspective){
}
//! Destructor
Camera::~Camera(){
}

//////////////////////////////////////////////////////////////////////////

//! sets the camera position
void Camera::setPosition(math::Vector3f const &position){
    m_position = position;
}
//! sets our orientation
void Camera::setOrientation(math::Quaternion4f const &orientation){
    m_orientation = orientation;
}
//! sets our orientation
void Camera::setOrientation(math::Vector3f const &forward){
    m_orientation = math::Quaternion4f::rotation(forward);
}

//! gets the camera position
math::Vector3f& Camera::getPosition(){
    return m_position;
}
//! gets the camera orientation
math::Quaternion4f& Camera::getOrientation(){
    return m_orientation;
}
//! gets the camera position
math::Vector3f Camera::getPosition() const{
    return m_position;
}
//! gets the camera orientation
math::Quaternion4f Camera::getOrientation() const{
    return m_orientation;
}

//////////////////////////////////////////////////////////////////////////

//! sets the projection type
void Camera::setProjectionType(graphics::eProjectionType projectionType){
    m_projectionType = projectionType;
}
//! sets the camera field of view
void Camera::setFieldOfView(float fieldOfView){
    m_fieldOfView = fieldOfView;
}
//! sets the camera near plane
void Camera::setNearPlane(float nearPlane){
    m_nearPlane = nearPlane;
}
//! sets the camera far plane
void Camera::setFarPlane(float farPlane){
    m_farPlane = farPlane;
}

//////////////////////////////////////////////////////////////////////////

//! returns the projection type
graphics::eProjectionType Camera::getProjectionType() const{
    return m_projectionType;
}
//! gets the camera field of view
float Camera::getFieldOfView() const{
    return m_fieldOfView;
}
//! gets the camera near plane
float Camera::getNearPlane() const{
    return m_nearPlane;
}
//! gets the camera far plane
float Camera::getFarPlane() const{
    return m_farPlane;
}

//////////////////////////////////////////////////////////////////////////