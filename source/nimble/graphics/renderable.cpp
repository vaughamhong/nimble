//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/graphics/renderable.h>
#include <nimble/graphics/mesh.h>
#include <nimble/graphics/material.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::graphics;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Renderable::Renderable()
:m_pMesh(0)
,m_pTransformedMesh(0)
,m_pMaterial(0){
}
//! Destructor
Renderable::~Renderable(){
}

//////////////////////////////////////////////////////////////////////////

//! Sets our mesh
void Renderable::setMesh(graphics::IMesh *pMesh){
    m_pMesh = pMesh;
}
//! Returns the mesh
graphics::IMesh* Renderable::getMesh() const{
    return m_pMesh;
}

//////////////////////////////////////////////////////////////////////////

//! Sets our mesh
void Renderable::setTransformedMesh(graphics::IMesh *pMesh){
    m_pTransformedMesh = pMesh;
}
//! Returns the transformed mesh
graphics::IMesh* Renderable::getTransformedMesh() const{
    return m_pTransformedMesh;
}

//////////////////////////////////////////////////////////////////////////

//! Sets our material
void Renderable::setMaterial(graphics::IMaterial *pMaterial){
    m_pMaterial = pMaterial;
}
//! Returns the material
graphics::IMaterial* Renderable::getMaterial() const{
    return m_pMaterial;
}

//////////////////////////////////////////////////////////////////////////