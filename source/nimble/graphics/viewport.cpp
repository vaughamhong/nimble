//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/graphics/viewport.h>
#include <nimble/math/mathops.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::graphics;

//////////////////////////////////////////////////////////////////////////

//! constructor
Viewport::Viewport()
:m_position(math::Vector2i(0, 0))
,m_size(math::Vector2i(1, 1)){
}
//! constructor
Viewport::Viewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height)
:m_position(math::Vector2i(x, y))
,m_size(math::Vector2i(width, height)){
}
//! destructor
Viewport::~Viewport(){
}
//! sets this camera's members to be equal with another camera
void Viewport::operator=(Viewport const& viewport){
	setViewport(viewport.getX(), viewport.getY(),
                viewport.getWidth(), viewport.getHeight());
}

//////////////////////////////////////////////////////////////////////////

//! sets the viewport information
void Viewport::setViewport(uint32_t x, uint32_t y, uint32_t width, uint32_t height){
    m_position[0] = x;
    m_position[1] = y;
    m_size[0] = width;
    m_size[1] = height;
}

//////////////////////////////////////////////////////////////////////////

//! gets the camera viewport position
uint32_t Viewport::getX() const{
    return m_position[0];
}
//! gets the camera viewport position
uint32_t Viewport::getY() const{
    return m_position[1];
}
//! gets the camera viewport width
uint32_t Viewport::getWidth() const{
    return m_size[0];
}
//! gets the camera viewport height
uint32_t Viewport::getHeight() const{
    return m_size[1];
}

//////////////////////////////////////////////////////////////////////////