//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/graphics/materiallist.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::graphics;

//////////////////////////////////////////////////////////////////////////
    
//! Constructor
MaterialList::MaterialList(){
}
//! Destructor
MaterialList::~MaterialList(){
}

//////////////////////////////////////////////////////////////////////////

//! Add a material
size_t MaterialList::addMaterial(IMaterial *pMaterial){
    ListType::const_iterator it = std::find(m_list.begin(), m_list.end(), pMaterial);
    if(it != m_list.end()){
        return it - m_list.begin();
    }else{
        m_list.push_back(pMaterial);
        return m_list.size() - 1;
    }
}
//! Removes a material
void MaterialList::removeMaterial(size_t index){
    if(index < getNumMaterials()){
        ListType::iterator nth = m_list.begin() + index;
        m_list.erase(nth);
    }
}
//! Remove all materials
void MaterialList::removeAllMaterials(){
    m_list.clear();
}
//! Returns a material by index
IMaterial* MaterialList::getMaterial(size_t index){
    if(index < getNumMaterials()){
        ListType::iterator nth = m_list.begin() + index;
        return *nth;
    }else{
        return 0;
    }
}
//! Returns the number of materials
size_t MaterialList::getNumMaterials() const{
    return m_list.size();
}

//////////////////////////////////////////////////////////////////////////

//! Check if material already exists
bool MaterialList::exists(IMaterial *pMaterial) const{
    ListType::const_iterator it = std::find(m_list.begin(), m_list.end(), pMaterial);
    return (it != m_list.end());
}

//////////////////////////////////////////////////////////////////////////