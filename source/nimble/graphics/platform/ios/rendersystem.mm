//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/renderer/platform/ios/rendersystem.h>
#include <nimble/view/platform/ios/window.h>
#include <nimble/renderdevice-opengles_1_1/renderdevice.h>
#include <nimble/renderdevice-opengles_2_0/renderdevice.h>
#include <nimble/core/platform/ios/uidevice+detect.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::renderer::ios;

//////////////////////////////////////////////////////////////////////////

//! default constructor
RenderSystem::RenderSystem(){
}
//! a destructor
RenderSystem::~RenderSystem(){
}

//////////////////////////////////////////////////////////////////////////

//! Initialize render data
void RenderSystem::initializeRenderData(core::PropertyList &propertyList){
    renderdevice::IRenderDevice *pRenderDevice = getRenderDevice();
    view::ios::Window *pWindow(dynamic_cast<view::ios::Window*>(getWindow()));
    UIView *pView = pWindow->getUIView();
    core::Bool hasStatusBar = pWindow->hasStatusBar();
    core::Bool hasNavigationBar = pWindow->hasNavigationBar();
    math::Size2f windowSize = pWindow->getSize();
	
    // set our render context
    if(dynamic_cast<renderdevice::opengles_1_1::RenderDevice*>(pRenderDevice)){
        m_context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
    }else if(dynamic_cast<renderdevice::opengles_2_0::RenderDevice*>(pRenderDevice)){
        m_context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    }
    core::assert_error(m_context != NULL, "WindowUpdateSystem Could not create a proper OpenGLES context");
    [EAGLContext setCurrentContext:m_context];
    
    // set our layer properties
    CAEAGLLayer* layer = (CAEAGLLayer*)pView.layer;
	layer.opaque = TRUE;
	layer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithBool:FALSE],
                                kEAGLDrawablePropertyRetainedBacking,
                                kEAGLColorFormatRGB565,
                                kEAGLDrawablePropertyColorFormat,
                                nil];
    
	// Get our true bounds
    static float const kNavigationBarHeight = 44;
    static float const kStatusBarHeight = 20;
	UIScreen* mainScreen = [UIScreen mainScreen];
    CGFloat viewScale = mainScreen.scale;
    CGSize normalizedScreenSize = CGSizeMake(windowSize[0] / viewScale, windowSize[1] / viewScale);
    if(hasStatusBar){normalizedScreenSize.height -= kStatusBarHeight;}
    if(hasNavigationBar){normalizedScreenSize.height -= kNavigationBarHeight;}
    CGRect screenResolution = CGRectMake(0, 0, normalizedScreenSize.width * viewScale, normalizedScreenSize.height * viewScale);
    if([UIDevice isIPad]){
        // iPad defaults to landscape dimensions
        screenResolution = CGRectMake(0, 0, normalizedScreenSize.height * viewScale, normalizedScreenSize.width * viewScale);
    }
    layer.bounds = screenResolution;
    
    // create our frame buffer
    // note: render buffer based depth buffer needs to be initialized pre color buffer attachment
    m_pMainFrameBuffer = pRenderDevice->createFrameBuffer(screenResolution.size.width, screenResolution.size.height);
    glGenRenderbuffersOES(1, &m_colorBufferHandle);
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, m_colorBufferHandle);
    [m_context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:layer];
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, m_colorBufferHandle);
    
    // set our frame buffer
    pRenderDevice->bindFrameBuffer(m_pMainFrameBuffer);
	
    // Restore original layer bounds
    // On presentRenderbuffer: the layer will stretch the renderbuffer backing to fit the bounds.
    CGRect originalScreenRect = CGRectMake(0, 0, normalizedScreenSize.width, normalizedScreenSize.height);
    layer.bounds = originalScreenRect;
    [pView becomeFirstResponder];
    renderer::RenderSystem::initializeRenderData(propertyList);
}
//! Destroy render data
void RenderSystem::destroyRenderData(){
    renderer::RenderSystem::destroyRenderData();
}

//////////////////////////////////////////////////////////////////////////

//! the frame has begun
void RenderSystem::frameHasBegun(){
    renderdevice::IRenderDevice *pRenderDevice = getRenderDevice();
    pRenderDevice->bindFrameBuffer(m_pMainFrameBuffer, true);
}
//! the frame has ended
void RenderSystem::frameHasEnded(){
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, m_colorBufferHandle);
    [m_context presentRenderbuffer:GL_RENDERBUFFER_OES];
}

//////////////////////////////////////////////////////////////////////////