//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/graphics/meshlist.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::graphics;

//////////////////////////////////////////////////////////////////////////
    
//! Constructor
MeshList::MeshList(){
}
//! Destructor
MeshList::~MeshList(){
}

//////////////////////////////////////////////////////////////////////////

//! Add a mesh
size_t MeshList::addMesh(IMesh *pMesh){
    ListType::const_iterator it = std::find(m_list.begin(), m_list.end(), pMesh);
    if(it != m_list.end()){
        return it - m_list.begin();
    }else{
        m_list.push_back(pMesh);
        return m_list.size() - 1;
    }
}
//! Removes a mesh
void MeshList::removeMesh(size_t index){
    if(index < getNumMeshes()){
        ListType::iterator nth = m_list.begin() + index;
        m_list.erase(nth);
    }
}
//! Remove all meshes
void MeshList::removeAllMeshes(){
    m_list.clear();
}
//! Returns a mesh by index
IMesh* MeshList::getMesh(size_t index){
    if(index < getNumMeshes()){
        ListType::iterator nth = m_list.begin() + index;
        return *nth;
    }else{
        return 0;
    }
}
//! Returns the number of meshes
size_t MeshList::getNumMeshes() const{
    return m_list.size();
}

//////////////////////////////////////////////////////////////////////////

//! Check if mesh already exists
bool MeshList::exists(IMesh *pMesh) const{
    ListType::const_iterator it = std::find(m_list.begin(), m_list.end(), pMesh);
    return (it != m_list.end());
}

//////////////////////////////////////////////////////////////////////////