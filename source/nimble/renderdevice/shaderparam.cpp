//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/renderdevice/shaderparam.h>
#include <memory.h>
#include <string>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::renderdevice;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ShaderParam::ShaderParam()
:m_type(kTypeNull)
,m_pData(0){
    memset(m_name, 0, kMaxShaderParamNameSize);
}
//! Constructor
ShaderParam::ShaderParam(const char *name, renderdevice::eShaderParamType type, void *pData)
:m_type(type)
,m_pData(pData){
    strncpy(m_name, name, kMaxShaderParamNameSize);
}
//! Constructor
ShaderParam::ShaderParam(ShaderParam const &param)
:m_type(param.m_type)
,m_pData(param.m_pData){
    strncpy(m_name, param.m_name, kMaxShaderParamNameSize);
}
//! Destructor
ShaderParam::~ShaderParam(){
}

//////////////////////////////////////////////////////////////////////////

//! Assignment operator
ShaderParam& ShaderParam::operator=(ShaderParam const &param){
    m_type = param.m_type;
    m_pData = param.m_pData;
    strncpy(m_name, param.m_name, kMaxShaderParamNameSize);
    return *this;
}
//! Equivalence operator
bool ShaderParam::operator==(ShaderParam const &param){
    return (m_pData == param.m_pData) && (m_type == param.m_type) && (strcmp(m_name, param.m_name) == 0);
}
//! Equivalence operator
bool ShaderParam::operator!=(ShaderParam const &param){
    return !this->operator==(param);
}

//////////////////////////////////////////////////////////////////////////

//! Returns the name
const char* ShaderParam::getName() const{
    return m_name;
}
//! Returns the data type
renderdevice::eShaderParamType ShaderParam::getType() const{
    return m_type;
}

//////////////////////////////////////////////////////////////////////////

//! Returns the value
void* ShaderParam::getData() const{
    return m_pData;
}
//! Sets the value
void ShaderParam::setData(void *pData){
    m_pData = pData;
}

//////////////////////////////////////////////////////////////////////////