//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/renderdevice/vertexformat.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <nimble/math/mathops.h>

//////////////////////////////////////////////////////////////////////////

#define VERTEXFORMAT_ATTRIBUTENAME_TUPLE(ENUM, NAME, STRING) const char* nimble::renderdevice::VertexFormat::NAME = STRING;
    VERTEXFORMAT_ATTRIBUTENAME_TUPLESET
#undef VERTEXFORMAT_ATTRIBUTENAME_TUPLE

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::renderdevice;

//////////////////////////////////////////////////////////////////////////

//! Constructor
VertexFormat::VertexFormat()
:m_vertexStride(0)
,m_numAttributes(0){
    clearAttributes();
}
//! Constructor
VertexFormat::VertexFormat(VertexFormat const &vertexFormat)
:m_vertexStride(0)
,m_numAttributes(0){
    copy(vertexFormat);
}
//! Destructor
VertexFormat::~VertexFormat(){
}

//////////////////////////////////////////////////////////////////////////

//! returns the size of the offset from one vertex to the next
//! \return returns the size of the offset from one vertex to the next
size_t VertexFormat::getVertexStride() const{
    return m_vertexStride;
}
//! returns the number of elements in each vertex
//! \return returns the number of elements in each vertex
size_t VertexFormat::getNumAttributes() const{
    return m_numAttributes;
}

//////////////////////////////////////////////////////////////////////////

//! adds an attribute
//! \param attributeIndex the index of the element we are interested in
//! \param semantic the attribute semantic
//! \param type the attribute type
void VertexFormat::addAttribute(eAttributeSemantic semantic, eAttributeType type, bool normalize){
    //! define attribute binding names
    static const char *names[] = {
    #define VERTEXFORMAT_ATTRIBUTENAME_TUPLE(ENUM, NAME, STRING) NAME,
        VERTEXFORMAT_ATTRIBUTENAME_TUPLESET
    #undef VERTEXFORMAT_ATTRIBUTENAME_TUPLE
    };

    NIMBLE_ASSERT(m_numAttributes < kMaxAttributeSemantics);
    m_attributes[m_numAttributes].semantic = semantic;
    m_attributes[m_numAttributes].type = type;
    m_attributes[m_numAttributes].name = names[semantic];
    m_attributes[m_numAttributes].normalize = normalize;
    m_numAttributes += 1;
    updateVertexStrideAndOffsets();
}
//! clears all our attributes
void VertexFormat::clearAttributes(){
    m_numAttributes = 0;
    updateVertexStrideAndOffsets();
}
//! returns the vertex element's semantic
//! \param elementIndex the index of the element we are interested in
//! \return returns the vertex element's semantic
VertexFormat::eAttributeSemantic VertexFormat::getAttributeSemantic(uint32_t attributeIndex) const{
    NIMBLE_ASSERT(attributeIndex < m_numAttributes);
    return m_attributes[attributeIndex].semantic;
}
//! returns the vertex element's type
//! \param attributeIndex the index of the element we are interested in
//! \return returns the vertex element's type
VertexFormat::eAttributeType VertexFormat::getAttributeType(uint32_t attributeIndex) const{
    NIMBLE_ASSERT(attributeIndex < m_numAttributes);
    return m_attributes[attributeIndex].type;
}
//! returns the vertex element's name
//! \param attributeIndex the index of the element we are interested in
//! \return returns the vertex element's name
const char* VertexFormat::getAttributeName(uint32_t attributeIndex) const{
    NIMBLE_ASSERT(attributeIndex < m_numAttributes);
    return m_attributes[attributeIndex].name.c_str();
}
//! returns the vertex element's normalize flag
//! \param attributeIndex the index of the element we are interested in
//! \return returns the vertex element's normalize flag
bool VertexFormat::getAttributeNormalize(uint32_t attributeIndex) const{
    NIMBLE_ASSERT(attributeIndex < m_numAttributes);
    return m_attributes[attributeIndex].normalize;
}
//! returns the byte offset to a specific attribute
//! \return the byte offset to a specific attribute
size_t VertexFormat::getAttributeByteOffset(uint32_t attributeIndex) const{
    NIMBLE_ASSERT(attributeIndex < m_numAttributes);
    return m_attributes[attributeIndex].offset;
}
//! returns the byte offset to a specific attribute
//! \return the byte offset to a specific attribute
size_t VertexFormat::getAttributeByteOffsetBySemantic(eAttributeSemantic semantic) const{
    for(uint32_t i = 0; i < m_numAttributes; ++i){
        if(m_attributes[i].semantic == semantic){
            return this->getAttributeByteOffset(i);
        }
	}
    return -1;
}
//! returns the vertex element's type
//! \return returns the vertex element's type
VertexFormat::eAttributeType VertexFormat::getAttributeTypeBySemantic(eAttributeSemantic semantic) const{
    for(uint32_t i = 0; i < m_numAttributes; ++i){
        if(m_attributes[i].semantic == semantic){
            return m_attributes[i].type;
        }
	}
    return kTypeNull;
}
//!	gets the size in bytes for the type specified
//! \param type the type we are interested in
//! \return the size in bytes for the type specified
size_t VertexFormat::getAttributeTypeByteSize(VertexFormat::eAttributeType type) const{
    if(type == kTypeFloat1){return (sizeof(float) * 1);}
	if(type == kTypeFloat2){return (sizeof(float) * 2);}
	if(type == kTypeFloat3){return (sizeof(float) * 3);}
	if(type == kTypeFloat4){return (sizeof(float) * 4);}
    
	if(type == kTypeByte1){return (sizeof(char) * 1);}
	if(type == kTypeByte2){return (sizeof(char) * 2);}
	if(type == kTypeByte3){return (sizeof(char) * 3);}
	if(type == kTypeByte4){return (sizeof(char) * 4);}
    
    if(type == kTypeShort1){return (sizeof(short) * 1);}
	if(type == kTypeShort2){return (sizeof(short) * 2);}
	if(type == kTypeShort3){return (sizeof(short) * 3);}
	if(type == kTypeShort4){return (sizeof(short) * 4);}
    
	return 0;
}
//! returns if a vertex in this array has an element with a specific semantic
//! \param semantic the semantic we are interested in
//! \return returns true if a vertex in this array has an element with a specific semantic
bool VertexFormat::hasAttribute(VertexFormat::eAttributeSemantic semantic) const{
    for(uint32_t i = 0; i < m_numAttributes; ++i){
        if(m_attributes[i].semantic == semantic){
            return true;
        }
	}
    return false;
}
//! copy
//! \param vertexFormat the vertex format to copy against
//! \return returns the copied vertex format
VertexFormat& VertexFormat::copy(VertexFormat const &vertexFormat){
    clearAttributes();
    for(uint32_t i = 0; i < vertexFormat.m_numAttributes; i++){
        addAttribute(vertexFormat.getAttributeSemantic(i), vertexFormat.getAttributeType(i), vertexFormat.getAttributeNormalize(i));
    }
    return *this;
}
//! assignment operator
//! \param vertexFormat the vertex format to copy against
//! \return returns the copied vertex format
VertexFormat& VertexFormat::operator=(VertexFormat const &vertexFormat){
    return copy(vertexFormat);
}
//! returns true if matching vertex format
//! \param vertexFormat the vertex format to compare against
//! \return returns true if matching vertex format
bool VertexFormat::equals(VertexFormat const &vertexFormat) const{
    if(m_numAttributes == vertexFormat.m_numAttributes &&
       m_vertexStride == vertexFormat.m_vertexStride){
        for(uint32_t i = 0; i < m_numAttributes; ++i){
            if(m_attributes[i].semantic != vertexFormat.m_attributes[i].semantic ||
               m_attributes[i].type != vertexFormat.m_attributes[i].type){
                return false;
            }
        }
        return true;
    }
    return false;
}
//! returns true if matching vertex format
//! \param vertexFormat the vertex format to compare against
//! \return returns true if matching vertex format
bool VertexFormat::operator==(VertexFormat const &vertexFormat) const{
    return equals(vertexFormat);
}
//! returns true if matching vertex format
//! \param vertexFormat the vertex format to compare against
//! \return returns true if matching vertex format
bool VertexFormat::operator!=(VertexFormat const &vertexFormat) const{
    return !equals(vertexFormat);
}
//! returns the vertex format sort index
int64_t VertexFormat::getSortIndex() const{
    int numSemanticBits = (int)((math::logf(VertexFormat::kMaxAttributeSemantics) / math::logf(2.0f)) + 1);
    int numTypeBits = (int)((math::logf(VertexFormat::kMaxAttributeTypes) / math::logf(2.0f)) + 1);
    int numAttributeBits = numSemanticBits + numTypeBits;
    NIMBLE_ASSERT(numAttributeBits <= 64);

    int64_t hash = 0;
    for(uint32_t i = 0; i < getNumAttributes(); i++){
        VertexFormat::eAttributeSemantic semantic = getAttributeSemantic(i);
        VertexFormat::eAttributeType type = getAttributeType(i);
        int attributeHash = (semantic << 0) | (type << numSemanticBits);
        hash ^= attributeHash;
    }
    return hash;
}
//! Updates our vertex stride
void VertexFormat::updateVertexStrideAndOffsets(){
    // note: when updating offsets, we must take into
    // account data alignment. Every attribute needs to be
    // aligned to 4 bytes, which means we need to calculate and
    // add additional padding to each attribute offset.
    const size_t kByteAlignment = 4;
    
    m_vertexStride = 0;
	for(uint32_t i = 0; i < m_numAttributes; ++i){
        // set our attribute offset
        m_attributes[i].offset = m_vertexStride;

        // accumulate stride
        size_t typeSize = getAttributeTypeByteSize(m_attributes[i].type);
        size_t padding = (m_vertexStride + (kByteAlignment - typeSize)) % kByteAlignment;
        m_vertexStride += (typeSize + padding);
	}
}

//////////////////////////////////////////////////////////////////////////