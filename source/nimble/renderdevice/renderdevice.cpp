//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/renderdevice/renderdevice.h>
#include <nimble/renderdevice/irenderdevice.h>
#include <nimble/resource/resourcemanager.h>
#include <nimble/core/locator.h>
#include <nimble/core/uuid.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::renderdevice;

//////////////////////////////////////////////////////////////////////////

//! returns the global render device
renderdevice::IRenderDevice* nimble::renderdevice::get(){
    return core::locator_acquire<renderdevice::IRenderDevice>();
}

//////////////////////////////////////////////////////////////////////////

//! creates a frame buffer
//! \param descriptor the frame buffer's descriptor
//! \return a frame buffer
renderdevice::IFrameBuffer* nimble::renderdevice::createFrameBuffer(uint32_t width, uint32_t height, uint32_t flags, renderdevice::IRenderDevice *pRenderDevice){
    if(pRenderDevice == 0){
        pRenderDevice = core::locator_acquire<renderdevice::IRenderDevice>();
    }
    if(pRenderDevice != 0){
        renderdevice::IFrameBuffer* pFrameBuffer = pRenderDevice->createFrameBuffer(width, height, flags);
        NIMBLE_ASSERT_PRINT(pFrameBuffer != 0, "Failed to create FrameBuffer");
        return pFrameBuffer;
    }
    NIMBLE_ASSERT_PRINT(pRenderDevice != 0, "Failed to create FrameBuffer - RenderDevice not found");
    return 0;
}

//////////////////////////////////////////////////////////////////////////

//! Creates a vertex buffer resource
//! \param format the vertex format
//! \param numVertices the number of vertices in this buffer
//! \param usage the usage hints for this buffer
//! \param pRenderDevice the RenderDevice to create this resource with
//! \return a VertexBuffer resource
renderdevice::IVertexBuffer* nimble::renderdevice::createVertexBuffer(renderdevice::VertexFormat const &format, uint32_t numVertices, uint32_t usage, renderdevice::IRenderDevice *pRenderDevice){
    if(pRenderDevice == 0){
        pRenderDevice = core::locator_acquire<renderdevice::IRenderDevice>();
    }
    if(pRenderDevice != 0){
        renderdevice::IVertexBuffer* pVertexBuffer = pRenderDevice->createVertexBuffer(format, numVertices, usage);
        NIMBLE_ASSERT_PRINT(pVertexBuffer != 0, "Failed to create VertexBuffer");
        return pVertexBuffer;
    }
    NIMBLE_ASSERT_PRINT(pRenderDevice != 0, "Failed to create VertexBuffer - RenderDevice not found");
    return 0;
}

//! Creates a index buffer resource
//! \param primitiveType the primitive type
//! \param indexType the index type
//! \param numIndices the number of indices in this buffer
//! \param usage the usage hints for this buffer
//! \param pRenderDevice the RenderDevice to create this resource with
//! \return a IndexBuffer resource
renderdevice::IIndexBuffer* nimble::renderdevice::createIndexBuffer(renderdevice::ePrimitiveType primitiveType, renderdevice::eIndexType indexType, uint32_t numIndices, uint32_t usage, renderdevice::IRenderDevice *pRenderDevice){
    if(pRenderDevice == 0){
        pRenderDevice = core::locator_acquire<renderdevice::IRenderDevice>();
    }
    if(pRenderDevice != 0){
        renderdevice::IIndexBuffer* pIndexBuffer = pRenderDevice->createIndexBuffer(primitiveType, indexType, numIndices, usage);
        NIMBLE_ASSERT_PRINT(pIndexBuffer != 0, "Failed to create IndexBuffer");
        return pIndexBuffer;
    }
    NIMBLE_ASSERT_PRINT(pRenderDevice != 0, "Failed to create IndexBuffer - RenderDevice not found");
    return 0;
}

//////////////////////////////////////////////////////////////////////////

//! Creates a texture resource
//! \param width the width of the texture
//! \param height the height of the texture
//! \param textureFormat the texture format
//! \param usage the usage hint for this texture
//! \param pRenderDevice the RenderDevice to create this resource with
//! \return a Texture resource
renderdevice::ITexture* nimble::renderdevice::createTexture(uint32_t width, uint32_t height, renderdevice::eTextureFormat textureFormat, uint32_t usage, renderdevice::IRenderDevice *pRenderDevice){
    if(pRenderDevice == 0){
        pRenderDevice = core::locator_acquire<renderdevice::IRenderDevice>();
    }
    if(pRenderDevice != 0){
        renderdevice::ITexture* pTexture = pRenderDevice->createTexture(width, height, textureFormat, usage);
        NIMBLE_ASSERT_PRINT(pTexture != 0, "Failed to create Texture");
        return pTexture;
    }
    NIMBLE_ASSERT_PRINT(pRenderDevice != 0, "Failed to create Texture - RenderDevice not found");
    return 0;
}

//////////////////////////////////////////////////////////////////////////

//! Creates a vertex shader resource
//! \param pRenderDevice the RenderDevice to create this resource with
//! \return a VertexBuffer resource
renderdevice::IShader* nimble::renderdevice::createShader(renderdevice::eShaderType type, renderdevice::IRenderDevice *pRenderDevice){
    if(pRenderDevice == 0){
        pRenderDevice = core::locator_acquire<renderdevice::IRenderDevice>();
    }
    if(pRenderDevice != 0){
        renderdevice::IShader* pShader = pRenderDevice->createShader(type);
        NIMBLE_ASSERT_PRINT(pShader != 0, "Failed to create Shader");
        return pShader;
    }
    NIMBLE_ASSERT_PRINT(pRenderDevice != 0, "Failed to create Shader - RenderDevice not found");
    return 0;
}
//! creates a program
//! \param pRenderDevice the RenderDevice to create this resource with
//! \return a program
renderdevice::IShaderProgram* createShaderProgram(renderdevice::IRenderDevice *pRenderDevice){
    if(pRenderDevice == 0){
        pRenderDevice = core::locator_acquire<renderdevice::IRenderDevice>();
    }
    if(pRenderDevice != 0){
        renderdevice::IShaderProgram* pShaderProgram = pRenderDevice->createShaderProgram();
        NIMBLE_ASSERT_PRINT(pShaderProgram != 0, "Failed to create ShaderProgram");
        return pShaderProgram;
    }
    NIMBLE_ASSERT_PRINT(pRenderDevice != 0, "Failed to create ShaderProgram - RenderDevice not found");
    return 0;
}

//////////////////////////////////////////////////////////////////////////