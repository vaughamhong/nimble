//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/script/objects/javascriptobject.log.h>
#include <nimble/core/logger.h>
#include <sstream>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::script;

//////////////////////////////////////////////////////////////////////////

//! Write formatted data to output
static std::string sprintf(const v8::FunctionCallbackInfo<v8::Value>& args){
    if(args.Length() > 0){
        // get our format string
        v8::String::Utf8Value format(args[0]);
        
        // substitute values into format list
        int i = 1;
        const char *p = *format;
        std::stringstream out;
        while(*p){
            if(*p == '%'){
                
                // note: we do not support preceision or other substitution anotations
                p++;
                
                // add argument to our string if it exists
                if(i < args.Length()){
                    v8::String::Utf8Value value(args[i++]);
                    out << *value;
                }
            }else{
                out << *p;
            }
            p++;
        }
        return out.str();
    }else{
        return "";
    }
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
LogJavascriptModule::LogJavascriptModule(){
}
//! Destructor
LogJavascriptModule::~LogJavascriptModule(){
}

//////////////////////////////////////////////////////////////////////////

//! Exports module
void LogJavascriptModule::exportModule(v8::Local<v8::Object> &object, script::JavascriptEnvironment *pEnvironment){
    v8::Isolate* pIsolate = v8::Isolate::GetCurrent();
    
    // this is our root module object template where we register / attach other methods / objects
    v8::Local<v8::ObjectTemplate> module = v8::ObjectTemplate::New(pIsolate);
    
    // register various methods / objects for our module
    module->Set(v8::String::NewFromUtf8(pIsolate, "info"), v8::FunctionTemplate::New(pIsolate, logInfo));
    module->Set(v8::String::NewFromUtf8(pIsolate, "error"), v8::FunctionTemplate::New(pIsolate, logError));
    module->Set(v8::String::NewFromUtf8(pIsolate, "warning"), v8::FunctionTemplate::New(pIsolate, logWarning));

    // export object
    object = module->NewInstance();
}

//////////////////////////////////////////////////////////////////////////

//! Native log implementation
void LogJavascriptModule::logInfo(const v8::FunctionCallbackInfo<v8::Value>& args){
#if defined(NIMBLE_DEBUG)
    std::string str = sprintf(args);
    NIMBLE_LOG_INFO("javascript", str.c_str());
#endif
}
//! Native error implementation
void LogJavascriptModule::logError(const v8::FunctionCallbackInfo<v8::Value>& args){
#if defined(NIMBLE_DEBUG)
    std::string str = sprintf(args);
    NIMBLE_LOG_ERROR("javascript", str.c_str());
#endif
}
//! Native warning implementation
void LogJavascriptModule::logWarning(const v8::FunctionCallbackInfo<v8::Value>& args){
#if defined(NIMBLE_DEBUG)
    std::string str = sprintf(args);
    NIMBLE_LOG_WARNING("javascript", str.c_str());
#endif
}

//////////////////////////////////////////////////////////////////////////