//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/script/objects/javascriptobject.http.h>
#include <nimble/script/javascriptfunction.h>
#include <nimble/script/javascriptenvironment.h>
#include <nimble/core/assert.h>
#include <nimble/core/locator.h>
#include <nimble/core/functor.h>
#include <nimble/core/thread.pool.h>
#include <nimble/core/scopedprofiler.h>
#include <nimble/network/http.connection.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::script;

//////////////////////////////////////////////////////////////////////////

// TODO
// + Replace cleanuplist with dispatchInfo pool

//////////////////////////////////////////////////////////////////////////

nimble::script::HttpJavascriptModule::CleanupList nimble::script::HttpJavascriptModule::m_cleanupList;

//////////////////////////////////////////////////////////////////////////

//! Constructor
HttpJavascriptModule::HttpJavascriptModule(script::JavascriptEnvironment *pEnvironment)
:JavascriptObject(pEnvironment){
}
//! Destructor
HttpJavascriptModule::~HttpJavascriptModule(){
}

//////////////////////////////////////////////////////////////////////////

//! Update
void HttpJavascriptModule::cleanup(){
    SCOPEDPROFILE("HttpJavascriptModule::cleanup");
    for(CleanupList::iterator it = m_cleanupList.begin(); it != m_cleanupList.end(); it++){
        delete *it;
    }
    m_cleanupList.clear();
}

//////////////////////////////////////////////////////////////////////////

//! Exports module
void HttpJavascriptModule::exportModule(v8::Local<v8::Object> &object, script::JavascriptEnvironment *pEnvironment){
    v8::Isolate* pIsolate = v8::Isolate::GetCurrent();
    
    // this is our root module object template where we register / attach other methods / objects
    v8::Local<v8::ObjectTemplate> module = v8::ObjectTemplate::New(pIsolate);
    
    // register various methods / objects for our module
    v8::Handle<v8::Value> environment = v8::External::New(pIsolate, pEnvironment);
    module->Set(v8::String::NewFromUtf8(pIsolate, "request"), v8::FunctionTemplate::New(pIsolate, nativeRequest, environment));
    HttpResponseJavascriptObject::exportObjectTemplate(module);
    
    // export object
    object = module->NewInstance();
}

//////////////////////////////////////////////////////////////////////////

//! Dispatches a request
void HttpJavascriptModule::nativeRequest(const v8::FunctionCallbackInfo<v8::Value>& args){
    // make sure we have the right arguments
    if(args.Length() == 0){
        NIMBLE_LOG_ERROR("javascript", "Failed to dispatch http request - no arguments provided");
        return;
    }
    if(args[0]->IsObject() == false){
        NIMBLE_LOG_ERROR("javascript", "Failed to dispatch http request - invalid request argument");
        return;
    }
    if(args[1]->IsFunction() == false){
        NIMBLE_LOG_ERROR("javascript", "Failed to dispatch http request - invalid callback argument");
        return;
    }
    
    v8::Isolate* pIsolate = v8::Isolate::GetCurrent();

    // default request parmaeters
    std::string host = "localhost";
    std::string path = "/";
    std::string method = "GET";
    std::string port = "80";
    std::string protocol = "http";
    
    // convert request parameters into a request
    v8::Local<v8::Object> requestParameters = args[0]->ToObject();
    if(requestParameters->Has(v8::String::NewFromUtf8(pIsolate, "protocol"))){
        v8::String::Utf8Value value(requestParameters->Get(v8::String::NewFromUtf8(pIsolate, "protocol")));
        protocol = *value;
        
        // default port 443 for https
        if(protocol == "https"){
            port = "443";
        }
    }
    if(requestParameters->Has(v8::String::NewFromUtf8(pIsolate, "host"))){
        v8::String::Utf8Value value(requestParameters->Get(v8::String::NewFromUtf8(pIsolate, "host")));
        host = *value;
    }
    if(requestParameters->Has(v8::String::NewFromUtf8(pIsolate, "path"))){
        v8::String::Utf8Value value(requestParameters->Get(v8::String::NewFromUtf8(pIsolate, "path")));
        path = *value;
    }
    if(requestParameters->Has(v8::String::NewFromUtf8(pIsolate, "method"))){
        v8::String::Utf8Value value(requestParameters->Get(v8::String::NewFromUtf8(pIsolate, "method")));
        method = *value;
    }
    if(requestParameters->Has(v8::String::NewFromUtf8(pIsolate, "port"))){
        v8::String::Utf8Value value(requestParameters->Get(v8::String::NewFromUtf8(pIsolate, "port")));
        port = *value;
    }
    
    // construct our request
    network::HttpRequest request;
    request.setUrl(network::Url(protocol + std::string("://") + host + std::string(":") + port + path));
    if(method == "GET"){
        request.setMethod(network::HttpRequest::kGet);
    }else if(method == "POST"){
        request.setMethod(network::HttpRequest::kPost);
    }else if(method == "PUT"){
        request.setMethod(network::HttpRequest::kPut);
    }else if(method == "DELETE"){
        request.setMethod(network::HttpRequest::kDelete);
    }else{
        NIMBLE_LOG_ERROR("javascript", "Failed to dispatch http request - invalid method parameter");
        return;
    }
    request.enableOption(network::HttpRequest::kOptionFollowRedirect);
    
    // get environment
    v8::Handle<v8::External> environment = v8::Handle<v8::External>::Cast(args.Data());
    script::JavascriptEnvironment *pEnvironment = static_cast<script::JavascriptEnvironment*>(environment->Value());
    
    // track callback
    v8::Local<v8::Function> callback = v8::Handle<v8::Function>::Cast(args[1]);
    
    // create our dispatch info
    dispatchInfo_t *pInfo = allocateDispatchInfo();
    pInfo->request = request;
    pInfo->pCallback = new /*()*/ script::JavascriptFunction();
    pInfo->pCallback->wrap(callback);
    
    // create our download callback which is just a call to our sync download (but from a separate thread)
    core::Functor<dispatchInfo_t*, TLIST_1(dispatchInfo_t*)> dispatchRequestFunc(HttpJavascriptModule::dispatchRequest);
    
    // dispatch async download
    core::ThreadPool *pThread = core::locator_acquire<core::ThreadPool>();
    pInfo->future = pThread->executeAsyncTask<dispatchInfo_t*>(core::bindAll<
                                                               dispatchInfo_t*,
                                                               dispatchInfo_t*>(dispatchRequestFunc, pInfo));
    core::Functor<void, TLIST_2(script::JavascriptEnvironment*, core::Future<dispatchInfo_t*>&)> onRequestCompletedFunc(HttpJavascriptModule::onRequestCompleted);
    pInfo->future.then(core::Future<dispatchInfo_t*>::Continuation(core::bindFirst(onRequestCompletedFunc, pEnvironment)));
}
//! Dispatches a request
HttpJavascriptModule::dispatchInfo_t* HttpJavascriptModule::dispatchRequest(dispatchInfo_t *pDispatchInfo){
    network::HttpConnection connection;
    pDispatchInfo->response = connection.dispatch(pDispatchInfo->request);
    return pDispatchInfo;
}
//! Called when request was completed
void HttpJavascriptModule::onRequestCompleted(script::JavascriptEnvironment *pEnvironment, core::Future<dispatchInfo_t*> &future){
    try{
        dispatchInfo_t *pInfo = future.get();
        
        // set our scopes
        v8::Isolate* pIsolate = pEnvironment->getIsolate();
        v8::Isolate::Scope isolateScope(pIsolate);
        v8::HandleScope handleScope(pIsolate);
        v8::Local<v8::Context> context = v8::Local<v8::Context>::New(pIsolate, pEnvironment->getContext());
        v8::Context::Scope contextScope(context);
        HttpResponseJavascriptObject *pResponse = HttpResponseJavascriptObject::createInstance(pEnvironment, pInfo->response);
        v8::Local<v8::Object> response = pResponse->getObject();

        // call callback
        v8::Persistent<v8::Function> &persistent = pInfo->pCallback->getPersistent();
        v8::Local<v8::Function> function = v8::Local<v8::Function>::New(pIsolate, persistent);
        v8::Handle<v8::Value> args[1];
        args[0] = response;
        function->Call(function, 1, args);

        // destroy callback
        delete pInfo->pCallback;
        pInfo->pCallback = 0;
        
        // deallocate dispatch info
        // note: if we simply delete the pInfo here, the attached future will be cleared which
        // includes the continuation that is calling this method (onRequestCompleted). We crash
        // upon exiting this method because we access the (destroyed) continuation on this destroyed future.
        // We instead add the info to a list to be cleaned up at a later date.
        deallocateDispatchInfo(pInfo);
    }catch(...){
        NIMBLE_ASSERT_PRINT(false, "Failed to dispatch http request - this should NEVER happen or we will be leaking memory");
    };
}

//////////////////////////////////////////////////////////////////////////

//! Allocates a dispatch info
HttpJavascriptModule::dispatchInfo_t* HttpJavascriptModule::allocateDispatchInfo(){
    return new /*()*/ dispatchInfo_t();
}
//! Deallocates a dispatch info
void HttpJavascriptModule::deallocateDispatchInfo(HttpJavascriptModule::dispatchInfo_t *pInfo){
    // add our info to a cleanup list which will get cleaned up on the next
    // HttpJavascriptModule::update.
    m_cleanupList.push_back(pInfo);
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
HttpResponseJavascriptObject::HttpResponseJavascriptObject(script::JavascriptEnvironment *pEnvironment, network::HttpResponse const &response)
:script::JavascriptObject(pEnvironment)
,m_response(response){
}
//! Destructor
HttpResponseJavascriptObject::~HttpResponseJavascriptObject(){
}

//////////////////////////////////////////////////////////////////////////

//! Exports object template to a target
//! \param target the target to export to
void HttpResponseJavascriptObject::exportObjectTemplate(v8::Handle<v8::ObjectTemplate> &target){
    v8::Isolate* pIsolate = v8::Isolate::GetCurrent();
    
    // a function template is one that can be invoked in javascript
    // as a function. We create a constructor function template to construct
    // an object of a specific type (defined by our instance template).
    v8::Local<v8::FunctionTemplate> constructor = v8::FunctionTemplate::New(pIsolate, construct);
    // we will track a single internal field which will be bound on wrap
    constructor->InstanceTemplate()->SetInternalFieldCount(1);
    // add various definitions for this template
    constructor->InstanceTemplate()->SetAccessor(v8::String::NewFromUtf8(pIsolate, "code"), getCode);
    constructor->InstanceTemplate()->SetAccessor(v8::String::NewFromUtf8(pIsolate, "body"), getBody);

    // export our constructor template to our target
    target->Set(v8::String::NewFromUtf8(pIsolate, "Response"), constructor);
}
//! Creates an instance
HttpResponseJavascriptObject* HttpResponseJavascriptObject::createInstance(script::JavascriptEnvironment *pEnvironment, network::HttpResponse response){
    v8::Isolate *pIsolate = v8::Isolate::GetCurrent();
    
    // fetch our object template for creating our object
    v8::Handle<v8::ObjectTemplate> objectTemplate = v8::ObjectTemplate::New(pIsolate);
    HttpResponseJavascriptObject::exportObjectTemplate(objectTemplate);
    
    // fetch object constructor function
    v8::Local<v8::Object> templateInstance = objectTemplate->NewInstance();
    v8::Handle<v8::Value> value = templateInstance->Get(v8::String::NewFromUtf8(pIsolate, "Response"));
    NIMBLE_ASSERT(value->IsFunction());
    
    // fill our object
    v8::Handle<v8::Value> args[2];
    args[0] = v8::External::New(pIsolate, (void*)pEnvironment);
    args[1] = v8::External::New(pIsolate, (void*)&response);
    v8::Handle<v8::Function> func = v8::Handle<v8::Function>::Cast(value);
    v8::Local<v8::Object> object = v8::Local<v8::Object>::Cast(func->CallAsConstructor(2, args));
    v8::Local<v8::External> wrapper = v8::Local<v8::External>::Cast(object->GetInternalField(0));
    return static_cast<HttpResponseJavascriptObject*>(wrapper->Value());
}

//////////////////////////////////////////////////////////////////////////

//! Creates an instance
void HttpResponseJavascriptObject::construct(const v8::FunctionCallbackInfo<v8::Value>& args){
    script::JavascriptEnvironment *pEnvironment = 0;
    network::HttpResponse *pResponse = 0;
    if(args.Length() > 0){
        v8::Local<v8::External> external = v8::Local<v8::External>::Cast(args[0]->ToObject());
        pEnvironment = (script::JavascriptEnvironment*)external->Value();
    }
    if(args.Length() > 1){
        v8::Local<v8::External> external = v8::Local<v8::External>::Cast(args[1]->ToObject());
        pResponse = (network::HttpResponse*)external->Value();
    }
    HttpResponseJavascriptObject *pObject = new /*()*/ HttpResponseJavascriptObject(pEnvironment, *pResponse);
    pObject->wrap(args.This());
}
//! Gets our response code
//! \param property the property name of this operation
//! \param info extra information about this operation
void HttpResponseJavascriptObject::getCode(v8::Local<v8::String> property, const v8::PropertyCallbackInfo<v8::Value>& info){
    v8::Local<v8::Object> self = info.Holder();
    v8::Local<v8::External> wrap = v8::Local<v8::External>::Cast(self->GetInternalField(0));
    void *ptr = wrap->Value();
    int value = static_cast<HttpResponseJavascriptObject*>(ptr)->m_response.getCode();
    info.GetReturnValue().Set(value);
}
//! Gets our response body
//! \param property the property name of this operation
//! \param info extra information about this operation
void HttpResponseJavascriptObject::getBody(v8::Local<v8::String> property, const v8::PropertyCallbackInfo<v8::Value>& info){
    v8::Local<v8::Object> self = info.Holder();
    v8::Local<v8::External> wrap = v8::Local<v8::External>::Cast(self->GetInternalField(0));
    void *ptr = wrap->Value();
    v8::Isolate* pIsolate = v8::Isolate::GetCurrent();
    std::string body = static_cast<HttpResponseJavascriptObject*>(ptr)->m_response.getBody();
    v8::Handle<v8::String> value = v8::String::NewFromUtf8(pIsolate, body.c_str());
    info.GetReturnValue().Set(value);
}

//////////////////////////////////////////////////////////////////////////