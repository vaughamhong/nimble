//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/script/javascriptfunction.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::script;

//////////////////////////////////////////////////////////////////////////

//! Constructor
JavascriptFunction::JavascriptFunction(){
}
//! Destructor
JavascriptFunction::~JavascriptFunction(){
    m_persistent.Reset();
}

//////////////////////////////////////////////////////////////////////////

//! Wrap this javascript native object
void JavascriptFunction::wrap(v8::Handle<v8::Function> function){
    m_persistent.Reset(v8::Isolate::GetCurrent(), function);
}
//! Returns the persistent object
v8::Persistent<v8::Function>& JavascriptFunction::getPersistent(){
    return m_persistent;
}

//////////////////////////////////////////////////////////////////////////