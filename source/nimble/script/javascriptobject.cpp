//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/script/javascriptobject.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::script;

//////////////////////////////////////////////////////////////////////////

//! Constructor
JavascriptObject::JavascriptObject(script::JavascriptEnvironment *pEnvironment)
:m_pEnvironment(pEnvironment){
    NIMBLE_ASSERT(m_pEnvironment);
}
//! Destructor
JavascriptObject::~JavascriptObject(){
    if(m_persistent.IsEmpty()){
        return;
    }
    m_persistent.ClearWeak();
    m_persistent.Reset();
}

//////////////////////////////////////////////////////////////////////////

//! Wrap this javascript native object
void JavascriptObject::wrap(v8::Handle<v8::Object> object){
    v8::Isolate* pIsolate = v8::Isolate::GetCurrent();
    object->SetInternalField(0, v8::External::New(pIsolate, this));
    m_persistent.Reset(pIsolate, object);
    m_persistent.SetWeak(this, cleanupJavascriptObject);
    m_persistent.MarkIndependent();
}
//! Returns the persistent object
v8::Persistent<v8::Object>& JavascriptObject::getPersistent(){
    return m_persistent;
}
//! Returns the local object
v8::Local<v8::Object> JavascriptObject::getObject(){
    v8::Isolate* pIsolate = v8::Isolate::GetCurrent();
    return v8::Local<v8::Object>::New(pIsolate, m_persistent);
}
//! Returns the environment
script::JavascriptEnvironment* JavascriptObject::getEnvironment() const{
    return m_pEnvironment;
}

//////////////////////////////////////////////////////////////////////////

//! Cleans up native object when invoked for garbage collection
void JavascriptObject::cleanupJavascriptObject(const v8::WeakCallbackData<v8::Object, JavascriptObject>& data){
    v8::Isolate* isolate = data.GetIsolate();
    v8::HandleScope scope(isolate);
    delete data.GetParameter();
}

//////////////////////////////////////////////////////////////////////////