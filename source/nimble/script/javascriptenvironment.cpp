//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/script/javascriptenvironment.h>
#include <nimble/script/javascriptfunction.h>
#include <nimble/core/hash.h>
#include <nimble/core/textfile.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::script;

//////////////////////////////////////////////////////////////////////////

//! Constructor
JSVar::JSVar()
:m_type(JSVar::kTypeVoid){
}
//! Constructor
JSVar::JSVar(JSVar const &var)
:m_type(var.m_type)
,m_var(var.m_var)
,m_string(var.m_string){
}
//! Constructor
JSVar::JSVar(int value)
:m_type(JSVar::kTypeInt){
    m_var.i = value;
}
//! Constructor
JSVar::JSVar(bool value)
:m_type(JSVar::kTypeBool){
    m_var.b = value;
}
//! Constructor
JSVar::JSVar(const char *value)
:m_type(JSVar::kTypeString){
    m_string = value;
}
//! Destructor
JSVar::~JSVar(){
}

//////////////////////////////////////////////////////////////////////////

//! Returns the type
JSVar::eType JSVar::getType() const{
    return m_type;
}
//! Returns the int
int JSVar::getInt() const{
    return m_var.i;
}
//! Returns the bool
bool JSVar::getBool() const{
    return m_var.b;
}
//! Returns the string
const char* JSVar::getString() const{
    return m_string.c_str();
}
//! Copies variable
JSVar& JSVar::operator=(const JSVar& var){
    m_var = var.m_var;
    m_string = var.m_string;
    m_type = var.m_type;
    return *this;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
JavascriptEnvironment::JavascriptEnvironment(v8::Isolate *pIsolate, JavascriptEnvironmentDelegate *pDelegate)
:m_pDelegate(pDelegate)
,m_pIsolate(pIsolate)
,m_initialized(false){
}
//! Destructor
JavascriptEnvironment::~JavascriptEnvironment(){
    unload();
    destroyEnvironment();
}

//////////////////////////////////////////////////////////////////////////

//! Sets delegate
void JavascriptEnvironment::setDelegate(JavascriptEnvironmentDelegate *pDelegate){
    m_pDelegate = pDelegate;
}

//////////////////////////////////////////////////////////////////////////

//! Returns the isolate
v8::Isolate* JavascriptEnvironment::getIsolate(){
    return m_pIsolate;
}
//! Returns the global template
v8::Persistent<v8::ObjectTemplate>& JavascriptEnvironment::getGlobalTemplate(){
    return m_globalTemplate;
}
//! Returns the context
v8::Persistent<v8::Context>& JavascriptEnvironment::getContext(){
    return m_context;
}
//! Returns the script
v8::Persistent<v8::Script>& JavascriptEnvironment::getScript(){
    return m_script;
}

//////////////////////////////////////////////////////////////////////////

//! Returns true if loaded
bool JavascriptEnvironment::isLoaded() const{
    return !m_script.IsEmpty();
}

//////////////////////////////////////////////////////////////////////////

//! Load script
void JavascriptEnvironment::loadWithFilePath(const char *filePath){
    if(!m_script.IsEmpty()){
        NIMBLE_LOG_WARNING("javascript", "Failed to load script - already loaded");
        return;
    }
    
    core::TextFile textFile(filePath);
    loadWithBuffer(textFile.getConstPointer());
}
//! Load script
void JavascriptEnvironment::loadWithBuffer(const char *pBuffer){
    if(!m_script.IsEmpty()){
        NIMBLE_LOG_WARNING("javascript", "Failed to load script - already loaded");
        return;
    }
    
    if(!m_initialized){
        initializeEnvironment();
        m_initialized = true;
    }
    
    if(m_pDelegate){
        m_pDelegate->javascriptEnvironmentScriptWillLoad(this);
    }

    // set our scopes
    v8::Isolate::Scope isolateScope(m_pIsolate);
    v8::HandleScope handleScope(m_pIsolate);
    v8::Local<v8::Context> context = v8::Local<v8::Context>::New(m_pIsolate, m_context);
    v8::Context::Scope contextScope(context);
    
    // load script
    {
        v8::TryCatch trycatch;
        v8::Local<v8::String> source = v8::String::NewFromUtf8(m_pIsolate, pBuffer);
        v8::Local<v8::Script> script = v8::Script::Compile(source);
        // save our script into a persistent handle which means it will not be collected once out of scope
        // we will manually clean this up once our component has been destroyed
        m_script.Reset(m_pIsolate, script);
        
        // report exceptions
        v8::String::Utf8Value exception_str(trycatch.Exception());
        if(const char *exception = *exception_str){
            NIMBLE_LOG_ERROR("javascript", exception);
            if(m_pDelegate){
                m_pDelegate->javascriptEnvironmentScriptLoadDidFail(this);
            }
            return;
        }
    }
    
    if(m_pDelegate){
        m_pDelegate->javascriptEnvironmentScriptDidLoad(this);
    }
}
//! Unload script
void JavascriptEnvironment::unload(){
    if(m_pDelegate){
        m_pDelegate->javascriptEnvironmentScriptWillUnload(this);
    }
    removeAllEventListeners();
    m_script.Reset();
}

//////////////////////////////////////////////////////////////////////////

//! initializes our environment
void JavascriptEnvironment::initializeEnvironment(){
    // set our scopes
    v8::Isolate::Scope isolateScope(m_pIsolate);
    v8::HandleScope handleScope(m_pIsolate);
    
    // create our global object template which all objects will be attached
    v8::Handle<v8::ObjectTemplate> globalTemplate = v8::ObjectTemplate::New(m_pIsolate);
    // save our global template into a persistent handle which means it will not be collected once out of scope
    // we will manually clean this up once our component has been destroyed
    m_globalTemplate.Reset(m_pIsolate, globalTemplate);
    
    // create our context with a global template
    v8::Handle<v8::Context> context = v8::Context::New(m_pIsolate, 0, globalTemplate);
    // save our context into a persistent handle which means it will not be collected once out of scope
    // we will manually clean this up once our component has been destroyed
    m_context.Reset(m_pIsolate, context);
    m_pIsolate->ContextDisposedNotification();
}
//! destroys our environment
void JavascriptEnvironment::destroyEnvironment(){
    m_context.Reset();
    m_globalTemplate.Reset();
    m_pIsolate->ContextDisposedNotification();
}

//////////////////////////////////////////////////////////////////////////

//! Run script
void JavascriptEnvironment::run(){
    if(m_script.IsEmpty()){
        NIMBLE_LOG_WARNING("javascript", "Failed to run script - no script loaded");
        return;
    }
    
    // set our scopes
    v8::Isolate::Scope isolateScope(m_pIsolate);
    v8::HandleScope handleScope(m_pIsolate);
    v8::Local<v8::Context> context = v8::Local<v8::Context>::New(m_pIsolate, m_context);
    v8::Context::Scope contextScope(context);

    // run script which will initialize global
    // variables and call global methods
    {
        v8::TryCatch trycatch;
        v8::Local<v8::Script> script = v8::Local<v8::Script>::New(m_pIsolate, m_script);
        script->Run();
        
        // report exceptions
        v8::String::Utf8Value exception_str(trycatch.Exception());
        if(const char *exception = *exception_str){
            NIMBLE_LOG_ERROR("javascript", exception);
        }
    }
}

//////////////////////////////////////////////////////////////////////////

//! Returns true if method exists
bool JavascriptEnvironment::existsMethodWithName(const char *name){
    if(m_script.IsEmpty()){
        NIMBLE_LOG_WARNING("javascript", "Failed to run script - no script loaded");
        return false;
    }

    // set our scopes
    v8::Isolate::Scope isolateScope(m_pIsolate);
    v8::HandleScope handleScope(m_pIsolate);
    v8::Local<v8::Context> context = v8::Local<v8::Context>::New(m_pIsolate, m_context);
    v8::Context::Scope contextScope(context);
    
    // check for function
    v8::Handle<v8::Object> global = context->Global();
    v8::Handle<v8::Value> value = global->Get(v8::String::NewFromUtf8(m_pIsolate, name));
    return value->IsFunction();
}
//! Calls a javascript method and returns a value
script::JSVar JavascriptEnvironment::callMethod(const char *name, int numParams, ...){
    va_list args;
    va_start(args, numParams);
    script::JSVar var = this->callMethod(name, numParams, args);
    va_end(args);
    return var;
}
//! Calls a javascript method and returns a value
script::JSVar JavascriptEnvironment::callMethod(const char *name, int numParams, va_list va){
    // set our scopes
    v8::Isolate::Scope isolateScope(m_pIsolate);
    v8::HandleScope handleScope(m_pIsolate);
    v8::Local<v8::Context> context = v8::Local<v8::Context>::New(m_pIsolate, m_context);
    v8::Context::Scope contextScope(context);
    
    // retreive our function
    v8::Handle<v8::Object> global = context->Global();
    v8::Handle<v8::Value> value = global->Get(v8::String::NewFromUtf8(m_pIsolate, name));

    // exit if no method was found
    if(value->IsFunction() == false){
        NIMBLE_LOG_WARNING("javascript", "Failed to run script method - no method named %s", name);
        return script::JSVar();
    }
    
    v8::Handle<v8::Function> func = v8::Handle<v8::Function>::Cast(value);
    return this->callMethod(func, numParams, va);
}
//! Calls a javascript method and returns a value
script::JSVar JavascriptEnvironment::callMethod(v8::Handle<v8::Function> &func, int numParams, va_list va){
    // this is only valid if our script has been loaded
    if(m_script.IsEmpty()){
        NIMBLE_LOG_WARNING("javascript", "Failed to run script method - no script loaded");
        return script::JSVar();
    }
    
    // set our scopes
    v8::Isolate::Scope isolateScope(m_pIsolate);
    v8::HandleScope handleScope(m_pIsolate);
    v8::Local<v8::Context> context = v8::Local<v8::Context>::New(m_pIsolate, m_context);
    v8::Context::Scope contextScope(context);
    v8::Handle<v8::Object> global = context->Global();
    
    // generate our parameters list
    if(numParams > kCallMethodMaxParameters){
        NIMBLE_LOG_WARNING("javascript", "Failed to add all parameters (%d) into method call - max %d", numParams, kCallMethodMaxParameters);
        numParams = kCallMethodMaxParameters;
    }
    v8::Handle<v8::Value> args[kCallMethodMaxParameters];
    for(int i = 0; i < numParams; i++){
        JSVar *pVariable = va_arg(va, JSVar*);
        if(pVariable->getType() == JSVar::kTypeInt){
            args[i] = v8::Integer::New(m_pIsolate, pVariable->getInt());
        }else if(pVariable->getType() == JSVar::kTypeBool){
            args[i] = v8::Boolean::New(m_pIsolate, pVariable->getBool());
        }else if(pVariable->getType() == JSVar::kTypeString){
            args[i] = v8::String::NewFromUtf8(m_pIsolate, pVariable->getString());
        }
    }
    
    // run a specific function
    v8::Local<v8::Value> returnValue = func->Call(global, numParams, args);
    if(returnValue.IsEmpty() == false){
        if(returnValue->IsInt32()){
            return script::JSVar(returnValue->Int32Value());
        }else if(returnValue->IsBoolean()){
            return script::JSVar(returnValue->BooleanValue());
        }else if(returnValue->IsString()){
            v8::String::Utf8Value str(returnValue->ToString(m_pIsolate));
            return script::JSVar(*str);
        }else{
            if(!returnValue->IsUndefined()){
                v8::String::Utf8Value name(func->GetName());
                NIMBLE_LOG_WARNING("javascript", "Failed to convert return type for method named %s", *name);
            }
            return script::JSVar();
        }
    }else{
        return script::JSVar();        
    }
}

//////////////////////////////////////////////////////////////////////////

//! Add event listener
void JavascriptEnvironment::addEventListener(const char *eventName, v8::Handle<v8::Function> &function){
    // find our event info
    int64_t eventHash = core::hash_djb2(eventName);
    EventHashToInfoIndex::iterator it = m_eventHashToInfoIndex.find(eventHash);
    if(it != m_eventHashToInfoIndex.end()){
        // replace existing handler
        it->second.pFunction->wrap(function);
    }else{
        // add new handler
        eventInfo_t info;
        info.pFunction = new /*()*/ script::JavascriptFunction();
        info.pFunction->wrap(function);
        m_eventHashToInfoIndex.insert(std::make_pair(eventHash, info));
    }
}
//! Remove event listener
void JavascriptEnvironment::removeEventListener(const char *eventName){
    // find our event info
    int64_t eventHash = core::hash_djb2(eventName);
    EventHashToInfoIndex::iterator it = m_eventHashToInfoIndex.find(eventHash);
    if(it != m_eventHashToInfoIndex.end()){
        delete it->second.pFunction;
        m_eventHashToInfoIndex.erase(it);
    }
}
//! Removes all listeners
void JavascriptEnvironment::removeAllEventListeners(){
    for(EventHashToInfoIndex::iterator it = m_eventHashToInfoIndex.begin(); it != m_eventHashToInfoIndex.end();){
        delete it->second.pFunction;
        it = m_eventHashToInfoIndex.erase(it);
    }
}
//! Dispatch event
void JavascriptEnvironment::dispatchEvent(const char *eventName, int numParams, ...){
    va_list args;
    va_start(args, numParams);
    this->dispatchEvent(eventName, numParams, args);
    va_end(args);
}
//! Dispatch event
void JavascriptEnvironment::dispatchEvent(const char *eventName, int numParams, va_list va){
    // find our event info
    int64_t eventHash = core::hash_djb2(eventName);
    EventHashToInfoIndex::iterator it = m_eventHashToInfoIndex.find(eventHash);
    if(it == m_eventHashToInfoIndex.end()){
        return;
    }
    
    // set our scopes
    v8::Isolate::Scope isolateScope(m_pIsolate);
    v8::HandleScope handleScope(m_pIsolate);
    v8::Local<v8::Context> context = v8::Local<v8::Context>::New(m_pIsolate, m_context);
    v8::Context::Scope contextScope(context);

    // call function
    v8::Persistent<v8::Function> &persistent = it->second.pFunction->getPersistent();
    v8::Local<v8::Function> function = v8::Local<v8::Function>::New(m_pIsolate, persistent);

    // call method
    this->callMethod(function, numParams, va);
}

//////////////////////////////////////////////////////////////////////////