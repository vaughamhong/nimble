//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.request.file.upload.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
HttpFileUploadRequest::HttpFileUploadRequest(PathType const &localPath, Url const &url)
:HttpRequest(HttpRequest::kPut, url)
,m_localPath(localPath){
}
//! Constructor
HttpFileUploadRequest::HttpFileUploadRequest(HttpFileUploadRequest const &rhs)
:HttpRequest(rhs)
,m_localPath(rhs.m_localPath){
}
//! Destructor
HttpFileUploadRequest::~HttpFileUploadRequest(){
}

///////////////////////////////////////////////////////////////////////////////

//! Assignment operator
void HttpFileUploadRequest::operator=(HttpFileUploadRequest const &rhs){
    HttpRequest::operator=(rhs);
    m_localPath = rhs.m_localPath;
}

//! Sets the local path for this request
void HttpFileUploadRequest::setLocalPath(HttpFileUploadRequest::PathType const &localPath){
    m_localPath = localPath;
}
//! Gets the local path for this request
HttpFileUploadRequest::PathType HttpFileUploadRequest::getLocalPath() const{
    return m_localPath;
}

///////////////////////////////////////////////////////////////////////////////