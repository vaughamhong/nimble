//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.url.h>
#include <algorithm>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

void nimble::network::parseUrl(const char *url, std::string &protocol, std::string &host, std::string &port, std::string &path){
    std::string _url(url);
    protocol = "";
    host = "";
    port = "";
    path = "";
    
    if(_url.length() == 0){
        return;
    }
    
    typedef std::string::const_iterator Iterator;
    
    Iterator urlStart = _url.end();
    Iterator urlEnd = _url.end();
    
    // get query start
    Iterator queryStart = std::find(urlStart, urlEnd, '?');
    
    // protocol
    Iterator protocolStart = _url.begin();
    Iterator protocolEnd = std::find(protocolStart, urlEnd, ':');
    if(protocolEnd != urlEnd){
        std::string prot = &*(protocolEnd);
        if((prot.length() > 3) && (prot.substr(0, 3) == "://")){
            protocol = std::string(protocolStart, protocolEnd);
            protocolEnd += 3;
        }else{
            protocolEnd = _url.begin();  // no protocol
        }
    }else{
        protocolEnd = _url.begin();  // no protocol
    }
    
    // host
    Iterator hostStart = protocolEnd;
    Iterator pathStart = std::find(hostStart, urlEnd, '/');  // get pathStart
    Iterator hostEnd = std::find(protocolEnd,
                                (pathStart != urlEnd) ? pathStart : queryStart, ':');  // check for port
    host = std::string(hostStart, hostEnd);
    
    // port
    if((hostEnd != urlEnd) && ((&*(hostEnd))[0] == ':')){
        hostEnd++;
        Iterator portEnd = (pathStart != urlEnd) ? pathStart : queryStart;
        port = std::string(hostEnd, portEnd);
    }
    
    // path
    if(pathStart != urlEnd){
        path = std::string(pathStart, queryStart);
    }
}
void nimble::network::resolveAbsolutePath(const char *pRemoteRootDir, const char *pPath, std::string &absolutePath){
    network::Url remoteRootDir(pRemoteRootDir);
    std::string host = remoteRootDir.getHost();
    std::string port = remoteRootDir.getPort();
    std::string path = remoteRootDir.getPath();
    std::string protocol = remoteRootDir.getProtocol();
    
    // determine the path type
    std::string tempPath = "";
    if(strncmp(pPath, "http://", strlen("http://")) == 0 || strncmp(pPath, "https://", strlen("https://")) == 0){
        // this is an absolute path
        // eg. "http://www.google.com/scene/scene.xml"
        tempPath = pPath;
    }else if(pPath[0] == '/'){
        // this is a path relative to the root
        // eg. "/scene/scene.xml"
        tempPath = protocol + std::string("://") + host + std::string(pPath);
    }else{
        // this is a path relative to the current directory
        // eg. "scene.xml"
        // eg. "scene/scene.xml"
        // eg. "./scene/scene.xml"
        // eg. "../scene/scene.xml"
        tempPath = protocol + std::string("://") + host + path + std::string("/") + std::string(pPath);
    }
    
    std::size_t pathStart = tempPath.find("//") + 2;
    std::size_t currentSlash, lastSlash, prevSlash;

    // resolve ./ by removing them
    absolutePath = tempPath.substr(0, pathStart);
    currentSlash = tempPath.find("/./", pathStart);
    lastSlash = pathStart;
    do{
        // exit if we have reached the end
        if(currentSlash == std::string::npos){
            // copy data up to the end
            absolutePath += tempPath.substr(lastSlash);
            break;
        }
        
        // copy data up to /./
        absolutePath += tempPath.substr(lastSlash, (currentSlash - lastSlash));
        
        // place copy start pointer past the /./
        lastSlash = currentSlash + 2;
        
        // find the next /./
        currentSlash = tempPath.find("/./", lastSlash);
    }while(true);
    
    // resolve ../ by collapsing previous directory
    tempPath = absolutePath;
    absolutePath = tempPath.substr(0, pathStart);
    currentSlash = tempPath.find("/", pathStart);
    lastSlash = pathStart;
    do{
        // exit if we have reached the end
        if(currentSlash == std::string::npos){
            // copy data up to the end
            absolutePath += tempPath.substr(lastSlash);
            break;
        }
        
        // push directory
        absolutePath += tempPath.substr(lastSlash, (currentSlash - lastSlash));
        
        std::string str = tempPath.substr(currentSlash, 4);
        if(str == "/../"){
            // we found a /../ pop last directory from our currentl constructed path
            prevSlash = absolutePath.find_last_of("/");
            absolutePath.erase(prevSlash, std::string::npos);
            
            lastSlash = currentSlash + 3;
            currentSlash = tempPath.find("/", lastSlash);
        }else{
            // copy data up to last found /
            lastSlash = currentSlash;
            currentSlash = tempPath.find("/", lastSlash + 1);
        }
    }while(true);
}

///////////////////////////////////////////////////////////////////////////////

//! Constructor
Url::Url()
:m_location("")
,m_host("")
,m_port("")
,m_path("")
,m_protocol(""){
}
//! Constructor
Url::Url(Url::LocationType location)
:m_location(location){
    for(size_t pos = m_location.find(' '); pos != std::string::npos; pos = m_location.find(' ', pos)){
        m_location.replace(pos, 1, "%20");
    }
    parseUrl(m_location.c_str(), m_protocol, m_host, m_port, m_path);
}
//! Destructor
Url::~Url(){
}

///////////////////////////////////////////////////////////////////////////////

//! Returns the location
Url::LocationType Url::getLocation() const{
    return m_location;
}

//! Returns the host
std::string Url::getHost() const{
    return m_host;
}
//! Returns the port
std::string Url::getPort() const{
    return m_port;
}
//! Returns the path
std::string Url::getPath() const{
    return m_path;
}
//! Returns the protocol
std::string Url::getProtocol() const{
    return m_protocol;
}

///////////////////////////////////////////////////////////////////////////////