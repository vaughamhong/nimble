//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/socket.client.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#if defined(NIMBLE_TARGET_WIN32)
#pragma message("TODO - IMPL network socket client")
#elif defined(NIMBLE_TARGET_OSX)
#include <unistd.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
SocketClient::SocketClient(int network, int type)
:m_host("")
,m_port(-1)
,m_socketFileDescriptor(-1)
,m_network(network)
,m_type(type)
,m_isConnected(false){
}
//! Destructor
SocketClient::~SocketClient(){
}

///////////////////////////////////////////////////////////////////////////////

//! Connects to a host
void SocketClient::connect(const char *host, int port){
#if defined(NIMBLE_TARGET_OSX)
    if(isConnected()){
        NIMBLE_LOG_ERROR("network", "Failed to connect with previous connection still active");
        return;
    }
    
    // create our socket file descriptor
    m_socketFileDescriptor = socket(m_network, m_type, 0);
    if(m_socketFileDescriptor < 0){
        NIMBLE_LOG_ERROR("network", "Failed to create socket handle with error %s", strerror(errno));
        return;
    }
    
    m_host = host;
    m_port = port;
    struct sockaddr_in socket_addr;
    struct hostent *server;
    
    // create server descriptor
    server = gethostbyname(m_host.c_str());
    if(server == NULL){
        NIMBLE_LOG_ERROR("network", "Failed to create server descriptor with error %s", strerror(errno));
        close(m_socketFileDescriptor);
        m_socketFileDescriptor = -1;
        return;
    }
    
    // configure socket address descriptor
    bzero((char *)&socket_addr, sizeof(socket_addr));
    bcopy((char *)server->h_addr, (char *)&socket_addr.sin_addr.s_addr, server->h_length);
    socket_addr.sin_family = m_network;
    socket_addr.sin_port = htons(m_port);
    
    // try and connect
    if(::connect(m_socketFileDescriptor, (const struct sockaddr*)&socket_addr, sizeof(socket_addr)) < 0){
        NIMBLE_LOG_ERROR("network", "Failed to connect with error %s", strerror(errno));
        close(m_socketFileDescriptor);
        m_socketFileDescriptor = -1;
        return;
    }
    m_isConnected = true;
#else
    NIMBLE_ASSERT(false);
#endif
}
//! Disconnect
void SocketClient::disconnect(){
#if defined(NIMBLE_TARGET_OSX)
    if(isConnected()){
        close(m_socketFileDescriptor);
        m_socketFileDescriptor = -1;
        m_isConnected = false;
    }
#else
    NIMBLE_ASSERT(false);
#endif
}

///////////////////////////////////////////////////////////////////////////////

//! Receives data from a host
void SocketClient::read(char *pBuffer, uint32_t size) const{
#if defined(NIMBLE_TARGET_OSX)
    // early exit if we are not connected
    if(!isConnected()){
        NIMBLE_LOG_ERROR("network", "Failed send message - connection was invalid");
        return;
    }
    // read from socket
    bzero(pBuffer, size);
    if(::read(m_socketFileDescriptor, pBuffer, size) < 0){
        NIMBLE_LOG_ERROR("network", "Failed to reading from socket with error %s", strerror(errno));
        return;
    }
#else
    NIMBLE_ASSERT(false);
#endif
}
//! Sends data to a host
void SocketClient::write(const char *pBuffer) const{
#if defined(NIMBLE_TARGET_OSX)
    // early exit if we are not connected
    if(!isConnected()){
        NIMBLE_LOG_ERROR("network", "Failed send message - connection was invalid");
        return;
    }
    // write to socket
    if(::write(m_socketFileDescriptor, pBuffer, strlen(pBuffer)) < 0){
        NIMBLE_LOG_ERROR("network", "Failed to write to socket with error %s", strerror(errno));
        return;
    }
#else
    NIMBLE_ASSERT(false);
#endif
}

///////////////////////////////////////////////////////////////////////////////

//! Check how many bytes are ready to be read
int32_t SocketClient::bytesReady() const{
#if defined(NIMBLE_TARGET_OSX)
    // early exit if we are not connected
    if(!isConnected()){
        return 0;
    }
    
    // add our file descriptor to our fd_set (for checking)
    fd_set read_fd_set;
    FD_ZERO(&read_fd_set);
    FD_SET(m_socketFileDescriptor, &read_fd_set);

    // specify that we don't want to wait
    struct timeval timeout;
    memset((char *)&timeout,0,sizeof(timeout));
    
    // check if there is any data ready
    if(select(m_socketFileDescriptor + 1, &read_fd_set, NULL, NULL, &timeout) < 0){
        return 0;
    }
    if(FD_ISSET(m_socketFileDescriptor, &read_fd_set)){
        // figure out how many bytes are ready
        int32_t count;
        ioctl(m_socketFileDescriptor, FIONREAD, &count);
        return count;
    }
    return 0;
#else
    NIMBLE_ASSERT(false);
    return 0;
#endif
}
//! Returns true if we are connected
bool SocketClient::isConnected() const{
    return m_isConnected;
}

///////////////////////////////////////////////////////////////////////////////