//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.init.h>
#include <nimble/core/thread.h>
#include <nimble/core/logger.h>

///////////////////////////////////////////////////////////////////////////////

#define CURL_STATICLIB
#include <curl/curl.h>
#include <openssl/crypto.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
static core::Mutex *g_pLockArray = 0;
#endif

///////////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
// OpenSSL callback for entering / exiting critical section
static void lock_callback(int mode, int type, char *file, int line){
	if(mode & CRYPTO_LOCK){
		g_pLockArray[type].lock();
	}else{
		g_pLockArray[type].unlock();
	}
}
// OpenSSL callback for returning thread id
static unsigned long thread_id(void){
	return core::getCurrentThreadId();
}
#endif

///////////////////////////////////////////////////////////////////////////////

//! initialize
void nimble::network::http_init(){
#if !defined(NIMBLE_TARGET_ANDROID)
	// don't initialize if it has already been initialized
	if (g_pLockArray != 0){
		NIMBLE_LOG_WARNING("network", "Failed to initialize http - already initialized");
	}

	// must initialize libcurl before any threads are started
	curl_global_init(CURL_GLOBAL_ALL);

	// create locks for OpenSSL
	// OpenSSL is not thread-safe (http://curl.haxx.se/libcurl/c/threaded-ssl.html)
	// OpenSSL uses these locks to manage shared state in a multi-threaded environment
	int numLocks = CRYPTO_num_locks();
	g_pLockArray = (core::Mutex*)OPENSSL_malloc(numLocks * sizeof(core::Mutex));
	for(int i = 0; i < numLocks; i++){
		new (&g_pLockArray[i]) core::Mutex();
	}
	CRYPTO_set_id_callback((unsigned long(*)())thread_id);
	CRYPTO_set_locking_callback((void(*)(int, int, const char*, int))lock_callback);
#endif
}
//! terminate
void nimble::network::http_terminate(){
#if !defined(NIMBLE_TARGET_ANDROID)
	// clean up OpenSSL locks
	CRYPTO_set_locking_callback(NULL);
	CRYPTO_set_id_callback(NULL);
	int numLocks = CRYPTO_num_locks();
	for (int i = 0; i < numLocks; i++){
		g_pLockArray[i].~Mutex();
	}
	OPENSSL_free(g_pLockArray); 
	g_pLockArray = 0;
#endif
}

///////////////////////////////////////////////////////////////////////////////