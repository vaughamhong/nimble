//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.headeritem.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
HttpHeaderItem::HttpHeaderItem()
:m_key("")
,m_value(""){
}
//! Constructor
HttpHeaderItem::HttpHeaderItem(HttpHeaderItem::KeyType key, HttpHeaderItem::ValueType value)
:m_key(key)
,m_value(value){
}
//! Constructor
HttpHeaderItem::HttpHeaderItem(HttpHeaderItem const &rhs)
:m_key(rhs.m_key)
,m_value(rhs.m_value){
}
//! Destructor
HttpHeaderItem::~HttpHeaderItem(){
}

///////////////////////////////////////////////////////////////////////////////

//! Assignment operator
void HttpHeaderItem::operator=(HttpHeaderItem const &rhs){
    m_key = rhs.m_key;
    m_value = rhs.m_value;
}

///////////////////////////////////////////////////////////////////////////////

//! Sets our key
void HttpHeaderItem::setKey(HttpHeaderItem::KeyType key){
    m_key = key;
}
//! Gets a key
HttpHeaderItem::KeyType HttpHeaderItem::getKey() const{
    return m_key;
}

//! Sets our value
void HttpHeaderItem::setValue(HttpHeaderItem::ValueType value){
    m_value = value;
}
//! Gets a value
HttpHeaderItem::ValueType HttpHeaderItem::getValue() const{
    return m_value;
}

///////////////////////////////////////////////////////////////////////////////