//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.request.file.header.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
HttpFileHeaderRequest::HttpFileHeaderRequest(Url const &url)
:HttpRequest(HttpRequest::kGet, url){
}
//! Constructor
HttpFileHeaderRequest::HttpFileHeaderRequest(HttpFileHeaderRequest const &rhs)
:HttpRequest(rhs){
}
//! Destructor
HttpFileHeaderRequest::~HttpFileHeaderRequest(){
}

///////////////////////////////////////////////////////////////////////////////

//! Assignment operator
void HttpFileHeaderRequest::operator=(HttpFileHeaderRequest const &rhs){
    HttpRequest::operator=(rhs);
}

///////////////////////////////////////////////////////////////////////////////