//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.message.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
HttpMessage::HttpMessage()
:m_body(""){
}
//! Constructor
HttpMessage::HttpMessage(HttpMessage const &rhs)
:m_headers(rhs.m_headers)
,m_body(rhs.m_body){
}
//! Destructor
HttpMessage::~HttpMessage(){
}

///////////////////////////////////////////////////////////////////////////////

//! Assignment operator
void HttpMessage::operator=(HttpMessage const &rhs){
    m_headers = rhs.m_headers;
    m_body = rhs.m_body;
}

///////////////////////////////////////////////////////////////////////////////

//! Sets body
void HttpMessage::setBody(HttpMessage::BodyType body){
    m_body = body;
}
//! Appends to our body
void HttpMessage::appendBody(HttpMessage::BodyType append){
    m_body += append;
}
//! Gets body
HttpMessage::BodyType HttpMessage::getBody() const{
    return m_body;
}

///////////////////////////////////////////////////////////////////////////////

//! Adds a header item
void HttpMessage::addHeaderItem(HttpHeaderItem const &item){
    m_headers.push_back(item);
}
//! Returns the number of header items
uint32_t HttpMessage::getNumHeaderItems() const{
    return m_headers.size();
}
//! Returns the start iterator for our headers
HttpMessage::HeaderIterator HttpMessage::findHeaderWithKey(network::HttpHeaderItem::KeyType const &key){
    for(HttpMessage::HeaderIterator it = m_headers.begin(); it != m_headers.end(); it++){
        if(it->getKey() == key){
            return it;
        }
    }
    return m_headers.end();
}
//! Returns the start iterator for our headers
HttpMessage::HeaderIterator HttpMessage::headerItemsBegin(){
    return m_headers.begin();
}
//! Returns the end iterator for our headers
HttpMessage::HeaderIterator HttpMessage::headerItemsEnd(){
    return m_headers.end();
}
//! Returns the start iterator for our headers
HttpMessage::HeaderConstIterator HttpMessage::headerItemsBegin() const{
    return m_headers.begin();
}
//! Returns the end iterator for our headers
HttpMessage::HeaderConstIterator HttpMessage::headerItemsEnd() const{
    return m_headers.end();
}

///////////////////////////////////////////////////////////////////////////////