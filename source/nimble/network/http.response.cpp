//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.response.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
HttpResponse::HttpResponse()
:m_code(kResponseCodeFailure){
}
//! Constructor
HttpResponse::HttpResponse(eResponseCode code)
:m_code(code){
}
//! Constructor
HttpResponse::HttpResponse(HttpResponse const &rhs)
:HttpMessage(rhs)
,m_code(rhs.m_code){
}
//! Destructor
HttpResponse::~HttpResponse(){
}

///////////////////////////////////////////////////////////////////////////////

//! Assignment operator
void HttpResponse::operator=(HttpResponse const &rhs){
    HttpMessage::operator=(rhs);
    m_code = rhs.m_code;
}

///////////////////////////////////////////////////////////////////////////////

//! Sets our response code
void HttpResponse::setCode(eResponseCode const &code){
    m_code = code;
}
//! Returns our response code
eResponseCode HttpResponse::getCode() const{
    return m_code;
}

///////////////////////////////////////////////////////////////////////////////