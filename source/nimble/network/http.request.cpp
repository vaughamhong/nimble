//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.request.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
HttpRequest::HttpRequest()
:m_url("")
,m_timeoutInSeconds(-1){
    memset(&m_options, 0, sizeof(bool) * kNumOptions);
}
//! Constructor
HttpRequest::HttpRequest(Url const &url)
:m_method(HttpRequest::kGet)
,m_url(url)
,m_timeoutInSeconds(-1){
    memset(&m_options, 0, sizeof(bool) * kNumOptions);
}
//! Constructor
HttpRequest::HttpRequest(HttpRequest::eMethod method, Url const &url)
:m_method(method)
,m_url(url)
,m_timeoutInSeconds(-1){
    memset(&m_options, 0, sizeof(bool) * kNumOptions);
}
//! Constructor
HttpRequest::HttpRequest(HttpRequest const &rhs)
:HttpMessage(rhs)
,m_method(rhs.m_method)
,m_url(rhs.m_url)
,m_timeoutInSeconds(rhs.m_timeoutInSeconds){
    memcpy(&m_options, &rhs.m_options, sizeof(bool) * kNumOptions);
}
//! Destructor
HttpRequest::~HttpRequest(){
}

///////////////////////////////////////////////////////////////////////////////

//! Assignment operator
void HttpRequest::operator=(HttpRequest const &rhs){
    HttpMessage::operator=(rhs);
    m_method = rhs.m_method;
    m_url = rhs.m_url;
    memcpy(&m_options, &rhs.m_options, sizeof(bool) * kNumOptions);
}

///////////////////////////////////////////////////////////////////////////////

//! Sets timeout in seconds
void HttpRequest::setTimeout(int32_t seconds){
    m_timeoutInSeconds = seconds;
}
//! Gets timeout in seconds
int32_t HttpRequest::getTimeout() const{
    return m_timeoutInSeconds;
}

//! Sets the url for this request
void HttpRequest::setUrl(Url const &url){
    m_url = url;
}
//! Gets the url for this request
Url HttpRequest::getUrl() const{
    return m_url;
}
//! Sets the method for this request
void HttpRequest::setMethod(HttpRequest::eMethod const &method){
    m_method = method;
}
//! Gets the method for this request
HttpRequest::eMethod HttpRequest::getMethod() const{
    return m_method;
}

///////////////////////////////////////////////////////////////////////////////

//! Enable an option
void HttpRequest::enableOption(eOption const &option){
    m_options[option] = true;
}
//! Disable an option
void HttpRequest::disableOption(eOption const &option){
    m_options[option] = false;
}
//! Returns option value
bool HttpRequest::getOption(eOption const &option) const{
    return m_options[option];
}

///////////////////////////////////////////////////////////////////////////////