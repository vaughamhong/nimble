//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.request.file.download.h>

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

//! Constructor
HttpFileDownloadRequest::HttpFileDownloadRequest(PathType const &localPath, Url const &url)
:HttpRequest(HttpRequest::kGet, url)
,m_localPath(localPath){
}
//! Constructor
HttpFileDownloadRequest::HttpFileDownloadRequest(HttpFileDownloadRequest const &rhs)
:HttpRequest(rhs)
,m_localPath(rhs.m_localPath){
}
//! Destructor
HttpFileDownloadRequest::~HttpFileDownloadRequest(){
}

///////////////////////////////////////////////////////////////////////////////

//! Assignment operator
void HttpFileDownloadRequest::operator=(HttpFileDownloadRequest const &rhs){
    HttpRequest::operator=(rhs);
    m_localPath = rhs.m_localPath;
}

//! Sets the local path for this request
void HttpFileDownloadRequest::setLocalPath(HttpFileDownloadRequest::PathType const &localPath){
    m_localPath = localPath;
}
//! Gets the local path for this request
HttpFileDownloadRequest::PathType HttpFileDownloadRequest::getLocalPath() const{
    return m_localPath;
}

///////////////////////////////////////////////////////////////////////////////