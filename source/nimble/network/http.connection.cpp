//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/network/http.connection.h>
#include <nimble/network/http.request.file.download.h>
#include <nimble/network/http.request.file.header.h>
#include <nimble/network/http.request.file.upload.h>
#include <nimble/core/logger.h>
#include <sstream>
#include <algorithm>
#include <assert.h>
#include <sys/stat.h>

///////////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
#define CURL_STATICLIB
#include <curl/curl.h>
#endif

///////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::network;

///////////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
size_t writeToFile(void *ptr, size_t size, size_t nmemb, FILE *fp){
    size_t written;
    written = fwrite(ptr, size, nmemb, fp);
    return written;
}
size_t writeToStream(void *ptr, size_t size, size_t nmemb, std::stringstream* pStream){
    pStream->write((char*)ptr, size * nmemb);
    return size * nmemb;
}
size_t readFromFile(void *ptr, size_t size, size_t nmemb, FILE *fp){
   return fread(ptr, size, nmemb, fp);
}
int debug_callback(CURL *handle, curl_infotype type, char *data, size_t size, void *userptr){
    switch(type){
        case CURLINFO_TEXT:
            NIMBLE_LOG_INFO("network", "%s", data);
        case CURLINFO_HEADER_OUT:
            NIMBLE_LOG_INFO("network", "=> Send header");
            break;
        case CURLINFO_DATA_OUT:
            NIMBLE_LOG_INFO("network", "=> Send data");
            break;
        case CURLINFO_SSL_DATA_OUT:
            NIMBLE_LOG_INFO("network", "=> Send SSL data");
            break;
        case CURLINFO_HEADER_IN:
            NIMBLE_LOG_INFO("network", "<= Recv header");
            break;
        case CURLINFO_DATA_IN:
            NIMBLE_LOG_INFO("network", "<= Recv data");
            break;
        case CURLINFO_SSL_DATA_IN:
            NIMBLE_LOG_INFO("network", "<= Recv SSL data");
            break;
        default:
            break;
    }
    return 0;
}
#endif

///////////////////////////////////////////////////////////////////////////////

//! Constructor
HttpConnection::HttpConnection(){
#if !defined(NIMBLE_TARGET_ANDROID)
    m_pCurlHandle = curl_easy_init();
#endif
}
//! Destructor
HttpConnection::~HttpConnection(){
#if !defined(NIMBLE_TARGET_ANDROID)
    curl_easy_cleanup(m_pCurlHandle);
#endif
}

///////////////////////////////////////////////////////////////////////////////

//! Dispatches a request
HttpResponse HttpConnection::dispatch(HttpRequest const &request){
#if !defined(NIMBLE_TARGET_ANDROID)
    std::stringstream responseHeaders;
    std::stringstream responseBody;
    FILE *pFileHandle = 0;

    // setup our request location
    std::string url = request.getUrl().getLocation();
    curl_easy_setopt(m_pCurlHandle, CURLOPT_URL, url.c_str());
    curl_easy_setopt(m_pCurlHandle, CURLOPT_NOBODY, 0);
    curl_easy_setopt(m_pCurlHandle, CURLOPT_NOPROGRESS, 1);
    curl_easy_setopt(m_pCurlHandle, CURLOPT_NOSIGNAL, 0);
    curl_easy_setopt(m_pCurlHandle, CURLOPT_FILETIME, 1);
    
    // setup various options
    if(request.getOption(network::HttpRequest::kOptionDebugPrint)){
        curl_easy_setopt(m_pCurlHandle, CURLOPT_DEBUGFUNCTION, debug_callback);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_VERBOSE, 1);
    }
    if(request.getUrl().getProtocol() == "https"){
        if(request.getOption(network::HttpRequest::kOptionSSLVerifyPeer)){
            curl_easy_setopt(m_pCurlHandle, CURLOPT_SSL_VERIFYPEER, 1);
        }else{
            curl_easy_setopt(m_pCurlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        }
        if(request.getOption(network::HttpRequest::kOptionSSLVerifyHost)){
            curl_easy_setopt(m_pCurlHandle, CURLOPT_SSL_VERIFYHOST, 1);
        }else{
            curl_easy_setopt(m_pCurlHandle, CURLOPT_SSL_VERIFYHOST, 0);
        }
    }
    if(request.getTimeout() >= 0){
        curl_easy_setopt(m_pCurlHandle, CURLOPT_TIMEOUT, request.getTimeout());
    }
    if(request.getOption(network::HttpRequest::kOptionFollowRedirect)){
        curl_easy_setopt(m_pCurlHandle, CURLOPT_FOLLOWLOCATION, 1);
    }
    
    // setup read / write functions
    if(HttpFileDownloadRequest const *httpFileDownloadRequest = dynamic_cast<HttpFileDownloadRequest const*>(&request)){
        std::string localPath = httpFileDownloadRequest->getLocalPath();
#if defined(NIMBLE_TARGET_WIN32)
		fopen_s(&pFileHandle, localPath.c_str(), "wb");
#else
		pFileHandle = fopen(localPath.c_str(), "wb");
#endif
		if(pFileHandle != 0){
			curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERFUNCTION, &writeToStream);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERDATA, &responseHeaders);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEFUNCTION, &writeToFile);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEDATA, pFileHandle);
		}else{
			return HttpResponse(network::kResponseCodeFailure);
		}
    }else if(HttpFileUploadRequest const *httpFileUploadRequest = dynamic_cast<HttpFileUploadRequest const*>(&request)){
        std::string localPath = httpFileUploadRequest->getLocalPath();
#if defined(NIMBLE_TARGET_WIN32)
		fopen_s(&pFileHandle, localPath.c_str(), "rb");
#else
		pFileHandle = fopen(localPath.c_str(), "rb");
#endif
		if(pFileHandle != 0){
			struct stat st;
			stat(localPath.c_str(), &st);
			curl_off_t fileSize = (curl_off_t)st.st_size;
			curl_easy_setopt(m_pCurlHandle, CURLOPT_UPLOAD, 1);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_INFILESIZE_LARGE, fileSize);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_READFUNCTION, &readFromFile);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_READDATA, pFileHandle);        
			curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERFUNCTION, &writeToStream);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERDATA, &responseHeaders);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEFUNCTION, &writeToStream);
			curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEDATA, &responseBody);
		}else{
			return HttpResponse(network::kResponseCodeFailure);
		}
    }else if(dynamic_cast<HttpFileHeaderRequest const*>(&request)){
        curl_easy_setopt(m_pCurlHandle, CURLOPT_NOBODY, 1L);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERFUNCTION, &writeToStream);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERDATA, &responseHeaders);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEFUNCTION, &writeToStream);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEDATA, &responseBody);
    }else{
        curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERFUNCTION, &writeToStream);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_HEADERDATA, &responseHeaders);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEFUNCTION, &writeToStream);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_WRITEDATA, &responseBody);
    }
    
    // method setup
    if(request.getMethod() == HttpRequest::kPost){
        curl_easy_setopt(m_pCurlHandle, CURLOPT_POST, 1);
        curl_easy_setopt(m_pCurlHandle, CURLOPT_POSTFIELDS, request.getBody().c_str());
    }else if(request.getMethod() == HttpRequest::kPut){
        curl_easy_setopt(m_pCurlHandle, CURLOPT_PUT, 1);
    }else if(request.getMethod() == HttpRequest::kDelete){
        assert(false);
    }
    
    // fill request headers
    struct curl_slist* headers = 0;
    for(HttpMessage::HeaderConstIterator it = request.headerItemsBegin(); it != request.headerItemsEnd(); it++){
        std::string item = it->getKey() + std::string(": ") + it->getValue();
        headers = curl_slist_append(headers, item.c_str());
    }
    if(headers != 0){
        curl_easy_setopt(m_pCurlHandle, CURLOPT_HTTPHEADER, headers);
    }

    // enable automatic decompression of http downloads
    if(request.getOption(network::HttpRequest::kOptionEnableGZipEncoding)){
        curl_easy_setopt(m_pCurlHandle, CURLOPT_ACCEPT_ENCODING, "gzip");
    }

    // perform our http operation
    curl_easy_perform(m_pCurlHandle);
    
    // parse response
    HttpResponse response;
    long responseCode = 0;
    std::string headerLine;
    curl_easy_getinfo(m_pCurlHandle, CURLINFO_RESPONSE_CODE, &responseCode);    
    response.setCode((eResponseCode)responseCode);
    response.setBody(responseBody.str().c_str());
    while(getline(responseHeaders, headerLine)){
        std::string::size_type pos = headerLine.find(":");
        if(pos != std::string::npos){
            std::string key = headerLine.substr(0, pos);
            std::string value = headerLine.substr(pos + 2, (headerLine.length() - pos));
            key.erase(std::remove(key.begin(), key.end(), '\r'), key.end());
            key.erase(std::remove(key.begin(), key.end(), '\n'), key.end());
            value.erase(std::remove(value.begin(), value.end(), '\r'), value.end());
            value.erase(std::remove(value.begin(), value.end(), '\n'), value.end());
            std::transform(key.begin(), key.end(), key.begin(), ::tolower);
            response.addHeaderItem(HttpHeaderItem(key, value));
        }
    }
    
    // clean things up
    if(headers != 0){
        curl_slist_free_all(headers);
    }
    if(pFileHandle != 0){
        if(dynamic_cast<HttpFileDownloadRequest const*>(&request)){
            fclose(pFileHandle);
        }else if(dynamic_cast<HttpFileUploadRequest const*>(&request)){
            fclose(pFileHandle);
        }
    }
    
    return response;
#else
    return HttpResponse(kResponseCodeFailure);
#endif
}

///////////////////////////////////////////////////////////////////////////////