//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/input/joystick.h>
#include <algorithm>

using namespace nimble::input;

//! subscribe
void JoystickDevice::subscribe(EventHandler* pEventHandler){
	m_observersList.push_back(pEventHandler);
}
//! unsubscribe
void JoystickDevice::unsubscribe(EventHandler* pEventHandler){
	ObserverIterator it;
	it = find(m_observersList.begin(), m_observersList.end(), pEventHandler);
	if(it != m_observersList.end()){
		m_observersList.erase(it);
	}
}