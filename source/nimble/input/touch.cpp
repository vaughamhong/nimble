//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/input/touch.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::input;

//////////////////////////////////////////////////////////////////////////

//! subscribe
void TouchDevice::subscribe(EventHandler* pEventHandler){
	m_observersList.push_back(pEventHandler);
}
//! unsubscribe
void TouchDevice::unsubscribe(EventHandler* pEventHandler){
    input::TouchDevice::ObserverIterator it = find(m_observersList.begin(), m_observersList.end(), pEventHandler);
	if(it != m_observersList.end()){
		m_observersList.erase(it);
	}
}

//////////////////////////////////////////////////////////////////////////

//! Update device
void TouchDevice::update(){
}
//! injects input into the device
void TouchDevice::injectInput(input::TouchDevice::eTouchEvent event, int x, int y){
    for(input::TouchDevice::ObserverIterator it = this->observersListBegin(); it != this->observersListEnd(); it++){
        if(input::TouchDevice::EventHandler *pEventHandler = *it){
            if(event == input::TouchDevice::kTouchEventMove){
                pEventHandler->handleTouchMove(TouchEvent(x, y, event));
            }else if(event == input::TouchDevice::kTouchEventBegin){
                pEventHandler->handleTouchBegin(TouchEvent(x, y, event));
            }else if(event == input::TouchDevice::kTouchEventEnd){
                pEventHandler->handleTouchEnd(TouchEvent(x, y, event));
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////