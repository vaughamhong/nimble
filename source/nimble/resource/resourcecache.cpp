//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/resource/resourcecache.h>
#include <nimble/resource/resource.h>
#include <nimble/message/messageservice.h>
#include <nimble/core/locator.h>
#include <nimble/core/logger.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::resource;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ResourceCache::ResourceCache()
:m_nextId(0){
}
//! Destructor
ResourceCache::~ResourceCache(){
}

//////////////////////////////////////////////////////////////////////////

//! Adds a resource to our cache
void ResourceCache::add(resource::IResource *pResource){
    core::ScopedLock lock(&m_lock);
    
    // make sure we have a valid resource to add
    if(pResource == 0){
        NIMBLE_LOG_WARNING("resource", "Failed to add resource - invalid input resource param");
        return;
    }
    
    // if the resource already has an idea, it already
    // belongs to another resource cache
    if(pResource->getResourceId() != resource::kNullResourceId){
        NIMBLE_LOG_WARNING("resource", "Failed to add resource - resource is owned by another ResourceCache");
        return;
    }
    
    // set resourceId
    resource::ResourceId resourceId = m_nextId++;
    pResource->setResourceCache(this);
    pResource->setResourceId(resourceId);
    
    // add resource to index
    m_idToResourceIndex.insert(std::pair<resource::ResourceId, resource::IResource*>(resourceId, pResource));
    m_list.push_back(pResource);
    
    // tell observers
    ResourceWasAdded msg;
    msg.m_id = resourceId;
    message::MessageService *pMessageService = core::locator_acquire<message::MessageService>();
    pMessageService->send(&msg);
}
//! Removes a resource from our cache
void ResourceCache::remove(resource::ResourceId resourceId){
    core::ScopedLock lock(&m_lock);
    
    // search for resource in our index
    ResourceIdToResourceIndex::iterator it = m_idToResourceIndex.find(resourceId);
    
    // exit we did not find our resource by resourceId
    if(it == m_idToResourceIndex.end()){
        NIMBLE_LOG_WARNING("resource", "Failed to remove resource - could not find resource to remove");
        return;
    }
    
    // nullify resourceId
    it->second->setResourceCache(0);
    it->second->setResourceId(resource::kNullResourceId);
    
    // remove resource from index
    m_list.erase(std::find(m_list.begin(), m_list.end(), it->second));
    m_idToResourceIndex.erase(it);
    
    // tell observers
    ResourceWasRemoved msg;
    msg.m_id = resourceId;
    message::MessageService *pMessageService = core::locator_acquire<message::MessageService>();
    pMessageService->send(&msg);
}
//! Gets a resource from our cache
resource::IResource* ResourceCache::find(resource::ResourceId resourceId) const{
    core::ScopedLock lock(&m_lock);
    ResourceIdToResourceIndex::const_iterator it = m_idToResourceIndex.find(resourceId);
    if(it != m_idToResourceIndex.end()){
        return it->second;
    }else{
        return 0;
    }
}

//! Clears all resources
void ResourceCache::clear(){
    core::ScopedLock lock(&m_lock);
    for(ResourceIdToResourceIndex::iterator it = m_idToResourceIndex.begin(); it != m_idToResourceIndex.end(); it++){
        it->second->setResourceId(resource::kNullResourceId);
        
        // tell observers
        ResourceWasRemoved msg;
        msg.m_id = it->first;
        message::MessageService *pMessageService = core::locator_acquire<message::MessageService>();
        pMessageService->send(&msg);
    }
    m_idToResourceIndex.clear();
    m_list.clear();
}

//////////////////////////////////////////////////////////////////////////

//! The begin resource iterator
ResourceList::iterator ResourceCache::begin(){
    core::ScopedLock lock(&m_lock);
    return m_list.begin();
}
//! The begin resource iterator
ResourceList::const_iterator ResourceCache::begin() const{
    core::ScopedLock lock(&m_lock);
    return m_list.begin();
}
//! The end resource iterator
ResourceList::iterator ResourceCache::end(){
    core::ScopedLock lock(&m_lock);
    return m_list.end();
}
//! The end resource iterator
ResourceList::const_iterator ResourceCache::end() const{
    core::ScopedLock lock(&m_lock);
    return m_list.end();
}

//////////////////////////////////////////////////////////////////////////