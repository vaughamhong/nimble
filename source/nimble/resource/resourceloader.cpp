//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/resource/resourceloader.h>
#include <nimble/core/searchpaths.h>
#include <nimble/core/logger.h>
#include <nimble/core/path.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::resource;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ResourceLoader::ResourceLoader(core::SearchPaths *pSearchPaths)
:m_pSearchPaths(pSearchPaths){
}
//! Destructor
ResourceLoader::~ResourceLoader(){
}

//////////////////////////////////////////////////////////////////////////

//! Adds a resource loader for an extension
void ResourceLoader::addResourceLoaderForExtension(resource::IResourceLoader *pLoader, const char *extension){
    if(canLoadExtension(extension) == true){
        NIMBLE_LOG_ERROR("resource", "Failed to add resource loader for extension %s - a loader with the same extension already exists", extension);
        return;
    }
    m_extensionToLoaderIndex.insert(std::make_pair(std::string(extension), pLoader));
}
//! Removes a resource loader for an extension
void ResourceLoader::removeResourceLoaderForExtension(const char *extension){
    ExtensionToLoaderIndex::iterator it = m_extensionToLoaderIndex.find(extension);
    if(it == m_extensionToLoaderIndex.end()){
        NIMBLE_LOG_WARNING("resource", "Failed to remove resource loader for extension %s - loader does not exist", extension);
        return;
    }
    m_extensionToLoaderIndex.erase(it);
}
//! Gets a resource loader for an extension
resource::IResourceLoader* ResourceLoader::getResourceLoaderForExtension(const char *extension) const{
    ExtensionToLoaderIndex::const_iterator it = m_extensionToLoaderIndex.find(extension);
    if(it == m_extensionToLoaderIndex.end()){
        return 0;
    }
    return it->second;
}

//////////////////////////////////////////////////////////////////////////

//! Checks if we can load this file given the extension
//! \param extension the extension of the file we want to check
bool ResourceLoader::canLoadExtension(const char *extension) const{
    ExtensionToLoaderIndex::const_iterator it = m_extensionToLoaderIndex.find(extension);
    return (it != m_extensionToLoaderIndex.end());
}
//! Checks if we can load this file given a path
//! \param path the path of the file we want to check
bool ResourceLoader::canLoadPath(const char *path) const{
    std::string extension = core::Path(path).getExtension();
    return canLoadExtension(extension.c_str());
}
//! Checks if a file exists on a path
//! \param path the path we want to check
//! \return true if a file at the path exists
bool ResourceLoader::existsPath(const char *path) const{
    std::string fullPath = m_pSearchPaths->findFullPath(path);
    return (fullPath != "");
}

//////////////////////////////////////////////////////////////////////////

//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* ResourceLoader::loadResource(const char* path){
    // try and find the full path
    std::string fullPath = m_pSearchPaths->findFullPath(path);
    if(fullPath == ""){
        NIMBLE_LOG_ERROR("resource", "Failed to load resource at path %s - full path not found", path);
        return 0;
    }

    // try and find a suitable loader
    std::string extension = core::Path(path).getExtension();
    ExtensionToLoaderIndex::iterator it = m_extensionToLoaderIndex.find(extension);
    if(it == m_extensionToLoaderIndex.end()){
        NIMBLE_LOG_ERROR("resource", "Failed to load resource at path %s - no suitable loader found", path);
        return 0;
    }
    
    // do load
    return it->second->loadResource(fullPath.c_str());
}

//////////////////////////////////////////////////////////////////////////