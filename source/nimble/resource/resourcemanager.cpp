//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/resource/resourcemanager.h>
#include <nimble/resource/resourcecache.h>
#include <nimble/resource/resourceloader.h>
#include <nimble/core/thread.pool.h>
#include <nimble/core/functor.h>
#include <nimble/core/path.h>
#include <nimble/core/assert.h>
#include <nimble/core/locator.h>
#include <nimble/core/scopedprofiler.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::resource;

//////////////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
//! \param pResourceLoader the resource loader this manager will use to load with
ResourceManager::ResourceManager(resource::ResourceLoader *pResourceLoader)
:m_pResourceLoader(pResourceLoader){
}
//! Destructor
ResourceManager::~ResourceManager(){
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

//! Checks if we can load this file given a path
//! \param path the path we want to check
//! \return true if we can load from path
bool ResourceManager::canLoadPath(const char *path) const{
    return m_pResourceLoader->canLoadPath(path);
}
//! Checks if a file exists on a path
//! \param path the path we want to check
//! \return true if a file at the path exists
bool ResourceManager::existsPath(const char *path) const{
    return m_pResourceLoader->existsPath(path);
}
//! Loads a resource given a file path
//! \param path the path we want to load from
//! \return a resource given a file path
resource::IResource* ResourceManager::load(const char *path){
    SCOPEDPROFILE("ResourceManager::load")
    
    core::ScopedLock lock(&m_lock);
    
    // if a resource at the path already exists, increment its reference
    // counter and return the resource
    ResourceManager::resourceInfo_t *pResourceInfo = findResourceInfoByPath(path);
    if(pResourceInfo != 0){
        pResourceInfo->referenceCount += 1;
        return pResourceInfo->pResource;
    }
    
    // try and load our resource
    resource::IResource *pResource = m_pResourceLoader->loadResource(path);
    if(pResource == 0){
        NIMBLE_LOG_ERROR("resource", "Failed to load resource with path %s", path);
        return 0;
    }
    
    // calculate our path hash
    int64_t pathHash = core::hash_djb2(path);

    // create our resource index
    ResourceManager::resourceInfo_t info;
    info.path = path;
    info.pResource = pResource;
    info.referenceCount = 1;
    
    // add resource to indices
    m_pathHashToResourceInfoIndex.insert(std::make_pair(pathHash, info));
    m_resourceToPathHashIndex.insert(std::make_pair(pResource, pathHash));
    
    // add resource to cache
    m_cache.add(pResource);
    
    // return resource
    NIMBLE_LOG_INFO("resource", "ResourceManager(size:%d) loaded %s", m_pathHashToResourceInfoIndex.size(), path);
    return pResource;
}
//! Async load resource given a file path
//! \param path the path we want to load from
//! \pThreadPool the thread pool to dispatch async call
//! \return a resource future
resource::ResourceFuture ResourceManager::asyncLoadResource(const char *path, core::ThreadPool *pThreadPool){
    // find global thread pool if it was not provided
    if(pThreadPool == 0){
        pThreadPool = core::locator_acquire<core::ThreadPool>();
    }
    
    // create our load callback which is just a call to our sync load (but from a separate thread)
    core::Functor<resource::IResource*, TLIST_1(const char*)> loadFunc(this, &nimble::resource::ResourceManager::load);
    
    // dispatch async load
    return pThreadPool->executeAsyncTask<resource::IResource*>(core::bindAll<
                                                               resource::IResource*,
                                                               const char*>(loadFunc, path));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

//! Releases a resource
//! \param pResource the resource we want to release
void ResourceManager::releaseResource(resource::IResource *pResource){
    SCOPEDPROFILE("ResourceManager::releaseResource")

    // find our resource info by resource
    ResourceManager::resourceInfo_t *pResourceInfo = findResourceInfoByResource(pResource);
    if(pResourceInfo == 0){
        NIMBLE_LOG_WARNING("resource", "Failed to release resource - could not find requested resource");
        return;
    }
    
    // make sure reference count is stable (worst case there is one reference left)
    // references should not be negative after this call or else something has gone wrong.
    NIMBLE_ASSERT(pResourceInfo->referenceCount >= 1);
    
    {
        core::ScopedLock lock(&m_lock);
        
        // destroy resource if we have reached 0 reference count
        pResourceInfo->referenceCount -= 1;
        if(pResourceInfo->referenceCount == 0){
            resource::IResource *pResource = pResourceInfo->pResource;

            // add resource to cleanup list
            {
                core::ScopedLock lock(&m_cleanupLock);
                m_cleanupList.push_back(pResource);
            }
            
            // remove from resource cache
            m_cache.remove(pResource);
            
            // remove resource from indices
            NIMBLE_LOG_INFO("resource", "ResourceManager(size:%d) unloaded %s", m_pathHashToResourceInfoIndex.size() - 1, pResourceInfo->path.c_str());
            ResourceToPathHashIndex::iterator it1 = m_resourceToPathHashIndex.find(pResource);
            NIMBLE_ASSERT(it1 != m_resourceToPathHashIndex.end());
            PathHashToResourceInfoIndex::iterator it2 = m_pathHashToResourceInfoIndex.find(it1->second);
            NIMBLE_ASSERT(it2 != m_pathHashToResourceInfoIndex.end());
            m_resourceToPathHashIndex.erase(it1);
            m_pathHashToResourceInfoIndex.erase(it2);
        }
    }
}
//! Releases a resource
//! \param path the path of the resource we want to release
void ResourceManager::releaseResourceByPath(const char *path){
    // find and release resource
    ResourceManager::resourceInfo_t *pResourceInfo = findResourceInfoByPath(path);
    if(pResourceInfo == 0){
        NIMBLE_LOG_WARNING("resource", "Failed to release resource %s - could not find requested resource", path);
        return;
    }
    
    // release resource
    releaseResource(pResourceInfo->pResource);
}
//! Checks if there exists a pre-loaded resource at a path
//! \param path the path of the resource we want to check
bool ResourceManager::existsResourceWithPath(const char *path){
	core::ScopedLock lock(&m_lock);

    ResourceManager::resourceInfo_t *pResourceInfo = findResourceInfoByPath(path);
    return (pResourceInfo != 0);
}
//! Cleanup resources
void ResourceManager::cleanupResources(){
    if(!m_cleanupList.empty()){
        core::ScopedLock lock(&m_cleanupLock);
        
        // destroy all resources
        for(CleanupList::iterator it = m_cleanupList.begin(); it != m_cleanupList.end(); it++){
            delete *it;
        }
        m_cleanupList.clear();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

//! Returns the resource info by resource
ResourceManager::resourceInfo_t* ResourceManager::findResourceInfoByResource(resource::IResource *pResource){
	core::ScopedLock lock(&m_lock);

    // find our path hash given a resource
    ResourceToPathHashIndex::iterator it1 = m_resourceToPathHashIndex.find(pResource);
    if(it1 == m_resourceToPathHashIndex.end()){
        return 0;
    }
    // find resourceInfo given a path hash
    PathHashToResourceInfoIndex::iterator it2 = m_pathHashToResourceInfoIndex.find(it1->second);
    if(it2 == m_pathHashToResourceInfoIndex.end()){
        return 0;
    }
    // return resourceInfo
    return &it2->second;
}
//! Returns the resource info by path
ResourceManager::resourceInfo_t* ResourceManager::findResourceInfoByPath(const char *path){
	core::ScopedLock lock(&m_lock);

    // calculate our path hash
    int64_t pathHash = core::hash_djb2(path);
    
    // find resourceInfo given a path hash
    PathHashToResourceInfoIndex::iterator it = m_pathHashToResourceInfoIndex.find(pathHash);
    if(it == m_pathHashToResourceInfoIndex.end()){
        return 0;
    }
    return &it->second;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////