//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/resource/resource.h>

////////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::resource;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Resource::Resource()
:m_pCache(0)
,m_id(resource::kNullResourceId){
}
//! Destructor
Resource::~Resource(){
}
//! Returns our identifier
//! \return the the resource cache this belongs to
resource::ResourceCache* Resource::getResourceCache() const{
    return m_pCache;
}
//! Returns our identifier
//! \return the resource Id
resource::ResourceId Resource::getResourceId() const{
    return m_id;
}
//! Sets our resource cache
//! \param pCache the cache this resource belongs to
void Resource::setResourceCache(resource::ResourceCache *pCache){
    m_pCache = pCache;
}
//! Sets our resource identifier
//! \param id the identifier to set
void Resource::setResourceId(resource::ResourceId id){
    m_id = id;
}

//////////////////////////////////////////////////////////////////////////