//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/scene/visitor.flatten.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::scene;

//////////////////////////////////////////////////////////////////////////

//! Constructor
FlattenVisitor::FlattenVisitor(scene::SceneTransform::ListType &list, eType type)
:m_type(type)
,m_list(list){
}
//! Destructor
FlattenVisitor::~FlattenVisitor(){
}

//////////////////////////////////////////////////////////////////////////

//! Executes on an object
void FlattenVisitor::execute(scene::SceneTransform *pSceneTransform){
    if(m_type == kBreadthFirst){
        m_list.push_back(pSceneTransform);
    }
    
    // update each object's children
    scene::SceneTransform::ListType::iterator it;
    scene::SceneTransform::ListType children;
    pSceneTransform->getChildren(children);
    for(scene::SceneTransform::ListType::iterator it = children.begin(); it != children.end(); it++){
        this->execute(*it);
    }
    
    if(m_type == kDepthFirst){
        m_list.push_back(pSceneTransform);
    }
}

//////////////////////////////////////////////////////////////////////////