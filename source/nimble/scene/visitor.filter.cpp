//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/scene/visitor.filter.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::scene;

//////////////////////////////////////////////////////////////////////////

//! Constructor
FilterVisitor::FilterVisitor(scene::SceneTransform::ListType &filteredList, FilterVisitor::Func filter)
:m_filteredList(filteredList)
,m_filter(filter){
}
//! Destructor
FilterVisitor::~FilterVisitor(){
}

//////////////////////////////////////////////////////////////////////////

//! Executes on an object
void FilterVisitor::execute(scene::SceneTransform *pSceneTransform){
    // call our filter
    if(true == m_filter(pSceneTransform)){
        m_filteredList.push_back(pSceneTransform);
    }
    // update each object's children
    scene::SceneTransform::ListType::iterator it;
    scene::SceneTransform::ListType children;
    pSceneTransform->getChildren(children);
    for(scene::SceneTransform::ListType::iterator it = children.begin(); it != children.end(); it++){
        this->execute(*it);
    }
}

//////////////////////////////////////////////////////////////////////////