//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/scene/scenetransform.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::scene;

//////////////////////////////////////////////////////////////////////////

//! Constructor
SceneTransform::SceneTransform()
:m_localBounds(math::AxisAlignedBox3f::identity())
,m_worldBounds(math::AxisAlignedBox3f::identity())
,m_localTransform(math::Matrix4x4f::identity())
,m_invLocalTransform(math::Matrix4x4f::identity())
,m_worldTransform(math::Matrix4x4f::identity())
,m_translation(math::Vector3f::zero())
,m_rotation(math::Quaternion4f::zero())
,m_scale(math::Vector3f(1.0f, 1.0f, 1.0f)){
}
//! Destructor
SceneTransform::~SceneTransform(){
}

//////////////////////////////////////////////////////////////////////////

//! sets local translation
void SceneTransform::setTranslation(math::Vector3f const &translation){
    m_translation = translation;
    updateLocalTransform();
}
//! gets local translation
math::Vector3f const& SceneTransform::getTranslation() const{
    return m_translation;
}
//! sets local rotation
void SceneTransform::setRotation(math::Quaternion4f const &rotation){
    m_rotation = rotation;
    updateLocalTransform();
}
//! gets local rotation
math::Quaternion4f const& SceneTransform::getRotation() const{
    return m_rotation;
}
//! sets local scale
void SceneTransform::setScale(math::Vector3f const &scale){
    m_scale = scale;
    updateLocalTransform();
}
//! gets local scale
math::Vector3f const& SceneTransform::getScale(){
    return m_scale;
}

//////////////////////////////////////////////////////////////////////////

//! returns our transform
const math::Matrix4x4f& SceneTransform::getLocalTransform() const{
    return m_localTransform;
}
//! returns our transform
const math::Matrix4x4f& SceneTransform::getWorldTransform() const{
    return m_worldTransform;
}

//////////////////////////////////////////////////////////////////////////

//! returns our inverse transform
const math::Matrix4x4f SceneTransform::getInvLocalTransform() const{
    return m_invLocalTransform;
}
//! returns our inverse transform
const math::Matrix4x4f SceneTransform::getInvWorldTransform() const{
    if(this->hasParent()){
        const SceneTransform *pSceneTransform = dynamic_cast<const SceneTransform*>(this->getParent());
        return getInvLocalTransform() * pSceneTransform->getInvWorldTransform();
    }else{
        return getInvLocalTransform();
    }
}

//////////////////////////////////////////////////////////////////////////

//! returns the node local bounds
math::AxisAlignedBox3f SceneTransform::getLocalBounds() const{
    return m_localBounds;
}
//! returns the node world bounds
math::AxisAlignedBox3f SceneTransform::getWorldBounds() const{
    return m_worldBounds;
}
//! sets the local bounds
void SceneTransform::setLocalBounds(math::AxisAlignedBox3f const& bounds){
    m_localBounds = bounds;
    updateWorldBounds();
}

//////////////////////////////////////////////////////////////////////////

//! Update local transform
void SceneTransform::updateLocalTransform(){
    math::Matrix4x4f translationMatrix = math::Matrix4x4f::translationMatrix(m_translation);
    math::Matrix4x4f rotationMatrix = math::Matrix4x4f::rotationMatrix(m_rotation);
    math::Matrix4x4f scaleMatrix = math::Matrix4x4f::scaleMatrix(m_scale);
    m_localTransform = translationMatrix * rotationMatrix * scaleMatrix;
    m_invLocalTransform = m_localTransform.inverse();
}
//! Update world transform
void SceneTransform::updateWorldTransform(){
    if(hasParent()){
        const SceneTransform *pSceneTransform = dynamic_cast<const SceneTransform*>(this->getParent());
        m_worldTransform = pSceneTransform->getWorldTransform() * m_localTransform;
    }else{
        m_worldTransform = m_localTransform;
    }
}

//////////////////////////////////////////////////////////////////////////

//! Update world bounds
void SceneTransform::updateWorldBounds(){
    if(hasParent()){
        // TODO: calculate world bounds with scale
        m_worldBounds = m_localBounds;
    }else{
        m_worldBounds = m_localBounds;
    }
}

//////////////////////////////////////////////////////////////////////////
