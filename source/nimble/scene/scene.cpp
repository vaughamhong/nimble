//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/scene/scene.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::scene;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Scene::Scene(){
}
//! Destructor
Scene::~Scene(){
}

//////////////////////////////////////////////////////////////////////////

//! Returns the root transform
scene::SceneTransform* Scene::getRootSceneTransform(){
    return dynamic_cast<scene::SceneTransform*>(this);
}
//! Returns the root transform
scene::SceneTransform const* Scene::getRootSceneTransform() const{
    return dynamic_cast<scene::SceneTransform const*>(this);
}

//////////////////////////////////////////////////////////////////////////
