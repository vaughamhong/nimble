//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/gui/text.h>
#include <nimble/font/font.h>
#include <nimble/renderdevice/itexture.h>

////////////////////////////////////////////////////////////////////////////
//
//using namespace nimble;
//using namespace nimble::text;
//
////////////////////////////////////////////////////////////////////////////
//
////! Constructor
//Text::Text(text::Font *pFont, const char *text)
//:m_pFont(pFont)
//,m_text(text){
////,m_mesh(){
//}
////! Destructor
//Text::~Text(){
//}
//
////////////////////////////////////////////////////////////////////////////
//
////! Set our text
//void Text::setText(const char *text){
//    m_text = text;
//    regenerateMesh();
//}
////! Get our text
//const char* Text::getText() const{
//    return m_text.c_str();
//}
////! Get our font
//text::Font* Text::getFont() const{
//    return m_pFont;
//}
//
////////////////////////////////////////////////////////////////////////////
//
////! Regenerate mesh
//void Text::regenerateMesh(){
//    if(m_pFont == 0){
//        NIMBLE_LOG_ERROR("text", "Failed to generate mesh - font not found");
//        return;
//    }
//    
////    // reset our mesh
////    m_mesh.clear();
////    
////    // find information about our texture size for calculating unit values
////    renderdevice::ITexture *pTexture = m_pFont->getTexture();
////    size_t textureWidth = pTexture->getWidth();
////    size_t textureHeight = pTexture->getHeight();
////    
////    // loop through each text character and generate our mesh data
////    for(size_t i = 0; i < m_text.size(); i++){
////        char character = m_text[i];
////        
////        // find our glyph info to generate unit values
////        text::glyphInfo_t info = m_pFont->getGlyphInfo(character);
////        float unitX = (float) info.x / textureWidth;
////        float unitY = (float) info.y / textureHeight;
////        float unitWidth = (float) info.width / textureWidth;
////        float unitHeight = (float) info.height / textureHeight;
////        
////        // generate our geometry
////        text::vertex_t vertex;
////        vertex.position = math::Vector3f(0.0f, 0.0f, 0.0f);
////        vertex.color = math::Vector4c(255, 255, 255, 255);
////        vertex.texCoord = math::Vector2f(0.0f, 0.0f);
////    }
//}

//////////////////////////////////////////////////////////////////////////