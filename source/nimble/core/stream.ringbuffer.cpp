//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/stream.ringbuffer.h>
#include <assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
RingBufferStream::RingBufferStream(size_t size)
:m_buffer(size){
}
//! Destructor
RingBufferStream::~RingBufferStream(){
}

//////////////////////////////////////////////////////////////////////////

//! returns the size of our Stream
size_t RingBufferStream::size() const{
    return m_buffer.getSize();
}
//! returns true if this buffer is empty
bool RingBufferStream::isEmpty() const{
    return m_buffer.isEmpty();
}

//////////////////////////////////////////////////////////////////////////

//! Writes to this stream
void RingBufferStream::write(char const *pBuffer, size_t size){
    assert(pBuffer != 0);
    if(size == 0){return;}
    m_buffer.push(pBuffer, size);
}
//! Reads from this stream
void RingBufferStream::read(char *pBuffer, size_t size){
    assert(pBuffer != 0);
    if(size == 0){return;}
    m_buffer.pop(&pBuffer, size);
}

//////////////////////////////////////////////////////////////////////////