//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/logger.h>
#include <nimble/core/locator.h>
#include <nimble/core/stream.h>
#include <nimble/core/stream.file.h>
#include <nimble/core/defines.h>
#include <nimble/core/stringutil.h>
#include <nimble/core/thread.mutex.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <string>
#include <map>

//////////////////////////////////////////////////////////////////////////

int formattedStringLengthWithArgs(const char *format, va_list args){
#if defined(NIMBLE_TARGET_WIN32)
	return _vscprintf(format, args);
#else
	return vsnprintf(0, 0, format, args);
#endif
}
int formattedStringLength(const char *format, ...){
	va_list args;
	va_start(args, format);
	int length = formattedStringLengthWithArgs(format, args);
	va_end(args);
	return length;
}
#define SNPRINTSIZE(format, ...) formattedStringLength(format, __VA_ARGS__)
#define VSPRINTSIZE(format, args) formattedStringLengthWithArgs(format, args)

#if defined(NIMBLE_TARGET_WIN32)
	#include <Windows.h>
    #define SNPRINTF(buffer, size, format, ...) _snprintf_s(buffer, size, size, format, __VA_ARGS__)
    #define VSNPRINTF(buffer, size, format, args) vsnprintf_s(buffer, size, size, format, args)
    #define CONSOLEOUTPUT(string) OutputDebugString(string)
#elif defined(NIMBLE_TARGET_OSX)
    #define SNPRINTF(buffer, size, format, ...) snprintf(buffer, size, format, __VA_ARGS__)
    #define VSNPRINTF(buffer, size, format, args) vsnprintf(buffer, size, format, args)
    #define CONSOLEOUTPUT(string) fprintf(stderr, "%s", string)
#elif defined(NIMBLE_TARGET_ANDROID)
    #include <android/log.h>
    #define SNPRINTF(buffer, size, format, ...) snprintf(buffer, size, format, __VA_ARGS__)
    #define VSNPRINTF(buffer, size, format, args) vsnprintf(buffer, size, format, args)
    #define CONSOLEOUTPUT(string) __android_log_print(ANDROID_LOG_DEBUG, "NIMBLE", "%s", string);
#endif

//! logs a message
//! \param format the message format
//! \param ... variable argument list
void print(const char *category, const char *format, va_list args){
    const int kMaxBufferSize = 1024;
    static char pBodyBuffer[kMaxBufferSize];
    static char pOutputBuffer[kMaxBufferSize];
    
    // calculate our full string size with the format
    // <header><body><footer>
    va_list argsCopy;
    va_copy(argsCopy, args);
    int headerStringSize = 0, bodyStringSize = 0, footerStringSize = 0, outputStringSize = 0;
    if(strlen(category) != 0){
        headerStringSize = SNPRINTSIZE("[%s]", category);
    }
    bodyStringSize = VSPRINTSIZE(format, argsCopy);
    footerStringSize = SNPRINTSIZE("%s", "\n");
    outputStringSize = headerStringSize + bodyStringSize + footerStringSize;
    
    // early exit if we've exceeded our string length
    if(bodyStringSize > kMaxBufferSize || outputStringSize > kMaxBufferSize){
        return;
    }
    
    // generate body string
    va_copy(argsCopy, args);
    VSNPRINTF(pBodyBuffer, kMaxBufferSize, format, argsCopy);
    
    // generate final output string
    if(headerStringSize > 0){
        SNPRINTF(pOutputBuffer, kMaxBufferSize, "[%s]%s\n", category, pBodyBuffer);
    }else{
        SNPRINTF(pOutputBuffer, kMaxBufferSize, "%s\n", pBodyBuffer);
    }
    
    // null termination
    pBodyBuffer[bodyStringSize] = 0;
    pOutputBuffer[outputStringSize] = 0;
    
    // print to stream or output
    if(nimble::core::Stream *pStream = nimble::core::logger_getCategoryStream(category)){
        // include null termination on string output
        pStream->write(pOutputBuffer, outputStringSize + 1);
    }else{
        CONSOLEOUTPUT(pOutputBuffer);
    }
}

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

struct categoryInfo_t{
    core::Stream *pStream;
    bool isEnabled;
};
typedef std::map<std::string, categoryInfo_t> CategoryToInfoIndex;
static CategoryToInfoIndex g_categoryToInfoIndex;
static core::Mutex g_printLock;

//////////////////////////////////////////////////////////////////////////

//! creates a log category
void nimble::core::logger_createCategory(const char *category, bool enabled, core::Stream *pStream){
    core::ScopedLock lock(&g_printLock);
    CategoryToInfoIndex::iterator it = g_categoryToInfoIndex.find(category);
    if(it == g_categoryToInfoIndex.end()){
        categoryInfo_t info;
        info.isEnabled = enabled;
        info.pStream = pStream;
        g_categoryToInfoIndex.insert(std::pair<const char*, categoryInfo_t>(category, info));
    }
}
//! destroys a log category
void nimble::core::logger_destroyCategory(const char *category){
    core::ScopedLock lock(&g_printLock);
    CategoryToInfoIndex::iterator it = g_categoryToInfoIndex.find(category);
    if(it != g_categoryToInfoIndex.end()){
        g_categoryToInfoIndex.erase(it);
    }
}

//////////////////////////////////////////////////////////////////////////

//! enables a log category
void nimble::core::logger_enableCategory(const char *category){
    core::ScopedLock lock(&g_printLock);
    CategoryToInfoIndex::iterator it = g_categoryToInfoIndex.find(category);
    if(it != g_categoryToInfoIndex.end()){
        it->second.isEnabled = true;
    }
}
//! returns true if category is enabled
bool nimble::core::logger_isCategoryEnabled(const char *category){
    core::ScopedLock lock(&g_printLock);
    CategoryToInfoIndex::iterator it = g_categoryToInfoIndex.find(category);
    if(it != g_categoryToInfoIndex.end()){
        return it->second.isEnabled;
    }
    return true;
}
//! returns true if category exists
bool logger_categoryExists(const char *category){
    core::ScopedLock lock(&g_printLock);
    CategoryToInfoIndex::iterator it = g_categoryToInfoIndex.find(category);
    return (it != g_categoryToInfoIndex.end());
}
//! disables a log category
void nimble::core::logger_disableCategory(const char *category){
    core::ScopedLock lock(&g_printLock);
    CategoryToInfoIndex::iterator it = g_categoryToInfoIndex.find(category);
    if(it != g_categoryToInfoIndex.end()){
        it->second.isEnabled = false;
    }
}
//! sets a category stream
void nimble::core::logger_setCategoryStream(const char *category, core::Stream *pStream){
    core::ScopedLock lock(&g_printLock);
    CategoryToInfoIndex::iterator it = g_categoryToInfoIndex.find(category);
    if(it != g_categoryToInfoIndex.end()){
        it->second.pStream = pStream;
    }
}
//! returns the category stream
core::Stream* nimble::core::logger_getCategoryStream(const char *category){
    core::ScopedLock lock(&g_printLock);
    CategoryToInfoIndex::iterator it = g_categoryToInfoIndex.find(category);
    if(it != g_categoryToInfoIndex.end()){
        return it->second.pStream;
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////

//! prints to a category
void nimble::core::log_info(const char *category, int line, const char *file, const char *format, ...){
    if(core::logger_isCategoryEnabled(category)){
        core::ScopedLock lock(&g_printLock);
        va_list args;
        va_start(args, format);
        log_info(category, line, file, format, args);
        va_end(args);
    }
}
//! prints warning to a category
void nimble::core::log_warning(const char *category, int line, const char *file, const char *format, ...){
    if(core::logger_isCategoryEnabled(category)){
        core::ScopedLock lock(&g_printLock);
        va_list args;
        va_start(args, format);
        log_warning(category, line, file, format, args);
        va_end(args);
    }
}
//! prints error to a category
void nimble::core::log_error(const char *category, int line, const char *file, const char *format, ...){
    if(core::logger_isCategoryEnabled(category)){
        core::ScopedLock lock(&g_printLock);
        va_list args;
        va_start(args, format);
        log_error(category, line, file, format, args);
        va_end(args);
    }
}

//////////////////////////////////////////////////////////////////////////

//! prints to a category
void nimble::core::log_info(const char *category, int line, const char *file, const char *format, va_list args){
    if(core::logger_isCategoryEnabled(category)){
        core::ScopedLock lock(&g_printLock);
        print(category, format, args);
    }
}
//! prints warning to a category
void nimble::core::log_warning(const char *category, int line, const char *file, const char *format, va_list args){
    if(core::logger_isCategoryEnabled(category)){
        core::ScopedLock lock(&g_printLock);
        std::string warnFormat = std::string("[Warning]") +
            std::string(file) + std::string(":") + core::lexical_cast(line) + std::string(" ") +
            std::string(format);
        print(category, warnFormat.c_str(), args);
    }
}
//! prints error to a category
void nimble::core::log_error(const char *category, int line, const char *file, const char *format, va_list args){
    if(core::logger_isCategoryEnabled(category)){
        core::ScopedLock lock(&g_printLock);
        std::string warnFormat = std::string("[Error]") +
            std::string(file) + std::string(":") + core::lexical_cast(line) + std::string(" ") +
            std::string(format);
        print(category, warnFormat.c_str(), args);
    }
}

//////////////////////////////////////////////////////////////////////////