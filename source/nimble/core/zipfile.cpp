//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/zipfile.h>
#include <nimble/core/filesystem.h>
#include <errno.h>
#if !defined(WIN32)

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

////! Constructor
//ZipFile::ZipFile()
//:m_zipFile(0){
//}
////! Constructor
//ZipFile::ZipFile(const char *path)
//:m_zipFile(0){
//    loadFromFile(path);
//}
////! Destructor
//ZipFile::~ZipFile(){
//    if(m_zipFile != 0){
//        unzClose(m_zipFile);
//        m_zipFile = 0;
//    }
//}
//
////////////////////////////////////////////////////////////////////////////
//
////! Load from file
//void ZipFile::loadFromFile(const char *path){
//    
//    // TODO - finish!
//    
//    // note:
//    // + use minizip (contrib/minizip/zip.h) to manage virtual file system
//    // + be able to identify if a file lives within the zip
//    // + be able to inflate an existing file from within the zip
//    // + (optional) be able to deflate data into a zip
//    
//    // + consider partial deflate using streams
//    //  + loading a subfile will return a stream
//    //  + stream is a zipfilestream which will piecemeal deflate
//    
//    // http://bobobobo.wordpress.com/2008/02/23/how-to-use-zlib/
//    // http://stackoverflow.com/questions/10440113/simple-way-to-unzip-a-zip-file-using-zlib
//
//    // open our zip file
//    m_zipFile = unzOpen(path);
//    if(m_zipFile == 0){
//        NIMBLE_LOG_ERROR("zipfile", "Failed to open zip file: %s", path);
//        return;
//    }
//    
//    // Get info about the zip file
//    if(unzGetGlobalInfo(m_zipFile, &m_globalInfo) != UNZ_OK){
//        NIMBLE_LOG_ERROR("zipfile", "Failed to retreive zip file global info - %s", path);
//        unzClose(m_zipFile);
//        m_zipFile = 0;
//        return;
//    }
//    
////    // Loop to extract all files
////    for(size_t i = 0; i < m_globalInfo.number_entry; ++i){
////        // Get info about current file.
////        unz_file_info file_info;
////        char filename[kMaxFilenameSize];
////        if(unzGetCurrentFileInfo(m_zipFile,
////                                 &file_info,
////                                 filename,
////                                 kMaxFilenameSize,
////                                 NULL, 0,
////                                 NULL, 0 ) != UNZ_OK){
////            NIMBLE_LOG_ERROR("zipfile", "Failed to read file info for zipfile %s", path);
////            unzClose(m_zipFile);
////            return;
////        }
////        
////        // Check if this entry is a directory or file.
////        const size_t filename_length = strlen(filename);
////        if(filename[filename_length - 1] == kDirDelimeter){
////            NIMBLE_LOG_INFO("zipfile", "directory found with name %s", filename);
////            // TODO: make directory - see http://stackoverflow.com/questions/10440113/simple-way-to-unzip-a-zip-file-using-zlib
////        }else{
////            NIMBLE_LOG_INFO("zipfile", "file found with name %s", filename);
////            // TODO: load file - see http://stackoverflow.com/questions/10440113/simple-way-to-unzip-a-zip-file-using-zlib
////        }
////        
////        unzCloseCurrentFile(m_zipFile);
////        
////        // Go the the next entry listed in the zip file.
////        if((i + 1) < m_globalInfo.number_entry){
////            if(unzGoToNextFile(m_zipFile) != UNZ_OK){
////                NIMBLE_LOG_ERROR("zipfile", "Failed to read next file info for zipfile %s", path);
////                unzClose(m_zipFile);
////                return;
////            }
////        }
////    }
//    
////    // close our zip file
////    unzClose(m_zipFile);
//    
////    // open our zip file
////    FILE * readFile = fopen(path, "rb");
////    if(readFile == 0){
////        NIMBLE_LOG_ERROR("zipfile", "Failed to load zipfile - could not load file: \"%s\"", path);
////        return;
////    }
////    
////    // get size of file
////    fseek(readFile, 0, SEEK_END);
////    size_t fileLength = ftell(readFile);
////    rewind(readFile);
////    
////    // allocate enough space to store our zipfile
////    m_buffer.resize(fileLength);
////    char *pData = m_buffer.getPointer();
////    
////    // read our zipfile into our buffer
////    fread(pData, fileLength, 1, readFile);
////    
////    // close our file
////    fclose(readFile);
////    readFile = NULL;
//    
////    // open our file stream
////    std::fstream fileStream(path, std::fstream::in | std::fstream::out | std::ios::binary);
////    if(fileStream.fail()){
////        NIMBLE_LOG_ERROR("system", "Failed to open file %s - %s", path, strerror(errno));
////        return;
////    }
////
////    // find our file size
////    long fileSize = core::fileSize(path);
////    
////    // allocate one larger for null termination
////    m_buffer.resize(fileSize + 1);
////    
////    // copy file into memory
////    char *pBuffer = m_buffer.getPointer();
////    fileStream.seekg(0, std::ios::beg);
////    fileStream.read(pBuffer, fileSize);
////    pBuffer[fileSize] = 0;
////    fileStream.close();
//}
////! Save to file
//void ZipFile::saveToFile(const char *path){
//}
//
/////////////////////////////////////////////////////////////////////////////////////////////////
//
////! Check if a path exists
//bool ZipFile::existsFileInArchive(const char *path){
//    if(m_zipFile == 0){
//        return false;
//    }
//    
////    // Loop to extract all files
////    for(size_t i = 0; i < m_globalInfo.number_entry; ++i){
////        // Get info about current file.
////        unz_file_info file_info;
////        char filename[kMaxFilenameSize];
////        if(unzGetCurrentFileInfo(m_zipFile,
////                                 &file_info,
////                                 filename,
////                                 kMaxFilenameSize,
////                                 NULL, 0,
////                                 NULL, 0 ) != UNZ_OK){
////            NIMBLE_LOG_ERROR("zipfile", "Failed to read file info for zipfile %s", path);
////            unzClose(m_zipFile);
////            return;
////        }
////        
////        // Check if this entry is a directory or file.
////        const size_t filename_length = strlen(filename);
////        if(filename[filename_length - 1] == kDirDelimeter){
////            NIMBLE_LOG_INFO("zipfile", "directory found with name %s", filename);
////            // TODO: make directory - see http://stackoverflow.com/questions/10440113/simple-way-to-unzip-a-zip-file-using-zlib
////        }else{
////            NIMBLE_LOG_INFO("zipfile", "file found with name %s", filename);
////            // TODO: load file - see http://stackoverflow.com/questions/10440113/simple-way-to-unzip-a-zip-file-using-zlib
////        }
////        
////        unzCloseCurrentFile(m_zipFile);
////        
////        // Go the the next entry listed in the zip file.
////        if((i + 1) < m_globalInfo.number_entry){
////            if(unzGoToNextFile(m_zipFile) != UNZ_OK){
////                NIMBLE_LOG_ERROR("zipfile", "Failed to read next file info for zipfile %s", path);
////                unzClose(m_zipFile);
////                return;
////            }
////        }
////    }
//}
//
/////////////////////////////////////////////////////////////////////////////////////////////////
//
////! Constructor
//ZextFileLoader::ZextFileLoader(){
//}
////! Destructor
//ZextFileLoader::~ZextFileLoader(){
//}
////! loads a resource
////! \param path the path of the file we want to load
//resource::IResource* ZextFileLoader::loadResource(const char* path){
//    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<core::ZipFile>();
//    core::ZipFile *pObject = dynamic_cast<core::ZipFile*>(pResource);
//    pObject->loadFromFile(path);
//    return pResource;
//}

#endif
//////////////////////////////////////////////////////////////////////////