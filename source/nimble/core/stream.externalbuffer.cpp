//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/stream.externalbuffer.h>
#include <assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ExternalBufferStream::ExternalBufferStream()
:m_pBuffer(0)
,m_bufferSize(0)
,m_readPosition(0)
,m_writePosition(0){
}
//! Constructor
ExternalBufferStream::ExternalBufferStream(char *pBuffer, size_t size)
:m_pBuffer(pBuffer)
,m_bufferSize(size)
,m_readPosition(0)
,m_writePosition(size){
}
//! Destructor
ExternalBufferStream::~ExternalBufferStream(){
}

//////////////////////////////////////////////////////////////////////////

//! returns the size of our Stream
size_t ExternalBufferStream::size() const{
    return m_bufferSize;
}
//! returns true if this buffer is empty
bool ExternalBufferStream::isEmpty() const{
    return m_writePosition == m_readPosition;
}

//////////////////////////////////////////////////////////////////////////

//! Writes to this stream
void ExternalBufferStream::write(char const *pBuffer, size_t size){
    assert(pBuffer != 0);
    assert(size > 0);
    assert(m_writePosition + size <= m_bufferSize);
    
    char *pDataPointer = m_pBuffer;
    char *pWritePosition = &pDataPointer[m_writePosition];
    memcpy(pWritePosition, pBuffer, size);
    m_writePosition += size;
}
//! Reads from this stream
void ExternalBufferStream::read(char *pBuffer, size_t size){
    assert(pBuffer != 0);
    assert(size > 0);
    assert(m_readPosition + size <= m_bufferSize);
    
    char *pDataPointer = m_pBuffer;
    char *pReadPosition = &pDataPointer[m_readPosition];
    memcpy(pBuffer, pReadPosition, size);
    m_readPosition += size;
}

//////////////////////////////////////////////////////////////////////////