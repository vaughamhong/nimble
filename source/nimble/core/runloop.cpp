//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/runloop.h>
#include <nimble/core/thread.semaphore.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/timer.h>
#include <nimble/core/scopedprofiler.h>
#include <sstream>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

static nimble::core::IRunLoop *g_mainRunLoop = 0;
nimble::core::ThreadLocalStorage<nimble::core::IRunLoop> g_threadLocalRunLoop;

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
RunLoop::RunLoop()
:m_activeStartOffset(0){
}
//! Destructor
RunLoop::~RunLoop(){
}

//////////////////////////////////////////////////////////////////////////

//! runs a synchronous operation on this run loop
//! \param operation the operation to run
void RunLoop::runSync(core::RunOperation operation){
    // create semaphore for synchronizing respone
    std::stringstream semName;
    semName << std::hex << &operation;
    core::Semaphore *pWaitLock = new /*(pool)*/ core::Semaphore(semName.str().c_str(), 0);

    // create our operation info for tracking purposes
    operation_t info;
    info.operation = operation;
    info.pWaitLock = pWaitLock;

    // add to our new operations list
    {
        core::ScopedLock lock(&m_newOperationsLock);
        m_newOperations.push_back(info);
    }
    
    // make sure this is not within the scopedlocked (above)
    // or we may deadlock (block with wait() while waiting for post())
    if(pWaitLock != 0){
        pWaitLock->wait();
        delete pWaitLock;
    }
}
//! runs a asynchronous operation on this run loop
//! \param operation the operation to run
void RunLoop::runAsync(core::RunOperation operation){
    // create our operation info for tracking purposes
    operation_t info;
    info.operation = operation;
    info.pWaitLock = 0;

    // add to our new operations list
    {
        core::ScopedLock lock(&m_newOperationsLock);
        m_newOperations.push_back(info);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Adds a runloop operation
//! \param operation adds a runloop operation to execute
void RunLoop::addLoopOperation(core::RunLoopOperation operation){
    core::ScopedLock lock(&m_newLoopOperationsLock);
    m_newLoopOperations.push_back(operation);
}
//! Removes a runloop operation
//! \param operation removes a runloop operation from execution
void RunLoop::removeLoopOperation(core::RunLoopOperation operation){
    core::ScopedLock lock(&m_inactiveLoopOperationsLock);
    m_inactiveLoopOperations.push_back(operation);
}

//////////////////////////////////////////////////////////////////////////

//! Runs a single loop update
void RunLoop::update(){
    executeLoopOperations();
    executeOperations();
}

//! Executes loop operations
void RunLoop::executeLoopOperations(){
//    if(core::getMainRunLoop() == this){
//        SCOPEDPROFILE("RunLoop::executeLoopOperations");
//    }
    
    const double maxTime = (1.0 / 120.0);
    core::Timer timer;
    timer.reset();
    
    // add new operations
    {
        core::ScopedLock lock(&m_newLoopOperationsLock);
        if(!m_newLoopOperations.empty()){
            m_activeLoopOperations.insert(m_activeLoopOperations.end(), m_newLoopOperations.begin(), m_newLoopOperations.end());
            m_newLoopOperations.clear();
        }
        // exit if we've reached max time
        if(timer.getElapsedTimeInMilliSeconds() > maxTime){
            return;
        }
    }
    
    // remove inactive operations
    {
        core::ScopedLock lock(&m_inactiveLoopOperationsLock);
        if(!m_inactiveLoopOperations.empty()){
            core::RunLoop::LoopOperationList::iterator inactiveIt;
            for(inactiveIt = m_inactiveLoopOperations.begin(); inactiveIt != m_inactiveLoopOperations.end();){
                core::RunLoop::LoopOperationList::iterator removeIt = std::find(m_activeLoopOperations.begin(), m_activeLoopOperations.end(), *inactiveIt);
                if(removeIt != m_activeLoopOperations.end()){
                    m_activeLoopOperations.erase(removeIt);
                }
                inactiveIt = m_inactiveLoopOperations.erase(inactiveIt);
                
                // exit if we've reached max time
                if(timer.getElapsedTimeInMilliSeconds() > maxTime){
                    return;
                }
            }
        }
    }
    
    // run operations
    // round robin execute loop operations within a certain time limit
    if(!m_activeLoopOperations.empty()){
        int32_t offset = m_activeStartOffset % m_activeLoopOperations.size();
        for(core::RunLoop::LoopOperationList::iterator it = m_activeLoopOperations.begin() + offset; it != m_activeLoopOperations.end(); it++){
            // execute loop operation
            (*it)();
            m_activeStartOffset += 1;
            
            // if running our loop operation meant adding to our inactive list
            // we simply break to clean up (on next execute) before running any more loop operations
            if(!m_inactiveLoopOperations.empty()){
                break;
            }
            
            // exit if we've reached max time
            if(timer.getElapsedTimeInMilliSeconds() > maxTime){
                return;
            }
        }
    }
}
//! Executes operations
void RunLoop::executeOperations(){
//    SCOPEDPROFILE("RunLoop::executeOperations");
    
    // transfer new operations to our list
    {
        core::ScopedLock lock(&m_newOperationsLock);
        if(!m_newOperations.empty()){
            m_operations.insert(m_operations.end(), m_newOperations.begin(), m_newOperations.end());
            m_newOperations.clear();
        }
    }
    
    // execute our one off operations
    for(core::RunLoop::OperationList::iterator it = m_operations.begin(); it != m_operations.end(); it++){
        core::RunLoop::operation_t &info = *it;
        info.operation();
        if(info.pWaitLock){
            info.pWaitLock->post();
        }
    }
    m_operations.clear();
}

//////////////////////////////////////////////////////////////////////////

//! Sets the current run loop
//! \param pRunLoop the run loop to set for this thread
void nimble::core::setCurrentRunLoop(core::IRunLoop *pRunLoop, bool isMainRunLoop){
    g_threadLocalRunLoop = pRunLoop;
    if(isMainRunLoop){
        g_mainRunLoop = pRunLoop;
    }
}
//! Returns the current run loop
//! \return the current run loop
core::IRunLoop* nimble::core::getCurrentRunLoop(){
    return g_threadLocalRunLoop.get();
}
//! Returns the main run loop
//! \return the main run loop
core::IRunLoop* nimble::core::getMainRunLoop(){
    return g_mainRunLoop;
}

//////////////////////////////////////////////////////////////////////////

