//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 

#include <nimble/core/thread.taskqueue.h>
#include <nimble/core/thread.semaphore.h>
#include <sstream>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
TaskQueue::TaskQueue(){
}
//! Destructor
TaskQueue::~TaskQueue(){
}
//! pushes a job into the queue
TaskPtr TaskQueue::push(core::TaskPtr pTask){
    core::ScopedLock lock(&m_taskQueueLock);
    m_taskQueue.push(pTask);
    return pTask;
}
//! pops a job from the queue
core::TaskPtr TaskQueue::pop(){
    core::ScopedLock lock(&m_taskQueueLock);
    if(m_taskQueue.size() > 0){
        core::TaskPtr pTask = m_taskQueue.top();
        m_taskQueue.pop();
        return pTask;
    }else{
        return core::TaskPtr();
    }
}
//! removes a job
void TaskQueue::cancelTask(TaskPtr pTask){
    ScopedLock lock(&m_taskQueueLock);
    std::vector<TaskPtr> list;
    
    // note: eww
    
    // transfer to vector
    while(!m_taskQueue.empty()){
        list.push_back(m_taskQueue.top());
        m_taskQueue.pop();
    }
    
    // find / erase
    std::vector<TaskPtr>::iterator it = std::find(list.begin(), list.end(), pTask);
    list.erase(it);
    
    // transfer back to priority queue
    while(!list.empty()){
        m_taskQueue.push(list.back());
        list.pop_back();
    }
}
//! removes all jobs
std::vector<TaskPtr> TaskQueue::cancelAllTasks(){
    ScopedLock lock(&m_taskQueueLock);
    std::vector<TaskPtr> deletedJobs;
    
    while(!m_taskQueue.empty()){
        deletedJobs.push_back(m_taskQueue.top());
        m_taskQueue.pop();
    }
    return deletedJobs;
}
//! returns the queue size
uint32_t TaskQueue::size(){
    core::ScopedLock lock(&m_taskQueueLock);
    return m_taskQueue.size();
}

//////////////////////////////////////////////////////////////////////////
