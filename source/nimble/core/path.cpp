//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/path.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Path::Path()
:m_path(""){
}
//! Constructor
Path::Path(const char *path)
:m_path(path){
}
//! Constructor
Path::Path(std::string const &path)
:m_path(path){
}
//! Constructor
Path::Path(Path const &path)
:m_path(path.m_path){
}
//! Destructor
Path::~Path(){
}

//////////////////////////////////////////////////////////////////////////

//! Sets a path
Path& Path::operator=(Path const &path){
    m_path = path.m_path;
    return *this;
}

//! Checks for equality
bool Path::operator==(Path const &path){
    return m_path == path.m_path;
}
//! Checks for equality
bool Path::operator==(const char *path){
    return m_path == std::string(path);
}

//! Checks for inequality
bool Path::operator!=(Path const &path){
    return !Path::operator==(path);
}
//! Checks for inequality
bool Path::operator!=(const char *path){
    return !Path::operator==(path);
}

//////////////////////////////////////////////////////////////////////////

//! Returns the full path
std::string Path::getFullPath() const{
    return m_path;
}
//! Returns the extension
std::string Path::getExtension() const{
    return m_path.substr(m_path.find_last_of(".") + 1);
}
//! Returns the basename
std::string Path::getBasename() const{
    unsigned long lastindex = m_path.find_last_of(".");
    if(lastindex != std::string::npos){
        return m_path.substr(0, lastindex);
    }else{
        return "";
    }
}
//! Returns the filename
std::string Path::getFilename() const{
    unsigned long lastindex = m_path.find_last_of("/");
    if(lastindex != std::string::npos){
        return m_path.substr(lastindex);
    }else{
        return "";
    }
}
//! Returns the directory
std::string Path::getDirectory() const{
    unsigned long lastindex = m_path.find_last_of("/");
    if(lastindex != std::string::npos){
        return m_path.substr(0, lastindex);
    }else{
        return "";
    }
}

//////////////////////////////////////////////////////////////////////////

//! returns the directory of the path up one level
Path Path::popDirectory() const{
    unsigned long lastindex = m_path.find_last_of("/");
    if(lastindex != std::string::npos){
        return m_path.substr(0, lastindex);
    }else{
        return Path();
    }
}

//////////////////////////////////////////////////////////////////////////