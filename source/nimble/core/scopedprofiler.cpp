//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/scopedprofiler.h>
#include <nimble/core/logger.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

static int g_scopeDepth = 0;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ScopedProfiler::ScopedProfiler(std::string name)
:m_name(name){
	m_timer.reset();
    std::string spaces = "";
    for(int i = 0; i < g_scopeDepth; i++){
        spaces += "  ";
    }
    g_scopeDepth += 1;
    NIMBLE_LOG_INFO("profile", "%s [%s]", spaces.c_str(), m_name.c_str());
}
//! Destructor
ScopedProfiler::~ScopedProfiler(){
    g_scopeDepth -= 1;
    std::string spaces = "";
    for(int i = 0; i < g_scopeDepth; i++){
        spaces += "  ";
    }
    NIMBLE_LOG_INFO("profile", "%s [%s][%.2f ms]", spaces.c_str(), m_name.c_str(), m_timer.getElapsedTimeInMilliSeconds());
}

//////////////////////////////////////////////////////////////////////////
