//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/tokenize.h>
#include <sstream>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! tokenizes a string relative to a separation delimeter
//! \param tokens the resulting tokens
//! \param pString the string to tokenize
//! \param delimeter the separation delimeter
void nimble::core::tokenize(std::vector<std::string> &tokens, const char *pString, const char delimeter){
    std::istringstream ss(pString);
    tokens.clear();
    for(std::string each; std::getline(ss, each, delimeter); tokens.push_back(each));
}

//////////////////////////////////////////////////////////////////////////

