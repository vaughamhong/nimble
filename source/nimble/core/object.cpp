//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/object.h>
#include <nimble/core/typemetadata.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Destructor
Object::Object()
:m_pType(0){
}
//! Constructor
Object::Object(core::TypeMetaData *pType)
:m_pType(pType){
}
//! Destructor
Object::~Object(){
    m_pType = 0;
}
//! Returns the type info for this object
//! \return the type info for this object
core::TypeMetaData* Object::getTypeMetaData() const{
    return m_pType;
}
//! Sets this object's type
//! \param[in] pType the type to set
void Object::setTypeMetaData(core::TypeMetaData *pType){
    m_pType = pType;
}

//////////////////////////////////////////////////////////////////////////