//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/Lockable.h>
#include <nimble/core/logger.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Lockable::Lockable()
:m_isLocked(false)
,m_lockType(core::kLockTypeNone){
}
//! Destructor
Lockable::~Lockable(){
}

//////////////////////////////////////////////////////////////////////////

//! locks data for access rights
//! \param lockType the type of lock to the data
//! \param ppPointer pointer to the data
bool Lockable::lock(eLockType lockType, char** ppPointer){
    if(0 <= lockType && lockType < core::kMaxLockTypes){
        if(ppPointer != 0){
            if(!isLocked()){
                *ppPointer = this->mapBuffer(lockType);
                if(*ppPointer != 0){
                    m_lockType = lockType;
                    m_isLocked = true;
                    return true;
                }else{
                    NIMBLE_LOG_ERROR("lock", "Failed to map buffer");
                }
            }else{
                NIMBLE_LOG_ERROR("lock", "Failed to lock already locked resource");
            }
        }else{
            NIMBLE_LOG_ERROR("lock", "Failed to lock with invalid input ppointer");
        }
    }else{
        NIMBLE_LOG_ERROR("lock", "Failed to lock with invalid input lock type");
    }
    return false;
}
//! locks data for access rights
//! \param lockType the type of lock to the data
//! \param offset the offset of the data to lock
//! \param size the size of the data to lock
//! \param ppPointer pointer to the data
bool Lockable::lockRange(eLockType lockType, char **ppPointer, int32_t offset, int32_t size){
    if(0 <= lockType && lockType < core::kMaxLockTypes){
        if(ppPointer != 0){
            if(!isLocked()){
                *ppPointer = this->mapBufferRange(lockType, offset, size);
                if(*ppPointer != 0){
                    m_lockType = lockType;
                    m_isLocked = true;
                    return true;
                }else{
                    NIMBLE_LOG_ERROR("lock", "Failed to map buffer");
                }
            }else{
                NIMBLE_LOG_ERROR("lock", "Failed to lock already locked resource");
            }
        }else{
            NIMBLE_LOG_ERROR("lock", "Failed to lock with invalid input ppointer");
        }
    }else{
        NIMBLE_LOG_ERROR("lock", "Failed to lock with invalid input lock type");
    }
    return false;
}
//! unlocks data
void Lockable::unlock(){
    if(isLocked()){
        this->unmapBuffer();
        m_lockType = core::kLockTypeNone;
        m_isLocked = false;
    }else{
        NIMBLE_LOG_WARNING("lock", "Failed to unlock already unlocked resource");
    }
}

//////////////////////////////////////////////////////////////////////////

//! \return true if locked
bool Lockable::isLocked() const{
    return m_isLocked;
}
//! \return the lock type for this asset
core::eLockType Lockable::getLockType() const{
    return m_lockType;
}

//////////////////////////////////////////////////////////////////////////