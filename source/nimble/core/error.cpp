//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/error.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

////////////////////////////////////////////////////////////////////////////////

#define ERRORS_TUPLE(VAR, CODE, CATEGORY, STRING) nimble::core::Error nimble::core::VAR(CODE, CATEGORY, STRING);
    ERRORS_TUPLESET
#undef ERRORS_TUPLE

////////////////////////////////////////////////////////////////////////////////

//! Constructor
Error::Error()
:m_code(0)
,m_category("")
,m_string(""){
}
//! Constructor
Error::Error(int32_t code, const char *category, const char *string)
:m_code(code){
    if(category){m_category = category;}
    if(string){m_string = string;}
}
//! copy constructor
Error::Error(Error const &error)
:m_code(error.m_code)
,m_category(error.m_category)
,m_string(error.m_string){
}
//! Destructor
Error::~Error(){
}

////////////////////////////////////////////////////////////////////////////////

//! returns the error code
int32_t Error::getCode(){
    return m_code;
}
//! returns the category
const char* Error::getCategory(){
    return m_category.c_str();
}
//! returns the error string
const char* Error::getString(){
    return m_string.c_str();
}

////////////////////////////////////////////////////////////////////////////////

//! equality operator
bool Error::operator==(Error const &error){
    return (m_code == error.m_code) && (m_category == error.m_category);
}

//! inequality operator
bool Error::operator!=(Error const &error){
    return (m_code != error.m_code) || (m_category != error.m_category);
}

//////////////////////////////////////////////////////////////////////////

