//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/ithread.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

class MainThread
: public core::IThread{
public:
    //! Constructor
    MainThread(){}
    //! Destructor
    virtual ~MainThread(){}
    
    //! Starts running thread
    virtual void start(){
        NIMBLE_ASSERT(false);
    }
    //! Wait until thread has stopped
    virtual void join(){
        NIMBLE_ASSERT(false);
    }
    
    //! Returns true if thread is stopped
    virtual bool isStopped() const{
        return false;
    }
    //! Returns true if thread is running
    virtual bool isRunning() const{
        return true;
    }
    //! Returns true if thread is main thread
    virtual bool isMainThread() const{
        return true;
    }
    
    //! returns the threadId
    virtual int32_t getThreadId() const{
        return core::kMainThreadId;
    }
    //! returns the thread priority
    virtual core::eThreadPriority getThreadPriority() const{
        return core::kThreadPriorityNormal;
    }
};

//////////////////////////////////////////////////////////////////////////

nimble::core::ThreadLocalStorage<core::Thread>  g_threadLocalThread;
MainThread g_mainThread;

//////////////////////////////////////////////////////////////////////////

//! Returns the current threadId
//! \return the current threadId
int32_t nimble::core::getCurrentThreadId(){
    // 1. there is no way getCurrentThreadId will be called from a thread before
    // setCurreentThreadId is called (because setCurrentThreadId is the first
    // method called when a thread starts) - so it is impossible for a race
    // condition to happen on a secondary thread.
    // 2. the main thread does not set the local storage threadId, which means
    // if the local storage threadId is invalid (null), we are dealing with
    // the main thread (and defaults to nimble::core::kMainThreadId).
    return core::getCurrentThread()->getThreadId();
}

//! Returns the current thread
//! \return the current thread
core::IThread* nimble::core::getCurrentThread(){
    if(g_threadLocalThread.get()){
        return g_threadLocalThread.get();
    }else{
        return &g_mainThread;
    }
}

//////////////////////////////////////////////////////////////////////////

