//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 

#include <nimble/core/thread.pool.h>
#include <nimble/core/thread.semaphore.h>
#include <nimble/core/logger.h>
#include <sstream>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

class CallbackTask
: public core::Task{
public:
    typedef core::Functor<void> Callback;
private:
    Callback m_callback;
public:
    //! Constructor
    CallbackTask(Callback const &callback)
    :m_callback(callback){
    }
    //! Destructor
    virtual ~CallbackTask(){
    }
public:
    //! execute
    virtual void execute(){
        m_callback();
    }
};

//////////////////////////////////////////////////////////////////////////

//! Tells a worker thread to stop running
class ExitTask
: public core::Task{
public:
    //! execute
    virtual void execute(){
        core::WorkerThread *pWorkerThread = this->getWorkerThread();
        pWorkerThread->stop();
    }
};

//////////////////////////////////////////////////////////////////////////

//! Constructor
WorkerThread::WorkerThread(uint32_t index, ITaskQueue *pTaskQueue, core::Semaphore *pFullSem, core::Semaphore *pEmptySem, core::eThreadPriority priority)
:core::RunLoopThread(priority)
,m_index(index)
,m_pTaskQueue(pTaskQueue)
,m_pFullSem(pFullSem)
,m_pEmptySem(pEmptySem)
,m_isDoneRunning(false){
    this->addLoopOperation(core::RunLoopOperation(this, &core::WorkerThread::runSingleFrame));
    NIMBLE_LOG_INFO("core", "WorkerThread %d created (threadId %d)", m_index, this->getThreadId());
}
//! Destructor
WorkerThread::~WorkerThread(){
    this->removeLoopOperation(core::RunLoopOperation(this, &core::WorkerThread::runSingleFrame));
    NIMBLE_LOG_INFO("core", "WorkerThread %d destroyed (threadId %d)", m_index, this->getThreadId());
}
//! returns the worker index
uint32_t WorkerThread::getIndex() const{
    return m_index;
}

//! called on each run loop iteration
void WorkerThread::runSingleFrame(){
    m_pEmptySem->wait();
    core::TaskPtr pTask = m_pTaskQueue->pop();
    m_pFullSem->post();
    
    if(pTask != 0){
        pTask->setWorkerThread(this);
        pTask->execute();
    }else{
        NIMBLE_LOG_ERROR("core", "Invalid (0) task was dispatched");
    }
}
//! returns true if we are done running
bool WorkerThread::isDoneRunning(){
    return m_isDoneRunning;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
ThreadPool::ThreadPool(int32_t numWorkers, uint32_t queueSize)
:m_numTotalTasks(0){
    NIMBLE_LOG_INFO("core", "Creating ThreadPool with %d worker(s)", numWorkers);
    
    std::stringstream fullSemName, emptySemName;
    fullSemName << std::hex << this << "::m_fullSem" << std::endl;
    emptySemName << std::hex << this << "::m_emptySem"  << std::endl;
    m_pFullSem = new core::Semaphore(fullSemName.str().c_str(), queueSize);
    m_pEmptySem = new core::Semaphore(emptySemName.str().c_str(), 0);
    
    // create our task executors
    for(int32_t i = 0; i < numWorkers; i++){
        core::WorkerThread *pWorker = new /*( pool )*/ core::WorkerThread(i, &m_taskQueue, m_pFullSem, m_pEmptySem, core::kThreadPriorityBackground);
        m_workerThreads.push_back(pWorker);
        pWorker->start();
    }
}
//! Destructor
ThreadPool::~ThreadPool(){
    // destroy all thread workers
    for(WorkerList::iterator it = m_workerThreads.begin(); it != m_workerThreads.end(); it++){
        this->executeAsyncTask(new /*( pool )*/ ExitTask());
    }
    for(WorkerList::iterator it = m_workerThreads.begin(); it != m_workerThreads.end(); it++){
        delete (*it);
    }
    m_workerThreads.clear();
    
    delete m_pEmptySem; m_pEmptySem = 0;
    delete m_pFullSem; m_pFullSem = 0;
}

//////////////////////////////////////////////////////////////////////////

//! adds a task to be executed
void ThreadPool::executeAsyncTask(core::Functor<void> const &callback, int32_t priority){
    return this->executeAsyncTask(new /* () */ CallbackTask(callback), priority);
}
//! adds a task to be executed
void ThreadPool::executeAsyncTask(core::Task *pTask, int32_t priority){
    ScopedLock lock(&m_lock);
    pTask->setIndex(m_numTotalTasks++);
    pTask->setThreadPool(this);
    pTask->setPriority(priority);
    m_pFullSem->wait();
    m_taskQueue.push(pTask);
    m_pEmptySem->post();
}
//! removes a task from execution
void ThreadPool::cancelTask(core::TaskPtr pTask){
    ScopedLock lock(&m_lock);
    m_taskQueue.cancelTask(pTask);
}
//! clears all tasks
std::vector<core::TaskPtr> ThreadPool::cancelAllTasks(){
    ScopedLock lock(&m_lock);
    return m_taskQueue.cancelAllTasks();
}

//////////////////////////////////////////////////////////////////////////
