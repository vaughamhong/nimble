//
// Copyright (c) 2011 Vaugham Hong
// This file is part of Nimble
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// 

#include <nimble/core/thread.task.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Task::Task(){
}
//! Destructor
Task::~Task(){
}
//! returns the job index
uint32_t Task::getIndex() const {
    return m_index;
}
//! returns the job priority
uint32_t Task::getPriority() const{
    return m_priority;
}
//! returns the parent job manager
core::ThreadPool* Task::getThreadPool() const {
    return m_pThreadPool;
}
//! returns the bound worker thread
core::WorkerThread* Task::getWorkerThread() const {
    return m_pWorkerThread;
}
//! cancel
void Task::cancel(){
    // TODO: implement
    NIMBLE_ASSERT(false);
}
//! sets a job index
void Task::setIndex(uint32_t index){
    m_index = index;
}
//! sets a job priority
void Task::setPriority(int32_t priority){
    m_priority = priority;
}
//! sets job manager
void Task::setThreadPool(core::ThreadPool *pThreadPool){
    m_pThreadPool = pThreadPool;
}
//! sets worker thread
void Task::setWorkerThread(core::WorkerThread *pWorkerThread){
    m_pWorkerThread = pWorkerThread;
}

//////////////////////////////////////////////////////////////////////////
