//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/memory.h>
#include <nimble/core/iallocator.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

core::IAllocator *g_pGlobalAllocator = 0;

//////////////////////////////////////////////////////////////////////////

struct allocationHeader_t{
    // note: OpenAL alcOpenDevice(0) seems to allocate something that
    // is not compatible with our header (most likely alignment related).
    // reproduce by running in release and calling alcOpenDevice(0) with
    // full optimizations - allocationHeader_t will require an extra
    // 4 bytes buffer for alcOpenDevice(0) to not segmentation fault.
    core::IAllocator *pAllocator;
};

//////////////////////////////////////////////////////////////////////////

//! allocates memory - alignment must be suitable for any data type
//! \param size the size to allocate
//! \param pAllocator the allocator to use
void* allocate(size_t size, nimble::core::IAllocator *pAllocator){
    // note: OpenAL alcOpenDevice(0) seems to allocate something that
    // is not compatible with our header (most likely alignment related).
    // reproduce by running in release and calling alcOpenDevice(0) with
    // full optimizations - allocationHeader_t will require an extra
    // 4 bytes buffer for alcOpenDevice(0) to not segmentation fault.
    return malloc(size);
    
    // calculate the actual size (which includes our header)
    size_t actualSize = size + sizeof(allocationHeader_t);
    
    // allocate memory
    void *pAllocation = 0;
    if(pAllocator){
        pAllocation = pAllocator->allocate(actualSize);
    }else if(g_pGlobalAllocator){
        pAllocation = g_pGlobalAllocator->allocate(actualSize);
        pAllocator = g_pGlobalAllocator;
    }else{
        pAllocation = malloc(actualSize);
    }
    
    // fill our header
    if(pAllocation){
        allocationHeader_t *pHeader = (allocationHeader_t*)pAllocation;
        pHeader->pAllocator = pAllocator;
    }else{
        NIMBLE_LOG_ERROR("memory", "Failed to allocate memory");
    }
    
    // return pointer to client envelope
    return (void*)((char*)pAllocation + sizeof(allocationHeader_t));
}
//! Deallocate a memory pointer
//! \param ptr the pointer to deallocate
void deallocate(void *ptr){
    // note: OpenAL alcOpenDevice(0) seems to allocate something that
    // is not compatible with our header (most likely alignment related).
    // reproduce by running in release and calling alcOpenDevice(0) with
    // full optimizations - allocationHeader_t will require an extra
    // 4 bytes buffer for alcOpenDevice(0) to not segmentation fault.
    free(ptr);
    return;
    
	if(ptr == 0){return;}

    // calculate our actual memory start location
    void *pActualAllocation = (void*)((char*)ptr - sizeof(allocationHeader_t));
    
    // decide how to deallocate our memory
    allocationHeader_t *pHeader = (allocationHeader_t*)(pActualAllocation);
    if(pHeader->pAllocator){
        pHeader->pAllocator->deallocate(pActualAllocation);
    }else{
        free(pActualAllocation);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Sets our global allocator
void nimble::core::setGlobalAllocator(core::IAllocator *pAllocator){
    g_pGlobalAllocator = pAllocator;
}
//! Returns our global allocator
//! \return our global allocator
core::IAllocator* nimble::core::getGlobalAllocator(){
    return g_pGlobalAllocator;
}

//////////////////////////////////////////////////////////////////////////

//! Calculates aligned address
void* nimble::core::alignAddress(void *pAddress, size_t alignment){
    NIMBLE_ASSERT(alignment > 0);
    uintptr_t ptr = reinterpret_cast<uintptr_t>(pAddress);
    
    // calculate our remainder bits with a bitwise AND (which is effectively a mod)
    uintptr_t mask = alignment - 1;
    uintptr_t remainder = ptr & mask;
    
    // calculate aligned address by adding an offset to the address
    // which brings us to an alignment boundary
    ptrdiff_t offset = alignment - remainder;
    uintptr_t alignedAddress = ptr + offset;
    
    return reinterpret_cast<void*>(alignedAddress);
}

//////////////////////////////////////////////////////////////////////////

#if kOverloadNewAndDelete
#if defined(NIMBLE_TARGET_WIN32)
void* operator new(size_t size) throw(){
    return allocate(size, 0);
}
#elif defined(NIMBLE_TARGET_OSX)
void* operator new(size_t size) throw(std::bad_alloc){
    return allocate(size, 0);
}
#elif defined(NIMBLE_TARGET_ANDROID)
void* operator new(size_t size){
    return allocate(size, 0);
}
#endif
void* operator new(size_t size, const std::nothrow_t&) throw(){
    return allocate(size, 0);
}
void* operator new(size_t size, nimble::core::IAllocator *pAllocator){
    return pAllocator->allocate(size);
}
void operator delete(void* ptr) throw(){
    deallocate(ptr);
}
void operator delete(void* ptr, const std::nothrow_t&) throw(){
    deallocate(ptr);
}
void operator delete(void* ptr, nimble::core::IAllocator *pAllocator) throw(){
    pAllocator->deallocate(ptr);
}

//////////////////////////////////////////////////////////////////////////

#if defined(NIMBLE_TARGET_WIN32)
void* operator new[](size_t size) throw(){
	return allocate(size, 0);
}
#elif defined(NIMBLE_TARGET_OSX)
void* operator new[](size_t size) throw(std::bad_alloc){
	return allocate(size, 0);
}
#elif defined(NIMBLE_TARGET_ANDROID)
void* operator new[](size_t size){
    return allocate(size, 0);
}
#endif
void* operator new[](size_t size, const std::nothrow_t&) throw(){
    return allocate(size, 0);
}
void* operator new[](size_t size, nimble::core::IAllocator *pAllocator){
    return pAllocator->allocate(size);
}
void operator delete[](void* ptr) throw(){
    deallocate(ptr);
}
void operator delete[](void* ptr, const std::nothrow_t&) throw(){
    deallocate(ptr);
}
void operator delete[](void* ptr, nimble::core::IAllocator *pAllocator) throw(){
    pAllocator->deallocate(ptr);
}
#endif

//////////////////////////////////////////////////////////////////////////