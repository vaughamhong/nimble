//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#import <nimble/core/platform/ios/NSDate+Utility.h>

//////////////////////////////////////////////////////////////////////////

@implementation NSDate(Utility)

//////////////////////////////////////////////////////////////////////////

- (NSString*)timeElapsedString{
    NSDate *endDate = [NSDate date];
    int totalSeconds = (int)[endDate timeIntervalSinceDate:self];
    int minutes = totalSeconds / 60;
    int hours = totalSeconds / (60 * 60);
    int days = totalSeconds / (60 * 60 * 24);
    
    NSString *timeLeftString = @"";
    if(days > 0){
        timeLeftString = [NSString stringWithFormat:@"%d days ago", days];
    }else if(hours > 0){
        timeLeftString = [NSString stringWithFormat:@"%d hours ago", hours];
    }else if(minutes > 0){
        timeLeftString = [NSString stringWithFormat:@"%d minutes ago", minutes];
    }else{
        timeLeftString = [NSString stringWithFormat:@"Just now"];
    }
    return timeLeftString;
}

//////////////////////////////////////////////////////////////////////////

@end

//////////////////////////////////////////////////////////////////////////