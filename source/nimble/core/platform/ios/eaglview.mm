//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#import <nimble/core/platform/ios/EAGLView.h>
#import <nimble/application/application.h>
#import <nimble/input/system.touchinput.h>
#import <nimble/core/locator.h>

//////////////////////////////////////////////////////////////////////////

@implementation EAGLView

+ (Class)layerClass{
    return [CAEAGLLayer class];
}

//////////////////////////////////////////////////////////////////////////

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	for(UITouch* touch in touches){
		CGPoint pnt = [touch locationInView:self];
        if(nimble::entity::TouchInputSystem *pTouchInputSystem = nimble::core::Locator<nimble::entity::TouchInputSystem>::acquire()){
            pTouchInputSystem->injectInput(nimble::input::TouchDevice::kTouchEventMove, pnt.x, pnt.y);
        }
	}
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	for(UITouch* touch in touches){
		CGPoint pnt = [touch locationInView:self];
        if(nimble::entity::TouchInputSystem *pTouchInputSystem = nimble::core::Locator<nimble::entity::TouchInputSystem>::acquire()){
            pTouchInputSystem->injectInput(nimble::input::TouchDevice::kTouchEventBegin, pnt.x, pnt.y);
        }
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	for(UITouch* touch in touches){
		CGPoint pnt = [touch locationInView:self];
        if(nimble::entity::TouchInputSystem *pTouchInputSystem = nimble::core::Locator<nimble::entity::TouchInputSystem>::acquire()){
            pTouchInputSystem->injectInput(nimble::input::TouchDevice::kTouchEventEnd, pnt.x, pnt.y);
        }
    }
}
- (BOOL)canBecomeFirstResponder{
    return YES;
}

//////////////////////////////////////////////////////////////////////////

@end

//////////////////////////////////////////////////////////////////////////