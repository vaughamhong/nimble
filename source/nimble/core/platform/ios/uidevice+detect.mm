//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#import <nimble/core/platform/ios/UIDevice+detect.h>
#include <sys/types.h>
#include <sys/sysctl.h>

//////////////////////////////////////////////////////////////////////////

@implementation UIDevice(detect)

//////////////////////////////////////////////////////////////////////////

#define DEVICEDETECTION_TUPLE(ENUM, deviceTypeString, MACHINESTRING) MACHINESTRING,
NSString *gMachineStringMap [] = {
    DEVICEDETECTION_TUPLESET
};
#undef DEVICEDETECTION_TUPLE

#define DEVICEDETECTION_TUPLE(ENUM, deviceTypeString, MACHINESTRING) deviceTypeString,
NSString *gDeviceTypeStringMap [] = {
    DEVICEDETECTION_TUPLESET
};
#undef DEVICEDETECTION_TUPLE

#define DEVICECLASS_TUPLE(ENUM, CLASSNAME) CLASSNAME,
NSString *gDeviceClassStringMap [] = {
    DEVICECLASS_TUPLESET
};
#undef DEVICECLASS_TUPLE

//////////////////////////////////////////////////////////////////////////

+ (eDeviceType) deviceType {
    // try literal string comparison (to find exact match)
    NSString *model = [[UIDevice currentDevice] machine];
    for(unsigned int i = 0; i < MAX_DEVICETYPES; i++){
        if([model compare:gMachineStringMap[i]] == NSOrderedSame){
            return (eDeviceType)i;
        }
    }    
    // invalid
    return DEVICE_INVALID;
}
+ (eDeviceClass) deviceClass {
    // try substring comparison (to find category)
    NSString *model = [[UIDevice currentDevice] machine];
    if([model rangeOfString:gDeviceClassStringMap[CLASS_IPHONE]].location != NSNotFound) {
        return CLASS_IPHONE;
    }else if([model rangeOfString:gDeviceClassStringMap[CLASS_IPAD]].location != NSNotFound) {
        return CLASS_IPAD;
    }else if([model rangeOfString:gDeviceClassStringMap[CLASS_IPOD_TOUCH]].location != NSNotFound) {
        return CLASS_IPOD_TOUCH;
    }else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return CLASS_IPAD;
    }else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        return CLASS_IPHONE;
    }
    // invalid
    return CLASS_INVALID;
}

//////////////////////////////////////////////////////////////////////////

+ (NSString *) deviceTypeString {
    eDeviceType type = [self deviceType];
    assert(type != DEVICE_INVALID);
    return gDeviceTypeStringMap[type];
}
+ (NSString *) deviceClassString {
    eDeviceClass type = [self deviceClass];
    assert(type != CLASS_INVALID);
    return gDeviceClassStringMap[type];
}
- (NSString *)machine {
    size_t size;
    
    // Set 'oldp' parameter to NULL to get the size of the data
    // returned so we can allocate appropriate amount of space
    sysctlbyname("hw.machine", NULL, &size, NULL, 0); 
    
    // Allocate the space to store name
    char *name = (char*)malloc(size);
    
    // Get the platform name
    sysctlbyname("hw.machine", name, &size, NULL, 0);
    
    // Place name into a string
    NSString *machine = [NSString stringWithUTF8String:name];
    
    // Done with this
    free(name);
    
    return machine;
}

//////////////////////////////////////////////////////////////////////////

+ (BOOL) isIPhone{
    return [UIDevice deviceClass] == CLASS_IPHONE;
}
+ (BOOL) isIPad{
    return [UIDevice deviceClass] == CLASS_IPAD;
}
+ (BOOL) isIPodTouch{
    return [UIDevice deviceClass] == CLASS_IPOD_TOUCH;
}

//////////////////////////////////////////////////////////////////////////

@end

//////////////////////////////////////////////////////////////////////////