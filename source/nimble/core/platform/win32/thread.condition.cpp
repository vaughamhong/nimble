//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.condition.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Condition::Condition(){
	InitializeConditionVariable(&m_condition);
}
//! Destructor
Condition::~Condition(){
}

//////////////////////////////////////////////////////////////////////////

//! waits for unlock
void Condition::wait(core::Mutex *pMutex){
	SleepConditionVariableCS(&m_condition, &pMutex->m_lock, INFINITE);
}
//! signals unlock
void Condition::signal(){
	WakeConditionVariable(&m_condition);
}
//! broadcast unlock
void Condition::broadcast(){
	WakeAllConditionVariable(&m_condition);
}

//////////////////////////////////////////////////////////////////////////