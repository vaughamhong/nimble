//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/platform/win32/thread.h>
#include <nimble/core/thread.h>
#include <nimble/core/thread.mutex.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/functor.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <vector>
#include <map>

//////////////////////////////////////////////////////////////////////////

int32_t nimble::core::Thread::threadIdCount = (nimble::core::kMainThreadId + 1);
nimble::core::Mutex nimble::core::Thread::threadIdCountLock;
extern nimble::core::ThreadLocalStorage<nimble::core::Thread> g_threadLocalThread;

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

typedef core::Functor<void> ThreadCallbackFunc;
struct nimble::core::threadData{
    core::Thread            *pThread;
    int32_t                 threadId;
    core::ThreadMainFunc    main;
    ThreadCallbackFunc      begin, end;
};

//////////////////////////////////////////////////////////////////////////

//! Thread main helper
DWORD WINAPI threadMain(LPVOID lpParam){
    core::threadData *pThreadData = (core::threadData*)lpParam;
    
    // setting up our local storage threadId here means it will be valid
    // before anyone calls getCurrentThreadId().
    g_threadLocalThread = pThreadData->pThread;

    // runs thread main
    pThreadData->begin();
    pThreadData->main();
    pThreadData->end();
    
    // exit
	ExitThread(0);
	return 0;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
Thread::Thread(ThreadMainFunc main, core::eThreadPriority priority)
:m_threadId(-1)
,m_priority(priority)
,m_state(kStateStopped){
    // assign a threadId to this thread object
    // we pass this down to our thread execution
    // which will set up the corresponding thread local storage
    #pragma message("TODO - consider wrapping this as an atomic counter")
    core::ScopedLock lock(&threadIdCountLock);
    m_threadId = threadIdCount++;

    // setup our thread data
    m_pThreadData = new /*( pool )*/ threadData();
    m_pThreadData->pThread = this;
    m_pThreadData->threadId = m_threadId;
    m_pThreadData->main = main;
    m_pThreadData->begin = ThreadCallbackFunc(this, &core::Thread::onThreadBegin);
    m_pThreadData->end = ThreadCallbackFunc(this, &core::Thread::onThreadEnd);
}
//! Destructor
Thread::~Thread(){
    delete m_pThreadData;
    m_pThreadData = 0;
    this->join();
}

//////////////////////////////////////////////////////////////////////////

//! Starts running thread
void Thread::start(){
    if(m_state == kStateStopped){
        core::ScopedLock lock(&m_stateLock);
        m_state = kStateStarting;

		m_securityAttributes.nLength = sizeof(SECURITY_ATTRIBUTES);
		m_securityAttributes.bInheritHandle = false;

		m_threadHandle = CreateThread(NULL, 0, threadMain, (LPVOID)m_pThreadData, 0, NULL);  
        core::assert_error(m_threadHandle != 0, "%d", GetLastError());
    }else{
		core::logger_warning("thread", "Failed to start thread - thread is already running");
    }
}
//! Wait until thread has stopped
void Thread::join(){
    if(m_state != kStateStopped){
        WaitForSingleObject(m_threadHandle, INFINITE);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Returns true if thread is stopped
bool Thread::isStopped() const{
    return (m_state == kStateStopped);
}
//! Returns true if thread is running
bool Thread::isRunning() const{
    return (m_state == kStateRunning) || (m_state == kStateStarting);
}
//! Returns true if thread is main thread
bool Thread::isMainThread() const{
    return false;
}
//! returns the threadId
int32_t Thread::getThreadId() const{
    return m_threadId;
}
//! returns the thread priority
core::eThreadPriority Thread::getThreadPriority() const{
	return m_priority;
}

//////////////////////////////////////////////////////////////////////////

//! called when thread has started
void Thread::onThreadBegin(){
    core::ScopedLock lock(&m_stateLock);
    m_state = kStateRunning;
	core::logger_info("thread", "Thread has started running (threadId %d)", m_threadId);

	if(m_priority == core::kThreadPriorityNormal){
		if(!SetThreadPriority(m_threadHandle, THREAD_PRIORITY_NORMAL)){
			core::logger_error("thread", "%d", GetLastError());
		}
	}else if(m_priority == core::kThreadPriorityBackground){
		if(!SetThreadPriority(m_threadHandle, THREAD_PRIORITY_LOWEST)){
			core::logger_error("thread", "%d", GetLastError());
		}
	}
}
//! called when thread has exited
void Thread::onThreadEnd(){
    core::ScopedLock lock(&m_stateLock);

	//if(m_priority == core::kThreadPriorityNormal){
	//	if(!SetThreadPriority(m_threadHandle, THREAD_PRIORITY_NORMAL)){
	//		core::logger_error("thread", "%d", GetLastError());
	//	}
	//}else if(m_priority == core::kThreadPriorityBackground){
	//	if(!SetThreadPriority(m_threadHandle, THREAD_MODE_BACKGROUND_END)){
	//		core::logger_error("thread", "%d", GetLastError());
	//	}
	//}

    m_state = kStateStopped;
	core::logger_info("thread", "Thread has stopped running (threadId %d)", m_threadId);
}

//////////////////////////////////////////////////////////////////////////

