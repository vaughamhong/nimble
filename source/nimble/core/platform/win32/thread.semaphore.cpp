//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.semaphore.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Semaphore::Semaphore(const char *name, int32_t value){
	m_handle = CreateSemaphore(NULL, value, 2048, name);
	core::assert_error(m_handle != 0, "%d", GetLastError());
}
//! Destructor
Semaphore::~Semaphore(){
	CloseHandle(m_handle);
}
//! waits for unlock
void Semaphore::wait(){
	WaitForSingleObject(m_handle, INFINITE);
}
//! signals semaphore unlock
void Semaphore::post(){
	ReleaseSemaphore(m_handle, 1, NULL);
}

//////////////////////////////////////////////////////////////////////////