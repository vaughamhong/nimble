//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.mutex.h>
#include <nimble/core/runloop.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Mutex::Mutex(){
	InitializeCriticalSection(&m_lock);
}
//! Destructor
Mutex::~Mutex(){
}
//! locks with this mutex
void Mutex::lock(){
	//if(core::getCurrentRunLoop() == core::getMainRunLoop()){
	//	if(TryEnterCriticalSection(&m_lock) != 0){
	//		return;
	//	}else{
	//		OutputDebugString("Main runloop is blocked...\n");
	//		EnterCriticalSection(&m_lock);
	//		return;
	//	}
	//}
	EnterCriticalSection(&m_lock);
}
//! tries to lock with this mutex
bool Mutex::tryLock(){
	return (TryEnterCriticalSection(&m_lock) != 0);
}
//! unlocks with this mutex
void Mutex::unlock(){
	LeaveCriticalSection(&m_lock);
}

//////////////////////////////////////////////////////////////////////////