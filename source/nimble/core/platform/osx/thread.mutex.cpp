//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.mutex.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <nimble/core/runloop.h>
#include <errno.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Mutex::Mutex(bool isRecursive){
    if(isRecursive){
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        int error = pthread_mutex_init(&m_lock, &attr);
        NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
    }else{
        int error = pthread_mutex_init(&m_lock, 0);
        NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
    }
}
//! Destructor
Mutex::~Mutex(){
    int error = pthread_mutex_destroy(&m_lock);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}
//! locks with this mutex
void Mutex::lock(){
    // Tests if our main loop gets blocked waiting for a lock
    if(core::getCurrentRunLoop() == core::getMainRunLoop()){
        if(pthread_mutex_trylock(&m_lock) == 0){
            return;
        }else{
            printf("Main loop is blocked...\n");
            int error = pthread_mutex_lock(&m_lock);
            NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
            return;
        }
    }
    
    int error = pthread_mutex_lock(&m_lock);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}
//! tries to lock
bool Mutex::tryLock(){
    return pthread_mutex_trylock(&m_lock);
}
//! unlocks with this mutex
void Mutex::unlock(){
    int error = pthread_mutex_unlock(&m_lock);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}

//////////////////////////////////////////////////////////////////////////