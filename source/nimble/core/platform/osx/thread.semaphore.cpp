//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.semaphore.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <errno.h>
#include <string.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Semaphore::Semaphore(const char *name, int32_t value)
:m_pSem(0){
    sem_unlink(name);
    m_pSem = sem_open(name, O_CREAT, 0600, value);
    NIMBLE_ASSERT_PRINT(m_pSem != SEM_FAILED, strerror(errno));
}
//! Destructor
Semaphore::~Semaphore(){
    int error = sem_close(m_pSem);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}

//! waits for unlock
void Semaphore::wait(){
    int error = sem_wait(m_pSem);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}
//! signals semaphore unlock
void Semaphore::post(){
    int error = sem_post(m_pSem);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}

//////////////////////////////////////////////////////////////////////////