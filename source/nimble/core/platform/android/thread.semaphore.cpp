//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.semaphore.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <errno.h>
#include <fcntl.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Semaphore::Semaphore(const char *name, int32_t value){
    int error = sem_init(&m_sem, 0, value);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}
//! Destructor
Semaphore::~Semaphore(){
    int error = sem_destroy(&m_sem);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}

//! waits for unlock
void Semaphore::wait(){
    int error = sem_wait(&m_sem);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}
//! signals semaphore unlock
void Semaphore::post(){
    int error = sem_post(&m_sem);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}

//////////////////////////////////////////////////////////////////////////