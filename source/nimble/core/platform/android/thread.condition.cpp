//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.condition.h>
#include <nimble/core/logger.h>
#include <nimble/core/assert.h>
#include <errno.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Condition::Condition(){
    int error = pthread_cond_init(&m_condition, 0);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}
//! Destructor
Condition::~Condition(){
    int error = pthread_cond_destroy(&m_condition);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}

//////////////////////////////////////////////////////////////////////////

//! waits for unlock
void Condition::wait(core::Mutex *pMutex){
    int error = pthread_cond_wait(&m_condition, &pMutex->m_lock);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}
//! signals unlock
void Condition::signal(){
    int error = pthread_cond_signal(&m_condition);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}
//! broadcast unlock
void Condition::broadcast(){
    int error = pthread_cond_broadcast(&m_condition);
    NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
}

//////////////////////////////////////////////////////////////////////////