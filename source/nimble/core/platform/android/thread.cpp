//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/platform/osx/thread.h>
#include <nimble/core/thread.h>
#include <nimble/core/thread.mutex.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/functor.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <vector>
#include <map>
#include <unistd.h>
#include <errno.h>

//////////////////////////////////////////////////////////////////////////

int32_t nimble::core::Thread::threadIdCount = (nimble::core::kMainThreadId + 1);
nimble::core::Mutex nimble::core::Thread::threadIdCountLock;
extern nimble::core::ThreadLocalStorage<nimble::core::Thread> g_threadLocalThread;

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

typedef core::Functor<void> ThreadCallbackFunc;
struct nimble::core::threadData{
    core::Thread            *pThread;
    int32_t                 threadId;
    core::ThreadMainFunc    main;
    ThreadCallbackFunc      begin, end;
};

//////////////////////////////////////////////////////////////////////////

//! Thread main helper
void* threadMain(void *data){
    core::threadData *pThreadData = (core::threadData*)data;
    
    // setting up our local storage threadId here means it will be valid
    // before anyone calls getCurrentThreadId().
    g_threadLocalThread = pThreadData->pThread;

    // runs thread main
    pThreadData->begin();
    pThreadData->main();
    pThreadData->end();
    
    // exit
    pthread_exit(0);
	return 0;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
Thread::Thread(ThreadMainFunc main, core::eThreadPriority priority, size_t stackSize)
:m_threadId(-1)
,m_priority(priority)
,m_stackSize(stackSize)
,m_state(kStateStopped){
    // assign a threadId to this thread object
    // we pass this down to our thread execution
    // which will set up the corresponding thread local storage
    
    // TODO: consider wrapping this as an atomic counter
    core::ScopedLock lock(&threadIdCountLock);
    m_threadId = threadIdCount++;

    // create thread attributes
    pthread_attr_init(&m_threadAttr);
    
    // set our thread params / attributes
    pthread_attr_setdetachstate(&m_threadAttr, PTHREAD_CREATE_JOINABLE);
    pthread_attr_setstacksize(&m_threadAttr, stackSize);
    
    // set priority
    if(m_priority == core::kThreadPriorityNormal){
        pthread_attr_setschedpolicy(&m_threadAttr, SCHED_OTHER);
    }else if(m_priority == core::kThreadPriorityBackground){
        struct sched_param param;
        param.sched_priority = sched_get_priority_min(SCHED_OTHER);
        pthread_attr_setschedpolicy(&m_threadAttr, SCHED_OTHER);
        pthread_attr_setschedparam(&m_threadAttr, &param);
    }
    
    // setup our thread data
    m_pThreadData = new /*( pool )*/ threadData();
    m_pThreadData->pThread = this;
    m_pThreadData->threadId = m_threadId;
    m_pThreadData->main = main;
    m_pThreadData->begin = ThreadCallbackFunc(this, &core::Thread::onThreadBegin);
    m_pThreadData->end = ThreadCallbackFunc(this, &core::Thread::onThreadEnd);
}
//! Destructor
Thread::~Thread(){
    delete m_pThreadData;
    m_pThreadData = 0;
    this->join();
    pthread_attr_destroy(&m_threadAttr);
}

//////////////////////////////////////////////////////////////////////////

//! Starts running thread
void Thread::start(){
    if(m_state == kStateStopped){
        core::ScopedLock lock(&m_stateLock);
        m_state = kStateStarting;

        int error = pthread_create(&m_thread, &m_threadAttr, threadMain, (void *)m_pThreadData);
        NIMBLE_ASSERT_PRINT(error == 0, strerror(errno));
    }else{
        NIMBLE_LOG_WARNING("thread", "Failed to start thread - thread is already running");
    }
}
//! Wait until thread has stopped
void Thread::join(){
    if(m_state != kStateStopped){
        pthread_join(m_thread, 0);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Returns true if thread is stopped
bool Thread::isStopped() const{
    return (m_state == kStateStopped);
}
//! Returns true if thread is running
bool Thread::isRunning() const{
    return (m_state == kStateRunning) || (m_state == kStateStarting);
}
//! Returns true if thread is main thread
bool Thread::isMainThread() const{
    return false;
}
//! returns the threadId
int32_t Thread::getThreadId() const{
    return m_threadId;
}
//! returns the thread priority
eThreadPriority Thread::getThreadPriority() const{
    return m_priority;
}

//////////////////////////////////////////////////////////////////////////

//! called when thread has started
void Thread::onThreadBegin(){
    core::ScopedLock lock(&m_stateLock);
    m_state = kStateRunning;
    NIMBLE_LOG_INFO("thread", "Thread has started running (threadId %d)", m_threadId);
}
//! called when thread has exited
void Thread::onThreadEnd(){
    core::ScopedLock lock(&m_stateLock);
    m_state = kStateStopped;
    NIMBLE_LOG_INFO("thread", "Thread has stopped running (threadId %d)", m_threadId);
}

//////////////////////////////////////////////////////////////////////////

