//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/propertytree.h>
#include <nimble/core/filesystem.h>
#include <nimble/resource/resource.h>
#include <fstream>

//////////////////////////////////////////////////////////////////////////

#include "tinyxml2.h"
#include <json/json.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Recursively populate property map
void recursivePopulatePropertyTree(core::PropertyTree &tree, tinyxml2::XMLElement const *pNode){
    for(tinyxml2::XMLAttribute const *pAttribute = pNode->FirstAttribute(); pAttribute != 0; pAttribute = pAttribute->Next()){
        const char *name = pAttribute->Name();
        const char *value = pAttribute->Value();
        tree.add(name, value);
    }
    for(tinyxml2::XMLElement const *pChild = pNode->FirstChildElement(); pChild != 0; pChild = pChild->NextSiblingElement()){
        const char *name = pChild->Value();
        const char *value = pChild->GetText() ? pChild->GetText() : "";
        PropertyTree &child = tree.add(name, value);
        recursivePopulatePropertyTree(child, pChild);
    }
}
//! Recursively populate property map
void recursivePopulatePropertyTree(core::PropertyTree &tree, Json::Value const &node){
    for(Json::Value::iterator it = node.begin(); it != node.end(); it++){
        Json::Value &nodeChild = *it;
        
        if(nodeChild.isArray()){
            const char *name = it.memberName();
            const char *value = "";
            PropertyTree &child = tree.add(name, value);
            recursivePopulatePropertyTree(child, *nodeChild.begin());
        }else{
            const char *name = it.memberName();
            const char *value = nodeChild.asCString();
            tree.add(name, value);
        }
    }
}

//////////////////////////////////////////////////////////////////////////

//! Loads property tree from xml file
void PropertyTree::loadFromXMLFile(const char *path){
    std::fstream fileStream(path, std::fstream::in | std::fstream::out | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("core", "Failed to open file %s", path);
        return;
    }
    
    // load and parse our xml file
    long fileSize = core::fileSize(path);
    size_t bufferSize = fileSize + 1;
    char *pBuffer = new /*( volatile )*/ char[bufferSize];
    fileStream.seekg(0, std::ios::beg);
    fileStream.read(pBuffer, fileSize);
    pBuffer[fileSize] = 0;
    fileStream.close();
    tinyxml2::XMLDocument doc;
    tinyxml2::XMLError error = doc.Parse(pBuffer);
    
    if(error == tinyxml2::XML_SUCCESS){
        // decode into our property tree
        this->m_name = "";
        this->m_value = "";
        for(tinyxml2::XMLElement *pNode = doc.FirstChildElement(); pNode != 0; pNode = pNode->NextSiblingElement()){
            // set our name and data
            const char *name = pNode->Name();
            const char *value = pNode->GetText() ? pNode->GetText() : "";
            PropertyTree &child = this->add(name, value);
            recursivePopulatePropertyTree(child, pNode);
        }
    }else{
        NIMBLE_LOG_ERROR("core", "Failed to parse xml file %s", path);
    }
    
    // clean up
    delete [] pBuffer;
}
//! Loads property tree from json file
void PropertyTree::loadFromJSONFile(const char *path){
    std::fstream fileStream(path, std::fstream::in | std::fstream::out | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("core", "Failed to open file %s", path);
        return;
    }

    // load and parse our json file
    long fileSize = core::fileSize(path);
    size_t bufferSize = fileSize + 1;
    char *pBuffer = new /*( volatile )*/ char[bufferSize];
    fileStream.seekg(0, std::ios::beg);
    fileStream.read(pBuffer, fileSize);
    pBuffer[fileSize] = 0;
    fileStream.close();

    Json::Value root;
    Json::Reader reader;
    if(reader.parse(&pBuffer[0], &pBuffer[fileSize], root) == true){
        // decode into our property tree
        this->m_name = "";
        this->m_value = "";
        
        for(Json::Value::iterator it = root.begin(); it != root.end(); it++){
            Json::Value &nodeChild = *it;
            
            if(nodeChild.isArray()){
                const char *name = it.memberName();
                const char *value = "";
                PropertyTree &child = this->add(name, value);
                recursivePopulatePropertyTree(child, *nodeChild.begin());
            }else{
                const char *name = it.memberName();
                const char *value = nodeChild.asCString();
                this->add(name, value);
            }
        }
    }else{
        NIMBLE_LOG_ERROR("core", "Failed to parse json file %s", path);
    }
    
    // clean up
    delete [] pBuffer;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
PropertyTree::PropertyTree()
:m_name("")
,m_value(""){
}
//! Constructor
PropertyTree::PropertyTree(PropertyTree const &tree)
:m_name(tree.getName())
,m_value(tree.getValue()){
    copy(tree);
}
//! Constructor
PropertyTree::PropertyTree(std::string const &name, std::string const &value)
:m_name(name)
,m_value(value){
}
//! Constructor
PropertyTree::PropertyTree(const char *name, const char *value)
:m_name(name)
,m_value(value){
}
//! Destructor
PropertyTree::~PropertyTree(){
}

//////////////////////////////////////////////////////////////////////////

//! copy operator
PropertyTree& PropertyTree::operator=(PropertyTree const &tree){
    copy(tree);
    return *this;
}
//! equality operator
bool PropertyTree::operator==(PropertyTree const &tree) const{
    return (m_name == tree.m_name) && (m_value == tree.m_value) && (m_children == tree.m_children);
}
//! inequality operator
bool PropertyTree::operator!=(PropertyTree const &tree) const{
    return !PropertyTree::operator==(tree);
}

//////////////////////////////////////////////////////////////////////////

//! adds a property
PropertyTree& PropertyTree::add(const char *name, const char *value){
    m_children.push_back(PropertyTree(name, value));
    return m_children.back();
}
//! adds a property
PropertyTree& PropertyTree::add(const char *name, int32_t value){
    m_children.push_back(PropertyTree(name, core::lexical_cast(value)));
    return m_children.back();
}
//! removes a property
void PropertyTree::remove(iterator it){
    m_children.erase(it);
}

//! returns the first property
PropertyTree::iterator PropertyTree::begin(){
    return m_children.begin();
}
//! returns the last property
PropertyTree::iterator PropertyTree::end(){
    return m_children.end();
}
//! returns the first property
PropertyTree::const_iterator PropertyTree::begin() const{
    return m_children.begin();
}
//! returns the last property
PropertyTree::const_iterator PropertyTree::end() const{
    return m_children.end();
}
//! returns true if property with a name exists
bool PropertyTree::existsChildWithName(const char *name) const{
    PropertyTree::PtrList list;
    filterChildrenWithName(name, list);
    return (list.size() > 0);
}
//! filters for chidlren with name
void PropertyTree::filterChildrenWithName(const char *name, PtrList &list) const{
    list.clear();
    for(PropertyTree::List::const_iterator it = begin(); it != end(); it++){
        if(strcmp(it->getName(), name) == 0){
            list.push_back(const_cast<PropertyTree*>(&(*it)));
        }
    }
}
//! returns list of children with name
PropertyTree::PtrList PropertyTree::operator[](const char *name) const{
    PropertyTree::PtrList list;
    filterChildrenWithName(name, list);
    return list;
}

//////////////////////////////////////////////////////////////////////////

//! sets our value
void PropertyTree::setValue(const char *value){
    m_value = value;
}

//////////////////////////////////////////////////////////////////////////

//! returns the name
const char* PropertyTree::getName() const{
    return m_name.c_str();
}
//! returns the value
const char* PropertyTree::getValue() const{
    return m_value.c_str();
}

//////////////////////////////////////////////////////////////////////////

//! returns the value as a float
float PropertyTree::getFloatValue() const{
    return (float)::atof(m_value.c_str());
}
//! returns the value as a integer
int PropertyTree::getIntValue() const{
    return ::atoi(m_value.c_str());
}
//! returns the value as a boolean
bool PropertyTree::getBoolValue() const{
    return strcmp(m_value.c_str(), "true") == 0 ||
    strcmp(m_value.c_str(), "True") == 0 ||
    strcmp(m_value.c_str(), "TRUE") == 0 ||
    strcmp(m_value.c_str(), "yes") == 0 ||
    strcmp(m_value.c_str(), "Yes") == 0 ||
    strcmp(m_value.c_str(), "YES") == 0 ||
    strcmp(m_value.c_str(), "1") == 0;
}

//////////////////////////////////////////////////////////////////////////

//! copies a propert tree
PropertyTree& PropertyTree::copy(PropertyTree const &tree){
    m_name = tree.m_name;
    m_value = tree.m_value;
    m_children = tree.m_children;
    return *this;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
PropertyTreeXMLLoader::PropertyTreeXMLLoader(){
}
//! Destructor
PropertyTreeXMLLoader::~PropertyTreeXMLLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* PropertyTreeXMLLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<core::PropertyTree>();
    core::PropertyTree *pTree = dynamic_cast<core::PropertyTree*>(pResource);
    pTree->loadFromXMLFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
PropertyTreeJSONLoader::PropertyTreeJSONLoader(){
}
//! Destructor
PropertyTreeJSONLoader::~PropertyTreeJSONLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* PropertyTreeJSONLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<core::PropertyTree>();
    core::PropertyTree *pTree = dynamic_cast<core::PropertyTree*>(pResource);
    pTree->loadFromJSONFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////