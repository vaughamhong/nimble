//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.runloop.h>
#include <nimble/core/thread.semaphore.h>
#include <nimble/core/thread.localstorage.h>
#include <nimble/core/thread.h>
#include <nimble/core/runloop.h>
#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <sstream>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
RunLoopThread::RunLoopThread(core::eThreadPriority priority)
:m_pRunLoop(0)
,m_pThread(0)
,m_stopRunning(false){
    m_pRunLoop = new /*( static )*/ core::RunLoop();
    m_pThread = new /*( static )*/ core::Thread(core::ThreadMainFunc(this, &core::RunLoopThread::main), priority);
}
//! Destructor
RunLoopThread::~RunLoopThread(){
    this->stop();
    delete m_pThread;
    m_pThread = 0;
}

//////////////////////////////////////////////////////////////////////////

//! Starts running thread
void RunLoopThread::start(){
    if(!this->isRunning()){
        m_pThread->start();
    }
}
//! Stops running loop
void RunLoopThread::stop(){
    if(this->isRunning()){
        m_stopRunning = true;
        this->join();
    }
}
//! Wait until thread has stopped
void RunLoopThread::join(){
    m_pThread->join();
}

//! Returns true if thread is stopped
bool RunLoopThread::isStopped() const{
    return m_pThread->isStopped();
}
//! Returns true if thread is running
bool RunLoopThread::isRunning() const{
    return m_pThread->isRunning();
}
//! Returns true if thread is main thread
bool RunLoopThread::isMainThread() const{
    return m_pThread->isMainThread();
}

//! returns the threadId
int32_t RunLoopThread::getThreadId() const{
    return m_pThread->getThreadId();
}
//! returns the thread priority
core::eThreadPriority RunLoopThread::getThreadPriority() const{
    return m_pThread->getThreadPriority();
}

//////////////////////////////////////////////////////////////////////////

//! called when thread has started
void RunLoopThread::onThreadBegin(){
}
//! called when thread has exited
void RunLoopThread::onThreadEnd(){
}

//////////////////////////////////////////////////////////////////////////

//! runs a synchronous operation on this run loop
//! \param operation the operation to run
void RunLoopThread::runSync(core::RunOperation operation){
    if((this->getThreadId() == core::getCurrentThreadId())){
        NIMBLE_LOG_ERROR("thread", "Failed to perform operation - waiting until done on the same thread (deadlock detected)");
        return;
    }
    if(!this->isRunning()){
        NIMBLE_LOG_ERROR("thread", "Failed to perform operation - thread loop is not in a running state (potential deadlock detected)");
        return;
    }
    m_pRunLoop->runSync(operation);
}
//! runs a asynchronous operation on this run loop
//! \param operation the operation to run
void RunLoopThread::runAsync(core::RunOperation operation){
    m_pRunLoop->runAsync(operation);
}

//////////////////////////////////////////////////////////////////////////

//! Adds a runloop operation
//! \param operation adds a runloop operation to execute
void RunLoopThread::addLoopOperation(core::RunLoopOperation operation){
    m_pRunLoop->addLoopOperation(operation);
}
//! Removes a runloop operation
//! \param operation removes a runloop operation from execution
void RunLoopThread::removeLoopOperation(core::RunLoopOperation operation){
    m_pRunLoop->removeLoopOperation(operation);
}

//////////////////////////////////////////////////////////////////////////

//! thread starting point
void RunLoopThread::main(){
    core::setCurrentRunLoop(dynamic_cast<nimble::core::IRunLoop*>(this));
    onThreadBegin();
    while(!m_stopRunning){
        m_pRunLoop->update();
    }
    m_stopRunning = false;
    onThreadEnd();
}

//////////////////////////////////////////////////////////////////////////

