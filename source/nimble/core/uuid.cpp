//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/uuid.h>
#include <nimble/core/assert.h>

#if defined(NIMBLE_TARGET_WIN32)
	#include <Windows.h>
#elif defined(NIMBLE_TARGET_OSX)
	#include <uuid/uuid.h>
#endif

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! generates a guid
std::string nimble::core::uuid(){
#if defined(NIMBLE_TARGET_WIN32)
	GUID uuid;
	char* buffer;
	UuidCreate(&uuid);
	UuidToString(&uuid, (RPC_CSTR*)&buffer);
	std::string s = buffer;
	RpcStringFree((RPC_CSTR*)&buffer);
	return s;
#elif defined(NIMBLE_TARGET_OSX)
    uuid_t uuid;
    uuid_generate_random(uuid);
    char s[37];
    uuid_unparse(uuid, s);
    return s;
#elif defined(NIMBLE_TARGET_ANDROID)
    NIMBLE_ASSERT_PRINT(false, "TODO - implement");
    return "";
#endif
}

//////////////////////////////////////////////////////////////////////////