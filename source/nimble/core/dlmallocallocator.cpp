//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/dlmallocallocator.h>
#include <nimble/core/dlmalloc.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! constructor
DLMallocAllocator::DLMallocAllocator(){
}
//! destructor
DLMallocAllocator::~DLMallocAllocator(){
}

//////////////////////////////////////////////////////////////////////////

//! allocates memory - alignment must be suitable for any data type
//! \return allocated aligned memory
void* DLMallocAllocator::allocate(size_t size){
    return dlmalloc(size);
}
//! deallocates memory
void DLMallocAllocator::deallocate(void* pAllocation){
    dlfree(pAllocation);
}

//////////////////////////////////////////////////////////////////////////