//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/stringutil.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

#if defined(NIMBLE_TARGET_WIN32)
#include <Windows.h>
#endif

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

// converts wchar_t strings to utf8 encoded std::string
std::string nimble::core::utf8_encode(const std::wstring &wstr){
#if defined(NIMBLE_TARGET_WIN32)
    int size = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string str(size, 0);
    WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &str[0], size, NULL, NULL);
    return str;
#else
    NIMBLE_ASSERT_PRINT(false, "Failed to convert wstring to string - no implementation on this platform");
    return "";
#endif
}
// replaces all strings that match 'from' to 'to'
void nimble::core::replaceAll(std::string& str, const std::string& from, const std::string& to){
    if(from.empty()){
        return;
    }
    size_t startPosition = 0;
    while((startPosition = str.find(from, startPosition)) != std::string::npos){
        str.replace(startPosition, from.length(), to);
        startPosition += to.length();
    }
}

//////////////////////////////////////////////////////////////////////////

