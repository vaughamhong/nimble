//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/statemachine.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! default constructor
State::State()
:m_pStateMachine(0){
}
//! a destructor
State::~State(){
}

//////////////////////////////////////////////////////////////////////////

//! called when we enter the state
void State::onStateEnter(){
}
//! called when we leave the state
void State::onStateExit(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void State::update(double interval){
}

//////////////////////////////////////////////////////////////////////////

//! Returns true if still active
//! \return true if still active
bool State::isActive(){
    return (getStateMachine() !=  0);
}

//////////////////////////////////////////////////////////////////////////

//! returns the state machine
core::StateMachine* State::getStateMachine(){
    return m_pStateMachine;
}
//! sets the state machine
void State::setStateMachine(core::StateMachine *pStateMachine){
    m_pStateMachine = pStateMachine;
}

//////////////////////////////////////////////////////////////////////////

//! default constructor
StateMachine::StateMachine(){
}
//! a destrutor
StateMachine::~StateMachine(){
}

//////////////////////////////////////////////////////////////////////////

//! Returns the active state
StateMachine::StatePtr StateMachine::getActiveState(){
    return m_activeState;
}
//! Switches the top state
void StateMachine::switchState(StatePtr state){
    if(m_activeState.isValid()){
        m_activeState->onStateExit();
        m_activeState->setStateMachine(0);
    }
    m_activeState = state;
    if(m_activeState.isValid()){
        m_activeState->setStateMachine(this);
        m_activeState->onStateEnter();
    }
}
//! Updates relative to an interval
void StateMachine::update(double interval){
    if(m_activeState.isValid()){
        m_activeState->update(interval);
    }
}

//////////////////////////////////////////////////////////////////////////