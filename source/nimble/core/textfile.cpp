//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/textfile.h>
#include <nimble/core/stream.buffer.h>
#include <nimble/core/filesystem.h>
#include <errno.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
TextFile::TextFile(core::IAllocator *pAllocator)
:m_buffer(pAllocator){
}
//! Constructor
TextFile::TextFile(const char *path, core::IAllocator *pAllocator)
:m_buffer(pAllocator){
    loadFromFile(path);
}
//! Destructor
TextFile::~TextFile(){
}

//////////////////////////////////////////////////////////////////////////

//! Load from file
void TextFile::loadFromFile(const char *path){
    // open our file stream
    std::fstream fileStream(path, std::fstream::in | std::fstream::out | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("system", "Failed to open file %s - %s", path, strerror(errno));
        return;
    }

    // find our file size
    long fileSize = core::fileSize(path);
    
    // allocate one larger for null termination
    m_buffer.resize(fileSize + 1);
    
    // copy file into memory
    char *pBuffer = m_buffer.getPointer();
    fileStream.seekg(0, std::ios::beg);
    fileStream.read(pBuffer, fileSize);
    pBuffer[fileSize] = 0;
    fileStream.close();
}
//! Returns a const pointer
const char* TextFile::getConstPointer() const{
    return m_buffer.getConstPointer();
}
//! Returns buffer
char* TextFile::getPointer() const{
    return m_buffer.getPointer();
}
//! Returns the file length
size_t TextFile::getSize() const{
    return m_buffer.getSize();
}

///////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
TextFileLoader::TextFileLoader(){
}
//! Destructor
TextFileLoader::~TextFileLoader(){
}
//! loads a resource
//! \param path the path of the file we want to load
resource::IResource* TextFileLoader::loadResource(const char* path){
    resource::IResource *pResource = new /*( external dynamic )*/ resource::ResourceWrapper<core::TextFile>();
    core::TextFile *pObject = dynamic_cast<core::TextFile*>(pResource);
    pObject->loadFromFile(path);
    return pResource;
}

//////////////////////////////////////////////////////////////////////////