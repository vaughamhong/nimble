//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/mallocallocator.h>
#include <cstdlib>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! constructor
MallocAllocator::MallocAllocator(){
}
//! destructor
MallocAllocator::~MallocAllocator(){
}

//////////////////////////////////////////////////////////////////////////

//! allocates memory - alignment must be suitable for any data type
//! \return allocated aligned memory
void* MallocAllocator::allocate(size_t size){
    return malloc(size);
}
//! deallocates memory
void MallocAllocator::deallocate(void* pAllocation){
    free(pAllocation);
}

//////////////////////////////////////////////////////////////////////////