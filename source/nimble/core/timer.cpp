//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/timer.h>
#include <nimble/core/logger.h>
#include <nimble/core/assert.h>
#include <stdlib.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! constuctor
Timer::Timer()
:m_startTime(0)
,m_endTime(0)
,m_stopped(false){
#if defined(NIMBLE_TARGET_WIN32)
    QueryPerformanceFrequency(&frequency);
    startTick.QuadPart = 0;
    endTick.QuadPart = 0;
#elif defined(NIMBLE_TARGET_OSX)
    mach_timebase_info(&m_info);
#endif
}
//! Destructor
Timer::~Timer(){
}

//////////////////////////////////////////////////////////////////////////

//! starts the timer
void Timer::start(){
    m_stopped = false;
#if defined(NIMBLE_TARGET_WIN32)
    QueryPerformanceCounter(&startTick);
#elif defined(NIMBLE_TARGET_OSX)
    startTick = mach_absolute_time();
#elif defined(NIMBLE_TARGET_ANDROID)
    clock_gettime(CLOCK_MONOTONIC, &m_start);
#endif
}
void Timer::stop(){
    m_stopped = true;
#if defined(NIMBLE_TARGET_WIN32)
    QueryPerformanceCounter(&endTick);
#elif defined(NIMBLE_TARGET_OSX)
    endTick = mach_absolute_time();
#elif defined(NIMBLE_TARGET_ANDROID)
    clock_gettime(CLOCK_MONOTONIC, &m_end);
#endif
}
//! reset
void Timer::reset(){
    this->stop();
    this->start();
}

//////////////////////////////////////////////////////////////////////////

//! returns the elapsed ns
double Timer::getElapsedTimeInNanoSeconds(){
#if defined(NIMBLE_TARGET_WIN32)
    if(!m_stopped){
        QueryPerformanceCounter(&endTick);
	}
    m_startTime = 1000000000.0 * startTick.QuadPart / frequency.QuadPart;
    m_endTime = 1000000000.0 * endTick.QuadPart / frequency.QuadPart;
    return m_endTime - m_startTime;
#elif defined(NIMBLE_TARGET_OSX)
    if(!m_stopped)
        endTick = mach_absolute_time();
    m_startTime = startTick * m_info.numer / m_info.denom;
    m_endTime = endTick * m_info.numer / m_info.denom;
    return m_endTime - m_startTime;
#elif defined(NIMBLE_TARGET_ANDROID)
    if(!m_stopped)
        clock_gettime(CLOCK_MONOTONIC, &m_end);
    m_startTime = m_start.tv_sec * 1000000000LL + m_start.tv_nsec;
    m_endTime = m_end.tv_sec * 1000000000LL + m_end.tv_nsec;
    return m_endTime - m_startTime;
#endif
}
//////////////////////////////////////////////////////////////////////////

double Timer::getElapsedTimeInMicroSeconds(){
    return this->getElapsedTimeInNanoSeconds() * 0.001;
}
double Timer::getElapsedTimeInMilliSeconds(){
    return this->getElapsedTimeInNanoSeconds() * 0.000001;
}
double Timer::getElapsedTimeInSeconds(){
    return this->getElapsedTimeInNanoSeconds() * 0.000000001;
}
double Timer::getElapsedTicks(){
#if defined(NIMBLE_TARGET_WIN32)
    if(!m_stopped)
        QueryPerformanceCounter(&endTick);
    return (double)(endTick.QuadPart - startTick.QuadPart);
#elif defined(NIMBLE_TARGET_OSX)
    if(!m_stopped)
        endTick = mach_absolute_time();
    return endTick - startTick;
#elif defined(NIMBLE_TARGET_ANDROID)
    return 0.0;
#endif
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
ScopedTimer::ScopedTimer(Timer &timer)
:m_timer(timer){
    m_timer.reset();
}
//! Destructor
ScopedTimer::~ScopedTimer(){
    m_timer.stop();
}
//! Assignment operator
ScopedTimer& ScopedTimer::operator = (ScopedTimer const &timer){
	m_timer = timer.m_timer;
	return *this;
}

//////////////////////////////////////////////////////////////////////////

