//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/smallblockallocator.h>
#include <cstdlib>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! constructor
SmallBlockAllocator::SmallBlockAllocator(){
}
//! destructor
SmallBlockAllocator::~SmallBlockAllocator(){
}

//////////////////////////////////////////////////////////////////////////

//! allocates memory - alignment must be suitable for any data type
//! \return allocated aligned memory
void* SmallBlockAllocator::allocate(size_t size){
    return malloc(size);
}
//! deallocates memory
void SmallBlockAllocator::deallocate(void* pAllocation){
    free(pAllocation);
}

// bucket size policy
class BucketSizePolicy{
private:
    
    static const unsigned int BASE_ALLOCATION_SIZE			= 16;		//!< smallest allocation size
    static const unsigned int SMALL_ALLOCATION_SIZE			= 16;		//!< allocation increments for small allocations
    static const unsigned int SMALL_ALLOCATION_MARKER		= 128;		//!< end of small allocation marker
    static const unsigned int MEDIUM_ALLOCATION_SIZE		= 128;		//!< allocation increments for medium allocations
    static const unsigned int MEDIUM_ALLOCATION_MARKER		= 2048;		//!< end of medium allocation marker
    static const unsigned int LARGE_ALLOCATION_SIZE			= 1024;		//!< allocation increments for large allocations
    
public:
    
    //! returns the bucket size given an index
    static unsigned int getBucketSize(unsigned int bucketIndex){
        unsigned int bucketSize = BASE_ALLOCATION_SIZE;
        for(unsigned int i = 0;; ++i){
            // initialize bucket sizes
            if(bucketSize < SMALL_ALLOCATION_MARKER){
                bucketSize += SMALL_ALLOCATION_SIZE;
            }else if(bucketSize < MEDIUM_ALLOCATION_MARKER){
                bucketSize += MEDIUM_ALLOCATION_SIZE;
            }else{
                bucketSize += LARGE_ALLOCATION_SIZE;
            }
            
            // exit when bucket size found
            if(i == bucketIndex){
                break;
            }
        }
        return bucketSize;
    }
};

////! top level heap that uses malloc/free
//template<int PRE_ALLOCATION_SPACE, int NUM_BLOCKS = kDefaultNumBlocks, int NUM_BUCKETS = kDefaultNumBuckets, typename BUCKET_SIZE_POLICY = BucketSizePolicy>
//class CustomHeapBase: public Heap{
//public:
//    
//    static const unsigned int kDefaultNumBlocks = 1024;
//    static const unsigned int kDefaultNumBuckets = 32;
//    
//private:
//    
//    static const unsigned int BLOCKNAME_STRING_SIZE			= 64;		//!< number of allocation buckets
//    
//    // allocation flags
//    enum eAllocationBlockFlag{
//        ALLOCATION_FLAG_FREE		= (1 << 0),			//!< a free block to be consumed
//        ALLOCATION_FLAG_USED		= (1 << 1),			//!< a used block
//    };
//    
//    struct blockHeader_t;
//    struct freeBlocksBucket_t;
//    
//    // a header describing the allocation block
//    struct blockHeader_t{
//        blockHeader_t*				pNextPhysicalBlock;				//!< pointer to the next physical block
//        blockHeader_t*				pPrevPhysicalBlock;				//!< pointer to the prev physical block
//        blockHeader_t*				pNextBucketBlock;				//!< pointer to the next bucket block
//        blockHeader_t*				pPrevBucketBlock;				//!< pointer to the prev bucket block
//        freeBlocksBucket_t*			pBucket;						//!< the bucket this block belongs to
//        char*						pMemory;						//!< pointer to our memory
//        unsigned int				size;							//!< the size of the block
//        eAllocationBlockFlag		flag;							//!< block state
//        char						name[BLOCKNAME_STRING_SIZE];	//!< block name string size
//    };
//    
//    // holds a set of free blocks to be search friendly
//    struct freeBlocksBucket_t{
//        unsigned int		size;			//!< size of free blocks that fit in this bucket (including header size)
//        blockHeader_t*		pFirstBlock;	//!< pointer to the first free block in this bucket
//    };
//    
//    blockHeader_t		m_block[NUM_BLOCKS];				//!< our total number of blocks
//    freeBlocksBucket_t	m_freeBuckets[NUM_BUCKETS];			//!< our free block buckets
//    
//    blockHeader_t*		m_freeBlockHeaderStack[NUM_BLOCKS];	//!< the free block header stack
//    unsigned int		m_freeBlockHeaderStackTop;			//!< the top of the free block header stack
//    
//    char*				m_preAllocatedMemory;				//!< our pre allocated memory
//    unsigned int		m_baseAllocationSize;				//!< base allocation size
//    
//    unsigned int		m_totalMemoryUsage;					//!< the total memory usage
//    unsigned int		m_peakMemoryUsage;					//!< the peak memory usage
//    
//public:
//    
//    //! Constructor
//    CustomHeapBase(){
//        m_preAllocatedMemory = (char*)malloc(PRE_ALLOCATION_SPACE);
//        
//        // initialize our free blocks storage
//        m_freeBlockHeaderStackTop = NUM_BLOCKS;
//        for(unsigned int i = 0; i < NUM_BLOCKS; ++i){
//            memset(&m_block[i], 0, sizeof(blockHeader_t));
//            m_freeBlockHeaderStack[i] = &m_block[i];
//        }
//        
//        // initialize buckets
//        for(unsigned int i = 0; i < NUM_BUCKETS; ++i){
//            m_freeBuckets[i].pFirstBlock = 0;
//            m_freeBuckets[i].size = BUCKET_SIZE_POLICY::getBucketSize(i);
//        }
//        m_baseAllocationSize = BUCKET_SIZE_POLICY::getBucketSize(0);
//        
//        // get a free allocation block
//        blockHeader_t* pFreeBlockHeader = getNewBlock();
//        assert(pFreeBlockHeader != 0);
//        
//        // set the block header
//        memcpy(m_preAllocatedMemory, &pFreeBlockHeader, sizeof(blockHeader_t*));
//        pFreeBlockHeader->flag = ALLOCATION_FLAG_FREE;
//        pFreeBlockHeader->size = PRE_ALLOCATION_SPACE;
//        pFreeBlockHeader->pMemory = m_preAllocatedMemory + sizeof(blockHeader_t*);
//        pFreeBlockHeader->pNextPhysicalBlock = 0;
//        pFreeBlockHeader->pPrevPhysicalBlock = 0;
//        pFreeBlockHeader->pNextBucketBlock = 0;
//        pFreeBlockHeader->pPrevBucketBlock = 0;
//        pFreeBlockHeader->pBucket = &m_freeBuckets[NUM_BUCKETS-1];
//        
//        // assign the free allocation block to the last bucket
//        m_freeBuckets[NUM_BUCKETS-1].pFirstBlock = pFreeBlockHeader;
//        
//        // set as default
//        if(Heap::getGlobalHeap() == 0){
//            Heap::setGlobalHeap(this);
//        }
//    }
//    //! Destructor
//    virtual ~CustomHeapBase(){
//        printContents();
//        
//        free((void*)m_preAllocatedMemory);
//        m_preAllocatedMemory = 0;
//    }
//    
//    //! allocates memory - alignment must be suitable for any data type
//    //! \param size the size to allocate
//    //! \return a pointer to the allocated memory
//    virtual void* allocate(uint32_t &size, uint32_t alignment, uint32_t &offset, const char* name){
//        // 1. search for best available block in buckets (sync)
//        unsigned int sizePlusHeader = size + sizeof(blockHeader_t*);
//        int upperBoundBucketIndex = findBucketIndex(sizePlusHeader, true);
//        
//        // select last bucket if block too large
//        if(upperBoundBucketIndex == -1){
//            upperBoundBucketIndex = NUM_BUCKETS - 1;
//        }
//        
//        // calculate our allocation size
//        unsigned int bucketSize = m_freeBuckets[upperBoundBucketIndex].size;
//        unsigned int allocationSize = bucketSize;
//        if(bucketSize < sizePlusHeader){
//            allocationSize = sizePlusHeader;
//        }
//        
//        // 2. find free block to allocate
//        int currentBucketIndex = upperBoundBucketIndex;
//        freeBlocksBucket_t* pBucket = 0;
//        while(currentBucketIndex < NUM_BUCKETS){
//            if((pBucket = &m_freeBuckets[currentBucketIndex])){
//                if(pBucket->pFirstBlock){
//                    break;
//                }
//            }
//            currentBucketIndex++;
//        }
//        
//        // 3. allocate and return free block
//        if(pBucket && pBucket->pFirstBlock && currentBucketIndex != NUM_BUCKETS){
//            // found bucket with a valid free block!
//            blockHeader_t* pAllocatedBlock = pBucket->pFirstBlock;
//            
//            // remove free block from bucket
//            pBucket->pFirstBlock = pAllocatedBlock->pNextBucketBlock;
//            if(pBucket->pFirstBlock){
//                pBucket->pFirstBlock->pPrevBucketBlock = 0;
//            }
//            pAllocatedBlock->pNextBucketBlock = 0;
//            pAllocatedBlock->pPrevBucketBlock = 0;
//            pAllocatedBlock->pBucket = 0;
//            
//            // do some sanity checks
//            assert(pAllocatedBlock != 0);
//            assert(pAllocatedBlock->flag == ALLOCATION_FLAG_FREE);
//            assert(pAllocatedBlock->size >= allocationSize);
//            
//            // check if we need to split the block
//            if((pAllocatedBlock->size - allocationSize) >= m_baseAllocationSize){
//                // split the block before use!
//                blockHeader_t* pNewFreeBlock = splitBlock(pAllocatedBlock, allocationSize);
//                addBlockToBucket(pNewFreeBlock);
//            }
//            
//            // return memory
//            pAllocatedBlock->flag = ALLOCATION_FLAG_USED;
//#ifdef DEBUG_CUSTOMHEAP
//            if(strcmp(name, "") == 0){
//                strncpy(pAllocatedBlock->name, "???", BLOCKNAME_STRING_SIZE);
//            }else{
//                strncpy(pAllocatedBlock->name, name, BLOCKNAME_STRING_SIZE);
//            }
//            
//            printf("[address: %p][allocating size: %d][name: %s]\n",
//                   pAllocatedBlock->pMemory, allocationSize, pAllocatedBlock->name);
//#endif
//            // update some internal numbers
//            m_totalMemoryUsage += allocationSize;
//            if(m_totalMemoryUsage > m_peakMemoryUsage){
//                m_peakMemoryUsage = m_totalMemoryUsage;
//            }
//            
//            return (void*)pAllocatedBlock->pMemory;
//        }
//        
//        // ran out of memory!
//        assert(false);
//        return 0;
//    }
//    
//    //! deallocates memory
//    //! \param pData pointer to the data to deallocate
//    virtual void deallocate(void* pData){
//        // 1. get the block header
//        char* pDataStartAddress = (char*)(pData);
//        char* pBlockHeaderStartAddress = (char*)(pDataStartAddress - sizeof(blockHeader_t*));
//        
//        blockHeader_t* pBlock = 0;
//        memcpy(&pBlock, pBlockHeaderStartAddress, sizeof(blockHeader_t*));
//        m_totalMemoryUsage -= pBlock->size;
//        
//#ifdef DEBUG_CUSTOMHEAP
//        if(strcmp(pBlock->name, "") != 0){
//            printf("[address: %p][deallocating size: %d][name: %s]\n",
//                   pData, pBlock->size, pBlock->name);
//        }
//#endif
//        
//        // 2. merge adjacent free blocks
//        
//        // set this block to free
//        pBlock->flag = ALLOCATION_FLAG_FREE;
//        addBlockToBucket(pBlock);
//        
//        // see if we can merge adjacent blocks
//        blockHeader_t* mergedBlock = 0;
//        if(pBlock->pNextPhysicalBlock){
//            if((mergedBlock = mergeBlocks(pBlock, pBlock->pNextPhysicalBlock))){
//                pBlock = mergedBlock;
//            }
//        }
//        if(pBlock->pPrevPhysicalBlock){
//            if((mergedBlock = mergeBlocks(pBlock->pPrevPhysicalBlock, pBlock))){
//                pBlock = mergedBlock;
//            }
//        }
//    }
//    //! returns the offset from this allocation
//    uint32_t getOffset(){
//        return T::getOffset();
//    }
//    
//    //! prints the contents of this heap
//    virtual void printContents(){
//#ifdef DEBUG_CUSTOMHEAP
//        printf("==================Memory Report==================\n");
//        printf("[heap][bytes: %d][blocks: %d][buckets: %d]\n", PRE_ALLOCATION_SPACE, NUM_BLOCKS, NUM_BUCKETS);
//        int numUsedBlocks = 0;
//        int numTotalSize = 0;
//        for(int i = 0; i < NUM_BLOCKS; i++){
//            blockHeader_t& block = m_block[i];
//            if(block.flag == ALLOCATION_FLAG_USED){
//                numUsedBlocks += 1;
//                numTotalSize += block.size;
//                printf("[leak][address: %p][size: %d][name: %s]\n",
//                       block.pMemory, block.size, block.name);
//            }
//        }
//        printf("[total leaked][bytes: %d][blocks: %d]\n", numTotalSize, numUsedBlocks);
//        printf("[peak usage: %d bytes]\n", m_peakMemoryUsage);
//        printf("==================================================\n");
//#endif
//    }
//    
//private://! block header functions
//    
//    //! returns a new free block struct
//    blockHeader_t* getNewBlock(){
//        assert(m_freeBlockHeaderStackTop != 0);
//        unsigned int stackTop = m_freeBlockHeaderStackTop-1;
//        blockHeader_t* pFreeBlock = m_freeBlockHeaderStack[stackTop];
//        m_freeBlockHeaderStackTop -= 1;
//        return pFreeBlock;
//    }
//    
//private://! bucket functions
//    
//    //! returns the allocation bucket of a certain size
//    int findBucketIndex(unsigned int size, bool upperBound){
//        return findBucketIndexRecurse(size, 0, NUM_BUCKETS - 1, upperBound);
//    }
//    
//    //! returns the allocation bucket of a certain size
//    int findBucketIndexRecurse(unsigned int size, int min, int max, bool upperBound){
//        int halfSize = (max - min) / 2;
//        int mid = min + halfSize;
//        
//        // if we have two items left to compare
//        if(halfSize == 0){
//            if(upperBound){
//                if(size < m_freeBuckets[max].size){
//                    if(size < m_freeBuckets[min].size){
//                        // size < [min] < max
//                        return min;
//                    }else{
//                        // min < size < [max]
//                        return max;
//                    }
//                }else{
//                    // min < max < size
//                    return -1;
//                }
//            }else{
//                if(size < m_freeBuckets[max].size){
//                    if(size < m_freeBuckets[min].size){
//                        // size < min < max
//                        return -1;
//                    }else{
//                        // [min] < size < max
//                        return min;
//                    }
//                }else{
//                    // min < [max] < size
//                    return max;
//                }
//            }
//            assert(false);
//            return -1;
//        }
//        
//        // recurse on halves
//        if(size < m_freeBuckets[mid].size){
//            return findBucketIndexRecurse(size, min, mid, upperBound);
//        }else{
//            return findBucketIndexRecurse(size, mid, max, upperBound);
//        }
//    }
//    
//    //! adds a block to a bucket
//    void addBlockToBucket(blockHeader_t* pBlock){
//        assert(pBlock != 0);
//        
//        // find our bucket
//        int bucketIndex = findBucketIndex(pBlock->size, false);
//        
//        // this should never happen because we round to bucket size
//        assert(bucketIndex != -1);
//        
//        if(m_freeBuckets[bucketIndex].pFirstBlock){
//            pBlock->pNextBucketBlock = m_freeBuckets[bucketIndex].pFirstBlock;
//            pBlock->pPrevBucketBlock = 0;
//            pBlock->pBucket = &m_freeBuckets[bucketIndex];
//            m_freeBuckets[bucketIndex].pFirstBlock->pPrevBucketBlock = pBlock;
//            m_freeBuckets[bucketIndex].pFirstBlock = pBlock;
//        }else{
//            pBlock->pNextBucketBlock = 0;
//            pBlock->pPrevBucketBlock = 0;
//            pBlock->pBucket = &m_freeBuckets[bucketIndex];
//            m_freeBuckets[bucketIndex].pFirstBlock = pBlock;
//        }
//    }
//    
//    //! removes a block from a bucket
//    void removeBlockFromBucket(blockHeader_t* pBlock){
//        assert(pBlock->pBucket != 0);
//        freeBlocksBucket_t* pBucket = pBlock->pBucket;
//        
//        if(pBlock->pPrevBucketBlock){
//            // we are not the first block in the bucket
//            pBlock->pPrevBucketBlock->pNextBucketBlock = pBlock->pNextBucketBlock;
//            if(pBlock->pPrevBucketBlock->pNextBucketBlock){
//                pBlock->pPrevBucketBlock->pNextBucketBlock->pPrevBucketBlock = pBlock->pPrevBucketBlock;
//            }
//        }else{
//            if(pBucket->pFirstBlock == pBlock){
//                // we are the first block in the bucket
//                pBucket->pFirstBlock = pBlock->pNextBucketBlock;
//                if(pBucket->pFirstBlock){
//                    pBucket->pFirstBlock->pPrevBucketBlock = 0;
//                }
//            }else{
//                // Something bad happened
//                assert(false);
//            }
//        }
//    }
//    
//private://! block functions
//    
//    //! splits a free block
//    blockHeader_t* splitBlock(blockHeader_t* pBlock, unsigned int size){
//        // do some sanity checks
//        assert(pBlock != 0);
//        assert(pBlock != 0);
//        assert(pBlock->flag == ALLOCATION_FLAG_FREE);
//        assert(pBlock->size >= size);
//        
//        // check if we need to split the block
//        if((pBlock->size - size) >= m_baseAllocationSize){
//            // split the block before use!
//            blockHeader_t* pNewFreeBlock = getNewBlock();
//            pNewFreeBlock->pNextBucketBlock = 0;
//            pNewFreeBlock->pPrevBucketBlock = 0;
//            
//            // find new memory address
//            char* pNewMemoryAddress = (char*)(pBlock->pMemory + size);
//            
//            // set the header properties for the new block
//            memcpy(pNewMemoryAddress, &pNewFreeBlock, sizeof(blockHeader_t*));
//            pNewFreeBlock->flag = ALLOCATION_FLAG_FREE;
//            pNewFreeBlock->size = (pBlock->size  - size);
//            pNewFreeBlock->pMemory = pNewMemoryAddress + sizeof(blockHeader_t*);
//            pNewFreeBlock->pNextBucketBlock = 0;
//            pNewFreeBlock->pPrevBucketBlock = 0;
//            pNewFreeBlock->pNextPhysicalBlock = pBlock->pNextPhysicalBlock;
//            pNewFreeBlock->pPrevPhysicalBlock = pBlock;
//            
//            // set the header properties for the old block
//            pBlock->size = size;
//            pBlock->pNextPhysicalBlock = pNewFreeBlock;
//            
//            return pNewFreeBlock;
//        }
//        return 0;
//    }
//    
//    //! attempts to merge two blocks
//    blockHeader_t* mergeBlocks(blockHeader_t* firstBlock, blockHeader_t* secondBlock){
//        if(firstBlock && secondBlock){
//            if(firstBlock->pNextPhysicalBlock == secondBlock){
//                if(	firstBlock->flag == ALLOCATION_FLAG_FREE &&
//                   secondBlock->flag == ALLOCATION_FLAG_FREE){
//                    
//                    // remove the blocks from their respective buckets
//                    removeBlockFromBucket(firstBlock);
//                    removeBlockFromBucket(secondBlock);
//                    
//                    // merge the second block into the first
//                    firstBlock->size += secondBlock->size;
//                    firstBlock->flag = ALLOCATION_FLAG_FREE;
//                    firstBlock->pNextPhysicalBlock = secondBlock->pNextPhysicalBlock;
//                    
//                    // add the block back to a bucket
//                    addBlockToBucket(firstBlock);
//                    
//                    // free the second block
//                    memset(secondBlock, 0, sizeof(blockHeader_t));
//                    // add the block back to our free stack
//                    m_freeBlockHeaderStack[m_freeBlockHeaderStackTop] = secondBlock;
//                    m_freeBlockHeaderStackTop++;
//                    
//                    // return the merged block
//                    return firstBlock;
//                }else{
//                    return 0;
//                }
//            }else if(secondBlock->pNextPhysicalBlock == firstBlock){
//                mergeBlocks(secondBlock, firstBlock);
//            }
//        }
//        return 0;
//    }
//};

//////////////////////////////////////////////////////////////////////////