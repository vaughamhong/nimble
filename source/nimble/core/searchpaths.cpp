//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/searchpaths.h>
#include <nimble/core/filesystem.h>
#include <nimble/core/logger.h>
#include <fstream>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
SearchPaths::SearchPaths(){
}
//! Constructor
SearchPaths::SearchPaths(SearchPaths const &searchPaths){
    m_searchPaths = searchPaths.m_searchPaths;
}
//! Destructor
SearchPaths::~SearchPaths(){
}

//////////////////////////////////////////////////////////////////////////

//! adds a search path
void SearchPaths::addSearchPath(const char *path){
    m_searchPaths.push_back(path);
}
//! removes a search path
void SearchPaths::removeSearchPath(const char *path){
    PathList::iterator it = std::find(m_searchPaths.begin(), m_searchPaths.end(), path);
    m_searchPaths.erase(it);
}

//////////////////////////////////////////////////////////////////////////

//! returns full path
//! \param path the file to look for
//! \return the full file path
std::string SearchPaths::findFullPath(const char *path){
    for(SearchPaths::PathList::iterator it = m_searchPaths.begin(); it != m_searchPaths.end(); it++){
        std::string fullPath = *it + path;
        if(core::fileExists(fullPath.c_str())){
            return fullPath;
        }
    }
    NIMBLE_LOG_WARNING("paths", "Failed to find full path for: %s", path);
    return "";
}

//////////////////////////////////////////////////////////////////////////