//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/stream.file.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
FileStream::FileStream(const char *filePath, bool createMissingPath)
:m_filePath(filePath){
    //m_fileStream.open(m_filePath.c_str(), std::fstream::in | std::fstream::out | std::fstream::binary);
	m_fileStream.open(m_filePath.c_str(), std::fstream::in | std::fstream::out);
    if(m_fileStream.fail()){
        std::fstream f;
        f.open(m_filePath.c_str(), std::fstream::out);
        f << std::flush;
        f.close();
		m_fileStream.open(m_filePath.c_str(), std::fstream::in | std::fstream::out);
		//m_fileStream.open(m_filePath.c_str(), std::fstream::in | std::fstream::out | std::fstream::binary);
    }
    NIMBLE_ASSERT_PRINT(!m_fileStream.fail(), (std::string("Could not open file at filePath: ") + filePath).c_str());
}
//! Destructor
FileStream::~FileStream(){
    m_fileStream.close();
}

//////////////////////////////////////////////////////////////////////////

//! returns the size of our Stream
size_t FileStream::size() const{
	std::streamoff currentPosition = m_fileStream.tellg();
    m_fileStream.seekg(0, std::ios::end);
	std::streamoff size = m_fileStream.tellg();
    m_fileStream.seekg(currentPosition, std::ios::beg);
    return size;
}
//! returns true if this buffer is empty
bool FileStream::isEmpty() const{
    std::streamoff currentPosition = m_fileStream.tellg();
    m_fileStream.seekg(0, std::ios::end);
    std::streamoff size = m_fileStream.tellg();
    m_fileStream.seekg(currentPosition, std::ios::beg);
    return (size - currentPosition) == 0;
}

//////////////////////////////////////////////////////////////////////////

//! Writes to this stream
void FileStream::write(char const *pBuffer, size_t size){
    NIMBLE_ASSERT(pBuffer != 0);
    m_fileStream.write(pBuffer, size);
    m_fileStream.flush();
}
//! Reads from this stream
void FileStream::read(char *pBuffer, size_t size){
    NIMBLE_ASSERT(pBuffer != 0);
    m_fileStream.read(pBuffer, size);
}
//! Reads line from this stream
void FileStream::readLine(char *pBuffer, size_t size){
    NIMBLE_ASSERT(pBuffer != 0);
    m_fileStream.getline(pBuffer, size);
}
//! seeks
void FileStream::seek(uint32_t location, eOffsetReference ref){
    if(ref == FileStream::kSeekBeginning){
        m_fileStream.seekg(location, std::ios::beg);
    }else if(ref == FileStream::kSeekCurrent){
        m_fileStream.seekg(location, std::ios::cur);
    }else if(ref == FileStream::kSeekEnd){
        m_fileStream.seekg(location, std::ios::end);
    }
}

//////////////////////////////////////////////////////////////////////////