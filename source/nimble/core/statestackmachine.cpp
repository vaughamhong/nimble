//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/statestackmachine.h>
#include <nimble/core/logger.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! default constructor
StackState::StackState()
:m_pStateMachine(0){
}
//! a destructor
StackState::~StackState(){
}

//////////////////////////////////////////////////////////////////////////

//! called when we enter the state
void StackState::onStateEnter(){
}
//! called when the state has focus
void StackState::onStateHasFocus(){
}
//! called when the state has lost focus
void StackState::onStateLostFocus(){
}
//! called when we leave the state
void StackState::onStateExit(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void StackState::update(double interval){
}

//////////////////////////////////////////////////////////////////////////

//! Returns true if still active
//! \return true if still active
bool StackState::isActive(){
    return (getStateMachine() != 0);
}

//////////////////////////////////////////////////////////////////////////

//! returns the state machine
core::StackStateMachine* StackState::getStateMachine(){
    return m_pStateMachine;
}
//! sets the state machine
void StackState::setStateMachine(core::StackStateMachine *pStateMachine){
    m_pStateMachine = pStateMachine;
}

//////////////////////////////////////////////////////////////////////////

//! default constructor
StackStateMachine::StackStateMachine()
:m_invalidateUpdate(false){
}
//! a destrutor
StackStateMachine::~StackStateMachine(){
}

//////////////////////////////////////////////////////////////////////////

//! returns the number of states in our stack
//! \return the size of the stack
uint32_t StackStateMachine::getStackSize() const{
    return m_stateStack.size();
}
//! returns the state at an index
StackStateMachine::StatePtr StackStateMachine::getStateAtIndex(uint32_t index){
    core::check_print(index < this->getStackSize(), __LINE__, __FILE__, "Invalid stack state index");
    return m_stateStack[index];
}

//////////////////////////////////////////////////////////////////////////

//! Switches the top state
//! \param[in] state the state to switch to
void StackStateMachine::switchState(StatePtr state){
    NIMBLE_ASSERT(!exists(state));
    
    core::StackStateMachine::StatePtr topState = this->topState();
    m_inactiveStates.push_back(topState);
    m_invalidateUpdate = true;
    
    // pop
    if(topState.isValid()){
        m_stateStack.pop_back();
        topState->onStateLostFocus();
        topState->onStateExit();
        topState->setStateMachine(0);
    }
    
    // push
    m_stateStack.push_back(state);
    state->setStateMachine(this);
    state->onStateEnter();
    state->onStateHasFocus();
}
//! pushes a state
//! \param[in] state the state to push
void StackStateMachine::pushState(StatePtr state){
    NIMBLE_ASSERT(!exists(state));
    
    core::StackStateMachine::StatePtr topState = this->topState();
    m_invalidateUpdate = true;
    
    // if the top state is valid, make it lose focus
    if(topState.isValid()){topState->onStateLostFocus();}
    
    m_stateStack.push_back(state);
    state->setStateMachine(this);
    state->onStateEnter();
    state->onStateHasFocus();
}
//! pops a state
void StackStateMachine::popState(uint32_t numPops){
    for(uint32_t i = 0; i < numPops; i++){
        core::StackStateMachine::StatePtr topState = this->topState();
        m_inactiveStates.push_back(topState);
        m_invalidateUpdate = true;
        
        if(topState.isValid()){
            m_stateStack.pop_back();
            
            // only the first state loses focus
            // all other states simply exit (they do not gain focus)
            if(i == 0){topState->onStateLostFocus();}
            topState->onStateExit();
            topState->setStateMachine(0);
        }else{
            NIMBLE_LOG_WARNING("core", "Popping on empty state stack");
        }
    }
    
    // if top state is valid, give it focus
    core::StackStateMachine::StatePtr topState = this->topState();
    if(topState.isValid()){topState->onStateHasFocus();}
}
//! popos to root
void StackStateMachine::popToRoot(){
    this->popState((uint32_t)(this->getStackSize() - 1));
}
//! top state
//! \return the top state
StackStateMachine::StatePtr StackStateMachine::topState(){
    if(this->getStackSize() > 0){
        return m_stateStack.back();
    }
    return core::StackStateMachine::StatePtr();
}
//! clears all states
void StackStateMachine::clearStates(){
    this->popState((uint32_t)this->getStackSize());
}
//! state exists
//! \param[in] state the state to check
//! \return true if the state exists in our stack
bool StackStateMachine::exists(StatePtr state){
    for(StatePtrStack::iterator it = m_stateStack.begin(); it != m_stateStack.end(); it++){
        if((*it) == state){
            return true;
        }
    }
    return false;
}

//! Updates relative to an interval
//! \param[in] interval the interval since last update
void StackStateMachine::update(uint64_t frameIndex, double interval){
    m_invalidateUpdate = false;
    
    // state changes invalidate our state stack
    for(StatePtrStack::iterator it = m_stateStack.begin(); it != m_stateStack.end(); it++){
        (*it)->update(interval);
        if(m_invalidateUpdate == true){
            break;
        }
    }
    
    // clear our inactive states
    // we defer this operation to update
    m_inactiveStates.clear();
}

//////////////////////////////////////////////////////////////////////////