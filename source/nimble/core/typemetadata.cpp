//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/typemetadata.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Copy constructor
//! \param[in] field to copy
FieldMetaData::FieldMetaData(FieldMetaData const &metaData)
:m_name(metaData.m_name)
,m_offset(metaData.m_offset)
,m_size(metaData.m_size)
,m_typeId(metaData.m_typeId)
,m_pType(metaData.m_pType){
}
//! Destructor
FieldMetaData::~FieldMetaData(){
}
//! Returns the name of the field
//! \return the name of the field
const char* FieldMetaData::getName() const{
    return m_name.c_str();
}
//! Returns the memory offset from the base object
//! \return the memory offset from the base object
size_t FieldMetaData::getOffset() const{
    return m_offset;
}
//! Returns the size of this field
//! \return the size of this field
size_t FieldMetaData::getSize() const{
    return m_size;
}
//! Returns the type for this field
//! \return the type for this field
core::TypeMetaData* FieldMetaData::getType() const{
    return m_pType;
}
//! Returns true if this field is a pointer
//! \return true if this field is a pointer
bool FieldMetaData::isPointer() const{
    return (m_flags & FieldMetaData::kFlagPointer) != 0;
}
//! Returns true if this field is read only
//! \return true if this field is read only
bool FieldMetaData::isReadOnly() const{
    return (m_flags & FieldMetaData::kFlagReadOnly) != 0;
}
//! Returns the typeId of this field
//! \return the typeId of this field
core::TypeId FieldMetaData::typeId() const{
    return m_typeId;
}
//! Sets the typeId of this field
//! \param[in] pType the type to set
void FieldMetaData::setType(core::TypeMetaData *pType){
    m_pType = pType;
}

//////////////////////////////////////////////////////////////////////////

//! Copy constructor
//! \param[in] enumeration to copy
EnumMetaData::EnumMetaData(core::EnumMetaData const &metaData)
:m_name(metaData.m_name)
,m_value(metaData.m_value){
}
//! Constructor
//! \param[in] name the name of the enumeration
//! \param[in] value the value of the enumeration
EnumMetaData::EnumMetaData(const char *name, int32_t value)
:m_name(name)
,m_value(value){
}
//! Destructor
EnumMetaData::~EnumMetaData(){
}
//! Returns the name of the enumeration
//! \return the name of the enumeration
const char* EnumMetaData::getName() const{
    return m_name.c_str();
}
//! Returns the value of the enumeration
//! \return the value of the enumeration
int32_t EnumMetaData::getValue() const{
    return m_value;
}

//////////////////////////////////////////////////////////////////////////

//! Copy constructor
//! \param[in] metadata the metadata to copy
TypeMetaData::TypeMetaData(core::TypeMetaData const &metaData)
:m_pTypeDatabase(metaData.m_pTypeDatabase)
,m_typeId(metaData.m_typeId)
,m_name(metaData.m_name)
,m_size(metaData.m_size)
,m_fields(metaData.m_fields)
,m_enums(metaData.m_enums){
}
//! Constructor
//! \param[in] pOwner the owner DB of this type
//! \param[in] typeId the typeId of this type
//! \param[in] name the name of this type
//! \param[in] size the size (bytes) of this type
TypeMetaData::TypeMetaData(TypeDatabase *pTypeDatabase, core::TypeId typeId, const char *name, int32_t size)
:m_pTypeDatabase(pTypeDatabase)
,m_typeId(typeId)
,m_name(name)
,m_size(size){
}
//! Destructor
TypeMetaData::~TypeMetaData(){
}

//! Sets the owner db
void TypeMetaData::setTypeDatabase(core::TypeDatabase *pTypeDatabase){
    m_pTypeDatabase = pTypeDatabase;
}
//! Returns the owner db
//! \return the owner db
core::TypeDatabase* TypeMetaData::getTypeDatabase() const{
    return m_pTypeDatabase;
}
//! Returns the typeId
//! \return the typeId
core::TypeId TypeMetaData::getTypeId() const{
    return m_typeId;
}
//! Returns the type name
//! \return the type name
const char* TypeMetaData::getName() const{
    return m_name.c_str();
}
//! Returns the type size (bytes)
//! \return the type size (bytes)
int32_t TypeMetaData::getSize() const{
    return m_size;
}
//! Returns a field associated with a name
//! \return a field associated with a name
core::FieldMetaData* TypeMetaData::findField(const char *name){
    for(TypeMetaData::field_iterator it = beginField(); it != endField(); it++){
        if(strcmp(it->getName(), name) == 0){
            return &(*it);
        }
    }
    return 0;
}
//! Returns the number of fields
//! \return the number of fields
size_t TypeMetaData::getNumFields(){
    return m_fields.size();
}
//! Returns the field by index
core::FieldMetaData const* TypeMetaData::getFieldByIndex(size_t index) const{
    if(index < m_fields.size()){
        return &m_fields[index];
    }else{
        return 0;
    }
}
//! Returns the begin field iterator
//! \return the begin field iterator
TypeMetaData::field_iterator TypeMetaData::beginField(){
    return m_fields.begin();
}
//! Returns the end field iterator
//! \return the end field iterator
TypeMetaData::field_iterator TypeMetaData::endField(){
    return m_fields.end();
}
//! Returns the begin const field iterator
//! \return the begin const field iterator
TypeMetaData::const_field_iterator TypeMetaData::beginField() const{
    return m_fields.begin();
}
//! Returns the end const field iterator
//! \return the end const field iterator
TypeMetaData::const_field_iterator TypeMetaData::endField() const{
    return m_fields.end();
}
//! Returns the begin enumeration iterator
//! \return the begin enumeration iterator
TypeMetaData::enumeration_iterator TypeMetaData::beginEnumeration(){
    return m_enums.begin();
}
//! Returns the end enumeration iterator
//! \return the end enumeration iterator
TypeMetaData::enumeration_iterator TypeMetaData::endEnumeration(){
    return m_enums.end();
}
//! Returns the begin const enumeration iterator
//! \return the begin const enumeration iterator
TypeMetaData::const_enumeration_iterator TypeMetaData::beginEnumeration() const{
    return m_enums.begin();
}
//! Returns the end const enumeration iterator
//! \return the end const enumeration iterator
TypeMetaData::const_enumeration_iterator TypeMetaData::endEnumeration() const{
    return m_enums.end();
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
TypeDatabase::TypeDatabase(){
    // register default types
    registerType<bool>("bool");
    registerType<char>("char");
    registerType<int16_t>("int16");
    registerType<int32_t>("int32");
    registerType<int64_t>("int64");
    registerType<uint8_t>("uint8");
    registerType<uint16_t>("uint16");
    registerType<uint32_t>("uint32");
    registerType<uint64_t>("uint64");
    registerType<float>("float");
    registerType<double>("double");
    registerType<uint32_t>("size");
}
//! Constructor
TypeDatabase::TypeDatabase(TypeDatabase const &typeDatabase)
:m_nameToTypeIdIndex(typeDatabase.m_nameToTypeIdIndex){
    // deep copy
    for(TypeIdToTypeIndex::const_iterator it = typeDatabase.m_typeIdToTypeIndex.begin(); it != typeDatabase.m_typeIdToTypeIndex.end(); it++){
        core::TypeId typeId = it->first;
        core::TypeMetaData *pTypeMetaData = new /* () */ core::TypeMetaData(*it->second);
        pTypeMetaData->setTypeDatabase(this);
        m_typeIdToTypeIndex.insert(std::pair<core::TypeId, core::TypeMetaData*>(typeId, pTypeMetaData));
    }
}
//! Destructor
TypeDatabase::~TypeDatabase(){
}

//! Unregister a type
//! \param[in[ name the name of the type to unregister
void TypeDatabase::unregisterType(const char *name){
    NameToTypeIdIndex::iterator it = m_nameToTypeIdIndex.find(name);
    if(it != m_nameToTypeIdIndex.end()){
        m_typeIdToTypeIndex.erase(m_typeIdToTypeIndex.find(it->second));
        m_nameToTypeIdIndex.erase(it);
    }
}
//! Returns a type by name
//! \return a type by name
core::TypeMetaData* TypeDatabase::findType(const char *name){
    NameToTypeIdIndex::iterator it = m_nameToTypeIdIndex.find(name);
    if(it != m_nameToTypeIdIndex.end()){
        return findType(it->second);
    }
    return 0;
}
//! Returns a type by typeId
//! \return a type by typeId
core::TypeMetaData* TypeDatabase::findType(core::TypeId typeId){
    TypeIdToTypeIndex::iterator it = m_typeIdToTypeIndex.find(typeId);
    if(it != m_typeIdToTypeIndex.end()){
        return it->second;
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////