//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/stream.buffer.h>
#include <nimble/math/mathops.h>
#include <assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
BufferStream::BufferStream(size_t size)
:m_readPosition(0)
,m_writePosition(0){
    m_buffer.resize(size);
}
//! Destructor
BufferStream::~BufferStream(){
    m_readPosition = 0;
    m_writePosition = 0;
    m_buffer = core::Buffer<>();
}

//////////////////////////////////////////////////////////////////////////

//! returns the size of our Stream
size_t BufferStream::size() const{
    return m_writePosition - m_readPosition;
};
//! returns true if this buffer is empty
bool BufferStream::isEmpty() const{
    return (size() == 0);
}

//////////////////////////////////////////////////////////////////////////

//! Writes to this stream
void BufferStream::write(char const *pBuffer, size_t size){
    assert(pBuffer != 0);
    assert(m_buffer.getPointer());
    
    if(m_writePosition + size > m_buffer.getSize()){
        m_buffer.resize(math::pow2RoundUp(m_writePosition + size));
    }
    char *pDataPointer = m_buffer.getPointer();
    char *pWritePosition = &pDataPointer[m_writePosition];
    memcpy(pWritePosition, pBuffer, size);
    m_writePosition += size;
};
//! Reads from this stream
void BufferStream::read(char *pBuffer, size_t size){
    assert(pBuffer != 0);
    assert(size > 0);
    assert(m_buffer.getPointer());
    assert(m_readPosition + size <= m_buffer.getSize());
    
    char *pDataPointer = m_buffer.getPointer();
    char *pReadPosition = &pDataPointer[m_readPosition];
    memcpy(pBuffer, pReadPosition, size);
    m_readPosition += size;
};

//////////////////////////////////////////////////////////////////////////

//! returns the buffer
const char* BufferStream::getConstPointer(){
    const char *pDataPointer = m_buffer.getPointer();
    return &pDataPointer[m_readPosition];
}

//////////////////////////////////////////////////////////////////////////