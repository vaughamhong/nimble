//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/objectfactory.h>
#include <nimble/core/typemetadata.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Unregister a type
//! \param[in] name the name of the type to unregister
void ObjectFactory::unregisterType(const char *name){
    m_typeDatabase.unregisterType(name);
    NameToTypeIdIndex::iterator it = m_nameToTypeIdIndex.find(name);
    if(it != m_nameToTypeIdIndex.end()){
        m_typeIdToObjectInfoIndex.erase(it->second);
        m_nameToTypeIdIndex.erase(it);
    }
}

//! Creates an object by name
//! \param[in] name the name of the object type to create from
//! \return the created object
core::Object* ObjectFactory::create(const char *name){
    if(core::TypeMetaData *pType = m_typeDatabase.findType(name)){
        core::TypeId typeId = pType->getTypeId();
        TypeIdToObjectInfoIndex::iterator it = m_typeIdToObjectInfoIndex.find(typeId);
        if(it != m_typeIdToObjectInfoIndex.end()){
            core::Object *pObject = it->second.constructor();
            pObject->setTypeMetaData(pType);
            return pObject;
        }else{
            NIMBLE_LOG_WARNING("core", "Failed to create object with name %s", name);
            return 0;
        }
    }else{
        NIMBLE_LOG_WARNING("core", "Failed to find metadata for type with name %s", name);
        return 0;
    }
}
//! Destroy an object
//! \param[in] pObject the object to destroy
void ObjectFactory::destroy(core::Object *pObject){
    const char *name = pObject->getTypeMetaData()->getName();
    core::TypeMetaData *pType = m_typeDatabase.findType(name);
    core::TypeId typeId = pType->getTypeId();
    TypeIdToObjectInfoIndex::iterator it = m_typeIdToObjectInfoIndex.find(typeId);
    if(it != m_typeIdToObjectInfoIndex.end()){
        it->second.destructor(pObject);
    }else{
        NIMBLE_LOG_WARNING("core", "Failed to destroy object with name %s", name);
    }
}

//////////////////////////////////////////////////////////////////////////