//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/filesystem.h>
#include <nimble/core/path.h>
#include <nimble/core/logger.h>
#include <nimble/core/assert.h>
#include <fstream>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <algorithm>

#if defined(NIMBLE_TARGET_WIN32)
#include <Windows.h>
#endif

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Returns true if the file exists
bool nimble::core::fileExists(core::Path const path){
    std::ifstream in(path.getFullPath().c_str(), std::ios::in | std::ios::binary);
    return in.good();
}
//! Returns the file size located at path
size_t nimble::core::fileSize(core::Path const path){
    std::fstream fileStream(path.getFullPath().c_str(), std::fstream::in | std::fstream::out | std::ios::binary);
    if(fileStream.fail()){
        NIMBLE_LOG_ERROR("core", "Failed to open file %s", path.getFullPath().c_str());
        return 0;
    }
    
	std::streamoff begin = 0, end = 0;
    fileStream.seekg(0, std::ios::beg);
    begin = fileStream.tellg();
    fileStream.seekg(0, std::ios::end);
    end = fileStream.tellg();
    return (end - begin);
}

//////////////////////////////////////////////////////////////////////////

//! Copies a file from the src to dest path
void nimble::core::copyFile(Path const srcPath, Path const destPath){
    core::createDirectory(core::Path(destPath).getDirectory().c_str());// directory(destPath).c_str());
    NIMBLE_ASSERT(false);
    //    core::FileStream srcFileStream(srcPath, false);
    //    core::FileStream destFileStream(destPath, true);
    //    uint32_t size = srcFileStream.size();
    //    const int kBufferSize = 128;
    //    char buffer[kBufferSize];
    //
    //    for(uint32_t i = 0; i < size && (i + kBufferSize) < size; i += kBufferSize){
    //        srcFileStream.read(buffer, kBufferSize);
    //        destFileStream.write(buffer, kBufferSize);
    //    }
    //    srcFileStream.read(buffer, size % kBufferSize);
    //    destFileStream.write(buffer, size % kBufferSize);
}
//! Creates a directory
void nimble::core::createDirectory(Path const srcPath){
    std::string tmp = srcPath.getFullPath();
    size_t len = tmp.length();
    
    // remove trailing forward slash
    if(tmp[len - 1] == '/'){
        tmp[len - 1] = 0;
    }
    // recursively create directories
    for(size_t i = 0; i < len; i++){
        if(tmp[i] == '/'){
            tmp[i] = 0;
#if defined(NIMBLE_TARGET_WIN32)
            CreateDirectory(tmp.c_str(), NULL);
#else
            mkdir(tmp.c_str(), S_IRWXU);
#endif
            tmp[i] = '/';
        }
    }
    // create last directory
#if defined(NIMBLE_TARGET_WIN32)
    CreateDirectory(tmp.c_str(), NULL);
#else
    mkdir(tmp.c_str(), S_IRWXU);
#endif
}

//////////////////////////////////////////////////////////////////////////