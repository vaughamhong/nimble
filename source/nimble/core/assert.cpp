//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/assert.h>
#include <nimble/core/logger.h>
#include <assert.h>
#include <stdio.h>
#include <string>
#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////

#if defined(NIMBLE_TARGET_WIN32)
	#include <Windows.h>
#endif

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! asserts on expresssion and halts
//! \param expression the expression to assert on
//! \param format the message format
//! \param ... variable argument list
void core::assert_print(bool expression, int line, const char *file, const char *format, ...){
#if defined(NIMBLE_DEBUG)
    if(!expression){
        va_list args;
        va_start(args, format);
        core::log_error("", line, file, format, args);
        va_end(args);
        assert(expression);
    }
#endif
}
//! asserts on expresssion and halts
//! \param expression the expression to assert on
//! \param line the check line
//! \param file the check file
//! \param format the message format
//! \param ... variable argument list
void core::check_print(bool expression, int line, const char *file, const char *format, ...){
#if defined(NIMBLE_DEBUG)
    if(!expression){
        va_list args;
        va_start(args, format);
        core::log_info("", line, file, format, args);
        va_end(args);
        assert(expression);
    }
#endif
}

//////////////////////////////////////////////////////////////////////////