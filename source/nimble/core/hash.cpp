//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/hash.h>
#include <nimble/core/hash/md5.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! returns a hash from a c string
int32_t nimble::core::hash(const char *data){
    int32_t hash = 0;
    if(data != 0){
	    const std::string tempString(data);
	    std::string::const_iterator p, p_end;
	    for(p = tempString.begin(), p_end = tempString.end(); p != p_end; ++p){
	        hash = 31 * hash + (*p);
	    }
	}
    return hash;
}
//! returns a hash from a std string
int32_t nimble::core::hash(const std::string &data){
    return core::hash(data.c_str());
}
//! returns a md5 hash from a std string
std::string nimble::core::hash_md5(const std::string &data){
    MD5 md5; return md5(data.c_str(), data.size());
}
//! returns a djb2 hash from a c string
int64_t nimble::core::hash_djb2(const char *str){
    int64_t hash = 5381;
    int c;
    while((c = *str++)){
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }
    return hash;
}

//////////////////////////////////////////////////////////////////////////

