//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/buffer.ring.h>
#include <nimble/core/logger.h>
#include <nimble/core/assert.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! a constructor
RingBuffer::RingBuffer()
:m_pBufferFront(0)
,m_allocatedSize(0)
,m_endOfBufferWaste(0){
}
//! Constructor
RingBuffer::RingBuffer(size_t size)
:Buffer(size){
    m_pBufferFront = this->getPointer();
    m_allocatedSize = 0;
    m_endOfBufferWaste = 0;
}
//! a constructor
RingBuffer::RingBuffer(core::RingBuffer const &buffer)
:Buffer(buffer)
,m_pBufferFront(getPointer())
,m_allocatedSize(buffer.m_allocatedSize)
,m_endOfBufferWaste(buffer.m_endOfBufferWaste){
}

//! Destructor
RingBuffer::~RingBuffer(){
    m_pBufferFront = 0;
    m_allocatedSize = 0;
    m_endOfBufferWaste = 0;
}

//////////////////////////////////////////////////////////////////////////

//! returns the allocated size of the buffer
//! \return the allocated size of the buffer
size_t RingBuffer::allocatedSize() const{
    return m_allocatedSize;
}
//! returns the free size of the buffer
//! \return the free size of the buffer
size_t RingBuffer::freeSize() const{
    return this->getSize() - m_allocatedSize - m_endOfBufferWaste;
}
//! returns true if buffer is empty
bool RingBuffer::isEmpty() const {
    return m_allocatedSize == 0;
}

//////////////////////////////////////////////////////////////////////////

//! clears the buffer contents
void RingBuffer::clear(){
    m_pBufferFront = this->getPointer();
    m_allocatedSize = 0;
    m_endOfBufferWaste = 0;
    ItemSizeQueue empty;
    std::swap(m_itemSizes, empty);
}
//! called when resizing, allowing child classes to resolve data as necessary
void RingBuffer::resolveResize(char *pOldBuffer, size_t oldSize, char *pNewBuffer, size_t newSize){
    NIMBLE_ASSERT_PRINT(m_allocatedSize <= newSize, "Requested ring buffer resize too small to hold allocated data");
    
    // optional: clear our new buffer
    memset(pNewBuffer, 0, newSize);
    
    // allocation changes depending on pointer relationships
    char *pBackOfBuffer = calculateBackOfBuffer();
    size_t totalAllocatedSize = m_allocatedSize + m_endOfBufferWaste;
    if(m_pBufferFront == pBackOfBuffer && totalAllocatedSize == oldSize){
        // we've filled the buffer
        // [xxxxxxxxxxxxxx]
        size_t part1Size = (pOldBuffer + oldSize) - m_endOfBufferWaste - m_pBufferFront;
        size_t part2Size = m_allocatedSize - part1Size;
        memcpy(pNewBuffer, m_pBufferFront, part1Size);
        memcpy(&pNewBuffer[part1Size], pOldBuffer, part2Size);
    }else if(m_pBufferFront < pBackOfBuffer || ((m_pBufferFront == pBackOfBuffer) && totalAllocatedSize == 0)){
        // the back has yet to wrap around
        // [---FxxxxxxB---]
        memcpy(pNewBuffer, m_pBufferFront, m_allocatedSize);
    }else if(pBackOfBuffer < m_pBufferFront){
        // the back has wrapped around already
        // [xxxB------Fxxx]
        size_t part1Size = (pOldBuffer + oldSize) - m_endOfBufferWaste - m_pBufferFront;
        size_t part2Size = m_allocatedSize - part1Size;
        memcpy(pNewBuffer, m_pBufferFront, part1Size);
        memcpy(&pNewBuffer[part1Size], pOldBuffer, part2Size);
    }
    m_pBufferFront = pNewBuffer;
    m_endOfBufferWaste = 0;
}
//! automatic resize policy
size_t RingBuffer::calculateAutoResizeAmount(size_t additionalSize){
    size_t oldSize = this->getSize();
    size_t reqSize = oldSize + additionalSize;
    size_t newSize = oldSize;
    while(newSize < reqSize){
        newSize *= 2;
    };
    return newSize;
}

//! Inserts a new item at the back of the queue.
//! \param buffer the buffer to push
//! \param bufferSize the size of the buffer to push
//! \return pointer to the data in the buffer
void RingBuffer::push(const char* pBuffer, size_t bufferSize){
    size_t size = this->getSize();
    char *pBufferStart = this->getPointer();
    char *pBackOfBuffer = calculateBackOfBuffer();
    size_t totalAllocatedSize = m_allocatedSize + m_endOfBufferWaste;
    
    // allocation changes depending on pointer relationships
    if(m_pBufferFront == pBackOfBuffer && totalAllocatedSize == size){
        // we've filled the buffer
        // [xxxxxxxxxxxxxx]
        size_t newSize = this->calculateAutoResizeAmount(bufferSize);
        this->resize(newSize);
        RingBuffer::push(pBuffer, bufferSize);
    }else if(m_pBufferFront < pBackOfBuffer || ((m_pBufferFront == pBackOfBuffer) && totalAllocatedSize == 0)){
        // the back has yet to wrap around
        // [---FxxxxxxB---]
        size_t spaceLeftAtEndOfBuffer = size - (pBackOfBuffer - pBufferStart);
        if(bufferSize <= spaceLeftAtEndOfBuffer){
            // allocate item at the end of the buffer
            memcpy(pBackOfBuffer, (void*)pBuffer, bufferSize);
            m_itemSizes.push(bufferSize);
            
            m_endOfBufferWaste = 0;
            m_allocatedSize += bufferSize;
        }else{
            m_endOfBufferWaste = spaceLeftAtEndOfBuffer;
            RingBuffer::push(pBuffer, bufferSize);
        }
    }else if(pBackOfBuffer < m_pBufferFront){
        // the back has wrapped around already
        // [xxxB------Fxxx]
        size_t physicalFreeSpace = m_pBufferFront - pBackOfBuffer;
        if(bufferSize <= physicalFreeSpace){
            // allocate
            memcpy(pBackOfBuffer, (void*)pBuffer, bufferSize);
            m_itemSizes.push(bufferSize);

            m_allocatedSize += bufferSize;
        }else{
            // ring buffer requires us to resize
            size_t newSize = this->calculateAutoResizeAmount(bufferSize);
            this->resize(newSize);
            RingBuffer::push(pBuffer, bufferSize);
        }
    }
}

//! Removes the item at the front of a non-empty queue.
//! \param ppBuffer the buffer to fill
//! \param bufferSize the buffer size
//! \return 0 if no errors occured
void RingBuffer::pop(char** ppBuffer, size_t bufferSize){
    size_t size = this->getSize();
    char *pBufferStart = this->getPointer();
    char* pBackOfBuffer = calculateBackOfBuffer();
    size_t totalAllocatedSize = m_allocatedSize + m_endOfBufferWaste;
    
    // allocation changes depending on pointer relationships
    if(m_pBufferFront < pBackOfBuffer){
        // the back has yet to wrap around
        // [---FxxxxxxB---]
        NIMBLE_ASSERT(m_allocatedSize > 0);
        
        // find our item size
        size_t itemSize = m_itemSizes.front();
        NIMBLE_ASSERT(bufferSize >= itemSize);
        
        // copy our item
        m_itemSizes.pop();
        NIMBLE_ASSERT(itemSize <= m_allocatedSize);
        memcpy(*ppBuffer, (void*)m_pBufferFront, itemSize);
        
        // update internals
        m_allocatedSize -= itemSize;
        m_pBufferFront += itemSize;
    }else if(pBackOfBuffer < m_pBufferFront || ((m_pBufferFront == pBackOfBuffer) && totalAllocatedSize == size)){
        // the back has wrapped around already
        // [xxxB------Fxxx]
        size_t sizeTillEndOfBuffer = size - (m_pBufferFront - pBufferStart);
        NIMBLE_ASSERT(sizeTillEndOfBuffer >= (size_t)m_endOfBufferWaste);
        if(sizeTillEndOfBuffer == (size_t)m_endOfBufferWaste){
            // VOM - I'm not even sure if this code path is possible. No matter what unit tests
            // I build, this code path does not run. Then again, it is 2am right now.
            // Future Vom, please figure this out when my/your mind is in a better state.
            
            // set our front to the beginning
            // [FxxB----------]
            m_pBufferFront = pBufferStart;
            m_endOfBufferWaste = 0;
            
            // try again
            return RingBuffer::pop(ppBuffer, bufferSize);
        }else{
            size_t itemSize = m_itemSizes.front();
            NIMBLE_ASSERT(bufferSize >= itemSize);
            // copy our item
            m_itemSizes.pop();
            memcpy(*ppBuffer, (void*)m_pBufferFront, itemSize);
            
            // update internals
            m_allocatedSize -= itemSize;
            m_pBufferFront += itemSize;
            
            // check if we are at the end of the buffer
            if(m_pBufferFront == pBufferStart + size){
                m_pBufferFront = pBufferStart;
                m_endOfBufferWaste = 0;
            }
        }
    }else if((m_pBufferFront == pBackOfBuffer) && totalAllocatedSize == 0){
    }else{
        NIMBLE_LOG_ERROR("core", "Reached invalid execution point");
    }
};

//////////////////////////////////////////////////////////////////////////

//! calculates the back of our buffer
char* RingBuffer::calculateBackOfBuffer(){
    size_t totalAllocatedSize = m_allocatedSize + m_endOfBufferWaste;
    char* pBackOfBuffer = m_pBufferFront + totalAllocatedSize;
    size_t size = this->getSize();
    char *pBufferStart = this->getPointer();
    
    // if our quick back of buffer calculation has not overflowed, we
    // can trust that it is the back of the buffer
    if(pBackOfBuffer < (pBufferStart + size)){
        return pBackOfBuffer;
    }else{
        // if our quick back of buffer calculation has overflowed, we
        // calculate our overflow difference and wrap from our starting point
        size_t difference = pBackOfBuffer - (pBufferStart + size);
        pBackOfBuffer = pBufferStart + difference;
        return pBackOfBuffer;
    }
}

//////////////////////////////////////////////////////////////////////////