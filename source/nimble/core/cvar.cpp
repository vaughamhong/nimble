//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/cvar.h>
#include <nimble/core/hash.h>
#include <nimble/core/logger.h>
#include <unordered_map>
#include <vector>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

#define kCVarTypeBool       0
#define kCVarTypeInt        1
#define kCVarTypeFloat      2
#define kCVarTypeString     3

//////////////////////////////////////////////////////////////////////////

struct nimble::core::cvar_t{
    std::string name;
    char type;
    
    union{
        bool bValue;
        float fValue;
        int iValue;
    };
    std::string sValue;
    
    // constructor
    cvar_t(const char *name)
    :name(name)
    ,type(kCVarTypeBool)
    ,bValue(false){
    }
    // constructor
    cvar_t(const char *name, bool value)
    :name(name)
    ,type(kCVarTypeBool)
    ,bValue(value){
    }
    // constructor
    cvar_t(const char *name, int value)
    :name(name)
    ,type(kCVarTypeInt)
    ,iValue(value){
    }
    // constructor
    cvar_t(const char *name, float value)
    :name(name)
    ,type(kCVarTypeFloat)
    ,fValue(value){
    }
    // constructor
    cvar_t(const char *name, const char *value)
    :name(name)
    ,type(kCVarTypeString)
    ,sValue(value){
    }
    
    // assignment operator
    nimble::core::cvar_t& operator=(nimble::core::cvar_t const &cvar){
        fValue = cvar.fValue;
        sValue = cvar.sValue;
        return *this;
    }
    
    // equality operator
    bool operator==(nimble::core::cvar_t const &cvar){
        if(type == kCVarTypeString){
            return cvar.sValue == sValue;
        }else{
            return cvar.fValue == fValue;
        }
    }
    bool operator!=(nimble::core::cvar_t const &cvar){
        return !this->operator==(cvar);
    }
};

//////////////////////////////////////////////////////////////////////////

typedef std::vector<int64_t> NameHashList;
typedef std::unordered_map<int64_t, cvar_t> NameHashToCVarIndex;
typedef std::vector<core::CVarChangeHandler> CVarChangeHandlerList;
typedef std::unordered_map<int64_t, CVarChangeHandlerList> NameHashToCVarChangeHandlerListIndex;

//////////////////////////////////////////////////////////////////////////

static NameHashList g_nameHashList;
static NameHashToCVarIndex g_nameHashToCVarIndex;
static NameHashToCVarChangeHandlerListIndex g_nameHashToCVarChangeHandlerListIndex;

//////////////////////////////////////////////////////////////////////////

//! gets a cvar value
cvar_t const* cvar_get(size_t index);
void cvar_callChangeHandlers(const char *name, core::cvar_t const &oldCVar, core::cvar_t const &newCVar);

//////////////////////////////////////////////////////////////////////////

// sets a cvar value
void nimble::core::cvar_set(const char *name, bool value){
    // convert our string name to a hash for lookup
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    if(it != g_nameHashToCVarIndex.end()){
        core::cvar_t newCVar(name, value);
        core::cvar_t oldCVar = it->second;
        it->second = newCVar;
        if(newCVar != oldCVar){
            cvar_callChangeHandlers(name, oldCVar, newCVar);
        }
    }else{
        core::cvar_t newCVar(name, value);
        core::cvar_t oldCVar(name);
        g_nameHashList.push_back(hash);
        g_nameHashToCVarIndex.insert(std::make_pair(hash, newCVar));
        cvar_callChangeHandlers(name, oldCVar, newCVar);
    }
}
// sets a cvar value
void nimble::core::cvar_set(const char *name, int value){
    // convert our string name to a hash for lookup
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    if(it != g_nameHashToCVarIndex.end()){
        core::cvar_t newCVar(name, value);
        core::cvar_t oldCVar = it->second;
        it->second = newCVar;
        if(newCVar != oldCVar){
            cvar_callChangeHandlers(name, oldCVar, newCVar);
        }
    }else{
        core::cvar_t newCVar(name, value);
        core::cvar_t oldCVar(name);
        g_nameHashList.push_back(hash);
        g_nameHashToCVarIndex.insert(std::make_pair(hash, newCVar));
        cvar_callChangeHandlers(name, oldCVar, newCVar);
    }
}
// sets a cvar value
void nimble::core::cvar_set(const char *name, float value){
    // convert our string name to a hash for lookup
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    if(it != g_nameHashToCVarIndex.end()){
        core::cvar_t newCVar(name, value);
        core::cvar_t oldCVar = it->second;
        it->second = newCVar;
        if(newCVar != oldCVar){
            cvar_callChangeHandlers(name, oldCVar, newCVar);
        }
    }else{
        core::cvar_t newCVar(name, value);
        core::cvar_t oldCVar(name);
        g_nameHashList.push_back(hash);
        g_nameHashToCVarIndex.insert(std::make_pair(hash, newCVar));
        cvar_callChangeHandlers(name, oldCVar, newCVar);
    }
}
// sets a cvar value
void nimble::core::cvar_set(const char *name, const char *value){
    // convert our string name to a hash for lookup
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    if(it != g_nameHashToCVarIndex.end()){
        core::cvar_t newCVar(name, value);
        core::cvar_t oldCVar = it->second;
        it->second = newCVar;
        if(newCVar != oldCVar){
            cvar_callChangeHandlers(name, oldCVar, newCVar);
        }
    }else{
        core::cvar_t newCVar(name, value);
        core::cvar_t oldCVar(name);
        g_nameHashList.push_back(hash);
        g_nameHashToCVarIndex.insert(std::make_pair(hash, newCVar));
        cvar_callChangeHandlers(name, oldCVar, newCVar);
    }
}
// gets a cvar value
bool nimble::core::cvar_getBoolValue(const char *name){
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    if(it != g_nameHashToCVarIndex.end()){
        return it->second.bValue;
    }else{
        return false;
    }
}
// gets a cvar value
int nimble::core::cvar_getIntValue(const char *name){
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    if(it != g_nameHashToCVarIndex.end()){
        return it->second.iValue;
    }else{
        return 0;
    }
}
// gets a cvar value
float nimble::core::cvar_getFloatValue(const char *name){
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    if(it != g_nameHashToCVarIndex.end()){
        return it->second.fValue;
    }else{
        return 0.0f;
    }
}
// gets a cvar value
const char* nimble::core::cvar_getStringValue(const char *name){
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    if(it != g_nameHashToCVarIndex.end()){
        return it->second.sValue.c_str();
    }else{
        return "";
    }
}
// returns true if cvar exists
bool nimble::core::cvar_exists(const char *name){
    int64_t hash = hash_djb2(name);
    NameHashToCVarIndex::iterator it = g_nameHashToCVarIndex.find(hash);
    return it != g_nameHashToCVarIndex.end();
}

//////////////////////////////////////////////////////////////////////////

//! returns the number of cvars
size_t nimble::core::cvar_count(){
    return g_nameHashList.size();
}
//! gets a cvar value
const char* nimble::core::cvar_getName(size_t index){
    if(index < cvar_count()){
        return cvar_get(index)->name.c_str();
    }else{
        return "";
    }
}
//! gets a cvar value by index
bool nimble::core::cvar_getBoolValue(size_t index){
    if(index < cvar_count()){
        return cvar_get(index)->bValue;
    }else{
        return false;
    }
}
//! gets a cvar value by index
int nimble::core::cvar_getIntValue(size_t index){
    if(index < cvar_count()){
        return cvar_get(index)->iValue;
    }else{
        return 0;
    }
}
//! gets a cvar value by index
float nimble::core::cvar_getFloatValue(size_t index){
    if(index < cvar_count()){
        return cvar_get(index)->fValue;
    }else{
        return 0.0f;
    }
}
//! gets a cvar value by index
const char* nimble::core::cvar_getStringValue(size_t index){
    if(index < cvar_count()){
        return cvar_get(index)->sValue.c_str();
    }else{
        return "";
    }
}

//////////////////////////////////////////////////////////////////////////

// registers a handler on a cvar state change
void nimble::core::cvar_registerHandler(const char *name, core::CVarChangeHandler func){
    int64_t hash = hash_djb2(name);
    NameHashToCVarChangeHandlerListIndex::iterator it = g_nameHashToCVarChangeHandlerListIndex.find(hash);
    if(it == g_nameHashToCVarChangeHandlerListIndex.end()){
        g_nameHashToCVarChangeHandlerListIndex.insert(std::make_pair(hash, CVarChangeHandlerList()));
        it = g_nameHashToCVarChangeHandlerListIndex.find(hash);
    }
    it->second.push_back(func);
}
// unregisters a handler on a cvar state change
void nimble::core::cvar_unregisterHandler(const char *name, core::CVarChangeHandler func){
    int64_t hash = hash_djb2(name);
    NameHashToCVarChangeHandlerListIndex::iterator it = g_nameHashToCVarChangeHandlerListIndex.find(hash);
    if(it != g_nameHashToCVarChangeHandlerListIndex.end()){
        CVarChangeHandlerList &list = it->second;
        for(CVarChangeHandlerList::iterator it2 = list.begin(); it2 != list.end();){
            if(*it2 == func){
                it2 = list.erase(it2);
            }else{
                it2++;
            }
        }
        if(list.size() == 0){
            g_nameHashToCVarChangeHandlerListIndex.erase(it);
        }
    }
}

//////////////////////////////////////////////////////////////////////////

//! gets a cvar value
cvar_t const* cvar_get(size_t index){
    if(index < cvar_count()){
        NameHashList::const_iterator it = g_nameHashList.begin() + index;
        NameHashToCVarIndex::const_iterator it2 = g_nameHashToCVarIndex.find(*it);
        return &it2->second;
    }else{
        return 0;
    }
}
// call change handlers
void cvar_callChangeHandlers(const char *name, core::cvar_t const &oldCVar, core::cvar_t const &newCVar){
    int64_t hash = hash_djb2(name);
    NameHashToCVarChangeHandlerListIndex::const_iterator it = g_nameHashToCVarChangeHandlerListIndex.find(hash);
    if(it != g_nameHashToCVarChangeHandlerListIndex.end()){
        CVarChangeHandlerList const &list = it->second;
        for(CVarChangeHandlerList::const_iterator it2 = list.begin(); it2 != list.end(); it2++){
            (*it2)(oldCVar, newCVar);
        }
    }
}

//////////////////////////////////////////////////////////////////////////

