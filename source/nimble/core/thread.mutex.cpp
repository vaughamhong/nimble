//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/core/thread.mutex.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::core;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ScopedLock::ScopedLock(Mutex *pLock)
:m_pLock(pLock){
    m_pLock->lock();
}
//! Destructor
ScopedLock::~ScopedLock(){
    m_pLock->unlock();
}

//////////////////////////////////////////////////////////////////////////