//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/system.h>
#include <nimble/resource/resourcemanager.h>
#include <nimble/core/locator.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
System::System()
:m_enabled(true){
}
//! Destructor
System::~System(){
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void System::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void System::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void System::registerComponents(){
}
//! Unregister the varius components associate with this system
void System::unregisterComponents(){
}

//////////////////////////////////////////////////////////////////////////

//! Called when entity was created
void System::entityManagerDidCreateEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity){
}
//! Called when entity was destroyed
void System::entityManagerWillDestroyEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity){
}
//! Called when component was added to an entity
void System::entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent){
}
//! Called when component was removed from an entity
void System::entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent){
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was created
void System::componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
}
//! Called when component will be destroyed
void System::componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
}

//////////////////////////////////////////////////////////////////////////

//! Returns our systemId
entity::SystemId System::getSystemId() const{
    return this->getResourceId();
}

//////////////////////////////////////////////////////////////////////////

//! Enables the system
void System::enable(){
    m_enabled = true;
}
//! Disables the system
void System::disable(){
    m_enabled = false;
}
//! Returns true if enabled
//! \return true if enabled
bool System::isEnabled() const{
    return m_enabled;
}

//////////////////////////////////////////////////////////////////////////

//! Sets the name of this system
void System::setName(const char *name){
	m_name = name;
}
//! Returns the name of this system
const char* System::getName() const{
	return m_name.c_str();
}

//////////////////////////////////////////////////////////////////////////