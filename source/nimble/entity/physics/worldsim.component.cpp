//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/physics/worldsim.component.h>

//////////////////////////////////////////////////////////////////////////

#include <btBulletDynamicsCommon.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
WorldSimComponent::WorldSimComponent(){
}
//! Destructor
WorldSimComponent::~WorldSimComponent(){
}

//////////////////////////////////////////////////////////////////////////

//! Deserialize from a property tree
//! Once this call is completed, the component should be ready to use
void WorldSimComponent::deserialize(core::PropertyTree &properties){
    // set up our initial gravity
    if(properties.existsChildWithName("gravity")){
        float gravity = properties["gravity"][0]->getFloatValue();
        m_pDiscreteDynamicsWorld->setGravity(btVector3(0, gravity, 0));
    }
}
//! Serailize into a property tree
void WorldSimComponent::serialize(core::PropertyTree &properties){
    NIMBLE_ASSERT(false);
}

//////////////////////////////////////////////////////////////////////////