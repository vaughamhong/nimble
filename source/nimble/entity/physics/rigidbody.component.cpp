//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/physics/rigidbody.component.h>
#include <nimble/entity/scene/scenetransform.component.h>
#include <nimble/entity/entity.h>
#include <nimble/scene/scenetransform.h>
#include <nimble/math/vector.h>

//////////////////////////////////////////////////////////////////////////

#include <btBulletDynamicsCommon.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
RigidBodyComponent::RigidBodyComponent(){
    m_pSceneTransform = 0;
    m_mass = 0.0f;
    m_pRigidBody = 0;
    m_pMotionState = 0;
    m_pCollisionShape = 0;
    m_groupMask = 0;
    m_collisionMask = 0;
}
//! Destructor
RigidBodyComponent::~RigidBodyComponent(){
    if(m_pRigidBody){
        delete m_pRigidBody;
        m_pRigidBody = 0;
    }
    if(m_pMotionState){
        delete m_pMotionState;
        m_pMotionState = 0;
    }
    if(m_pCollisionShape){
        delete m_pCollisionShape;
        m_pCollisionShape = 0;
    }
}

//////////////////////////////////////////////////////////////////////////

//! Deserialize from a property tree
//! Once this call is completed, the component should be ready to use
void RigidBodyComponent::deserialize(core::PropertyTree &properties){
    // setup our mass
    m_mass = 0.0f;
    if(properties.existsChildWithName("mass")){
        m_mass = properties["mass"][0]->getFloatValue();
    }
    
    // setup our mass
    m_groupMask = 0xffff;
    if(properties.existsChildWithName("group")){
        m_groupMask = properties["group"][0]->getIntValue();
    }
    
    // setup our mass
    m_collisionMask = 0xffff;
    if(properties.existsChildWithName("collisionMask")){
        m_collisionMask = properties["collisionMask"][0]->getIntValue();
    }
    
    // shape
    if(properties.existsChildWithName("shape")){
        core::PropertyTree &shape = *properties["shape"][0];
        std::string shapeType = shape["type"][0]->getValue();
        if(shapeType == "sphere"){
            float radius = shape["radius"][0]->getFloatValue();
            m_pCollisionShape = new /*( physics dynamic )*/ btSphereShape(radius);
        }else if(shapeType == "box"){
            float halfWidth = shape["width"][0]->getFloatValue() / 2.0f;
            float halfHeight = shape["height"][0]->getFloatValue() / 2.0f;
            float halfDepth = shape["depth"][0]->getFloatValue() / 2.0f;
            m_pCollisionShape = new /*( physics dynamic )*/ btBoxShape(btVector3(halfWidth, halfHeight, halfDepth));
        }else if(shapeType == "plane"){
            float x = shape["x"][0]->getFloatValue();
            float y = shape["y"][0]->getFloatValue();
            float z = shape["z"][0]->getFloatValue();
            float distance = shape["distance"][0]->getFloatValue();
            m_pCollisionShape = new /*( physics dynamic )*/ btStaticPlaneShape(btVector3(x, y, z), distance);
        }else if(shapeType == "convexhull"){
            if(shape.existsChildWithName("position")){
                core::PropertyTree::PtrList positions;
                shape.filterChildrenWithName("position", positions);
                
                btAlignedObjectArray<btVector3>	vertices;
                for(core::PropertyTree::PtrList::iterator it = positions.begin(); it != positions.end(); it++){
                    core::PropertyTree &position = *(*it);
                    float x = position["x"][0]->getFloatValue();
                    float y = position["y"][0]->getFloatValue();
                    float z = position["z"][0]->getFloatValue();
                    vertices.push_back(btVector3(x, y, z));
                }
                m_pCollisionShape = new /*( physics dynamic )*/ btConvexHullShape(&(vertices[0].getX()), vertices.size());
            }else{
                NIMBLE_LOG_WARNING("entity", "Failed to find convext hull points");
            }
        }else{
            NIMBLE_LOG_WARNING("entity", "No collision shape was defined. Default to sphere of size 1.");
            m_pCollisionShape = new /*( physics dynamic )*/ btSphereShape(1);
        }
    }else{
        NIMBLE_LOG_WARNING("entity", "No collision shape was defined. Default to sphere of size 1.");
        m_pCollisionShape = new /*( physics dynamic )*/ btSphereShape(1);
    }
    
    // initial rotation and position
    btVector3 position(0.0f, 0.0f, 0.0f);
    if(properties.existsChildWithName("position")){
        core::PropertyTree &positionProperty = *properties["position"][0];
        position[0] = positionProperty["x"][0]->getFloatValue();
        position[1] = positionProperty["y"][0]->getFloatValue();
        position[2] = positionProperty["z"][0]->getFloatValue();
    }
    btQuaternion rotation(0.0f, 0.0f, 0.0f, 1.0f);
    if(properties.existsChildWithName("rotation")){
        core::PropertyTree &rotationProperty = *properties["rotation"][0];
        rotation[0] = rotationProperty["x"][0]->getFloatValue();
        rotation[1] = rotationProperty["y"][0]->getFloatValue();
        rotation[2] = rotationProperty["z"][0]->getFloatValue();
        rotation[3] = rotationProperty["w"][0]->getFloatValue();
    }
    m_pMotionState = new /*( physics dynamic )*/ btDefaultMotionState(btTransform(rotation, position));
    
    // initial inertia
    btVector3 inertia(0.0f, 0.0f, 0.0f);
    if(properties.existsChildWithName("inertia")){
        core::PropertyTree &inertiaProperty = *properties["inertia"][0];
        inertia[0] = inertiaProperty["x"][0]->getFloatValue();
        inertia[1] = inertiaProperty["y"][0]->getFloatValue();
        inertia[2] = inertiaProperty["z"][0]->getFloatValue();
    }
    m_pCollisionShape->calculateLocalInertia(m_mass, inertia);
    
    // restitution
    // the elasticity of an object, how much energy is transfered back on collision.
    // the higher the restitution, the more energy is transfered back, making objects bouncier.
    btScalar restitution = 0.0f;
    if(properties.existsChildWithName("restitution")){
        restitution = properties["restitution"][0]->getFloatValue();
    }
    
    // friction
    btScalar friction = 0.0f;
    if(properties.existsChildWithName("friction")){
        friction = properties["friction"][0]->getFloatValue();
    }
    
    // rolling friction
    // round objects with no rolling friction will roll forever.
    btScalar rollingFriction = 0.0f;
    if(properties.existsChildWithName("rollingFriction")){
        rollingFriction = properties["rollingFriction"][0]->getFloatValue();
    }
    
    // linear damping
    btScalar linearDamping = 0.0f;
    if(properties.existsChildWithName("linearDamping")){
        linearDamping = properties["linearDamping"][0]->getFloatValue();
    }
    
    // angular damping
    btScalar angularDamping = 0.0f;
    if(properties.existsChildWithName("angularDamping")){
        angularDamping = properties["angularDamping"][0]->getFloatValue();
    }
    
    // set up our construction info
    btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(m_mass, m_pMotionState, m_pCollisionShape, inertia);
    rigidBodyCI.m_mass = m_mass;
    rigidBodyCI.m_restitution = restitution;
    rigidBodyCI.m_friction = friction;
    rigidBodyCI.m_rollingFriction = rollingFriction;
    rigidBodyCI.m_linearDamping = linearDamping;
    rigidBodyCI.m_angularDamping = angularDamping;
    
    // set up our rigid body
    m_pRigidBody = new /*( physics dynamic )*/ btRigidBody(rigidBodyCI);
}
//! Serailize into a property tree
void RigidBodyComponent::serialize(core::PropertyTree &properties){
    NIMBLE_ASSERT_PRINT(false, "TODO - Implement serialize");
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was added to an entity
void RigidBodyComponent::didAddParentEntity(entity::Entity *pEntity){
    m_pSceneTransform = dynamic_cast<scene::SceneTransform*>(getEntity()->getComponent<entity::SceneTransformComponent>());
    if(m_pSceneTransform != 0){
        // fetch our updated transform
        btTransform updatedTransform;
        this->m_pRigidBody->getMotionState()->getWorldTransform(updatedTransform);
        
        // update our position
        math::Vector3f updatedPosition(updatedTransform.getOrigin().getX(),
                                       updatedTransform.getOrigin().getY(),
                                       updatedTransform.getOrigin().getZ());
        math::Matrix4x4f translationMatrix = math::Matrix4x4f::translationMatrix(updatedPosition);
        
        // update our rotation
        math::Quaternion4f updatedRotation(updatedTransform.getRotation().getX(),
                                           updatedTransform.getRotation().getY(),
                                           updatedTransform.getRotation().getZ(),
                                           updatedTransform.getRotation().getW());
        math::Matrix4x4f rotationMatrix = math::Matrix4x4f::rotationMatrix(updatedRotation);
        
        this->m_pSceneTransform->setTranslation(updatedPosition);
        this->m_pSceneTransform->setRotation(updatedRotation);
    }else{
        NIMBLE_LOG_ERROR("physics", "Failed to query and setup transform node");
    }
}
//! Called when component will be removed from entity
void RigidBodyComponent::willRemoveParentEntity(entity::Entity *pEntity){
    m_pSceneTransform = 0;
}

//////////////////////////////////////////////////////////////////////////