//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/physics/worldsimupdate.system.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/entity/entity.h>
#include <nimble/physics/rigidbody.h>
#include <nimble/scene/scenetransform.h>
#include <nimble/math/vector.h>
#include <nimble/math/quaternion.h>
#include <btBulletDynamicsCommon.h>

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/physics/worldsim.component.h>
#include <nimble/entity/physics/rigidbody.component.h>
#define COMPONENT_TUPLESET \
COMPONENT_TUPLE(entity::WorldSimComponent,  "WorldSim",    core::ObjectFactory::ConstructorFunc(this, &WorldSimUpdateSystem::buildWorldSim)) \
COMPONENT_TUPLE(entity::RigidBodyComponent, "RigidBody",   core::ObjectFactory::ConstructorFunc(this, &WorldSimUpdateSystem::buildRigidBody))

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
WorldSimUpdateSystem::WorldSimUpdateSystem()
:m_maxSubSteps(kDefaultMaxSubSteps){
}
//! Destructor
WorldSimUpdateSystem::~WorldSimUpdateSystem(){
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void WorldSimUpdateSystem::registerComponents(){
#define COMPONENT_TUPLE(CLASS, NAME, BUILDER) entity::ComponentManager::get()->registerComponent<CLASS>(NAME, BUILDER);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}
//! Unregister the varius components associate with this system
void WorldSimUpdateSystem::unregisterComponents(){
#define COMPONENT_TUPLE(CLASS, NAME, BUILDER) entity::ComponentManager::get()->unregisterComponent(NAME);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}
//! Called when component was created
void WorldSimUpdateSystem::componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(physics::WorldSim *pWorldSim = dynamic_cast<physics::WorldSim*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // check that we don't already have a reference
        WorldSimList::iterator it = std::find(m_worldSimList.begin(), m_worldSimList.end(), pWorldSim);
        if(it != m_worldSimList.end()){
            return;
        }
        
        // add reference
        m_worldSimList.push_back(pWorldSim);
    }
}
//! Called when component will be destroyed
void WorldSimUpdateSystem::componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(physics::WorldSim *pWorldSim = dynamic_cast<physics::WorldSim*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // check that we have a reference
        WorldSimList::iterator it = std::find(m_worldSimList.begin(), m_worldSimList.end(), pWorldSim);
        if(it == m_worldSimList.end()){
            return;
        }
        
        // remove reference
        m_worldSimList.erase(it);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void WorldSimUpdateSystem::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void WorldSimUpdateSystem::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void WorldSimUpdateSystem::update(uint64_t frameIndex, double interval){
    core::ScopedLock lock(&m_lock);
    
    // simulate a single tick in our world
	btScalar timestep = ((float)interval / 1000.0f);
    
    // update world simulation
    for(WorldSimList::iterator it = m_worldSimList.begin(); it != m_worldSimList.end(); it++){
        physics::WorldSim *pWorldSim = dynamic_cast<physics::WorldSim*>(*it);
        
        // lock our world simulation data while we update
        // this prevents another thread from adding / removing rigid bodies
        core::ScopedLock lock(&pWorldSim->m_lock);
        
        // simulate
        pWorldSim->m_pDiscreteDynamicsWorld->stepSimulation(timestep, m_maxSubSteps);
        
        // update rigid bodies
        typedef std::vector<physics::rigidBody*> RigidBodyList;
        RigidBodyList &rigidBodies = pWorldSim->m_rigidBodies;
        for(RigidBodyList::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++){
			physics::rigidBody *pRigidBody = *it;
            
            // fetch our updated transform
            btTransform updatedTransform;
            pRigidBody->m_pMotionState->getWorldTransform(updatedTransform);

            // update our position
            math::Vector3f updatedPosition(updatedTransform.getOrigin().getX(),
                                           updatedTransform.getOrigin().getY(),
                                           updatedTransform.getOrigin().getZ());
            math::Matrix4x4f translationMatrix = math::Matrix4x4f::translationMatrix(updatedPosition);

            // update our rotation
            math::Quaternion4f updatedRotation(updatedTransform.getRotation().getX(),
                                               updatedTransform.getRotation().getY(),
                                               updatedTransform.getRotation().getZ(),
                                               updatedTransform.getRotation().getW());
            math::Matrix4x4f rotationMatrix = math::Matrix4x4f::rotationMatrix(updatedRotation);
            
            pRigidBody->m_pSceneTransform->setTranslation(updatedPosition);
            pRigidBody->m_pSceneTransform->setRotation(updatedRotation);
        }
    }
}

//////////////////////////////////////////////////////////////////////////

//! Sets the max sub steps
void WorldSimUpdateSystem::setMaxSubSteps(int32_t steps){
    m_maxSubSteps = steps;
}

//////////////////////////////////////////////////////////////////////////

//! Builds an bullet rigid body component
core::Object* WorldSimUpdateSystem::buildRigidBody(){
    return new /*( physics dynamic )*/ RigidBodyComponent();
}
//! Builds an bullet world simulation component
core::Object* WorldSimUpdateSystem::buildWorldSim(){
    return new /*( physics dynamic )*/ WorldSimComponent();
}

////////////////////////////////////////////////////////////////////////////