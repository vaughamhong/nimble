//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/scene/sceneupdate.system.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/scene/scenetransform.h>

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/scene/scene.component.h>
#include <nimble/entity/scene/scenetransform.component.h>
#define COMPONENT_TUPLESET \
COMPONENT_TUPLE(entity::SceneComponent,              "Scene") \
COMPONENT_TUPLE(entity::SceneTransformComponent,     "SceneTransform") \

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

class SceneUpdateVisitor{
public:
    //! Executes on an object
    void execute(scene::SceneTransform* pSceneTransform){
        // update our transform before our children
        pSceneTransform->updateWorldTransform();
        
        // update each of the scene object's children
        scene::SceneTransform::ListType::iterator it;
        scene::SceneTransform::ListType children;
        pSceneTransform->getChildren(children);
        for(scene::SceneTransform::ListType::iterator it = children.begin(); it != children.end(); it++){
            this->execute(*it);
        }
        // update bounds (now that all children have been updated)
        pSceneTransform->updateWorldBounds();
    }
};

//////////////////////////////////////////////////////////////////////////

//! Constructor
SceneUpdateSystem::SceneUpdateSystem(){
}
//! Destructor
SceneUpdateSystem::~SceneUpdateSystem(){
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void SceneUpdateSystem::registerComponents(){
#define COMPONENT_TUPLE(CLASS, NAME) entity::ComponentManager::get()->registerComponent<CLASS>(NAME);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}
//! Unregister the varius components associate with this system
void SceneUpdateSystem::unregisterComponents(){
#define COMPONENT_TUPLE(CLASS, NAME) entity::ComponentManager::get()->unregisterComponent(NAME);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was created
void SceneUpdateSystem::componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(scene::IScene *pScene = dynamic_cast<scene::IScene*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // components should only be created once
        SceneToSceneInfoIndex::iterator it = m_sceneToSceneInfoIndex.find(pScene);
        NIMBLE_ASSERT(it == m_sceneToSceneInfoIndex.end());
        
        // generate our scene info
        sceneInfo_t info;
        info.pScene = pScene;

        // add to our index
        m_sceneToSceneInfoIndex.insert(std::make_pair(pScene, info));
    }
}
//! Called when component will be destroyed
void SceneUpdateSystem::componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(scene::IScene *pScene = dynamic_cast<scene::IScene*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // components should only be created once
        SceneToSceneInfoIndex::iterator it = m_sceneToSceneInfoIndex.find(pScene);
        if(it == m_sceneToSceneInfoIndex.end()){
            NIMBLE_LOG_WARNING("sceneupdatesystem", "Failed to remove scene component %x from update system - update system does not own the scene");
            return;
        }
        
        // remove from index
        m_sceneToSceneInfoIndex.erase(it);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void SceneUpdateSystem::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void SceneUpdateSystem::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void SceneUpdateSystem::update(uint64_t frameIndex, double interval){
    core::ScopedLock lock(&m_lock);
    
    // process scenes
    for(SceneToSceneInfoIndex::iterator it = m_sceneToSceneInfoIndex.begin(); it != m_sceneToSceneInfoIndex.end(); it++){
        sceneInfo_t &info = it->second;
        
        // transform from root
        scene::SceneTransform *pRootTransform = info.pScene->getRootSceneTransform();
        SceneUpdateVisitor updateVisitor;
        updateVisitor.execute(pRootTransform);
    }
}

////////////////////////////////////////////////////////////////////////////