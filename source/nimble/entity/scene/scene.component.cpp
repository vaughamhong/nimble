//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/scene/scene.component.h>
#include <nimble/core/propertytree.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
SceneComponent::SceneComponent(){
}
//! Destructor
SceneComponent::~SceneComponent(){
}

//////////////////////////////////////////////////////////////////////////

//! Deserialize from a property tree
//! Once this call is completed, the component should be ready to use
void SceneComponent::deserialize(core::PropertyTree &properties){
    // initial rotation and position
    math::Vector3f translation(0.0f, 0.0f, 0.0f);
    math::Quaternion4f rotation(0.0f, 0.0f, 0.0f, 1.0f);
    math::Vector3f scale(1.0f, 1.0f, 1.0f);
    
    if(properties.existsChildWithName("position")){
        core::PropertyTree &positionProperty = *properties["position"][0];
        
        translation[0] = positionProperty["x"][0]->getFloatValue();
        translation[1] = positionProperty["y"][0]->getFloatValue();
        translation[2] = positionProperty["z"][0]->getFloatValue();
    }
    if(properties.existsChildWithName("rotation")){
        core::PropertyTree &rotationProperty = *properties["rotation"][0];
        
        rotation[0] = rotationProperty["x"][0]->getFloatValue();
        rotation[1] = rotationProperty["y"][0]->getFloatValue();
        rotation[2] = rotationProperty["z"][0]->getFloatValue();
        rotation[3] = rotationProperty["w"][0]->getFloatValue();
    }
    if(properties.existsChildWithName("scale")){
        core::PropertyTree &positionProperty = *properties["scale"][0];
        
        scale[0] = positionProperty["x"][0]->getFloatValue();
        scale[1] = positionProperty["y"][0]->getFloatValue();
        scale[2] = positionProperty["z"][0]->getFloatValue();
    }
    
    scene::SceneTransform *pSceneTransform = getRootSceneTransform();
    pSceneTransform->setTranslation(translation);
    pSceneTransform->setScale(scale);
    pSceneTransform->setRotation(rotation);
}
//! Serailize into a property tree
void SceneComponent::serialize(core::PropertyTree &properties){
    NIMBLE_ASSERT(false);
}

//////////////////////////////////////////////////////////////////////////
