//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/script/javascript.component.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
JavascriptComponent::JavascriptComponent(v8::Isolate *pIsolate)
:script::JavascriptEnvironment(pIsolate){
}
//! Destructor
JavascriptComponent::~JavascriptComponent(){
}

//////////////////////////////////////////////////////////////////////////