//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/script/javascriptupdate.system.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/math/mathops.h>
#include <nimble/core/cvar.h>
#include <nimble/core/scopedprofiler.h>

//////////////////////////////////////////////////////////////////////////

#include "v8.h"
#include "libplatform/libplatform.h"

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/script/javascript.component.h>
#define COMPONENT_TUPLESET \
COMPONENT_TUPLE(entity::JavascriptComponent, "Javascript", core::ObjectFactory::ConstructorFunc(this, &JavascriptUpdateSystem::buildJavascriptComponent))

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
JavascriptUpdateSystem::JavascriptUpdateSystem()
:m_pPlatform(0)
,m_pIsolate(0){
    v8::V8::InitializeICU();
    m_pPlatform = v8::platform::CreateDefaultPlatform();
    v8::V8::InitializePlatform(m_pPlatform);
    v8::V8::Initialize();
    m_pIsolate = v8::Isolate::New();
}
//! Destructor
JavascriptUpdateSystem::~JavascriptUpdateSystem(){
    //m_context.Reset();
    m_pIsolate->Dispose();
    v8::V8::Dispose();
    v8::V8::ShutdownPlatform();
    delete m_pPlatform;
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void JavascriptUpdateSystem::registerComponents(){
#define COMPONENT_TUPLE(CLASS, NAME, BUILDER) entity::ComponentManager::get()->registerComponent<CLASS>(NAME, BUILDER);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}
//! Unregister the varius components associate with this system
void JavascriptUpdateSystem::unregisterComponents(){
#define COMPONENT_TUPLE(CLASS, NAME, BUILDER) entity::ComponentManager::get()->unregisterComponent(NAME);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}

//! Called when component was added to an entity
void JavascriptUpdateSystem::entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(script::JavascriptEnvironment *pEnvironment = dynamic_cast<script::JavascriptEnvironment*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // components should only be created once
        EnvironmentToInfoIndex::iterator it = m_environmentToInfoIndex.find(pEnvironment);
        NIMBLE_ASSERT(it == m_environmentToInfoIndex.end());
        
        // generate our info
        environmentInfo_t info;
        info.pEnvironment = pEnvironment;
        
        // add to our index
        m_environmentToInfoIndex.insert(std::make_pair(pEnvironment, info));
        
        // setup our environment
        pEnvironment->setDelegate(this);
    }
}
//! Called when component was removed from an entity
void JavascriptUpdateSystem::entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(script::JavascriptEnvironment *pEnvironment = dynamic_cast<script::JavascriptEnvironment*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // components should only be created once
        EnvironmentToInfoIndex::iterator it = m_environmentToInfoIndex.find(pEnvironment);
        if(it == m_environmentToInfoIndex.end()){
            NIMBLE_LOG_WARNING("javascriptupdatesystem", "Failed to remove javascript environment component %x from update system - update system does not own the javascript environment");
            return;
        }
        
        // remove from index
        m_environmentToInfoIndex.erase(it);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void JavascriptUpdateSystem::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void JavascriptUpdateSystem::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void JavascriptUpdateSystem::update(uint64_t frameIndex, double interval){
    const int kGCModeLazy = 0;
    const int kGCModePassive = 1;
    const int kGCModeAggresive = 2;
    
    int mode = core::cvar_getIntValue("js_gcMode");
    if(mode == kGCModeLazy){
        // lazy garbage collection
        // every duration our idle notification completes without finishing
        // we give it more time on the next iteration
        
        static double lazyCleanupInterval = core::cvar_getFloatValue("js_gcLazyIntervalInMS");
        static double lazyMinCleanupDuration = core::cvar_getFloatValue("js_gcLazyMinDurationInMS");
        static double lazyMaxCleanupDuration = core::cvar_getFloatValue("js_gcLazyMaxDurationInMS");
        static double lazyCleanupDurationMult = core::cvar_getFloatValue("js_gcLazyDurationMult");
        static double lazyCleanupDuration = lazyMinCleanupDuration;
        static double duration = 0;

        duration += interval;
        if(duration > lazyCleanupInterval){
            if(m_pIsolate->IdleNotification(lazyCleanupDuration)){
                // IdleNotification returns true, we should stop calling it.
                // we default back to our minimual cleanup duration
                lazyCleanupDuration = lazyMinCleanupDuration;
            }else{
                // IdleNotification returns false, which means it needs to continue cleaning.
                // the next call will give it more time
                lazyCleanupDuration = math::min(lazyCleanupDuration * lazyCleanupDurationMult, lazyMaxCleanupDuration);
            }
            duration = 0.0;
        }
    }else if(mode == kGCModePassive){
        // passive garbage collection
        // every frame give idle notification a chance to collect
        // every time (loop) it does not finish, give it more time
        
        static double passiveMinCleanupDuration = core::cvar_getFloatValue("js_gcPassiveMinDurationInMS");
        static double passiveMaxCleanupDuration = core::cvar_getFloatValue("js_gcPassiveMaxDurationInMS");
        static double passiveCleanupDurationMult = core::cvar_getFloatValue("js_gcPassiveDurationMult");
        static double passiveCleanupDuration = passiveMinCleanupDuration;
        
        while(!m_pIsolate->IdleNotification(passiveCleanupDuration)){
            passiveCleanupDuration = math::min(passiveCleanupDuration * passiveCleanupDurationMult, passiveMaxCleanupDuration);
        }
        passiveCleanupDuration = passiveMinCleanupDuration;
    }else if(mode == kGCModeAggresive){
        // aggressive garbage collection
        // collect as much as it can every single frame
        
        m_pIsolate->LowMemoryNotification();
    }
}

////////////////////////////////////////////////////////////////////////////

//! Builds an javascript component
core::Object* JavascriptUpdateSystem::buildJavascriptComponent(){
    return new /*( dynamic )*/ entity::JavascriptComponent(m_pIsolate);
}

////////////////////////////////////////////////////////////////////////////

//! Called when a javascript environment script will load
void JavascriptUpdateSystem::javascriptEnvironmentScriptWillLoad(script::JavascriptEnvironment *pEnvironment){
    // set our scopes
    v8::Isolate::Scope isolateScope(m_pIsolate);
    v8::HandleScope handleScope(m_pIsolate);
    
    // create our global object template which all objects will be attached
    v8::Local<v8::ObjectTemplate> global = v8::Local<v8::ObjectTemplate>::New(m_pIsolate, pEnvironment->getGlobalTemplate());
    
    // setup global template
    setupGlobalTemplate(pEnvironment);
    
    // create our context with a global template
    v8::Handle<v8::Context> context = v8::Context::New(m_pIsolate, 0, global);
    
    // save our context into a persistent handle which means it will not be collected once out of scope
    // we will manually clean this up once our component has been destroyed
    pEnvironment->getGlobalTemplate().Reset(m_pIsolate, global);
    pEnvironment->getContext().Reset(m_pIsolate, context);
    m_pIsolate->ContextDisposedNotification();
}
//! Called when a javascript environment script failed to load
void JavascriptUpdateSystem::javascriptEnvironmentScriptLoadDidFail(script::JavascriptEnvironment *pEnvironment){
}
//! Called when a javascript environment script was loaded
void JavascriptUpdateSystem::javascriptEnvironmentScriptDidLoad(script::JavascriptEnvironment *pEnvironment){
}
//! Called when a javascript environment script will unload
void JavascriptUpdateSystem::javascriptEnvironmentScriptWillUnload(script::JavascriptEnvironment *pEnvironment){
}

////////////////////////////////////////////////////////////////////////////

//! Called to setup global template
void JavascriptUpdateSystem::setupGlobalTemplate(script::JavascriptEnvironment *pEnvironment){
    v8::Isolate *pIsolate = pEnvironment->getIsolate();
    v8::Local<v8::ObjectTemplate> global = v8::Local<v8::ObjectTemplate>::New(pIsolate,
                                                                              pEnvironment->getGlobalTemplate());
    v8::Handle<v8::Value> environment = v8::External::New(pIsolate, pEnvironment);
    global->Set(v8::String::NewFromUtf8(pIsolate, "addEventListener"),
                v8::FunctionTemplate::New(pIsolate, nativeAddEventListener, environment));
    global->Set(v8::String::NewFromUtf8(pIsolate, "removeEventListener"),
                v8::FunctionTemplate::New(pIsolate, nativeRemoveEventListener, environment));
}

////////////////////////////////////////////////////////////////////////////

//! Returns the platform
v8::Platform* JavascriptUpdateSystem::getPlatform() const{
    return m_pPlatform;
}
//! Returns the isolate
v8::Isolate* JavascriptUpdateSystem::getIsolate() const{
    return m_pIsolate;
}

////////////////////////////////////////////////////////////////////////////

//! Adds event listener
void JavascriptUpdateSystem::nativeAddEventListener(const v8::FunctionCallbackInfo<v8::Value>& args){
    // make sure parameters are what we are looking for
    if(args.Length() != 2){
        NIMBLE_LOG_WARNING("javascript", "Failed to add event listener - invalid number of parameters");
        return;
    }
    if(!args[0]->IsString()){
        NIMBLE_LOG_WARNING("javascript", "Failed to add event listener - invalid event name parameter");
        return;
    }
    if(!args[1]->IsFunction()){
        NIMBLE_LOG_WARNING("javascript", "Failed to add event listener - invalid function parameter");
        return;
    }

    // get our self pointer
    v8::Handle<v8::External> self = v8::Handle<v8::External>::Cast(args.Data());
    script::JavascriptEnvironment *pEnvironment = reinterpret_cast<script::JavascriptEnvironment*>(self->Value());

    // retreive our event name and function
    v8::String::Utf8Value eventName(args[0]);
    v8::Handle<v8::Function> callback = v8::Handle<v8::Function>::Cast(args[1]);

    // add event listener
    pEnvironment->addEventListener(*eventName, callback);
}
//! Removes event listener
void JavascriptUpdateSystem::nativeRemoveEventListener(const v8::FunctionCallbackInfo<v8::Value>& args){
    // make sure parameters are what we are looking for
    if(args.Length() != 1){
        NIMBLE_LOG_WARNING("javascript", "Failed to add event listener - invalid number of parameters");
        return;
    }
    if(!args[0]->IsString()){
        NIMBLE_LOG_WARNING("javascript", "Failed to add event listener - invalid event name parameter");
        return;
    }
    
    // get our self pointer
    v8::Handle<v8::External> self = v8::Handle<v8::External>::Cast(args.Data());
    script::JavascriptEnvironment *pEnvironment = reinterpret_cast<script::JavascriptEnvironment*>(self->Value());
    
    // retreive our event name and function
    v8::String::Utf8Value eventName(args[0]);
    
    // add event listener
    pEnvironment->removeEventListener(*eventName);
}

////////////////////////////////////////////////////////////////////////////