//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/systemsmanager.h>
#include <nimble/entity/system.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/entity/entitymanager.h>
#include <nimble/core/propertytree.h>
#include <nimble/core/assert.h>
#include <nimble/core/locator.h>
#include <nimble/core/scopedprofiler.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Gets our system
SystemsManager* SystemsManager::get(){
    return core::locator_acquire<entity::SystemsManager>();
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
SystemsManager::SystemsManager(entity::EntityManager *pEntityManager, entity::ComponentManager *pComponentManager)
:m_pDelegate(0)
,m_pEntityManager(pEntityManager)
,m_pComponentManager(pComponentManager){
    // make sure we have a valid managers
    if(!m_pEntityManager){
        m_pEntityManager = entity::EntityManager::get();
    }
    NIMBLE_ASSERT(m_pEntityManager != 0);
    m_pEntityManager->setDelegate(this);
    
    if(!m_pComponentManager){
        m_pComponentManager = entity::ComponentManager::get();
    }
    NIMBLE_ASSERT(m_pComponentManager != 0);
    m_pComponentManager->setDelegate(this);
}
//! Destructor
SystemsManager::~SystemsManager(){
    this->destroyAllSystems();
}

//////////////////////////////////////////////////////////////////////////

//! Configures all systems
void SystemsManager::initialize(core::PropertyTree &properties){
    core::PropertyTree::PtrList systems, list;
    properties.filterChildrenWithName("system", systems);
    
    // first pass registers components
    for(core::PropertyTree::PtrList::iterator it = systems.begin(); it != systems.end(); it++){
        // get our class name / config filename for our system
        core::PropertyTree &systemProperties = *(*it);
        
        // we must have a class parameter
        const char *className = 0;
        systemProperties.filterChildrenWithName("class", list);
        if(list.size() > 0){
            className = list[0]->getValue();
        }

        // optional name parameter
        const char *name = className;
        systemProperties.filterChildrenWithName("name", list);
        if(list.size() > 0){
            name = list[0]->getValue();
        }
        
        // create system
        if(entity::System *pSystem = this->createSystemByClass(name, className)){
			pSystem->setName(name);
            pSystem->registerComponents();
        }
    }
    
    // second pass configures system
    for(core::PropertyTree::PtrList::iterator it = systems.begin(); it != systems.end(); it++){
        // get our class name / config filename for our system
        core::PropertyTree &systemProperties = *(*it);
        
        // we must have a class parameter
        const char *className = 0;
        systemProperties.filterChildrenWithName("class", list);
        if(list.size() > 0){
            className = list[0]->getValue();
        }
        
        // optional name parameter
        const char *name = className;
        systemProperties.filterChildrenWithName("name", list);
        if(list.size() > 0){
            name = list[0]->getValue();
        }
        
        if(entity::System *pSystem = this->getSystem(name)){
            pSystem->initialize(systemProperties);
        }
    }
}
//! terminate
void SystemsManager::terminate(){
}
//! Sets our delegate
void SystemsManager::setDelegate(SystemsManagerDelegate *pDelegate){
    m_pDelegate = pDelegate;
}

//////////////////////////////////////////////////////////////////////////

//! Create a system by class
//! \param className the class name of the system
//! \return a pointer to the entity
entity::System* SystemsManager::createSystemByClass(const char *name, const char *className){
    core::ScopedLock lock(&m_lock);
    
    NameToSystemIndex::iterator it = m_nameToSystem.find(std::string(name));
    if(it != m_nameToSystem.end()){
        NIMBLE_LOG_WARNING("", "[SystemsManager::createSystemByClass] A system with a duplicate name has already been created %s", name);
        return 0;
    }
    
    if(entity::System *pSystem = m_factory.create<entity::System>(className)){
        m_database.add(pSystem);
        m_nameToSystem.insert(std::pair<std::string, entity::System*>(std::string(name), pSystem));
        if(m_pDelegate){m_pDelegate->systemWasAdded(this, pSystem);}
        
        NIMBLE_LOG_INFO("", "[Add][name:\"%s\"][class:%s]", name, className);
        return pSystem;
    }
    NIMBLE_LOG_WARNING("", "[SystemsManager::createSystemByClass] Failed to create system %s with name \"%s\"", className, name);
    return 0;
}
//! Destroys an system
//! \param system the system to destroy
void SystemsManager::destroySystem(entity::System* pSystem){
    core::ScopedLock lock(&m_lock);
    
    // remove from index
    for(NameToSystemIndex::iterator it = m_nameToSystem.begin(); it != m_nameToSystem.end();){
        if(it->second == pSystem){
            m_nameToSystem.erase(it++);
        }else{
            it++;
        }
    }
    m_database.remove(pSystem);
    if(m_pDelegate){m_pDelegate->systemWasRemoved(this, pSystem);}

    // destroy system
    if(pSystem != 0){
        delete pSystem;
    }
}
//! Destroys all systems
void SystemsManager::destroyAllSystems(){
    core::ScopedLock lock(&m_lock);
    
    // remove all systems
    for(resource::ResourceList::iterator it = m_database.begin(); it != m_database.end(); it++){
        if(entity::System *pSystem = dynamic_cast<entity::System*>(*it)){
            delete pSystem;
        }else{
            NIMBLE_LOG_ERROR("entity", "Failed to cast resource::Resource to entity::System");
        }
    }
    m_database.clear();
    m_nameToSystem.clear();
}
//! Returns the system by name
//! \param name the name of the system
//! \return the system by name
entity::System* SystemsManager::getSystem(const char *name){
    NameToSystemIndex::iterator it = m_nameToSystem.find(name);
    if(it != m_nameToSystem.end()){
        return it->second;
    }
    return 0;
}
//! Enables a system by name
//! \param name the name of the system to enable
void SystemsManager::enableSystem(const char *name){
    NameToSystemIndex::iterator it = m_nameToSystem.find(std::string(name));
    if(it != m_nameToSystem.end()){
        it->second->enable();
    }
}
//! Disables a system by name
//! \param name the name of the system to disable
void SystemsManager::disableSystem(const char *name){
    NameToSystemIndex::iterator it = m_nameToSystem.find(std::string(name));
    if(it != m_nameToSystem.end()){
        it->second->disable();
    }
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
//! \param interval the elapsed time since the last call
void SystemsManager::updateFrameWithInterval(uint64_t frameIndex, double interval){
    // TODO: consider system updates on a schedule (instead of updating systems on each frame update)
    for(resource::ResourceList::iterator it = m_database.begin(); it != m_database.end(); it++){
        entity::System *pSystem = dynamic_cast<entity::System*>(*it);
        
        NIMBLE_ASSERT(pSystem);
        if(pSystem->isEnabled() == true){
			SCOPEDPROFILE(pSystem->getName());
            pSystem->update(frameIndex, interval);
        }
    }
}

//////////////////////////////////////////////////////////////////////////

//! Called when entity was created
void SystemsManager::entityManagerDidCreateEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity){
    for(resource::ResourceList::iterator it = m_database.begin(); it != m_database.end(); it++){
        entity::System *pSystem = dynamic_cast<entity::System*>(*it);
        pSystem->entityManagerDidCreateEntity(pEntityManager, pEntity);
    }
}
//! Called when entity was destroyed
void SystemsManager::entityManagerWillDestroyEntity(entity::EntityManager *pEntityManager, entity::Entity *pEntity){
    for(resource::ResourceList::iterator it = m_database.begin(); it != m_database.end(); it++){
        entity::System *pSystem = dynamic_cast<entity::System*>(*it);
        pSystem->entityManagerWillDestroyEntity(pEntityManager, pEntity);
    }
}
//! Called when component was added to an entity
void SystemsManager::entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent){
    for(resource::ResourceList::iterator it = m_database.begin(); it != m_database.end(); it++){
        entity::System *pSystem = dynamic_cast<entity::System*>(*it);
        pSystem->entityDidAddComponent(pEntity, pComponent);
    }
}
//! Called when component was removed from an entity
void SystemsManager::entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent){
    for(resource::ResourceList::iterator it = m_database.begin(); it != m_database.end(); it++){
        entity::System *pSystem = dynamic_cast<entity::System*>(*it);
        pSystem->entityWillRemoveComponent(pEntity, pComponent);
    }
}
//! Called when component was created
void SystemsManager::componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
    for(resource::ResourceList::iterator it = m_database.begin(); it != m_database.end(); it++){
        entity::System *pSystem = dynamic_cast<entity::System*>(*it);
        pSystem->componentManagerDidCreateComponent(pComponentManager, pComponent);
    }
}
//! Called when component will be destroyed
void SystemsManager::componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
    for(resource::ResourceList::iterator it = m_database.begin(); it != m_database.end(); it++){
        entity::System *pSystem = dynamic_cast<entity::System*>(*it);
        pSystem->componentManagerWillDestroyComponent(pComponentManager, pComponent);
    }
}

//////////////////////////////////////////////////////////////////////////