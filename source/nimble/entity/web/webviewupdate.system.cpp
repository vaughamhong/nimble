//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/web/webviewupdate.system.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/core/scopedprofiler.h>

#if !defined(NIMBLE_TARGET_ANDROID)
#include <Awesomium/STLHelpers.h>
#include <Awesomium/BitmapSurface.h>
#endif

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/web/webview.component.h>
#define COMPONENT_TUPLESET \
COMPONENT_TUPLE(entity::WebViewComponent,  "WebView", core::ObjectFactory::ConstructorFunc(this, &WebViewUpdateSystem::buildWebViewComponent))

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
//! Constructor
WebViewUpdateSystem::WebViewUpdateSystem()
:m_pWebCore(0){
    m_pWebCore = Awesomium::WebCore::Initialize(Awesomium::WebConfig());
}
#else
//! Constructor
WebViewUpdateSystem::WebViewUpdateSystem(){
}
#endif
//! Destructor
WebViewUpdateSystem::~WebViewUpdateSystem(){
#if !defined(NIMBLE_TARGET_ANDROID)
    Awesomium::WebCore::Shutdown();
    m_pWebCore = 0;
#endif
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void WebViewUpdateSystem::registerComponents(){
#define COMPONENT_TUPLE(CLASS, NAME, BUILDER) entity::ComponentManager::get()->registerComponent<CLASS>(NAME, BUILDER);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}
//! Unregister the varius components associate with this system
void WebViewUpdateSystem::unregisterComponents(){
#define COMPONENT_TUPLE(CLASS, NAME, BUILDER) entity::ComponentManager::get()->unregisterComponent(NAME);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was added to an entity
void WebViewUpdateSystem::entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent){
	// we are only interested if the entity has component(s) we are interested in
    if (web::WebView *pWebView = dynamic_cast<web::WebView*>(pComponent)){
		core::ScopedLock lock(&m_lock);

		// components should only be created once
		WebViewToWebViewInfoIndex::iterator it = m_webViewToWebViewInfoIndex.find(pWebView);
		NIMBLE_ASSERT(it == m_webViewToWebViewInfoIndex.end());

		// generate our info
		webViewInfo_t info;
		info.pWebView = pWebView;

		// add to our index
		m_webViewToWebViewInfoIndex.insert(std::make_pair(pWebView, info));
	}
}
//! Called when component was removed from an entity
void WebViewUpdateSystem::entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent){
	// we are only interested if the entity has component(s) we are interested in
    if (web::WebView *pWebView = dynamic_cast<web::WebView*>(pComponent)){
		core::ScopedLock lock(&m_lock);

		// components should only be created once
		WebViewToWebViewInfoIndex::iterator it = m_webViewToWebViewInfoIndex.find(pWebView);
		if (it == m_webViewToWebViewInfoIndex.end()){
			NIMBLE_LOG_WARNING("webviewupdatesystem", "Failed to remove web view %x from update system - update system does not own the web view");
			return;
		}

		// remove from index
		m_webViewToWebViewInfoIndex.erase(it);
	}
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void WebViewUpdateSystem::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void WebViewUpdateSystem::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void WebViewUpdateSystem::update(uint64_t frameIndex, double interval){
#if !defined(NIMBLE_TARGET_ANDROID)
    SCOPEDPROFILE("WebViewUpdateSystem::update")
    
    // update our web core
    if(m_pWebCore != 0){
        m_pWebCore->Update();
    }
    
	// process scenes
	for(WebViewToWebViewInfoIndex::iterator it = m_webViewToWebViewInfoIndex.begin(); it != m_webViewToWebViewInfoIndex.end(); it++){
		webViewInfo_t &info = it->second;
        
        // make sure we have a valid web view
        web::WebView *pWebView = info.pWebView;
        if(pWebView == 0){
            continue;
        }
        
        // make sure we have a valid web view
        Awesomium::WebView *pAwesomiumWebView = pWebView->getWebView();
        if(pAwesomiumWebView == 0){
            continue;
        }
        
        // make sure our surface has changed
        Awesomium::BitmapSurface* surface = (Awesomium::BitmapSurface*)pAwesomiumWebView->surface();
        if(surface == 0){
            continue;
        }
        if(surface->is_dirty() == false){
            continue;
        }
        
		// make sure we have a lock target
		core::ILockable *pLockable = pWebView->getLockTarget();
		if(pLockable == 0){
			continue;
		}

		// lock our buffer
		char *pDestBuffer = 0;
		{
			SCOPEDPROFILE("webview lock")
			pLockable->lock(core::kLockTypeReadWrite, &pDestBuffer);
			if (pDestBuffer == 0){
				continue;
			}
		}

		// copy into our buffer
		{
			SCOPEDPROFILE("webview memcpy")
            int rowSpan = pWebView->getBufferWidth() * pWebView->getBytesPerPixel();
            int depth = pWebView->getBytesPerPixel();
            surface->CopyTo((unsigned char *)pDestBuffer, rowSpan, depth, false, false);
		}

		// unlock buffer
		{
			SCOPEDPROFILE("webview unlock")
			pLockable->unlock();
		}
	}
#endif
}

////////////////////////////////////////////////////////////////////////////

//! Builds an MediaPlayer component
core::Object* WebViewUpdateSystem::buildWebViewComponent(){
#if !defined(NIMBLE_TARGET_ANDROID)
    return new /*( dynamic )*/ entity::WebViewComponent(m_pWebCore);
#else
    return new /*( dynamic )*/ entity::WebViewComponent();
#endif
}

////////////////////////////////////////////////////////////////////////////