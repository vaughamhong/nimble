//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/web/webview.component.h>
#include <nimble/entity/entity.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

#if !defined(NIMBLE_TARGET_ANDROID)
//! Constructor
WebViewComponent::WebViewComponent(Awesomium::WebCore *pWebCore)
:web::WebView(pWebCore){
}
#else
//! Constructor
WebViewComponent::WebViewComponent()
:web::WebView(){
}
#endif
//! Destructor
WebViewComponent::~WebViewComponent(){
}

//////////////////////////////////////////////////////////////////////////