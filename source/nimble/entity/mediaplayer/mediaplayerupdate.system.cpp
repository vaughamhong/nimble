//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/mediaplayer/mediaplayerupdate.system.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/core/scopedprofiler.h>
#include <nimble/mediaplayer/mediaplayer.h>

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/mediaplayer/MediaPlayer.component.h>
#define COMPONENT_TUPLESET \
COMPONENT_TUPLE(entity::MediaPlayerComponent,  "MediaPlayer", core::ObjectFactory::ConstructorFunc(this, &MediaPlayerUpdateSystem::buildMediaPlayerComponent))

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
MediaPlayerUpdateSystem::MediaPlayerUpdateSystem(){
}
//! Destructor
MediaPlayerUpdateSystem::~MediaPlayerUpdateSystem(){
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void MediaPlayerUpdateSystem::registerComponents(){
#define COMPONENT_TUPLE(CLASS, NAME, BUILDER) entity::ComponentManager::get()->registerComponent<CLASS>(NAME, BUILDER);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}
//! Unregister the varius components associate with this system
void MediaPlayerUpdateSystem::unregisterComponents(){
#define COMPONENT_TUPLE(CLASS, NAME, BUILDER) entity::ComponentManager::get()->unregisterComponent(NAME);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was added to an entity
void MediaPlayerUpdateSystem::entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent){
	// we are only interested if the entity has component(s) we are interested in
	if (media::MediaPlayer *pMediaPlayer = dynamic_cast<media::MediaPlayer*>(pComponent)){
		core::ScopedLock lock(&m_lock);

		// components should only be created once
		MediaPlayerToMediaPlayerInfoIndex::iterator it = m_MediaPlayerToMediaPlayerInfoIndex.find(pMediaPlayer);
		NIMBLE_ASSERT(it == m_MediaPlayerToMediaPlayerInfoIndex.end());

		// generate our info
		MediaPlayerInfo_t info;
		info.pMediaPlayer = pMediaPlayer;

		// add to our index
		m_MediaPlayerToMediaPlayerInfoIndex.insert(std::make_pair(pMediaPlayer, info));
	}
}
//! Called when component was removed from an entity
void MediaPlayerUpdateSystem::entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent){
	// we are only interested if the entity has component(s) we are interested in
	if (media::MediaPlayer *pMediaPlayer = dynamic_cast<media::MediaPlayer*>(pComponent)){
		core::ScopedLock lock(&m_lock);

		// components should only be created once
		MediaPlayerToMediaPlayerInfoIndex::iterator it = m_MediaPlayerToMediaPlayerInfoIndex.find(pMediaPlayer);
		if (it == m_MediaPlayerToMediaPlayerInfoIndex.end()){
			NIMBLE_LOG_WARNING("MediaPlayerupdatesystem", "Failed to remove video player %x from update system - update system does not own the video player");
			return;
		}

		// remove from index
		m_MediaPlayerToMediaPlayerInfoIndex.erase(it);
	}
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void MediaPlayerUpdateSystem::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void MediaPlayerUpdateSystem::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void MediaPlayerUpdateSystem::update(uint64_t frameIndex, double interval){
	// process scenes
	for (MediaPlayerToMediaPlayerInfoIndex::iterator it = m_MediaPlayerToMediaPlayerInfoIndex.begin(); it != m_MediaPlayerToMediaPlayerInfoIndex.end(); it++){
		MediaPlayerInfo_t &info = it->second;
		media::MediaPlayer *pMediaPlayer = info.pMediaPlayer;

        // don't do anything if it is not playing
        if(!pMediaPlayer->isPlaying()){
            continue;
        }
        
		// make sure we have a lock target
		core::ILockable *pLockable = pMediaPlayer->getLockTarget();
		if(pLockable == 0){
			continue;
		}

		// lock our buffer
		char *pDestBuffer = 0;
		{
			SCOPEDPROFILE("video lock")
			pLockable->lock(core::kLockTypeReadWrite, &pDestBuffer);
			if (pDestBuffer == 0){
				continue;
			}
		}

		// copy into our buffer
		{
			SCOPEDPROFILE("video memcpy")
			memcpy(pDestBuffer, pMediaPlayer->getBuffer(),
                   pMediaPlayer->getBufferWidth() * pMediaPlayer->getBufferHeight() * pMediaPlayer->getBytesPerPixel());
		}

		// unlock buffer
		{
			SCOPEDPROFILE("video unlock")
			pLockable->unlock();
		}
	}
}

////////////////////////////////////////////////////////////////////////////

//! Builds an MediaPlayer component
core::Object* MediaPlayerUpdateSystem::buildMediaPlayerComponent(){
    return new /*( dynamic )*/ entity::MediaPlayerComponent();
}

////////////////////////////////////////////////////////////////////////////