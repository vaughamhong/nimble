//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/graphics/renderablelist.component.h>
#include <nimble/graphics/renderable.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
RenderableListComponent::RenderableListComponent(){
}
//! Destructor
RenderableListComponent::~RenderableListComponent(){
    removeAllRenderables();
}

//////////////////////////////////////////////////////////////////////////

//! Registers for renderable was added callback
void RenderableListComponent::registerCallbackOnRenderableListDidAddRenderable(RenderableListDidAddRenderableCallback callback){
    RenderableListDidAddRenderableCallbackList::iterator it = std::find(m_renderableListDidAddRenderableCallbacks.begin(), m_renderableListDidAddRenderableCallbacks.end(), callback);
    if(it == m_renderableListDidAddRenderableCallbacks.end()){
        m_renderableListDidAddRenderableCallbacks.push_back(callback);
    }
}
//! Unregisters for renderable was added callback
void RenderableListComponent::unregisterCallbackOnRenderableListDidAddRenderable(RenderableListDidAddRenderableCallback callback){
    RenderableListDidAddRenderableCallbackList::iterator it = std::find(m_renderableListDidAddRenderableCallbacks.begin(), m_renderableListDidAddRenderableCallbacks.end(), callback);
    if(it != m_renderableListDidAddRenderableCallbacks.end()){
        m_renderableListDidAddRenderableCallbacks.erase(it);
    }
}

//! Registers for renderable was removed callback
void RenderableListComponent::registerCallbackOnRenderableListWillRemoveRenderable(RenderableListWillRemoveRenderableCallback callback){
    RenderableListWillRemoveRenderableCallbackList::iterator it = std::find(m_renderableListWillRemoveRenderableCallbacks.begin(), m_renderableListWillRemoveRenderableCallbacks.end(), callback);
    if(it == m_renderableListWillRemoveRenderableCallbacks.end()){
        m_renderableListWillRemoveRenderableCallbacks.push_back(callback);
    }
}
//! Unregisters for renderable was removed callback
void RenderableListComponent::unregisterCallbackOnRenderableListWillRemoveRenderable(RenderableListWillRemoveRenderableCallback callback){
    RenderableListWillRemoveRenderableCallbackList::iterator it = std::find(m_renderableListWillRemoveRenderableCallbacks.begin(), m_renderableListWillRemoveRenderableCallbacks.end(), callback);
    if(it != m_renderableListWillRemoveRenderableCallbacks.end()){
        m_renderableListWillRemoveRenderableCallbacks.erase(it);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Add a renderable
size_t RenderableListComponent::addRenderable(graphics::IRenderable *pRenderable){
    graphics::RenderableList::const_iterator it = std::find(m_renderables.begin(), m_renderables.end(), pRenderable);
    if(it != m_renderables.end()){
        return it - m_renderables.begin();
    }else{
        m_renderables.push_back(pRenderable);
        
        // dispatch callbacks on added renderable
        RenderableListDidAddRenderableCallbackList callbackList = m_renderableListDidAddRenderableCallbacks;
        for(RenderableListDidAddRenderableCallbackList::iterator it = callbackList.begin(); it != callbackList.end(); it++){
            (*it)(this, pRenderable);
        }

        return m_renderables.size() - 1;
    }
}
//! Removes a renderable
void RenderableListComponent::removeRenderable(size_t index){
    if(index < getNumRenderables()){
        graphics::RenderableList::iterator it = m_renderables.begin() + index;
        graphics::IRenderable *pRenderable = *it;
        m_renderables.erase(it);
        
        // dispatch callbacks on removed renderable
        RenderableListWillRemoveRenderableCallbackList callbackList = m_renderableListWillRemoveRenderableCallbacks;
        for(RenderableListWillRemoveRenderableCallbackList::iterator it = callbackList.begin(); it != callbackList.end(); it++){
            (*it)(this, pRenderable);
        }
    }
}
//! Remove all renderables
void RenderableListComponent::removeAllRenderables(){
    // dispatch callbacks to remove all renderables
    for(size_t i = 0; i < getNumRenderables(); i++){
        graphics::IRenderable *pRenderable = this->getRenderable(i);
        
        // dispatch callbacks on removed renderable
        RenderableListWillRemoveRenderableCallbackList callbackList = m_renderableListWillRemoveRenderableCallbacks;
        for(RenderableListWillRemoveRenderableCallbackList::iterator it = callbackList.begin(); it != callbackList.end(); it++){
            (*it)(this, pRenderable);
        }
    }
    
    // remove renderables from this list
    m_renderables.clear();
}
//! Returns all renderables
const graphics::RenderableList& RenderableListComponent::getRenderables() const{
    return m_renderables;
}
//! Returns a renderable by index
graphics::IRenderable* RenderableListComponent::getRenderable(size_t index) const{
    if(index < getNumRenderables()){
        graphics::RenderableList::const_iterator nth = m_renderables.begin() + index;
        return *nth;
    }else{
        return 0;
    }
}
//! Returns the number of renderables
size_t RenderableListComponent::getNumRenderables() const{
    return m_renderables.size();
}

//////////////////////////////////////////////////////////////////////////