//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/graphics/meshskinningupdate.system.h>
#include <nimble/entity/graphics/renderablelist.component.h>
#include <nimble/graphics/irenderable.h>
#include <nimble/graphics/imesh.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/entity/component.h>
#include <nimble/entity/entity.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
MeshSkinningUpdateSystem::MeshSkinningUpdateSystem(){
}
//! Destructor
MeshSkinningUpdateSystem::~MeshSkinningUpdateSystem(){
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void MeshSkinningUpdateSystem::registerComponents(){
}
//! Unregister the varius components associate with this system
void MeshSkinningUpdateSystem::unregisterComponents(){
}
//! Called when component was created
void MeshSkinningUpdateSystem::componentManagerDidCreateComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(entity::RenderableListComponent *pRenderableList = dynamic_cast<entity::RenderableListComponent*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // iterate over renderable list to find suitable meshes to process
        for(size_t i = 0; i != pRenderableList->getNumRenderables(); i++){
            graphics::IRenderable *pRenderable = pRenderableList->getRenderable(i);
            
            // check that we don't already have a reference
            RenderableList::iterator localIt = std::find(m_renderableList.begin(), m_renderableList.end(), pRenderable);
            if(localIt != m_renderableList.end()){
                return;
            }
            
            // add reference
            m_renderableList.push_back(pRenderable);
        }
    }
}
//! Called when component will be destroyed
void MeshSkinningUpdateSystem::componentManagerWillDestroyComponent(entity::ComponentManager *pComponentManager, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(entity::RenderableListComponent *pRenderableList = dynamic_cast<entity::RenderableListComponent*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // iterate over renderable list to find suitable meshes to process
        for(size_t i = 0; i != pRenderableList->getNumRenderables(); i++){
            graphics::IRenderable *pRenderable = pRenderableList->getRenderable(i);
            
            // check that we don't already have a reference
            RenderableList::iterator localIt = std::find(m_renderableList.begin(), m_renderableList.end(), pRenderable);
            if(localIt == m_renderableList.end()){
                return;
            }
            
            // remove reference
            m_renderableList.erase(localIt);
        }
    }
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void MeshSkinningUpdateSystem::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void MeshSkinningUpdateSystem::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void MeshSkinningUpdateSystem::update(uint64_t frameIndex, double interval){
    core::ScopedLock lock(&m_lock);
    
    // // process dynamic meshes
    // for(RenderableList::iterator it = m_renderableList.begin(); it != m_renderableList.end(); it++){
    //     graphics::IRenderable *pRenderable = *it;
//        graphics::IDynamicMesh *pDynamicMesh = dynamic_cast<graphics::IDynamicMesh*>(pRenderable->getMesh());
//        entity::Component *pComponent = dynamic_cast<entity::Component*>(pDynamicMesh);
//        entity::Entity *pEntity = pComponent->getEntity();
//
//        scene::SceneTransform *pSceneTransform = pEntity->getComponent<entity::SceneTransformComponent>();
//        graphics::IMesh *pPoseMesh = pDynamicMesh->getPoseMesh();
//        graphics::IMesh *pTransformedMesh = dynamic_cast<graphics::IMesh*>(pDynamicMesh);
//        
//        // Optimization: check if dirty before transform
//
//        if(pPoseMesh && pTransformedMesh && pSceneTransform){
//            // figure out where our data is
//            char *pVertexArrayData = pPoseMesh->getVertexArrayPointer();
//            char *pTransformedVertexArrayData = pTransformedMesh->getVertexArrayPointer();
//            
//            // figure out data format / attribute properties
//            renderdevice::VertexFormat const &vertexFormat = pTransformedMesh->getVertexFormat();
//            renderdevice::VertexFormat::eAttributeType type = vertexFormat.getAttributeTypeBySemantic(renderdevice::VertexFormat::kSemanticPosition);
//            size_t positionOffset = vertexFormat.getAttributeByteOffsetBySemantic(renderdevice::VertexFormat::kSemanticPosition);
//            size_t vertexStride = vertexFormat.getVertexStride();
//            size_t numVertices = pTransformedMesh->getNumVertices();
//            math::Matrix4x4f transform = pSceneTransform->getWorldTransform();
//            
//            // Optimization: vectorize
//            // Optimization: jobify
//            
//            // do transform
//            if(type == renderdevice::VertexFormat::kTypeFloat3){
//                for(size_t i = 0; i < numVertices; i++){
//                    math::Vector3f &inPosition = *((math::Vector3f*)&pVertexArrayData[vertexStride * i + positionOffset]);
//                    math::Vector3f &outPosition = *((math::Vector3f*)&pTransformedVertexArrayData[vertexStride * i + positionOffset]);
//                    outPosition = transform * math::Vector4f(inPosition[0], inPosition[1], inPosition[2], 1.0f);
//                }
//            }else{
//                // unsupported vertex position type
//                assert(false);
//            }
//        }
    // }
}

////////////////////////////////////////////////////////////////////////////