//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/graphics/meshtransformupdate.system.h>
#include <nimble/entity/graphics/renderablelist.component.h>
#include <nimble/entity/scene/scenetransform.component.h>
#include <nimble/graphics/irenderable.h>
#include <nimble/graphics/imesh.h>
#include <nimble/graphics/iindexarray.h>
#include <nimble/graphics/ivertexstream.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/entity/component.h>
#include <nimble/entity/entity.h>
#include <nimble/math/matrix.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
MeshTransformUpdateSystem::MeshTransformUpdateSystem(){
}
//! Destructor
MeshTransformUpdateSystem::~MeshTransformUpdateSystem(){
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void MeshTransformUpdateSystem::registerComponents(){
}
//! Unregister the varius components associate with this system
void MeshTransformUpdateSystem::unregisterComponents(){
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was added to an entity
void MeshTransformUpdateSystem::entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    entity::SceneTransformComponent *pSceneTransform = pEntity->getComponent<entity::SceneTransformComponent>();
    entity::RenderableListComponent *pRenderableList = pEntity->findFirstComponentWithType<entity::RenderableListComponent>();
    if(pSceneTransform && pRenderableList){
        core::ScopedLock lock(&m_lock);
        
        // iterate over renderable list to find suitable meshes to process
        for(size_t i = 0; i != pRenderableList->getNumRenderables(); i++){
            graphics::IRenderable *pRenderable = pRenderableList->getRenderable(i);
            
            // add only if this mesh is dynamic
            graphics::IMesh *pMesh = pRenderable->getMesh();
            if(pMesh->getUsage() != graphics::kMeshUsageDynamic){
                return;
            }
            // add only if this mesh has a transform mesh
            graphics::IMesh *pTransformedMesh = pRenderable->getTransformedMesh();
            if(pTransformedMesh == 0){
                return;
            }
            
            // generate our transform info
            transformInfo_t info;
            info.pSceneTransform = pSceneTransform;
            info.pOriginalMesh = pMesh;
            info.pTransformedMesh = pTransformedMesh;
            
            // check that we don't already have a reference
            RenderableToTransformInfoIndex::iterator localIt = m_renderableToTransformInfoIndex.find(pRenderable);
            if(localIt != m_renderableToTransformInfoIndex.end()){
                // update info
                localIt->second = info;
            }else{
                // add info
                m_renderableToTransformInfoIndex.insert(std::make_pair(pRenderable, info));
            }
        }
        
        // register for when renderables are added / removed
        pRenderableList->registerCallbackOnRenderableListDidAddRenderable(entity::RenderableListDidAddRenderableCallback(this, &MeshTransformUpdateSystem::renderableListDidAddRenderable));
        pRenderableList->registerCallbackOnRenderableListWillRemoveRenderable(entity::RenderableListDidAddRenderableCallback(this, &MeshTransformUpdateSystem::renderableListWillRemoveRenderable));
    }
}
//! Called when component was removed from an entity
void MeshTransformUpdateSystem::entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    entity::SceneTransformComponent *pSceneTransform = pEntity->getComponent<entity::SceneTransformComponent>();
    entity::RenderableListComponent *pRenderableList = pEntity->findFirstComponentWithType<entity::RenderableListComponent>();
    if(pSceneTransform && pRenderableList){
        core::ScopedLock lock(&m_lock);
        
        // iterate over renderable list to find suitable meshes to process
        for(size_t i = 0; i != pRenderableList->getNumRenderables(); i++){
            graphics::IRenderable *pRenderable = pRenderableList->getRenderable(i);
            
            // check that we don't already have a reference
            RenderableToTransformInfoIndex::iterator localIt = m_renderableToTransformInfoIndex.find(pRenderable);
            if(localIt == m_renderableToTransformInfoIndex.end()){
                continue;
            }
            
            // remove info
            m_renderableToTransformInfoIndex.erase(localIt);
        }
        
        // register for when renderables are added / removed
        pRenderableList->unregisterCallbackOnRenderableListDidAddRenderable(entity::RenderableListDidAddRenderableCallback(this, &MeshTransformUpdateSystem::renderableListDidAddRenderable));
        pRenderableList->unregisterCallbackOnRenderableListWillRemoveRenderable(entity::RenderableListDidAddRenderableCallback(this, &MeshTransformUpdateSystem::renderableListWillRemoveRenderable));
    }
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void MeshTransformUpdateSystem::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void MeshTransformUpdateSystem::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void MeshTransformUpdateSystem::update(uint64_t frameIndex, double interval){
    core::ScopedLock lock(&m_lock);
    
    // process dynamic meshes
    for(RenderableToTransformInfoIndex::iterator it = m_renderableToTransformInfoIndex.begin(); it != m_renderableToTransformInfoIndex.end(); it++){
        transformInfo_t &transformInfo = it->second;
                
        // retreive our original mesh and the final transformed mesh
        graphics::IMesh *pOriginalMesh = transformInfo.pOriginalMesh;
        graphics::IMesh *pTransformedMesh = transformInfo.pTransformedMesh;
        size_t numVertexStreams = 0;

        // determine our transform
        math::Matrix4x4f transform = transformInfo.pSceneTransform->getWorldTransform();
        
        // find vertex stream with position attribute semantic
        graphics::IVertexStream *pOriginalVertexStream = 0;
        numVertexStreams = pOriginalMesh->getNumVertexStreams();
        for(size_t i = 0; i < numVertexStreams; i++){
            graphics::IVertexStream &vertexStream = pOriginalMesh->getVertexStream(i);
            renderdevice::VertexFormat const &format = vertexStream.getVertexFormat();
            size_t numVertexAttributes = format.getNumAttributes();
            for(size_t j = 0; j < numVertexAttributes; j++){
                if(format.getAttributeSemantic(j) == renderdevice::VertexFormat::kSemanticPosition){
                    pOriginalVertexStream = &vertexStream;
                    break;
                }
            }
        }
        // find vertex stream with position attribute semantic
        graphics::IVertexStream *pTransformedVertexStream = 0;
        numVertexStreams = pTransformedMesh->getNumVertexStreams();
        for(size_t i = 0; i < numVertexStreams; i++){
            graphics::IVertexStream &vertexStream = pTransformedMesh->getVertexStream(i);
            renderdevice::VertexFormat const &format = vertexStream.getVertexFormat();
            size_t numVertexAttributes = format.getNumAttributes();
            for(size_t j = 0; j < numVertexAttributes; j++){
                if(format.getAttributeSemantic(j) == renderdevice::VertexFormat::kSemanticPosition){
                    pTransformedVertexStream = &vertexStream;
                    break;
                }
            }
        }
        // failed to find vertex stream with position attribute semantic
        if(pOriginalVertexStream == 0 || pTransformedVertexStream == 0){
            NIMBLE_LOG_ERROR("graphics", "Failed to update mesh transform - no position attribute present");
            continue;
        }
        // vertex stream mismatch
        if(pOriginalVertexStream->getVertexFormat() != pTransformedVertexStream->getVertexFormat()){
            NIMBLE_LOG_ERROR("graphics", "Failed to update mesh transform - vertex stream formats mismatch");
            continue;
        }
        
        // make sure the transform mesh the the same size as the original mesh
        pTransformedMesh->getIndexArray().resize(pOriginalMesh->getIndexArray().getNumIndices());
        pTransformedVertexStream->resize(pOriginalVertexStream->getNumVertices());
        
        // retreive our vertex data (original and transformed)
        char *pOriginalVertexData = pOriginalVertexStream->getArrayPointer();
        char *pTransformedVertexData = pTransformedVertexStream->getArrayPointer();
        
        // determine vertex format information
        // note: the transformed mesh vertex format is assumed to be the same as our original mesh
        renderdevice::VertexFormat const &vertexFormat = pOriginalVertexStream->getVertexFormat();
        renderdevice::VertexFormat::eAttributeType posType = vertexFormat.getAttributeTypeBySemantic(renderdevice::VertexFormat::kSemanticPosition);
        size_t positionOffset = vertexFormat.getAttributeByteOffsetBySemantic(renderdevice::VertexFormat::kSemanticPosition);
        size_t vertexStride = vertexFormat.getVertexStride();
        size_t numVertices = pOriginalVertexStream->getNumVertices();

        // do transform
        // OPTIMIZE: vectorization, jobification
        if(posType == renderdevice::VertexFormat::kTypeFloat3){
            for(size_t i = 0; i < numVertices; i++){
                math::Vector3f &inPosition = *((math::Vector3f*)&pOriginalVertexData[vertexStride * i + positionOffset]);
                math::Vector3f &outPosition = *((math::Vector3f*)&pTransformedVertexData[vertexStride * i + positionOffset]);
                outPosition = transform * math::Vector4f(inPosition[0], inPosition[1], inPosition[2], 1.0f);
            }
        }else{
            // unsupported vertex position type
            NIMBLE_LOG_ERROR("graphics", "Failed to update mesh transform - unsupported position format detected");
            continue;
        }
    }
}

////////////////////////////////////////////////////////////////////////////

//! Called when renderable was added
void MeshTransformUpdateSystem::renderableListDidAddRenderable(entity::RenderableListComponent *pRenderableList, graphics::IRenderable *pRenderable){
    core::ScopedLock lock(&m_lock);
        
    // retreive our scene transform from our entity
    entity::Entity *pEntity = pRenderableList->getEntity();
    scene::SceneTransform *pSceneTransform = dynamic_cast<scene::SceneTransform*>(pEntity->getComponent<entity::SceneTransformComponent>());
    
    // add only if this mesh is dynamic
    graphics::IMesh *pMesh = pRenderable->getMesh();
    if(pMesh->getUsage() != graphics::kMeshUsageDynamic){
        return;
    }
    // add only if this mesh has a transform mesh
    graphics::IMesh *pTransformedMesh = pRenderable->getTransformedMesh();
    if(pTransformedMesh == 0){
        return;
    }
    
    // generate our transform info
    transformInfo_t info;
    info.pSceneTransform = pSceneTransform;
    info.pOriginalMesh = pMesh;
    info.pTransformedMesh = pTransformedMesh;
    
    // check that we don't already have a reference
    RenderableToTransformInfoIndex::iterator localIt = m_renderableToTransformInfoIndex.find(pRenderable);
    if(localIt != m_renderableToTransformInfoIndex.end()){
        // update info
        localIt->second = info;
    }else{
        // add info
        m_renderableToTransformInfoIndex.insert(std::make_pair(pRenderable, info));
    }
}
//! Called when renderable was removed
void MeshTransformUpdateSystem::renderableListWillRemoveRenderable(entity::RenderableListComponent *pRenderableList, graphics::IRenderable *pRenderable){
    core::ScopedLock lock(&m_lock);
    
    // check that we don't already have a reference
    RenderableToTransformInfoIndex::iterator localIt = m_renderableToTransformInfoIndex.find(pRenderable);
    if(localIt == m_renderableToTransformInfoIndex.end()){
        return;
    }
    
    // remove info
    m_renderableToTransformInfoIndex.erase(localIt);
}

////////////////////////////////////////////////////////////////////////////