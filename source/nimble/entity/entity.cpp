//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/entity.h>
#include <nimble/entity/component.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/core/propertytree.h>
#include <nimble/core/locator.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

// TODO
// + Consider removing ComponentManager from Entity

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! default constructor
Entity::Entity(entity::ComponentManager *pComponentManager, core::PropertyTree &properties, EntityDelegate *pDelegate)
:m_pComponentManager(pComponentManager)
,m_pDelegate(pDelegate){
    // make sure we have a valid component manager
    if(m_pComponentManager == 0){
        m_pComponentManager = core::locator_acquire<entity::ComponentManager>();
    }
    NIMBLE_ASSERT(m_pComponentManager != 0);
    
    // create our components specified on our properties
    core::PropertyTree::PtrList components, list;
    properties.filterChildrenWithName("component", components);
    for(core::PropertyTree::PtrList::iterator it = components.begin(); it != components.end(); it++){
        core::PropertyTree &component = *(*it);
        
        // add our component
        const char *className = "";
        component.filterChildrenWithName("class", list);
        if(list.size() > 0){
            className = list[0]->getValue();
            this->addComponent(m_pComponentManager->createComponent(className, component));
        }
    }
}
//! a destructor
Entity::~Entity(){
    removeAllComponents();
    m_pDelegate = 0;
}

//////////////////////////////////////////////////////////////////////////

//! Sets our delegate
void Entity::setDelegate(EntityDelegate *pDelegate){
    m_pDelegate = pDelegate;
}

//////////////////////////////////////////////////////////////////////////

//! Adds a component
//! \param pComponent the component to add
void Entity::addComponent(entity::Component *pComponent){
    core::check_print(!this->componentExists(pComponent), __LINE__, __FILE__, "[Entity::addComponent] adding a component that already exists");
    
    // add component to our entity
    if(core::Object *pObject = dynamic_cast<core::Object*>(pComponent)){
        core::TypeId typeId = pObject->getTypeMetaData()->getTypeId();
        m_typeToComponentIndex[typeId] = pComponent;
        m_components.push_back(pComponent);
        
        pComponent->setEntity(this);
        pComponent->didAddParentEntity(this);
        
        if(m_pDelegate){
            m_pDelegate->entityDidAddComponent(this, pComponent);
        }
    }else{
        NIMBLE_LOG_WARNING("entity", "Component is not a valid resource");
    }
}
//! Removes a component
//! \param type the component with type to remove
void Entity::removeComponent(core::TypeId type){
    if(entity::Component *pComponent = m_typeToComponentIndex[type]){
        if(m_pDelegate){
            m_pDelegate->entityWillRemoveComponent(this, pComponent);
        }
        
        // remove component from our entity
        m_typeToComponentIndex.erase(m_typeToComponentIndex.find(type));
        m_components.erase(std::find(m_components.begin(), m_components.end(), pComponent));
        
        pComponent->willRemoveParentEntity(this);
        pComponent->setEntity(0);
        
        // remove from component manager
        m_pComponentManager->destroyComponent(pComponent);
    }else{
        NIMBLE_LOG_ERROR("entity", "[Entity::addComponent] Failed removing component that doesn't exist");
    }
}
//! Removes all components from this entity
void Entity::removeAllComponents(){
    TypeToComponentIndex index = m_typeToComponentIndex;
    for(TypeToComponentIndex::iterator it = index.begin(); it != index.end(); it++){
        this->removeComponent(it->first);
    }
    m_typeToComponentIndex.clear();
}
//! Gets a component
//! \param type the component with type to get
entity::Component* Entity::getComponent(core::TypeId typeId){
    TypeToComponentIndex::iterator it = m_typeToComponentIndex.find(typeId);
    if(it != m_typeToComponentIndex.end()){
        return it->second;
    }
    return 0;
}
//! Gets a component
//! \param type the component with type to get
entity::Component const* Entity::getComponent(core::TypeId typeId) const{
    TypeToComponentIndex::const_iterator it = m_typeToComponentIndex.find(typeId);
    if(it != m_typeToComponentIndex.end()){
        return it->second;
    }
    return 0;
}
//! Returns all components
const ComponentList Entity::getComponents() const{
    return m_components;
}
//! Returns true if the component exists
//! \param pComponent the component to check for existence
//! \return true if the component exists
bool Entity::componentExists(entity::Component *pComponent){
    if(core::Object *pObject = dynamic_cast<core::Object*>(pComponent)){
        core::TypeId typeId = pObject->getTypeMetaData()->getTypeId();
        return componentWithTypeExists(typeId);
    }else{
        return false;
    }
}
//! Returns true if the component with type exists
//! \param type the type to check for existence
//! \return true if the component with type exists
bool Entity::componentWithTypeExists(core::TypeId type){
    return (m_typeToComponentIndex.find(type) != m_typeToComponentIndex.end());
}

//////////////////////////////////////////////////////////////////////////