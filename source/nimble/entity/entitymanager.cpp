//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/entitymanager.h>
#include <nimble/entity/componentmanager.h>
#include <nimble/entity/entity.h>
#include <nimble/core/searchpaths.h>
#include <nimble/core/locator.h>
#include <nimble/core/assert.h>
#include <nimble/core/propertytree.h>
#include <nimble/core/filesystem.h>
#include <nimble/resource/resource.h>
#include <nimble/resource/resourcecache.h>
#include <nimble/resource/resourceloader.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Gets our entity manager
entity::EntityManager* EntityManager::get(){
    return core::locator_acquire<entity::EntityManager>();
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
EntityManager::EntityManager(entity::ComponentManager *pComponentManager)
:m_pComponentManager(pComponentManager)
,m_pDelegate(0){
    // make sure we have a valid component manager
    if(!m_pComponentManager){
        m_pComponentManager = entity::ComponentManager::get();
    }
    NIMBLE_ASSERT(m_pComponentManager != 0);
}
//! Destructor
EntityManager::~EntityManager(){
    this->destroyAllEntities();
}

//////////////////////////////////////////////////////////////////////////

//! Updates manager
void EntityManager::update(double interval){
    // TODO -
}

//////////////////////////////////////////////////////////////////////////

//! Sets our delegate
void EntityManager::setDelegate(EntityManagerDelegate *pDelegate){
    m_pDelegate = pDelegate;
}

//////////////////////////////////////////////////////////////////////////

//! Create a entity
//! \return a pointer to the entity
entity::Entity* EntityManager::createEntity(){
    core::PropertyTree properties;
    return this->createEntityWithProperties(properties);
}
//! Create a entity with properties
//! \param type the type to create
//! \param property the property to configure with
entity::Entity* EntityManager::createEntityWithProperties(core::PropertyTree &properties){
    entity::Entity *pEntity = new /*( pool )*/ entity::Entity(m_pComponentManager, properties, this);
    {
        core::ScopedLock lock(&m_lock);
        m_cache.add(pEntity);
        NIMBLE_LOG_INFO("entity", "[Created][id:%d]", pEntity->getResourceId());

        if(m_pDelegate){
            m_pDelegate->entityManagerDidCreateEntity(this, pEntity);
        }
    }
    return pEntity;
}
//! Creates an entity from file
//! \param filename the filename to configure from
//! \return a pointer to the entity
entity::Entity* EntityManager::createEntityFromFile(const char *filepath, core::SearchPaths *pPaths){
    // load our config file
    if(pPaths == 0){
        pPaths = core::locator_acquire<core::SearchPaths>();
    }
    NIMBLE_ASSERT(pPaths != 0);
    core::PropertyTree rootProperties;
    std::string fullPath = pPaths->findFullPath(filepath);
    rootProperties.loadFromXMLFile(fullPath.c_str());
    return this->createEntityWithProperties(*rootProperties["entity"][0]);
}
//! Destroys an entity
//! \param entity the entity to destroy
void EntityManager::destroyEntity(entity::Entity *pEntity){
    if(!pEntity){
        return;
    }
    if(!this->ownsEntity(pEntity)){
        NIMBLE_LOG_ERROR("entity", "Failed to destroy entity - invalid ownership");
        return;
    }
    {
        core::ScopedLock lock(&m_lock);
        NIMBLE_LOG_INFO("entity", "[Destroyed][id:%d]", pEntity->getResourceId());
        
        if(m_pDelegate){
            m_pDelegate->entityManagerWillDestroyEntity(this, pEntity);
        }

        m_cache.remove(pEntity);
        delete pEntity;
    }
}
//! Destroys all entities
void EntityManager::destroyAllEntities(){
    core::ScopedLock lock(&m_lock);
    for(resource::ResourceList::iterator it = m_cache.begin(); it != m_cache.end(); it++){
        this->destroyEntity(dynamic_cast<entity::Entity*>(*it));
    }
    m_cache.clear();
}
//! Fetches an entity by id
//! \param id the unique id to fetch with
//! \return a pointer to the entity
entity::Entity* EntityManager::getEntityByResourceId(resource::ResourceId id){
    core::ScopedLock lock(&m_lock);
    return dynamic_cast<entity::Entity*>(m_cache.find(id));
}
//! Returns true if this manage owns this entity
bool EntityManager::ownsEntity(entity::Entity *pEntity){
    if(pEntity){
        core::ScopedLock lock(&m_lock);
        entity::Entity *pCachedEntity = this->getEntityByResourceId(pEntity->getResourceId());
        return (pCachedEntity == pEntity);
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was added to an entity
void EntityManager::entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent){
    if(m_pDelegate){
        m_pDelegate->entityDidAddComponent(pEntity, pComponent);
    }
}
//! Called when component was removed from an entity
void EntityManager::entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent){
    if(m_pDelegate){
        m_pDelegate->entityWillRemoveComponent(pEntity, pComponent);
    }
}

//////////////////////////////////////////////////////////////////////////