//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/input/touchinput.system.h>
#include <nimble/core/propertytree.h>
#include <nimble/input/messages.h>
#include <nimble/core/locator.h>
#include <nimble/message/messageservice.h>
#include <nimble/resource/resourcemanager.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
TouchInputSystem::TouchInputSystem(){
    m_pTouchDevice = core::locator_acquire<input::TouchDevice>();
}
//! Destructor
TouchInputSystem::~TouchInputSystem(){
    m_pTouchDevice = 0;
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void TouchInputSystem::registerComponents(){
}
//! Unregister the varius components associate with this system
void TouchInputSystem::unregisterComponents(){
}

////////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void TouchInputSystem::initialize(core::PropertyTree &properties){
    m_pTouchDevice->subscribe(this);
}
//! Terminate this system
void TouchInputSystem::terminate(){
}

////////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void TouchInputSystem::update(uint64_t frameIndex, double interval){
}

////////////////////////////////////////////////////////////////////////////

//! inject touch input
void TouchInputSystem::injectInput(nimble::input::TouchDevice::eTouchEvent event, unsigned int x, unsigned int y){
	m_pTouchDevice->injectInput(event, x, y);
}
//! invoked when touch move
void TouchInputSystem::handleTouchMove(const nimble::input::TouchDevice::TouchEvent& event){
    message::MessageService *pMessageService = core::locator_acquire<message::MessageService>();
    nimble::input::OnTouchUpdate msg(event);
    pMessageService->send(&msg);
}
//! invoked when touch move
void TouchInputSystem::handleTouchBegin(const nimble::input::TouchDevice::TouchEvent& event){
    message::MessageService *pMessageService = core::locator_acquire<message::MessageService>();
    nimble::input::OnTouchUpdate msg(event);
    pMessageService->send(&msg);
}
//! invoked when touch move
void TouchInputSystem::handleTouchEnd(const nimble::input::TouchDevice::TouchEvent& event){
    message::MessageService *pMessageService = core::locator_acquire<message::MessageService>();
    nimble::input::OnTouchUpdate msg(event);
    pMessageService->send(&msg);
}

////////////////////////////////////////////////////////////////////////////