//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/action/actionqueue.component.h>
#include <nimble/entity/scene/scenetransform.component.h>
#include <nimble/entity/entity.h>
#include <nimble/action/actions/action.animation.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ActionQueueComponent::ActionQueueComponent(){
}
//! Destructor
ActionQueueComponent::~ActionQueueComponent(){
}

//////////////////////////////////////////////////////////////////////////

//! Enqueue an action
void ActionQueueComponent::enqueue(action::ActionPtr action){
    // setup the action by injecting by type
    if(action::AnimationAction *pAnimationAction = dynamic_cast<action::AnimationAction*>(action.getPointer())){
        // an animation action requires a scene transform
        entity::Entity *pEntity = getEntity();
        scene::SceneTransform *pTransform = dynamic_cast<scene::SceneTransform*>(pEntity->getComponent<entity::SceneTransformComponent>());
        if(pTransform != 0){
            pAnimationAction->setSceneTransform(pTransform);
        }else{
            NIMBLE_LOG_WARNING("action", "Failed to enqueue animation action - entity does not have a scene transform component");
        }
    }
    action::ActionQueue::enqueue(action);
}

//////////////////////////////////////////////////////////////////////////