//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/action/actionqueueupdate.system.h>
#include <nimble/entity/action/actionqueue.component.h>
#include <nimble/core/propertytree.h>
#include <nimble/entity/componentmanager.h>

//////////////////////////////////////////////////////////////////////////

#include <nimble/entity/action/actionqueue.component.h>
#define COMPONENT_TUPLESET \
COMPONENT_TUPLE(entity::ActionQueueComponent,   "ActionQueue")

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
ActionQueueUpdateSystem::ActionQueueUpdateSystem(){
}
//! Destructor
ActionQueueUpdateSystem::~ActionQueueUpdateSystem(){
}

//////////////////////////////////////////////////////////////////////////

//! Register the varius components associate with this system
void ActionQueueUpdateSystem::registerComponents(){
#define COMPONENT_TUPLE(CLASS, NAME) entity::ComponentManager::get()->registerComponent<CLASS>(NAME);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}
//! Unregister the varius components associate with this system
void ActionQueueUpdateSystem::unregisterComponents(){
#define COMPONENT_TUPLE(CLASS, NAME) entity::ComponentManager::get()->unregisterComponent(NAME);
    COMPONENT_TUPLESET
#undef COMPONENT_TUPLE
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was added to an entity
void ActionQueueUpdateSystem::entityDidAddComponent(entity::Entity *pEntity, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(action::ActionQueue *pActionQueue = dynamic_cast<action::ActionQueue*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // components should only be created once
        ActionQueueToActionQueueInfoIndex::iterator it = m_actionQueueToActionQueueInfoIndex.find(pActionQueue);
        NIMBLE_ASSERT(it == m_actionQueueToActionQueueInfoIndex.end());
        
        // generate our info
        actionQueueInfo_t info;
        info.pActionQueue = pActionQueue;
        
        // add to our index
        m_actionQueueToActionQueueInfoIndex.insert(std::make_pair(pActionQueue, info));
    }
}
//! Called when component was removed from an entity
void ActionQueueUpdateSystem::entityWillRemoveComponent(entity::Entity *pEntity, entity::Component *pComponent){
    // we are only interested if the entity has component(s) we are interested in
    if(action::ActionQueue *pActionQueue = dynamic_cast<action::ActionQueue*>(pComponent)){
        core::ScopedLock lock(&m_lock);
        
        // components should only be created once
        ActionQueueToActionQueueInfoIndex::iterator it = m_actionQueueToActionQueueInfoIndex.find(pActionQueue);
        if(it == m_actionQueueToActionQueueInfoIndex.end()){
            NIMBLE_LOG_WARNING("sceneupdatesystem", "Failed to remove action queue component %x from update system - update system does not own the action queue");
            return;
        }
        
        // remove from index
        m_actionQueueToActionQueueInfoIndex.erase(it);
    }
}

//////////////////////////////////////////////////////////////////////////

//! Initialize from a property tree
//! Once this call is completed, the system should be ready to use
void ActionQueueUpdateSystem::initialize(core::PropertyTree &properties){
}
//! Terminate this system
void ActionQueueUpdateSystem::terminate(){
}

//////////////////////////////////////////////////////////////////////////

//! Updates relative to an interval
void ActionQueueUpdateSystem::update(uint64_t frameIndex, double interval){
    core::ScopedLock lock(&m_lock);
    
    // process scenes
    for(ActionQueueToActionQueueInfoIndex::iterator it = m_actionQueueToActionQueueInfoIndex.begin(); it != m_actionQueueToActionQueueInfoIndex.end(); it++){
        actionQueueInfo_t &info = it->second;
        action::ActionQueue *pActionQueue = info.pActionQueue;
        
        if(pActionQueue->getQueueSize()){
            action::ActionPtr action = pActionQueue->getFrontAction();
            
            // initialize if the front action has not
            if(!action->hasStarted()){
                action->start();
            }
            
            // run our action update
            action->update(interval);
            
            // clean up if we are finished
            if(action->isFinished()){
                action->stop();
                pActionQueue->dequeue();
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////