//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/component.h>
#include <nimble/core/thread.pool.h>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Constructor
Component::Component()
:m_pEntity(0){
}
//! Destructor
Component::~Component(){
    NIMBLE_ASSERT(core::getCurrentRunLoop() == core::getMainRunLoop());
}

//////////////////////////////////////////////////////////////////////////

//! Deserialize from a property tree
void Component::deserialize(core::PropertyTree &properties){
}
//! Serailize into a property tree
void Component::serialize(core::PropertyTree &properties){
}

//////////////////////////////////////////////////////////////////////////

//! Called when component was added to an entity
void Component::didAddParentEntity(entity::Entity *pEntity){
    // default does nothing - optional override by concrete class
}
//! Called when component will be removed from entity
void Component::willRemoveParentEntity(entity::Entity *pEntity){
    // default does nothing - optional override by concrete class
}

//////////////////////////////////////////////////////////////////////////

//! returns our owner
entity::Entity* Component::getEntity() const{
    return m_pEntity;
}
//! sets our owner
void Component::setEntity(entity::Entity *pEntity){
    m_pEntity = pEntity;
}

//////////////////////////////////////////////////////////////////////////