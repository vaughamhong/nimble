//
// Copyright (C) 2011 Vaugham Hong (vaughamhong@gmail.com)
//
// This file is subject to the terms and conditions defined in
// file 'license.txt', which is part of this source code package.
//

#include <nimble/entity/componentmanager.h>
#include <nimble/entity/component.h>
#include <nimble/core/propertytree.h>
#include <nimble/core/locator.h>
#include <nimble/core/assert.h>
#include <algorithm>

//////////////////////////////////////////////////////////////////////////

using namespace nimble;
using namespace nimble::entity;

//////////////////////////////////////////////////////////////////////////

//! Gets our component manager
ComponentManager* ComponentManager::get(){
    return core::locator_acquire<entity::ComponentManager>();
}

//////////////////////////////////////////////////////////////////////////

//! Constructor
ComponentManager::ComponentManager()
:m_pDelegate(0){
}
//! Destructor
ComponentManager::~ComponentManager(){
    this->destroyAllComponents();
}

//////////////////////////////////////////////////////////////////////////

//! Updates manager
void ComponentManager::update(double interval){
    // TODO - 
}

//////////////////////////////////////////////////////////////////////////

//! Sets our delegate
void ComponentManager::setDelegate(ComponentManagerDelegate *pDelegate){
    m_pDelegate = pDelegate;
}

//////////////////////////////////////////////////////////////////////////

//! Creates an component from file
//! \param name the name of the component to create
//! \return a pointer to the component
entity::Component* ComponentManager::createComponent(const char *name){
    core::ScopedLock lock(&m_lock);
    core::PropertyTree properties;
    return this->createComponent(name, properties);
}
//! Creates an component from file
//! \param filename the filename to configure from
//! \param properties the properties to create this component with
//! \return a pointer to the component
entity::Component* ComponentManager::createComponent(const char *name, core::PropertyTree &properties){
    core::ScopedLock lock(&m_lock);
    
    // find our builder by component name
    if(entity::Component *pComponent = m_factory.create<entity::Component>(name)){
        // add component to our cache (by resource id) for later retreival
        m_cache.add(pComponent);
        
        // add component to our index (by name) for later retreival
        m_typeNameToComponentsIndex[name].push_back(pComponent);
        NIMBLE_LOG_INFO("component", "[Created][id:%d][name:%s]", pComponent->getResourceId(), name);
        
        // derialize with the provided properties
        pComponent->deserialize(properties);
        
        // tell delegate that component was added
        if(m_pDelegate){
            m_pDelegate->componentManagerDidCreateComponent(this, pComponent);
        }
        
        return pComponent;
    }else{
        NIMBLE_LOG_WARNING("component", "Failed to create component with name \"%s\"", name);
        return 0;
    }
}
//! Destroys an component
//! \param component the component to destroy
void ComponentManager::destroyComponent(entity::Component *pComponent){
    if(!pComponent){
        NIMBLE_LOG_ERROR("entity", "Failed to destroy component - null component");
        return;
    }
    if(!this->ownsComponent(pComponent)){
        NIMBLE_LOG_ERROR("entity", "Failed to destroy component - invalid ownership");
        return;
    }

    {
        core::ScopedLock lock(&m_lock);
        
        // remove from our index
        // note: guarenteed only one pComponent in our index (only createComponent inserts into our index)
        for(TypeNameToComponentsIndex::iterator it = m_typeNameToComponentsIndex.begin(); it != m_typeNameToComponentsIndex.end(); it++){
            ComponentList &componentList = it->second;
            ComponentList::iterator componentIt = std::find(componentList.begin(), componentList.end(), pComponent);
            if(componentIt != componentList.end()){
                componentList.erase(componentIt);
                break;
            }
        }
        
        // remove from our cache
        NIMBLE_LOG_INFO("component", "[Destroyed][id:%d]", pComponent->getResourceId());
        m_cache.remove(pComponent);
        
        // tell delegate that component was removed
        if(m_pDelegate){
            m_pDelegate->componentManagerWillDestroyComponent(this, pComponent);
        }
        
        delete pComponent;
    }
}
//! Destroys all components
void ComponentManager::destroyAllComponents(){
    core::ScopedLock lock(&m_lock);
    for(resource::ResourceList::iterator it = m_cache.begin(); it != m_cache.end(); it++){
        this->destroyComponent(dynamic_cast<entity::Component*>(*it));
    }
    m_typeNameToComponentsIndex.clear();
    m_cache.clear();
}
//! Fetches an component by id
//! \param id the unique id to fetch with
//! \return a pointer to the component
entity::Component* ComponentManager::getComponentByResourceId(resource::ResourceId resourceId){
    core::ScopedLock lock(&m_lock);
    return m_cache.find<entity::Component>(resourceId);
}
//! Returns true if this manage owns this component
bool ComponentManager::ownsComponent(entity::Component *pComponent){
    if(pComponent){
        core::ScopedLock lock(&m_lock);
        entity::Component *pCachedComponent = this->getComponentByResourceId(pComponent->getResourceId());
        return (pCachedComponent == pComponent);
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////