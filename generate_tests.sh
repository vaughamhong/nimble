ROOT=`pwd`
PROJ_DIR=$ROOT/projects/xcode_osx
BUILD_DIR=$ROOT/builds/xcode_osx
LIB_DIR=$ROOT/builds/xcode_osx/libs
BIN_DIR=$ROOT/builds/xcode_osx/bins
rm -rf $PROJ_DIR
mkdir -p $PROJ_DIR
mkdir -p $LIB_DIR
mkdir -p $BIN_DIR
pushd $PROJ_DIR
cmake \
-DCMAKE_BUILD_TYPE=Debug \
-DNIMBLE_GENPROJ_TESTS=ON \
-DNIMBLE_TARGET_OSX=ON \
-DNIMBLE_LIBS_PATH=$LIB_DIR \
-DNIMBLE_BINS_PATH=$BIN_DIR \
-DNIMBLE_BUILD_COVERAGE=OFF \
-DNIMBLE_DISABLE_ASSERTS=ON \
-G "Unix Makefiles" $ROOT
popd