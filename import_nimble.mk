#
# import_vrlib.mk
#
# Common settings used by all VRLib based projects.  Import at the start of any module using VRLib.
#
# Use the LOCAL_EXPORT_ variants for the oculus prebuilt library instead of
# LOCAL_. This works for the following variables:
# 	LOCAL_EXPORT_LDLIBS
# 	LOCAL_EXPORT_CFLAGS
# 	LOCAL_EXPORT_C_INCLUDES
#
# See the NDK documentation for more details.

################################################################################

# save off the local path
LOCAL_PATH_TEMP := $(LOCAL_PATH)
LOCAL_PATH := $(call my-dir)

################################################################################

# nimble
include $(CLEAR_VARS)

LOCAL_ARM_MODE  := arm				# full speed arm instead of thumb
LOCAL_MODULE 	:= nimble

LOCAL_SRC_FILES := projects/android/obj/local/armeabi-v7a/libnimble.a

include $(PREBUILT_STATIC_LIBRARY)

# Restore the local path since we overwrote it when we started
LOCAL_PATH := $(LOCAL_PATH_TEMP)

################################################################################