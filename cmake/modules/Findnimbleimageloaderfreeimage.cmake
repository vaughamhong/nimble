# CMake module for finding NimbleRenderDeviceOpenGL
# XXX_FOUND           		Set to false, or undefined, if we haven't found, or don't want to use XXX.
# XXX_LIBRARY				Name of XXX Library.
# XXX_INCLUDE_DIRS			The final set of include directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_LIBRARIES      		The libraries to link against to use XXX. These should include full paths.  This should not be a cache entry.
# XXX_DEFINITIONS      		Definitions to use when compiling code that uses XXX.
# XXX_LIBRARY_DIRS     		Optionally, the final set of library directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_ROOT_DIR        		Where to find the base directory of XXX.
# XXX_VERSION_YY			Expect Version YY if true. Make sure at most one of these is ever true.
# XXX_RUNTIME_LIBRARY_DIRS 	Optionally, the runtime library search path for use when running an executable linked to shared libraries.
# XXX_VERSION_STRING    	A human-readable string containing the version of the package found, if any.
# XXX_VERSION_MAJOR     	The major version of the package found, if any.
# XXX_VERSION_MINOR      	The minor version of the package found, if any.
# XXX_VERSION_PATCH       	The patch version of the package found, if any.

if(NOT nimblerenderdeviceopengl_FOUND)
	set(nimblerenderdeviceopengl_LIBRARY nimblerenderdeviceopengl)

	if(INCLUDE_NIMBLE_RENDERDEVICE_OPENGL)
		# initialize our external variables
		get_filename_component(thisPath ${CMAKE_CURRENT_LIST_FILE} PATH)
		set(nimblerenderdeviceopengl_INCLUDE_DIRS ${thisPath}/../../dependencies/nimble_renderdevice_opengl/source)
		set(nimblerenderdeviceopengl_LIBS nimblerenderdeviceopengl)

		# add the cmake project to whoever is including this
		add_subdirectory(${thisPath}/../../dependencies/nimble_renderdevice_opengl/ ${CMAKE_CURRENT_BINARY_DIR}/intermediate/nimble_renderdevice_opengl)
		message(STATUS "NimbleRenderDeviceOpenGL module found - generating project")
	else()
		if(NIMBLE_TARGET_IOS)
			set(nimblerenderdeviceopengl_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/nimblerenderdeviceopengl/ios/include)
			set(nimblerenderdeviceopengl_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/nimblerenderdeviceopengl/ios/libs/${CMAKE_BUILD_TYPE})
			message(STATUS "NimbleRenderDeviceOpenGL module found")
		elseif(NIMBLE_TARGET_OSX)
			set(nimblerenderdeviceopengl_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/nimblerenderdeviceopengl/osx/include)
			set(nimblerenderdeviceopengl_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/nimblerenderdeviceopengl/osx/libs/${CMAKE_BUILD_TYPE})
			message(STATUS "NimbleRenderDeviceOpenGL module found")
		elseif(NIMBLE_TARGET_WIN32)
			set(nimblerenderdeviceopengl_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/nimblerenderdeviceopengl/win32/include)
			set(nimblerenderdeviceopengl_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/nimblerenderdeviceopengl/win32/libs/${TARGET_VSS_VERSION}/${CMAKE_BUILD_TYPE})
			message(STATUS "NimbleRenderDeviceOpenGL module found")
		else()
			message(STATUS "NimbleRenderDeviceOpenGL module not found")
		endif()
	endif()
endif()