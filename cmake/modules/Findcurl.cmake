# CMake module for finding Curl
# XXX_FOUND           		Set to false, or undefined, if we haven't found, or don't want to use XXX.
# XXX_LIBRARY				Name of XXX Library.
# XXX_INCLUDE_DIRS			The final set of include directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_LIBRARIES      		The libraries to link against to use XXX. These should include full paths.  This should not be a cache entry.
# XXX_DEFINITIONS      		Definitions to use when compiling code that uses XXX.
# XXX_LIBRARY_DIRS     		Optionally, the final set of library directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_ROOT_DIR        		Where to find the base directory of XXX.
# XXX_VERSION_YY			Expect Version YY if true. Make sure at most one of these is ever true.
# XXX_RUNTIME_LIBRARY_DIRS 	Optionally, the runtime library search path for use when running an executable linked to shared libraries.
# XXX_VERSION_STRING    	A human-readable string containing the version of the package found, if any.
# XXX_VERSION_MAJOR     	The major version of the package found, if any.
# XXX_VERSION_MINOR      	The minor version of the package found, if any.
# XXX_VERSION_PATCH       	The patch version of the package found, if any.

if(NOT curl_FOUND)
	set(curl_LIBRARY curl)
	if(NIMBLE_GENPROJ_CURL)
		# add the cmake project to whoever is including this
		# some key ideas here on how we manage external libraries that do not have a cmake build process
		# + Create a custom command linked to a script that will build / install the library. After this step,
		# we should have libs and an include directory
		# + Create a custom build target. This defines a new target for cmake, which will be able to execute on.
		# cmake custom build targets are ALWAYS DIRTY, which means they run every single time (so placing the 
		# build script for a build target to execute on will run every single time). If our build script takes a long time
		# this will eat up our iteration time.
		# + Link the custom build target to the custom command using the library as a dependency. This means the build command
		# will only run if the library dependency does not exist. This avoids constant rebuilding.
		# + Different platforms will run a different build script (installed into a different directory). LibCurl is 
		# configuration based, which means headers will be different depending on how we configure / build / install LibCurl. By
		# configuring and installing to different build directories, we isolate the different platform builds (libs and includes).
		if(NIMBLE_TARGET_IOS)
			set(curl_INCLUDE_DIRS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/i386/include)
			add_custom_command(
				OUTPUT ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a
	   			COMMAND /bin/sh ./build_curl_ios.sh ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl
	   			WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
	   		add_custom_target(
				curl_build_target
				DEPENDS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a
				WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
				COMMENT "Curl static makefile target")
			add_library(curl STATIC IMPORTED)
			set_property(TARGET curl PROPERTY IMPORTED_LOCATION ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a)
			add_dependencies(curl curl_build_target)
			message(STATUS "Curl module found - generating project")
		elseif(NIMBLE_TARGET_OSX)
			set(curl_INCLUDE_DIRS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/i386/include)
			add_custom_command(
				OUTPUT ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a
	   			COMMAND /bin/sh ./build_curl_osx.sh ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl
	   			WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
	   		add_custom_target(
				curl_build_target
				DEPENDS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a
				WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
				COMMENT "Curl static makefile target")
			add_library(curl STATIC IMPORTED)
			set_property(TARGET curl PROPERTY IMPORTED_LOCATION ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a)
			add_dependencies(curl curl_build_target)
			message(STATUS "Curl module found - generating project")
		elseif(NIMBLE_TARGET_WIN32)
			set(curl_INCLUDE_DIRS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/include)
			add_custom_command(
				OUTPUT ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a
	   			COMMAND /bin/sh ./build_curl_win32.sh ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl
	   			WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
	   		add_custom_target(
				curl_build_target
				DEPENDS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a
				WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
				COMMENT "Curl static makefile target")
			add_library(curl STATIC IMPORTED)
			set_property(TARGET curl PROPERTY IMPORTED_LOCATION ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/libcurl.a)
			add_dependencies(curl curl_build_target)
			message(STATUS "Curl module found - generating project")
		else()
			set(curl_INCLUDE_DIRS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/include)
			add_custom_command(
				OUTPUT ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/lib/libcurl.a
	   			COMMAND ./build_curl.sh ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl
	   			WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
	   		add_custom_target(
				curl_build_target
				DEPENDS ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/lib/libcurl.a
				WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
				COMMENT "Curl static makefile target")
			add_library(curl STATIC IMPORTED)
			set_property(TARGET curl PROPERTY IMPORTED_LOCATION ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/curl/lib/libcurl.a)
			add_dependencies(curl curl_build_target)
			message(STATUS "Curl module found - generating project")
		endif()
	else()
		if(NIMBLE_TARGET_IOS)
			set(curl_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/curl/ios/include)
			set(curl_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/curl/ios/libs/${CMAKE_BUILD_TYPE})
			message(STATUS "Curl prebuilt module found")
		elseif(NIMBLE_TARGET_OSX)
			set(curl_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/curl/osx/include)
			set(curl_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/curl/osx/libs/${CMAKE_BUILD_TYPE})
			message(STATUS "Curl prebuilt module found")
		elseif(NIMBLE_TARGET_WIN32)
			set(curl_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/curl/win32/include)
			set(curl_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/curl/win32/libs/${CMAKE_BUILD_TYPE})
			message(STATUS "Curl prebuilt module found")
		else()
			message(STATUS "Curl module not found")
		endif()
	endif()
endif()
