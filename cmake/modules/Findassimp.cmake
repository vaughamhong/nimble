# CMake module for finding AssImp
# XXX_FOUND           		Set to false, or undefined, if we haven't found, or don't want to use XXX.
# XXX_LIBRARY				Name of XXX Library.
# XXX_INCLUDE_DIRS			The final set of include directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_LIBRARIES      		The libraries to link against to use XXX. These should include full paths.  This should not be a cache entry.
# XXX_DEFINITIONS      		Definitions to use when compiling code that uses XXX.
# XXX_LIBRARY_DIRS     		Optionally, the final set of library directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_ROOT_DIR        		Where to find the base directory of XXX.
# XXX_VERSION_YY			Expect Version YY if true. Make sure at most one of these is ever true.
# XXX_RUNTIME_LIBRARY_DIRS 	Optionally, the runtime library search path for use when running an executable linked to shared libraries.
# XXX_VERSION_STRING    	A human-readable string containing the version of the package found, if any.
# XXX_VERSION_MAJOR     	The major version of the package found, if any.
# XXX_VERSION_MINOR      	The minor version of the package found, if any.
# XXX_VERSION_PATCH       	The patch version of the package found, if any.

if(NOT assimp_FOUND)
	set(assimp_LIBRARY assimp)

	if(NIMBLE_GENPROJ_ASSIMP)
		# AssImp build options
		set(ASSIMP_BUILD_STATIC_LIB "ON")

		# initialize our external variables
		get_filename_component(thisPath ${CMAKE_CURRENT_LIST_FILE} PATH)
		set(assimp_INCLUDE_DIRS ${thisPath}/../../external/assimp/include)
		set(assimp_LIBS assimp)

		# add the cmake project to whoever is including this
		add_subdirectory(${thisPath}/../../external/assimp/ ${CMAKE_CURRENT_BINARY_DIR}/intermediate/assimp)
		message(STATUS "AssImp module found - generating project")
	else()
		if(NIMBLE_TARGET_IOS)
			set(assimp_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/assimp/ios/include)
			set(assimp_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/assimp/ios/libs/${CMAKE_BUILD_TYPE})
			message(STATUS "AssImp prebuilt module found")
		elseif(NIMBLE_TARGET_OSX)
			set(assimp_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/assimp/osx/include)
			set(assimp_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/assimp/osx/libs/${CMAKE_BUILD_TYPE})
			message(STATUS "AssImp prebuilt module found")
		elseif(NIMBLE_TARGET_WIN32)
			set(assimp_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/assimp/win32/include)
			set(assimp_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/assimp/win32/libs/${TARGET_VSS_VERSION}/${CMAKE_BUILD_TYPE})
			message(STATUS "AssImp prebuilt module found")
		else()
			message(STATUS "AssImp module not found")
		endif()
	endif()
endif()