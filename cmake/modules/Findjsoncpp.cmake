# CMake module for finding JsonCpp
# XXX_FOUND           		Set to false, or undefined, if we haven't found, or don't want to use XXX.
# XXX_LIBRARY				Name of XXX Library.
# XXX_INCLUDE_DIRS			The final set of include directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_LIBRARIES      		The libraries to link against to use XXX. These should include full paths.  This should not be a cache entry.
# XXX_DEFINITIONS      		Definitions to use when compiling code that uses XXX.
# XXX_LIBRARY_DIRS     		Optionally, the final set of library directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_ROOT_DIR        		Where to find the base directory of XXX.
# XXX_VERSION_YY			Expect Version YY if true. Make sure at most one of these is ever true.
# XXX_RUNTIME_LIBRARY_DIRS 	Optionally, the runtime library search path for use when running an executable linked to shared libraries.
# XXX_VERSION_STRING    	A human-readable string containing the version of the package found, if any.
# XXX_VERSION_MAJOR     	The major version of the package found, if any.
# XXX_VERSION_MINOR      	The minor version of the package found, if any.
# XXX_VERSION_PATCH       	The patch version of the package found, if any.

if(NOT jsoncpp_FOUND)
	set(jsoncpp_LIBRARY jsoncpp)
	if(NIMBLE_GENPROJ_JSONCPP)
		set(JSONCPP_WITH_TESTS "OFF")
		set(JSONCPP_WITH_POST_BUILD_UNITTEST "OFF")
		set(JSONCPP_WITH_WARNING_AS_ERROR "OFF")
		set(JSONCPP_WITH_PKGCONFIG_SUPPORT "OFF")
		set(JSONCPP_WITH_CMAKE_PACKAGE "OFF")

		# initialize our external variables
		get_filename_component(thisPath ${CMAKE_CURRENT_LIST_FILE} PATH)
		set(jsoncpp_INCLUDE_DIRS ${thisPath}/../../external/jsoncpp)
		set(jsoncpp_LIBS jsoncpp)

		# add the cmake project to whoever is including this
		add_subdirectory(${thisPath}/../../external/jsoncpp ${CMAKE_CURRENT_BINARY_DIR}/intermediate/jsoncpp)
		message(STATUS "JsonCpp module found - generating project")
	else()
		if(NIMBLE_TARGET_IOS)
			set(jsoncpp_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/jsoncpp/ios/include)
			set(jsoncpp_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/jsoncpp/ios/libs)
			message(STATUS "JsonCpp prebuilt module found")
		elseif(NIMBLE_TARGET_OSX)
			set(jsoncpp_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/jsoncpp/osx/include)
			set(jsoncpp_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/jsoncpp/osx/libs)
			message(STATUS "JsonCpp prebuilt module found")
		elseif(NIMBLE_TARGET_WIN32)
			set(jsoncpp_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/jsoncpp/win32/include)
			set(jsoncpp_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/jsoncpp/win32/libs)
			message(STATUS "JsonCpp prebuilt module found")
		else()
			message(STATUS "JsonCpp module not found")
		endif()
	endif()
endif()