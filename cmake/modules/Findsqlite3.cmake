# CMake module for finding SQLite3
# XXX_FOUND           		Set to false, or undefined, if we haven't found, or don't want to use XXX.
# XXX_LIBRARY				Name of XXX Library.
# XXX_INCLUDE_DIRS			The final set of include directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_LIBRARIES      		The libraries to link against to use XXX. These should include full paths.  This should not be a cache entry.
# XXX_DEFINITIONS      		Definitions to use when compiling code that uses XXX.
# XXX_LIBRARY_DIRS     		Optionally, the final set of library directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_ROOT_DIR        		Where to find the base directory of XXX.
# XXX_VERSION_YY			Expect Version YY if true. Make sure at most one of these is ever true.
# XXX_RUNTIME_LIBRARY_DIRS 	Optionally, the runtime library search path for use when running an executable linked to shared libraries.
# XXX_VERSION_STRING    	A human-readable string containing the version of the package found, if any.
# XXX_VERSION_MAJOR     	The major version of the package found, if any.
# XXX_VERSION_MINOR      	The minor version of the package found, if any.
# XXX_VERSION_PATCH       	The patch version of the package found, if any.

if(NOT sqlite3_FOUND)
	set(sqlite3_LIBRARY sqlite3)
	if(NIMBLE_GENPROJ_SQLITE3)
		# initialize our external variables
		get_filename_component(thisPath ${CMAKE_CURRENT_LIST_FILE} PATH)
		set(sqlite3_INCLUDE_DIRS ${thisPath}/../../external/sqlite3)
		set(sqlite3_LIBS sqlite3)

		# add the cmake project to whoever is including this
		file(GLOB SQLite3Src ${thisPath}/../../external/sqlite3/*.c)
		file(GLOB SQLite3Inc ${thisPath}/../../external/sqlite3/*.h)
		add_library(sqlite3 ${SQLite3Src} ${SQLite3Inc})
		message(STATUS "SQlite module found - generating project")
	else()
		if(NIMBLE_TARGET_IOS)
			set(sqlite3_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/sqlite3/ios/include)
			set(sqlite3_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/sqlite3/ios/libs)
			message(STATUS "SQlite prebuilt module found")
		elseif(NIMBLE_TARGET_OSX)
			set(sqlite3_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/sqlite3/osx/include)
			set(sqlite3_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/sqlite3/osx/libs)
			message(STATUS "SQlite prebuilt module found")
		elseif(NIMBLE_TARGET_WIN32)
			set(sqlite3_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/sqlite3/win32/include)
			set(sqlite3_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/sqlite3/win32/libs)
			message(STATUS "SQlite prebuilt module found")
		else()
			message(STATUS "SQlite module not found")
		endif()
	endif()
endif()