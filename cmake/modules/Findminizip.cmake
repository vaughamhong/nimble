# CMake module for finding MiniZip
# XXX_FOUND           		Set to false, or undefined, if we haven't found, or don't want to use XXX.
# XXX_LIBRARY				Name of XXX Library.
# XXX_INCLUDE_DIRS			The final set of include directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_LIBRARIES      		The libraries to link against to use XXX. These should include full paths.  This should not be a cache entry.
# XXX_DEFINITIONS      		Definitions to use when compiling code that uses XXX.
# XXX_LIBRARY_DIRS     		Optionally, the final set of library directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_ROOT_DIR        		Where to find the base directory of XXX.
# XXX_VERSION_YY			Expect Version YY if true. Make sure at most one of these is ever true.
# XXX_RUNTIME_LIBRARY_DIRS 	Optionally, the runtime library search path for use when running an executable linked to shared libraries.
# XXX_VERSION_STRING    	A human-readable string containing the version of the package found, if any.
# XXX_VERSION_MAJOR     	The major version of the package found, if any.
# XXX_VERSION_MINOR      	The minor version of the package found, if any.
# XXX_VERSION_PATCH       	The patch version of the package found, if any.

if(NOT minizip_FOUND)
	set(minizip_LIBRARY minizip)
	if(NIMBLE_GENPROJ_MINIZIP)
		# which compilers to use for C and C++
		SET(CMAKE_C_COMPILER "${HOST}-gcc")
		SET(CMAKE_CXX_COMPILER "${HOST}-g++")

		# initialize our external variables
		get_filename_component(thisPath ${CMAKE_CURRENT_LIST_FILE} PATH)
		set(minizip_INCLUDE_DIRS ${thisPath}/../../external/minizip)
		set(minizip_LIBS minizip)

		# add the cmake project to whoever is including this
		add_subdirectory(${thisPath}/../../external/minizip ${CMAKE_CURRENT_BINARY_DIR}/intermediate/minizip)
		message(STATUS "MiniZip module found - generating project")
	else()
		if(NIMBLE_TARGET_IOS)
			set(minizip_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/minizip/ios/include)
			set(minizip_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/minizip/ios/libs)
			message(STATUS "MiniZip prebuilt module found")
		elseif(NIMBLE_TARGET_OSX)
			set(minizip_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/minizip/osx/include)
			set(minizip_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/minizip/osx/libs)
			message(STATUS "MiniZip prebuilt module found")
		elseif(NIMBLE_TARGET_WIN32)
			set(minizip_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/minizip/win32/include)
			set(minizip_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/minizip/win32/libs)
			message(STATUS "MiniZip prebuilt module found")
		else()
			message(STATUS "MiniZip module not found")
		endif()
	endif()
endif()