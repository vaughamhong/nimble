# CMake module for finding RakNet
# XXX_FOUND           		Set to false, or undefined, if we haven't found, or don't want to use XXX.
# XXX_LIBRARY				Name of XXX Library.
# XXX_INCLUDE_DIRS			The final set of include directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_LIBRARIES      		The libraries to link against to use XXX. These should include full paths.  This should not be a cache entry.
# XXX_DEFINITIONS      		Definitions to use when compiling code that uses XXX.
# XXX_LIBRARY_DIRS     		Optionally, the final set of library directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_ROOT_DIR        		Where to find the base directory of XXX.
# XXX_VERSION_YY			Expect Version YY if true. Make sure at most one of these is ever true.
# XXX_RUNTIME_LIBRARY_DIRS 	Optionally, the runtime library search path for use when running an executable linked to shared libraries.
# XXX_VERSION_STRING    	A human-readable string containing the version of the package found, if any.
# XXX_VERSION_MAJOR     	The major version of the package found, if any.
# XXX_VERSION_MINOR      	The minor version of the package found, if any.
# XXX_VERSION_PATCH       	The patch version of the package found, if any.

if(NOT raknet_FOUND)
	set(raknet_LIBRARY raknet)
	if(NIMBLE_GENPROJ_RAKNET)
		# initialize our external variables
		get_filename_component(thisPath ${CMAKE_CURRENT_LIST_FILE} PATH)
		set(raknet_INCLUDE_DIRS ${thisPath}/../../external/raknet)
		set(raknet_LIBS raknet)

		# add the cmake project to whoever is including this
		add_subdirectory(${thisPath}/../../external/raknet ${CMAKE_CURRENT_BINARY_DIR}/intermediate/raknet)
		message(STATUS "RakNet module found - generating project")
	else()
		if(NIMBLE_TARGET_IOS)
			set(raknet_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/raknet/ios/include)
			set(raknet_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/raknet/ios/libs)
			message(STATUS "RakNet prebuilt module found")
		elseif(NIMBLE_TARGET_OSX)
			set(raknet_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/raknet/osx/include)
			set(raknet_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/raknet/osx/libs)
			message(STATUS "RakNet prebuilt module found")
		elseif(NIMBLE_TARGET_WIN32)
			set(raknet_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/raknet/win32/include)
			set(raknet_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/raknet/win32/libs)
			message(STATUS "RakNet prebuilt module found")
		else()
			message(STATUS "RakNet module not found")
		endif()
	endif()
endif()