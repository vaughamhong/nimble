# CMake module for finding Bullet
# XXX_FOUND           		Set to false, or undefined, if we haven't found, or don't want to use XXX.
# XXX_LIBRARY				Name of XXX Library.
# XXX_INCLUDE_DIRS			The final set of include directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_LIBRARIES      		The libraries to link against to use XXX. These should include full paths.  This should not be a cache entry.
# XXX_DEFINITIONS      		Definitions to use when compiling code that uses XXX.
# XXX_LIBRARY_DIRS     		Optionally, the final set of library directories listed in one variable for use by client code.  This should not be a cache entry.
# XXX_ROOT_DIR        		Where to find the base directory of XXX.
# XXX_VERSION_YY			Expect Version YY if true. Make sure at most one of these is ever true.
# XXX_RUNTIME_LIBRARY_DIRS 	Optionally, the runtime library search path for use when running an executable linked to shared libraries.
# XXX_VERSION_STRING    	A human-readable string containing the version of the package found, if any.
# XXX_VERSION_MAJOR     	The major version of the package found, if any.
# XXX_VERSION_MINOR      	The minor version of the package found, if any.
# XXX_VERSION_PATCH       	The patch version of the package found, if any.

if(NOT bullet_FOUND)
	set(bullet_LIBRARY bullet)
	if(NIMBLE_GENPROJ_BULLET)
		# Bullet build options
		set(BUILD_MULTITHREADING "OFF")
		set(BUILD_DEMOS "OFF")
		set(BUILD_CPU_DEMOS "OFF")
		set(BUILD_MINICL_OPENCL_DEMOS "OFF")
		set(BUILD_EXTRAS "OFF")
		set(USE_GLUT "OFF")

		# initialize our external variables
		get_filename_component(thisPath ${CMAKE_CURRENT_LIST_FILE} PATH)
		set(bullet_INCLUDE_DIRS ${thisPath}/../../external/bullet/src)

		# add the cmake project to whoever is including this
		add_subdirectory(${thisPath}/../../external/bullet ${CMAKE_CURRENT_BINARY_DIR}/intermediate/bullet)
		message(STATUS "Bullet module found - generating project")
	else()
		get_filename_component(thisPath ${CMAKE_CURRENT_LIST_FILE} PATH)
		if(NIMBLE_TARGET_IOS)
			set(bullet_INCLUDE_DIRS ${thisPath}/../../external/bullet/src)
			set(bullet_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/bullet/ios/libs)
			message(STATUS "Bullet prebuilt module found")
		elseif(NIMBLE_TARGET_OSX)
			set(bullet_INCLUDE_DIRS ${thisPath}/../../external/bullet/src)
			set(bullet_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/bullet/osx/libs)
			message(STATUS "Bullet prebuilt module found")
		elseif(NIMBLE_TARGET_WIN32)
			set(bullet_INCLUDE_DIRS ${thisPath}/../../external/bullet/src)
			set(bullet_LIBRARY_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/prebuilt/bullet/win32/libs/${TARGET_VSS_VERSION})
			message(STATUS "Bullet prebuilt module found")
		else()
			message(STATUS "Bullet module not found")
		endif()
	endif()
endif()