ROOT=`pwd`
PROJ_DIR=$ROOT/generated/release
BUILD_DIR=$ROOT/builds/release
LIB_DIR=$ROOT/builds/release/libs
BIN_DIR=$ROOT/builds/release/bins
mkdir -p $PROJ_DIR
mkdir -p $LIB_DIR
mkdir -p $BIN_DIR
pushd $PROJ_DIR
cmake \
-DCMAKE_ARCHIVE_OUTPUT_DIRECTORY=$LIB_DIR \
-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=$LIB_DIR \
-DCMAKE_RUNTIME_OUTPUT_DIRECTORY=$BIN_DIR \
-DCMAKE_BUILD_TYPE=Release \
-DNIMBLE_GENPROJ_TESTS=ON \
-G "Unix Makefiles" $ROOT
popd